# !/bin/bash
# Necessary to execute with ./

: '
  First script to launch the experiments.
  It will download all the datasets (Foursquare, tripbuilder and sematic trails)
  and process the data:
    -Select the cities used in the experiments
    -Apply data preprocessing
    -Build sessions

  It will also download MyMedialite library and compile Fossil library
'


# Global variables for jvm
JAR=target/SeReRSys-0.0.1-SNAPSHOT.jar
jvmMemory=-Xmx12G

# directories that will be used to store the data
destinationParsedCities=ParsedCities
destinationSessionCities=SessionCitiesTrainFiles
OriginalCitiesFoursquare=OriginalCitiesFoursquare
OriginalCitiesTripbuilder=OriginalCitiesTripbuilder
OriginalSemanticTrails=OriginalSemanticTrails

javaCommand=java

extensionMap=_Mapping
extensionCoords=_Coords

mkdir -p SessionCitiesTrainFiles


FoursqrCheckingsOriginal=$OriginalCitiesFoursquare/dataset_TIST2015_Checkins.txt
FoursqrCitiesOriginal=$OriginalCitiesFoursquare/dataset_TIST2015_Cities.txt
FoursqrPOISOriginal=$OriginalCitiesFoursquare/dataset_TIST2015_POIs.txt

# Creation of the JAR file ()
mvn install

# Download the full dataset from Foursquare and move the original files
wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=0BwrgZ-IdrTotZ0U0ZER2ejI3VVk' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=0BwrgZ-IdrTotZ0U0ZER2ejI3VVk" -O dataset_TIST2015.zip && rm -rf /tmp/cookies.txt
unzip dataset_TIST2015.zip


mv dataset_TIST2015_Checkins.txt $OriginalCitiesFoursquare/
mv dataset_TIST2015_Cities.txt $OriginalCitiesFoursquare/
mv dataset_TIST2015_POIs.txt $OriginalCitiesFoursquare/


mkdir -p $OriginalCitiesFoursquare
mkdir -p $OriginalCitiesTripbuilder
mkdir -p $OriginalSemanticTrails

rm dataset_TIST2015.zip


# Split full dataset between cities
if [ ! "$(ls -A $OriginalCitiesFoursquare/Cities)" ]; then
  mkdir -p $OriginalCitiesFoursquare/Cities
  $javaCommand $jvmMemory -jar $JAR -o FoursqrCheckinsPerCity -trf $FoursqrCheckingsOriginal $FoursqrPOISOriginal $FoursqrCitiesOriginal $OriginalCitiesFoursquare/Cities/ $OriginalCitiesFoursquare/ItemCountryCityJoin.txt

  mv $OriginalCitiesFoursquare/Cities/JP_Tokyo.txt $OriginalCitiesFoursquare/JP_Tokyo.txt
  mv $OriginalCitiesFoursquare/Cities/US_NewYork.txt $OriginalCitiesFoursquare/US_NewYork.txt

fi
echo "Created cities folder"

# Create new map for users and items (map between old ids and new ids in order to reduce the space used by the datasets)
if [ ! -f $OriginalCitiesFoursquare/Users"$extensionMap"Foursquare.txt ]; then
  $javaCommand $jvmMemory -jar $JAR -o FoursqrObtainMapUsersItems -trf $FoursqrCheckingsOriginal -UMapping $OriginalCitiesFoursquare/Users"$extensionMap"Foursquare.txt -IMapping $OriginalCitiesFoursquare/POIS"$extensionMap"Foursquare.txt
fi
echo "Files of users and items mapping computation completed"

$javaCommand $jvmMemory -jar $JAR -o FoursqrGenerateCategoryLevel -trf $FoursqrPOISOriginal $OriginalCitiesFoursquare/category_tree.csv "2,1" "OriginalCitiesFoursquare/dataset_TIST2015_POIs__level2.txt,OriginalCitiesFoursquare/dataset_TIST2015_POIs__level1.txt"
cp $FoursqrPOISOriginal $OriginalCitiesFoursquare/dataset_TIST2015_POIs__level3.txt


# Mapping categories
: '
  Classic Foursquare section
'
for level in 1 2 3
do
  $javaCommand $jvmMemory -jar $JAR -o FoursqrObtainMapCategories -trf $OriginalCitiesFoursquare/dataset_TIST2015_POIs__level$level.txt -CMapping $OriginalCitiesFoursquare/MapCategories_level$level".txt"

  $javaCommand $jvmMemory -jar $JAR -o FoursqrObtainPoisFeatures -trf $OriginalCitiesFoursquare/dataset_TIST2015_POIs__level$level.txt -CMapping $OriginalCitiesFoursquare/MapCategories_level$level".txt" -IMapping $OriginalCitiesFoursquare/POIS"$extensionMap"Foursquare.txt -sfs $OriginalCitiesFoursquare/POIS_Categories_level$level".txt"
done # End categories
wait

# Create coordinates files of the POIs
if [ ! -f $OriginalCitiesFoursquare/POIS"$extensionCoords"_Foursquare.txt ]; then
  $javaCommand $jvmMemory -jar $JAR -o FoursqrObtainPoisCoords -trf $FoursqrPOISOriginal -IMapping $OriginalCitiesFoursquare/POIS"$extensionMap"Foursquare.txt -coordFile $OriginalCitiesFoursquare/POIS"$extensionCoords"_Foursquare.txt
fi
echo "File of coordinates computation completed"


# For Foursquare, two cities. New York and Tokyo
cities="JP_Tokyo.txt US_NewYork.txt"

for cityFile in $cities
do
  city=$(basename "$cityFile" .txt) #extension removed

  resultFileParsed=$destinationParsedCities/$city"Parsed"
  mkdir -p $resultFileParsed
  #if [ ! -f $resultFileParsed".txt" ]; then
    $javaCommand $jvmMemory -jar $JAR -o generateNewCheckingFileWithTimeStamps -trf $OriginalCitiesFoursquare/$cityFile -IMapping $OriginalCitiesFoursquare/POIS"$extensionMap"Foursquare.txt -UMapping $OriginalCitiesFoursquare/Users"$extensionMap"Foursquare.txt -newDataset $resultFileParsed".txt"
   #fi

  echo "Generated  $destinationParsedCities/$city"Parsed.txt" New Ids for users and Items"

  # Apply 2 Kcore
  minRatingsUserItems=2
  resultFileKCore=$resultFileParsed"KCore"$minRatingsUserItems
  if [ ! -f $resultFileKCore".txt" ]; then
    $javaCommand $jvmMemory -jar $JAR -o DatasetReduction -trf $resultFileParsed".txt" -mru $minRatingsUserItems -mri $minRatingsUserItems -orf $resultFileKCore".txt"
  fi


  # Delete all bots
  # minDiffTime is the difference between the timestamps to be considered as bot. 60 seconds
  # minClosePrefBot is the number of time in which we find preferences whose time difference is lower or equal minDiffTime in order to be considered as bot. 3 times
  minDiffTime=60
  minClosePrefBot=3
  resultFileNoBots=$resultFileKCore"DelBots""MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot
  if [ ! -f $resultFileNoBots".txt" ]; then
    $javaCommand $jvmMemory -jar $JAR -o filterBotsFromPreferences -trf $resultFileKCore".txt" -minDiffTime $minDiffTime -minClosePrefBot $minClosePrefBot -orf $resultFileNoBots".txt"
  fi


  # 8 hours between POI and POI. 8 * 3600 = 28800
  # 16 hours between first and last POI. 16 * 3600 = 57600
  diffBfItems=28800
  #maxDiffTime=57600
  resultFileSessions=$resultFileNoBots"SessDifBItems"$diffBfItems
  if [ ! -f $resultFileSessions".txt" ]; then
    $javaCommand $jvmMemory -jar $JAR -o generateUserSessions -trf $resultFileNoBots".txt" -sessionType TIME -limitBetweenItems $diffBfItems -coordFile $OriginalCitiesFoursquare/POIS"$extensionCoords"_Foursquare.txt -orf $resultFileSessions".txt"
  fi

  # Sessions created. Filtering out the sessions
  # 5 hours minimum per session. 5 * 3600 = 18000
  # Minimum length per session. 5
  minTimeSession=0
  minLengthSession=0
  resultFileFilterSessions=$resultFileSessions"FilterSess""MinTimeSess"$minTimeSession"minLengthSess"$minLengthSession
  if [ ! -f $resultFileFilterSessions".txt" ]; then
    $javaCommand $jvmMemory -jar $JAR -o filterSessions -trf $resultFileSessions".txt" -minDiffTime $minTimeSession -mri $minLengthSession -orf $resultFileFilterSessions".txt"
  fi



  # Compute the file of toursits, locals and bots
  # Max diff time between two timestamps fot Tourist vs Local is 21 days => 21 * 24 * 3600 = 1814400
  # minDiffTime is the difference between the timestamps to be considered as bot. 60 seconds
  # minClosePrefBot is the number of time in which we find preferences whose time difference is lower or equal minDiffTime in order to be considered as bot. 3 times

  if [ ! -f $resultUserLocalTouristBotSessions".txt" ]; then
    maxDiffTime=1814400
    minDiffTime=60
    minClosePrefBot=3
    resultUserLocalTouristBotSessions=$resultFileFilterSessions"MaxDifTourist"$maxDiffTime"MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot"LocTouBot"
    $javaCommand $jvmMemory -jar $JAR -o generateTouristLocalBotFile -trf $resultFileFilterSessions".txt" -maxDiffTime $maxDiffTime -minDiffTime $minDiffTime -minClosePrefBot $minClosePrefBot -orf $resultUserLocalTouristBotSessions".txt"
  fi



  #Now, prepare train, test set


  fileName=$(basename $resultFileFilterSessions)
  trainFile=$destinationSessionCities/$fileName
  testFile=$destinationSessionCities/$fileName
  # minimum number of sessions for the user to go to the test set, 3 (the last one will go to test, the other to train)
  # minimum number of items of that the last session -> 2 POIs
  minSessions=3
  mri=4
  if [ ! -f $trainFile".train" ]; then
    $javaCommand $jvmMemory -jar $JAR -o fixSessionSplit -trf $resultFileFilterSessions".txt" -orf $trainFile".train" -orf2 $testFile".test" -minSessions $minSessions -mri $mri
  fi

  # For the train file, we will also aggregate the results
  prefAggregateStrat=SUM # Sum the preferences
  timeAggregateTime=FIRST # Selecting the first timestamp
  newDataset=$trainFile"PrefAggre"$prefAggregateStrat"TimeAggr"$timeAggregateTime
  cut -f1,2,3,4 $trainFile".train" > $trainFile".trainAUX"
  $javaCommand $jvmMemory -jar $JAR -o AggregateWithWrapperTimeStamps -trf $trainFile".trainAUX" -wStrat $prefAggregateStrat -wStratTime $timeAggregateTime -newDataset $newDataset".train"
  rm $trainFile".trainAUX"

  mv $trainFile".train"  $destinationSessionCities/$city"TempTrain".txt
  mv $testFile".test" $destinationSessionCities/$city"Test".txt


  mv $newDataset".train" $destinationSessionCities/$city"AggrTrain".txt




done # End city
wait

#End Foursquare

: '
  TripbuilderSection (download and process dataset)
'

wget "https://raw.githubusercontent.com/igobrilhante/TripBuilder/master/tripbuilder-dataset-dist.zip"
unzip -o tripbuilder-dataset-dist.zip
cp -r tripbuilder-dataset-dist $OriginalCitiesTripbuilder/
rm tripbuilder-dataset-dist.zip
rm -rf tripbuilder-dataset-dist



cities="rome"

for cityFile in $cities
do

  mv $OriginalCitiesTripbuilder/tripbuilder-dataset-dist/$cityFile/$cityFile"-pois-clusters".txt $OriginalCitiesTripbuilder/
  mv $OriginalCitiesTripbuilder/tripbuilder-dataset-dist/$cityFile/$cityFile"-trajectories".txt $OriginalCitiesTripbuilder/

  # First, parse the dataset obtaning coords
  resultFileCoords=$OriginalCitiesTripbuilder/$cityFile"_Clusters_Coords_Tripbuilder".txt
  $javaCommand $jvmMemory -jar $JAR -o generateClustersCoordsTripbuilder -trf $OriginalCitiesTripbuilder/$cityFile"-pois-clusters".txt -orf $resultFileCoords

  # Now, parse the dataset obtaining the features. It is necessary to include the file of topic category mapping
  resultFileFeature=$OriginalCitiesTripbuilder/$cityFile"_Features_Tripbuilder".txt
  $javaCommand $jvmMemory -jar $JAR -o generatePOIGrupedCategoryTripBuilderByMax -trf $OriginalCitiesTripbuilder/$cityFile"-pois-clusters".txt -ff $OriginalCitiesTripbuilder/tripbuilder-topic_category_mapping.txt -orf $resultFileFeature

  #Now, obtain our session file
  resultFileParsed=$destinationParsedCities/$cityFile"_Parsed_Tripbuilder"
  $javaCommand $jvmMemory -jar $JAR -o processCityTrajectoriesTripBuilder -trf $OriginalCitiesTripbuilder/$cityFile"-trajectories".txt -orf $resultFileParsed".txt"

  # As the sessions are built, now we filter them ()
  minTimeSession=0
  minLengthSession=0
  minPrefs=1
  minDiffTime2=60
  minClosePrefBot=3
  resultFileFilterSessions=$resultFileParsed"DelBots""MinDifBot"$minDiffTime2"MinNumPrefBot"$minClosePrefBot"FilterSess""MinTimeSess"$minTimeSession"minLengthSess"$minLengthSession"MinPrefsUser"$minPrefs
  if [ ! -f $resultFileFilterSessions".txt" ]; then
    $javaCommand $jvmMemory -jar $JAR -o filterSessions -trf $resultFileParsed".txt" -minDiffTime $minTimeSession -mri $minLengthSession -mru $minPrefs -minDiffTime2 $minDiffTime2 -minClosePrefBot $minClosePrefBot -orf $resultFileFilterSessions".txt"
  fi

  if [ ! -f $resultUserLocalTouristBotSessions".txt" ]; then
    maxDiffTime=1814400
    minDiffTime=60
    minClosePrefBot=3
    resultUserLocalTouristBotSessions=$resultFileFilterSessions"MaxDifTourist"$maxDiffTime"MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot"LocTouBot"
    $javaCommand $jvmMemory -jar $JAR -o generateTouristLocalBotFile -trf $resultFileFilterSessions".txt" -maxDiffTime $maxDiffTime -minDiffTime $minDiffTime -minClosePrefBot $minClosePrefBot -orf $resultUserLocalTouristBotSessions".txt"
  fi



  fileName=$(basename $resultFileFilterSessions)
  trainFile=$destinationSessionCities/$fileName
  testFile=$destinationSessionCities/$fileName
  minSessions=3
  mri=4
  if [ ! -f $trainFile".train" ]; then
    $javaCommand $jvmMemory -jar $JAR -o fixSessionSplit -trf $resultFileFilterSessions".txt" -orf $trainFile".train" -orf2 $testFile".test" -minSessions $minSessions -mri $mri
  fi

  # For the train file, we will also aggregate the results
  prefAggregateStrat=SUM # Sum the preferences
  timeAggregateTime=FIRST # Selecting the first timestamp
  newDataset=$trainFile"PrefAggre"$prefAggregateStrat"TimeAggr"$timeAggregateTime

  cut -f1,2,3,4 $trainFile".train" > $trainFile".trainAUX"
  $javaCommand $jvmMemory -jar $JAR -o AggregateWithWrapperTimeStamps -trf $trainFile".trainAUX" -wStrat $prefAggregateStrat -wStratTime $timeAggregateTime -newDataset $newDataset".train"
  rm $trainFile".trainAUX"


  mv $trainFile".train" $destinationSessionCities/$cityFile"TempTrain".txt
  mv $testFile".test" $destinationSessionCities/$cityFile"Test".txt

  mv $newDataset".train" $destinationSessionCities/$cityFile"AggrTrain".txt



done # End cityFile
wait



: '
  Semantic Trails section (down and process dataset)
'

wget "https://s3-eu-west-1.amazonaws.com/pfigshare-u-files/14209544/std_2018.csv.gz"
gunzip std_2018.csv.gz

mv std_2018.csv $OriginalSemanticTrails

cities="Q864965"

# Create User Item mapping
$javaCommand $jvmMemory -jar $JAR -o mappingUserItemsSemanticTrails -trf $OriginalSemanticTrails/std_2018.csv -UMapping $OriginalSemanticTrails/ST_2018Users.txt -IMapping $OriginalSemanticTrails/ST_2018Items.txt

# Different cities semantic trails
$javaCommand $jvmMemory -jar $JAR -o obtainCheckingsDifferentCitiesSemanticTrails -trf $OriginalSemanticTrails/std_2018.csv -UMapping $OriginalSemanticTrails/ST_2018Users.txt -IMapping $OriginalSemanticTrails/ST_2018Items.txt -sf Q864965 -orf $OriginalSemanticTrails/

mv $OriginalSemanticTrails/Q864965.txt $destinationParsedCities/ST_PetalingJaya.txt

cities="ST_PetalingJaya"
for cityFile in $cities
do

  # As the sessions are built, now we filter them ()
  minTimeSession=0
  minLengthSession=0
  minPrefs=1
  minDiffTime2=60
  minClosePrefBot=3
  resultFileFilterSessions=$destinationParsedCities/$cityFile"DelBots""MinDifBot"$minDiffTime2"MinNumPrefBot"$minClosePrefBot"FilterSess""MinTimeSess"$minTimeSession"minLengthSess"$minLengthSession"MinPrefsUser"$minPrefs
  if [ ! -f $resultFileFilterSessions".txt" ]; then
    $javaCommand $jvmMemory -jar $JAR -o filterSessions -trf $destinationParsedCities/$cityFile".txt" -minDiffTime $minTimeSession -mri $minLengthSession -mru $minPrefs -minDiffTime2 $minDiffTime2 -minClosePrefBot $minClosePrefBot -orf $resultFileFilterSessions".txt"
  fi

  if [ ! -f $resultUserLocalTouristBotSessions".txt" ]; then
    maxDiffTime=1814400
    minDiffTime=60
    minClosePrefBot=3
    resultUserLocalTouristBotSessions=$resultFileFilterSessions"MaxDifTourist"$maxDiffTime"MinDifBot"$minDiffTime"MinNumPrefBot"$minClosePrefBot"LocTouBot"
    $javaCommand $jvmMemory -jar $JAR -o generateTouristLocalBotFile -trf $resultFileFilterSessions".txt" -maxDiffTime $maxDiffTime -minDiffTime $minDiffTime -minClosePrefBot $minClosePrefBot -orf $resultUserLocalTouristBotSessions".txt"
  fi

  fileName=$(basename $resultFileFilterSessions)
  trainFile=$destinationSessionCities/$fileName
  testFile=$destinationSessionCities/$fileName
  minSessions=3
  mri=4
  if [ ! -f $trainFile".train" ]; then
    $javaCommand $jvmMemory -jar $JAR -o fixSessionSplit -trf $resultFileFilterSessions".txt" -orf $trainFile".train" -orf2 $testFile".test" -minSessions $minSessions -mri $mri
  fi

  # For the train file, we will also aggregate the results
  prefAggregateStrat=SUM # Sum the preferences
  timeAggregateTime=FIRST # Selecting the first timestamp
  newDataset=$trainFile"PrefAggre"$prefAggregateStrat"TimeAggr"$timeAggregateTime
  cut -f1,2,3,4 $trainFile".train" > $trainFile".trainAUX"
  $javaCommand $jvmMemory -jar $JAR -o AggregateWithWrapperTimeStamps -trf $trainFile".trainAUX" -wStrat $prefAggregateStrat -wStratTime $timeAggregateTime -newDataset $newDataset".train"
  rm $trainFile".trainAUX"

  mv $trainFile".train"  $destinationSessionCities/$cityFile"TempTrain".txt
  mv $testFile".test" $destinationSessionCities/$cityFile"Test".txt


  mv $newDataset".train" $destinationSessionCities/$cityFile"AggrTrain".txt

done # End cityFile
wait


# Download Mymedialite

if [ ! -d "MyMediaLite-3.11" ]; then
  wget "http://mymedialite.net/download/MyMediaLite-3.11.tar.gz"
  tar zxvf MyMediaLite-3.11.tar.gz
  rm MyMediaLite-3.11.tar.gz
fi

mkdir -p Fossil_RankingModification/obj
mkdir -p Fossil_RankingModification/obj/models
make -C Fossil_RankingModification/
