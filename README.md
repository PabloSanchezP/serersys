# SeReRSys

SeReRSys: **Se**quences and **ReR**anking for **R**ecommender **Sys**tems



## Getting Started


### Prerequisites

Java 8

MATLAB (executed in Matlab 2015a)

Maven

python3 (anaconda, for Caser recommender)

Mono (for Mymedialite)


## Dependencies
[RankSys 0.4.3](https://github.com/RankSys/RankSys/releases/tag/0.4.3)

Rank fusion library, provided as an independent JAR.

[TempCDSeqEval](https://bitbucket.org/PabloSanchezP/tempcdseqeval/)

## Instructions

* git clone https://bitbucket.org/PabloSanchezP/serersys
* cd serersys

* Steps:
  * step1_ProcessDataGenerateSessions.sh
  * step2_GenerateRecommenders_Complete_NewYork.sh
  * step2_GenerateRecommenders_Complete_Tokyo.sh
  * step2_GenerateRecommenders_Complete_rome.sh
  * step2_GenerateRecommenders_Complete_SemanticTrails.sh


## Additional algorithms/frameworks used in the experiments
  * [MyMedialite](http://www.mymedialite.net/) (for BPR)
  * We have adapted the following approaches for ranking evaluation:
    * [Fusing Similarity Models with Markov Chains for Sparse Sequential Recommendation](https://cseweb.ucsd.edu/~jmcauley/pdfs/icdm16a.pdf) (for Fossil, MC and FPMC) [Original source code](https://sites.google.com/view/ruining-he/)
    * [Exploiting geographical neighborhood characteristics for location recommendation](https://ink.library.smu.edu.sg/cgi/viewcontent.cgi?referer=https://www.google.com/&httpsredir=1&article=4772&context=sis_research) (for IRenMF) [Original source code](http://spatialkeyword.sce.ntu.edu.sg/eval-vldb17/)
    * [Personalized top-n sequential recommendation via convolutional sequence embedding](http://www.sfu.ca/~jiaxit/resources/wsdm18caser.pdf) (for Caser) [Original Source code](https://github.com/graytowne/caser_pytorch)

## Authors

* **Pablo Sánchez** - [Universidad Autónoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)
* **Alejandro Bellogín** - [Universidad Autónoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)

## Contact

* **Pablo Sánchez** - <pablo.sanchezp@uam.es>

## License

This project is licensed under [the GNU GPLv3 License](LICENSE.txt)

## Acknowledgments

* This work has been funded by the Ministerio de Ciencia, Innovación y Universidades within the call for predoctoral contracts co-funded by the European Social Fund (ESF) and the project TIN2016-80630-P.
