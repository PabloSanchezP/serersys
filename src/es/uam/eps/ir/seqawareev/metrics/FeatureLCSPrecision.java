/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.metrics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel.UserRelevanceModel;
import es.uam.eps.ir.seqawareev.sim.LongestCommonSubsequence;

/****
 * Metric that computes the Longest Common Subsequence between the features of
 * the test set and the features of the recommended items for the users. It
 * works as a normal IR ranking metric, based on Precision
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 */
public class FeatureLCSPrecision<U, I, F> extends AbstractRecommendationMetric<U, I> {

	private final int cutoff;
	private final FeatureData<I, F, ?> featureData;
	private final FastTemporalPreferenceDataIF<U, I> testData;
	private final RelevanceModel<U, I> relModel;
	private final Map<F, Integer> internalMapping;

	public FeatureLCSPrecision(int cutoff, FeatureData<I, F, ?> featureData,
			FastTemporalPreferenceDataIF<U, I> testData, RelevanceModel<U, I> relModel) {
		this.cutoff = cutoff;
		this.featureData = featureData;
		this.testData = testData;
		this.relModel = relModel;
		this.internalMapping = new HashMap<>();
		buildInternalMapping();
	}

	private void buildInternalMapping() {
		AtomicInteger counter = new AtomicInteger(1);
		featureData.getAllFeatures().forEach(f -> {
			this.internalMapping.put(f, counter.get());
			counter.incrementAndGet();
		});
	}

	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		U user = recommendation.getUser();
		UserRelevanceModel<U, I> userRelModel = relModel.getModel(user);

		// First, retrieve the test list and the recommended list of features IN ORDER
		List<F> featuresTestUser = testData.getUidxTimePreferences(testData.user2uidx(user))
				.sorted(PreferenceComparators.timeComparatorIdxTimePref)
				.filter(pref -> userRelModel.isRelevant(testData.iidx2item(pref.v1)))
				.map(pref -> testData.iidx2item(pref.v1))
				.filter(i -> this.featureData.getItemFeatures(i).findFirst().isPresent())
				.map(i -> this.featureData.getItemFeatures(i).findFirst().get().v1).collect(Collectors.toList());

		List<Integer> featuresTestUserInteger = featuresTestUser.stream().map(f -> this.internalMapping.get(f))
				.collect(Collectors.toList());

		List<F> featuresRatedCutOff = recommendation.getItems().stream().limit(cutoff).map(Tuple2od::v1)
				.filter(i -> this.featureData.getItemFeatures(i).findFirst().isPresent())
				.map(i -> this.featureData.getItemFeatures(i).findFirst().get().v1).collect(Collectors.toList());

		List<Integer> featuresRatedCutOffInteger = featuresRatedCutOff.stream().map(f -> this.internalMapping.get(f))
				.collect(Collectors.toList());

		Integer[] featureTestUserArray = featuresTestUserInteger.toArray(new Integer[featuresTestUserInteger.size()]);
		Integer[] featuresRatedCutOffIntegerArray = featuresRatedCutOffInteger
				.toArray(new Integer[featuresRatedCutOffInteger.size()]);
		double lcs = LongestCommonSubsequence.longestCommonSubsequence(featureTestUserArray,
				featuresRatedCutOffIntegerArray);

		return lcs / cutoff;
	}

}
