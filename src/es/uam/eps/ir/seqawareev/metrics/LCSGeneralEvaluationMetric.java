/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.metrics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel.UserRelevanceModel;
import es.uam.eps.ir.seqawareev.sim.LongestCommonSubsequence;
import es.uam.eps.ir.seqawareev.sim.LongestCommonSubsequence.LCSOperationMatching;

/**
 * Metric based on the longest common subsequence algorithm. We will order the
 * test set by timestamp and see the order of our recommendations. The better
 * the subsequence matching the better
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class LCSGeneralEvaluationMetric<U, I> extends AbstractRecommendationMetric<U, I> {

	public enum Metric {
		PRECISION, RECALL, NDCG_LOG, MRR, ARHR
	};

	private final RelevanceModel<U, I> relModel;
	private final int cutOff;
	private final FastTemporalPreferenceDataIF<U, I> testData;
	private final Metric metricType;
	private Map<U, Integer[]> sequenceOfItemsAt;
	private Map<U, Double[]> sequenceOfRatingsAt;

	public LCSGeneralEvaluationMetric(int cutOff, RelevanceModel<U, I> relModel,
			FastTemporalPreferenceDataIF<U, I> testData, Metric metricType) {
		this.cutOff = cutOff;
		this.relModel = relModel;
		this.testData = testData;
		this.metricType = metricType;
		obtainMapTestUsers();
	}

	private void obtainMapTestUsers() {
		sequenceOfItemsAt = new HashMap<>();
		sequenceOfRatingsAt = new HashMap<>();

		testData.getUidxWithPreferences().forEach(uidx -> {
			UserRelevanceModel<U, I> userRelModel = relModel.getModel(testData.uidx2user(uidx));

			List<Integer> lstIndexUser = testData.getUidxTimePreferences(uidx)
					.sorted(PreferenceComparators.timeComparatorIdxTimePref)
					.filter(pref -> userRelModel.isRelevant(testData.iidx2item(pref.v1))).mapToInt(pref -> pref.v1)
					.boxed().collect(Collectors.toList());
			List<Double> lstRatingUser = testData.getUidxTimePreferences(uidx)
					.sorted(PreferenceComparators.timeComparatorIdxTimePref)
					.filter(pref -> userRelModel.isRelevant(testData.iidx2item(pref.v1))).mapToDouble(pref -> pref.v2)
					.boxed().collect(Collectors.toList());

			sequenceOfItemsAt.put(testData.uidx2user(uidx), lstIndexUser.toArray(new Integer[lstIndexUser.size()]));
			sequenceOfRatingsAt.put(testData.uidx2user(uidx), lstRatingUser.toArray(new Double[lstRatingUser.size()]));

		});
	}

	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		List<Integer> lstRec = recommendation.getItems().stream().limit(cutOff).map(Tuple2od::v1)
				.mapToInt(v1 -> testData.item2iidx(v1)).boxed().collect(Collectors.toList());

		LCSOperationMatching lcsOp = LCSOperationMatching.INCREMENT1;
		switch (metricType) {
		case PRECISION:
			lcsOp = LCSOperationMatching.METRIC_PRECISION;
			break;
		case RECALL:
			lcsOp = LCSOperationMatching.METRIC_RECALL;
			break;
		case NDCG_LOG:
			lcsOp = LCSOperationMatching.METRIC_NDCG_LOG;
			break;
		case MRR:
			lcsOp = LCSOperationMatching.METRIC_MRR;
			break;
		case ARHR:
			lcsOp = LCSOperationMatching.METRIC_ARHR;
			break;
		}

		Integer[] test = sequenceOfItemsAt.get(recommendation.getUser());
		Integer[] rec = lstRec.toArray(new Integer[lstRec.size()]);

		Double[] testR = sequenceOfRatingsAt.get(recommendation.getUser());
		Tuple3<Double, Integer, Integer> lcs = LongestCommonSubsequence.longestCommonSubsequenceMixed(test, rec, testR,
				lcsOp, cutOff, true);

		double eval = lcs.v1;

		switch (metricType) {
		case NDCG_LOG: {
			Integer[] test2 = new Integer[Math.min(cutOff, test.length)];
			IntStream.range(0, test2.length).forEach(i -> test2[i] = test[i]);
			Tuple3<Double, Integer, Integer> lcsIdeal = LongestCommonSubsequence.longestCommonSubsequenceMixed(test,
					test2, testR, lcsOp, cutOff, true);
			double ideal = lcsIdeal.v1;
			eval /= ideal;
		}
			break;
		case ARHR: {
			double ideal = IntStream.range(0, cutOff).mapToDouble(i -> 1.0 / (i + 1)).sum();
			eval /= ideal;
		}
			break;
		default:
			break;
		}

		return eval;
	}

	@Override
	public String toString() {
		return "LCSGeneral" + metricType + "@" + cutOff;
	}

}
