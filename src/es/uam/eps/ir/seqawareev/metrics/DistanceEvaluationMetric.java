/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.metrics;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.jooq.lambda.tuple.Tuple2;
import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.utils.FoursqrProcessData;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;

/***
 * Evaluation metric that will compute the distance of the route recommended
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class DistanceEvaluationMetric<U, I> extends AbstractRecommendationMetric<U, I> {

	private final int cutoff;
	private final Map<I, Tuple2<Double, Double>> coords;

	public DistanceEvaluationMetric(int cutoff, Map<I, Tuple2<Double, Double>> itemCoordinates) {
		this.cutoff = cutoff;
		this.coords = itemCoordinates;
	}

	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		if (recommendation.getItems().size() == 0 || recommendation.getItems().size() == 1) {
			return 0;
		}

		List<I> lstRec = recommendation.getItems().stream().limit(cutoff).map(Tuple2od::v1)
				.collect(Collectors.toList());

		Tuple2<Double, Double> startingCoordinates = coords.get(lstRec.get(0));
		double result = 0;
		for (int i = 1; i < lstRec.size(); i++) {
			Tuple2<Double, Double> coordiantesNext = coords.get(lstRec.get(i));
			result += FoursqrProcessData.haversine(startingCoordinates.v1, startingCoordinates.v2, coordiantesNext.v1,
					coordiantesNext.v2);
			startingCoordinates = coordiantesNext;
		}
		return result;
	}

}
