/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.metrics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.jooq.lambda.tuple.Tuple2;
import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.metrics.AbstractRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel.UserRelevanceModel;

/***
 * Metric for implementing the Test Feature Precision It works as the Precision
 * metric but it works with feature, not specific POIS
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 */
public class TestFeaturePrecision<U, I, F> extends AbstractRecommendationMetric<U, I> {

	private final int cutoff;
	private final FeatureData<I, F, ?> featureData;
	private final PreferenceData<U, I> testData;
	private final RelevanceModel<U, I> relModel;

	public TestFeaturePrecision(int cutoff, FeatureData<I, F, ?> featureData, PreferenceData<U, I> testData,
			RelevanceModel<U, I> relModel) {
		this.cutoff = cutoff;
		this.featureData = featureData;
		this.testData = testData;
		this.relModel = relModel;
	}

	@Override
	public double evaluate(Recommendation<U, I> recommendation) {
		Map<F, Integer> featuresCounter = getFeaturesTest(recommendation.getUser());
		List<I> itemsRatedCutOff = recommendation.getItems().stream().limit(cutoff).map(Tuple2od::v1)
				.collect(Collectors.toList());

		double acc = 0;
		for (I item : itemsRatedCutOff) {
			Tuple2<F, ?> featureItemOp = this.featureData.getItemFeatures(item).findFirst().orElse(null);
			if (featureItemOp != null) {
				F featureItem = featureItemOp.v1;
				if (featuresCounter.get(featureItem) != null && featuresCounter.get(featureItem) != 0) {
					featuresCounter.put(featureItem, featuresCounter.get(featureItem) - 1);
					acc += 1;
				}
			}
		}

		return acc / cutoff;
	}

	private Map<F, Integer> getFeaturesTest(U user) {
		Map<F, Integer> counter = new HashMap<>();
		UserRelevanceModel<U, I> userRelModel = relModel.getModel(user);
		this.testData.getUserPreferences(user).filter(pref -> userRelModel.isRelevant(pref.v1)).forEach(pref -> {
			Tuple2<F, ?> featureItemOp = this.featureData.getItemFeatures(pref.v1).findFirst().orElse(null);
			if (featureItemOp != null) {
				F featureItem = featureItemOp.v1;
				if (counter.get(featureItem) == null) {
					counter.put(featureItem, 0);
				}
				counter.put(featureItem, counter.get(featureItem) + 1);
			}
		});

		return counter;
	}

}
