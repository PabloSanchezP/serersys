/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.wrappers;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple3;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.fast.preference.FastPointWisePreferenceData;

import es.uam.eps.ir.crossdomainPOI.datamodel.SimpleFastTemporalFeaturePreferenceData;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.core.preference.IdPref;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.IdxPref;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import it.unimi.dsi.fastutil.doubles.DoubleIterator;
import it.unimi.dsi.fastutil.ints.IntIterator;

/**
 * * Wrapper that will transform a datamodel admitting repetitions and
 * timestamps to a normal ranksys datamodel with NO repetitions and NO
 * timestamps.
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 * @param <V>
 */
public class SimpleFastTemporalFeaturePreferenceDataWrapper<U, I>
		implements FastPreferenceData<U, I>, FastPointWisePreferenceData<U, I> {

	public enum RepetitionsStrategyPreference {
		FIRST, LAST, AVERAGE, SUM, NONE
	};

	public enum RepetitionsStrategyTime {
		FIRST, LAST, AVERAGE
	}

	SimpleFastPreferenceData<U, I> data;
	FastTemporalPreferenceDataIF<U, I> dataTimeStamps;
	RepetitionsStrategyPreference stratPreferenceScores;
	RepetitionsStrategyTime stratPreferenceTimes;

	public SimpleFastTemporalFeaturePreferenceDataWrapper(FastTemporalPreferenceDataIF<U, I> dataToRead,
			RepetitionsStrategyPreference strat) {
		this.stratPreferenceScores = strat;

		Stream<Tuple3<U, I, Double>> tuples = null;
		if (!strat.equals(RepetitionsStrategyPreference.NONE)) {
			tuples = obtainTuplesWithNoRepetitionsNoTimestamps(dataToRead, strat);
		} else {
			tuples = obtainTuplesWithNoTimestamps(dataToRead);
		}

		data = SimpleFastPreferenceData.load(tuples, dataToRead, dataToRead);
	}

	public SimpleFastTemporalFeaturePreferenceDataWrapper(FastTemporalPreferenceDataIF<U, I> dataToRead,
			RepetitionsStrategyPreference stratPreference, RepetitionsStrategyTime stratTime) {
		this.stratPreferenceScores = stratPreference;
		this.stratPreferenceTimes = stratTime;
		Stream<Tuple4<U, I, Double, Long>> tuples = obtainTuplesWithNoRepetitionsAndWithTimestamps(dataToRead);
		dataTimeStamps = SimpleFastTemporalFeaturePreferenceData.loadTemporalFeature(tuples, dataToRead, dataToRead);
	}

	private static <U, I, K, V> Stream<Tuple3<U, I, Double>> obtainTuplesWithNoTimestamps(
			FastTemporalPreferenceDataIF<U, I> data) {
		List<Tuple3<U, I, Double>> lstResult = new ArrayList<>();
		AtomicLong totalUsersAggregated = new AtomicLong(0L);
		data.getUidxWithPreferences().sorted().forEach(uidx -> {
			U user = data.uidx2user(uidx);
			Map<I, List<IdxTimePref>> map = getUserRepeated(data, uidx);
			for (I item : map.keySet()) {
				List<IdxTimePref> associated = map.get(item);
				for (IdxTimePref pref : associated) {
					lstResult.add(new Tuple3<>(user, item, pref.v2));
				}
			}
			totalUsersAggregated.incrementAndGet();
			if (totalUsersAggregated.get() % 1000 == 0) {
				System.out.println(totalUsersAggregated + " users out of " + data.numUsersWithPreferences());
			}
		});

		return lstResult.stream();
	}

	private static <U, I, K, V> Stream<Tuple3<U, I, Double>> obtainTuplesWithNoRepetitionsNoTimestamps(
			FastTemporalPreferenceDataIF<U, I> data, RepetitionsStrategyPreference strat) {
		List<Tuple3<U, I, Double>> lstResult = new ArrayList<>();
		AtomicLong totalUsersAggregated = new AtomicLong(0L);
		data.getUidxWithPreferences().sorted().forEach(uidx -> {
			U user = data.uidx2user(uidx);
			Map<I, List<IdxTimePref>> map = getUserRepeated(data, uidx);
			for (I item : map.keySet()) {
				List<IdxTimePref> associated = map.get(item);
				if (associated.size() == 1) {
					lstResult.add(new Tuple3<>(user, item, associated.get(0).v2));
				} else {
					lstResult.add(unifyPreferencesScore(user, item, associated, strat));
				}
			}
			totalUsersAggregated.incrementAndGet();
			if (totalUsersAggregated.get() % 1000 == 0) {
				System.out.println(totalUsersAggregated + " users out of " + data.numUsersWithPreferences());
			}
		});

		return lstResult.stream();
	}

	private Stream<Tuple4<U, I, Double, Long>> obtainTuplesWithNoRepetitionsAndWithTimestamps(
			FastTemporalPreferenceDataIF<U, I> data) {
		List<Tuple4<U, I, Double, Long>> lstResult = new ArrayList<>();
		AtomicLong totalUsersAggregated = new AtomicLong(0L);
		data.getUidxWithPreferences().forEach(uidx -> {
			U user = data.uidx2user(uidx);
			Map<I, List<IdxTimePref>> map = getUserRepeated(data, uidx);
			for (I item : map.keySet()) {
				List<IdxTimePref> associated = map.get(item);
				if (associated.size() == 1) {
					lstResult.add(new Tuple4<>(user, item, associated.get(0).v2, associated.get(0).v3));
				} else {
					lstResult.add(unifyPreferencesTime(user, item, associated));
				}
			}
			totalUsersAggregated.incrementAndGet();
			if (totalUsersAggregated.get() % 1000 == 0) {
				System.out.println(totalUsersAggregated + " users out of " + data.numUsersWithPreferences());
			}
		});

		return lstResult.stream();
	}

	private static <U, I> Map<I, List<IdxTimePref>> getUserRepeated(FastTemporalPreferenceDataIF<U, I> data, int uidx) {
		Map<I, List<IdxTimePref>> result = new HashMap<>();
		data.getUidxTimePreferences(uidx).forEach(pref -> {
			I item = data.iidx2item(pref.v1);
			if (result.get(item) == null) {
				result.put(item, new ArrayList<>());
			}
			result.get(item).add(pref);
		});
		return result;
	}

	private static <U, I> Tuple3<U, I, Double> unifyPreferencesScore(U user, I item, List<IdxTimePref> lstTimePref,
			RepetitionsStrategyPreference strat) {
		lstTimePref.sort(PreferenceComparators.timeComparatorIdxTimePref);
		switch (strat) {
		case FIRST:
			return new Tuple3<>(user, item, lstTimePref.get(0).v2);
		case LAST:
			return new Tuple3<>(user, item, lstTimePref.get(lstTimePref.size() - 1).v2);
		case AVERAGE:
			Double average = lstTimePref.stream().mapToDouble(tp -> tp.v2).average().getAsDouble();
			return new Tuple3<>(user, item, average);
		case SUM:
		default:
			Double sum = lstTimePref.stream().mapToDouble(tp -> tp.v2).sum();
			return new Tuple3<>(user, item, sum);
		}

	}

	private Tuple4<U, I, Double, Long> unifyPreferencesTime(U user, I item, List<IdxTimePref> lstTimePref) {
		lstTimePref.sort(PreferenceComparators.timeComparatorIdxTimePref);
		Double score = null;
		switch (stratPreferenceScores) {
		case FIRST:
			score = lstTimePref.get(0).v2;
			break;
		case LAST:
			score = lstTimePref.get(lstTimePref.size() - 1).v2;
			break;
		case AVERAGE:
			score = lstTimePref.stream().mapToDouble(tp -> tp.v2).average().getAsDouble();
			break;
		case SUM:
			score = lstTimePref.stream().mapToDouble(tp -> tp.v2).sum();
			break;
		default:
			break;

		}

		switch (stratPreferenceTimes) {
		case FIRST:
			return new Tuple4<>(user, item, score, lstTimePref.get(0).v3);
		case LAST:
			return new Tuple4<>(user, item, score, lstTimePref.get(lstTimePref.size() - 1).v3);
		case AVERAGE:
			Long averageTimeStamp = (long) lstTimePref.stream().mapToLong(tp -> tp.v3).average().getAsDouble();
			return new Tuple4<>(user, item, score, averageTimeStamp);
		default:
			// Default is first
			return new Tuple4<>(user, item, lstTimePref.get(0).v2, lstTimePref.get(0).v3);
		}

	}

	@Override
	public int numUsersWithPreferences() {
		return data.numUsersWithPreferences();
	}

	@Override
	public int numItemsWithPreferences() {
		return data.numItemsWithPreferences();
	}

	@Override
	public int numUsers(I i) {
		return data.numUsers(i);
	}

	@Override
	public int numItems(U u) {
		return data.numItems(u);
	}

	@Override
	public int numPreferences() {
		return data.numPreferences();
	}

	@Override
	public Stream<U> getUsersWithPreferences() {
		return data.getUsersWithPreferences();
	}

	@Override
	public Stream<I> getItemsWithPreferences() {
		return data.getItemsWithPreferences();
	}

	@Override
	public Stream<? extends IdPref<I>> getUserPreferences(U u) {
		return data.getUserPreferences(u);
	}

	@Override
	public Stream<? extends IdPref<U>> getItemPreferences(I i) {
		return data.getItemPreferences(i);
	}

	@Override
	public boolean containsUser(U u) {
		return data.containsUser(u);
	}

	@Override
	public int numUsers() {
		return data.numUsers();
	}

	@Override
	public Stream<U> getAllUsers() {
		return data.getAllUsers();
	}

	@Override
	public boolean containsItem(I i) {
		return data.containsItem(i);
	}

	@Override
	public int numItems() {
		return data.numItems();
	}

	@Override
	public Stream<I> getAllItems() {
		return data.getAllItems();
	}

	@Override
	public int user2uidx(U u) {
		return data.user2uidx(u);
	}

	@Override
	public U uidx2user(int uidx) {
		return data.uidx2user(uidx);
	}

	@Override
	public int item2iidx(I i) {
		return data.item2iidx(i);
	}

	@Override
	public I iidx2item(int iidx) {
		return data.iidx2item(iidx);
	}

	@Override
	public int numUsers(int iidx) {
		return data.numUsers(iidx);
	}

	@Override
	public int numItems(int uidx) {
		return data.numItems(uidx);
	}

	@Override
	public IntStream getUidxWithPreferences() {
		return data.getUidxWithPreferences();
	}

	@Override
	public IntStream getIidxWithPreferences() {
		return data.getIidxWithPreferences();
	}

	@Override
	public Stream<? extends IdxPref> getUidxPreferences(int uidx) {
		return data.getUidxPreferences(uidx);
	}

	@Override
	public Stream<? extends IdxPref> getIidxPreferences(int iidx) {
		return data.getIidxPreferences(iidx);
	}

	@Override
	public IntIterator getUidxIidxs(int uidx) {
		return data.getUidxIidxs(uidx);
	}

	@Override
	public DoubleIterator getUidxVs(int uidx) {
		return data.getUidxVs(uidx);
	}

	@Override
	public IntIterator getIidxUidxs(int iidx) {
		return data.getIidxUidxs(iidx);
	}

	@Override
	public DoubleIterator getIidxVs(int iidx) {
		return data.getIidxVs(iidx);
	}

	@Override
	public boolean useIteratorsPreferentially() {
		return data.useIteratorsPreferentially();
	}

	@Override
	public Optional<? extends IdPref<I>> getPreference(U u, I i) {
		return data.getPreference(u, i);
	}

	@Override
	public Optional<? extends IdxPref> getPreference(int uidx, int iidx) {
		return data.getPreference(uidx, iidx);
	}

	/**
	 * Method to write all the preferences of the wrapper to a file
	 *
	 * @param pathResFile
	 */
	public void writePreferences(String pathResFile) {
		try {
			PrintStream resultFile = new PrintStream(pathResFile);
			data.getAllUsers().forEach(u -> {
				data.getUserPreferences(u).forEach(p -> {
					resultFile.println(u + "\t" + p.v1 + "\t" + p.v2);
				});
			});
			resultFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static <U, I> FastPreferenceData<U, I> createFastPreferenceData(
			FastTemporalPreferenceDataIF<U, I> dataToRead, RepetitionsStrategyPreference strat) {
		Stream<Tuple3<U, I, Double>> tuples = obtainTuplesWithNoRepetitionsNoTimestamps(dataToRead, strat);
		return SimpleFastPreferenceData.load(tuples, dataToRead, dataToRead);
	}

	public void writePreferencesTimeStamps(String pathResFile) {
		try {
			PrintStream resultFile = new PrintStream(pathResFile);
			dataTimeStamps.getAllUsers().forEach(u -> {
				dataTimeStamps.getUserPreferences(u).forEach(p -> {
					resultFile.println(u + "\t" + p.v1 + "\t" + p.v2 + "\t" + p.v3);
				});
			});
			resultFile.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
