/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.tour.recommenders;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2id;
import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.fast.FastRecommendation;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.seqawareev.sim.AbstractTourCachedItemSimilarity;
import es.uam.eps.ir.seqawareev.tour.abstracts.AbstractFastTourRecommender;
import es.uam.eps.ir.seqawareev.utils.ReRankingUtils;

/***
 * Method to generate a tour recommendation using a cached similarity between
 * items
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class GenericItemSimilarityTourRecommender<U, I> extends AbstractFastTourRecommender<U, I> {
	private final AbstractTourCachedItemSimilarity<I> iSim;

	public GenericItemSimilarityTourRecommender(FastTemporalPreferenceDataIF<U, I> temporalPreferenceData,
			AbstractTourCachedItemSimilarity<I> isim) {
		super(temporalPreferenceData, temporalPreferenceData);
		this.iSim = isim;

	}

	public GenericItemSimilarityTourRecommender(FastPreferenceData<U, I> preferenceData,
			AbstractTourCachedItemSimilarity<I> isim) {
		super(preferenceData, preferenceData);
		this.iSim = isim;

	}

	@Override
	public FastRecommendation getRecommendation(int uidx, int maxLength, IntPredicate filter, Long timestamp,
			int iidx0) {
		List<Tuple2id> lstId = new ArrayList<>();
		Set<I> candidates = this.iIndex.getAllIidx().filter(filter).mapToObj(iidx -> this.iIndex.iidx2item(iidx))
				.collect(Collectors.toSet());
		int realMaxLength = Math.min(candidates.size(), maxLength);

		List<Tuple2od<I>> finalRank = ReRankingUtils.getSequenceItemOrderSimByPopularity(this.iIndex.iidx2item(iidx0),
				iSim, candidates, realMaxLength);

		finalRank = finalRank.subList(0, (maxLength > finalRank.size()) ? finalRank.size() : maxLength);
		for (Tuple2od<I> tod : finalRank) {
			lstId.add(new Tuple2id(this.iIndex.item2iidx(tod.v1), tod.v2));
		}

		return new FastRecommendation(uidx, lstId);
	}

}
