/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.tour.recommenders;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2id;
import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.fast.FastRecommendation;
import es.uam.eps.ir.seqawareev.structures.FeatureItemTransitionMatrix;
import es.uam.eps.ir.seqawareev.tour.abstracts.AbstractFastTourRecommender;
import es.uam.eps.ir.seqawareev.utils.ReRankingUtils;

/***
 * Recommender that uses a Suffix Tree for recommendations
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 */
public class FeatureSuffixTreeTourRecommender<U, I, F> extends AbstractFastTourRecommender<U, I> {
	private final float[][] distanceMatrix;
	private final FeatureData<I, F, Double> featureData;
	private final int patternKOrder;

	private final FastTemporalPreferenceDataIF<U, I> temporalPreferenceData;

	public FeatureSuffixTreeTourRecommender(FastTemporalPreferenceDataIF<U, I> temporalPreferenceData,
			FeatureData<I, F, Double> featureData, float[][] distanceMatrix, int patternKOrder) {
		super(temporalPreferenceData, temporalPreferenceData);
		this.temporalPreferenceData = temporalPreferenceData;
		this.distanceMatrix = distanceMatrix;
		this.featureData = featureData;
		this.patternKOrder = patternKOrder;
	}

	@Override
	public FastRecommendation getRecommendation(int uidx, int maxLength, IntPredicate filter, Long timestamp,
			int iidx0) {
		List<Tuple2id> lstId = new ArrayList<>();

		FeatureItemTransitionMatrix<U, I, F> iT = new FeatureItemTransitionMatrix<>(temporalPreferenceData,
				this.featureData);
		iT.initializeFor1User(uidx);
		U user = this.temporalPreferenceData.uidx2user(uidx);
		I item0 = this.temporalPreferenceData.iidx2item(iidx0);
		Set<I> candidates = this.iIndex.getAllIidx().filter(filter).mapToObj(iidx -> this.iIndex.iidx2item(iidx))
				.collect(Collectors.toSet());

		List<Tuple2od<I>> finalRank = ReRankingUtils.getSequenceSuffixTreeItemOrderByPopularity(item0, user, candidates,
				this.temporalPreferenceData, this.featureData, iT, distanceMatrix, this.patternKOrder, maxLength);

		finalRank = finalRank.subList(0, (maxLength > finalRank.size()) ? finalRank.size() : maxLength);
		for (Tuple2od<I> tod : finalRank) {
			lstId.add(new Tuple2id(this.iIndex.item2iidx(tod.v1), tod.v2));
		}

		return new FastRecommendation(uidx, lstId);
	}

}
