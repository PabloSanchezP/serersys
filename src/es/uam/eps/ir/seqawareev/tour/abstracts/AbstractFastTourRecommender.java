/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.tour.abstracts;

import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.fast.FastRecommendation;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.seqawareev.tour.interfaces.FastTourRecommender;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;

import static java.util.stream.Collectors.toList;

/***
 * Fast tour Recommender class (similar to the FastRecommender but working with an item0 to start the sequence)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public abstract class AbstractFastTourRecommender<U, I> extends AbstractTourRecommender<U, I>
		implements FastTourRecommender<U, I> {

	/**
	 * Fast user index.
	 */
	protected final FastUserIndex<U> uIndex;

	/**
	 * Fast item index.
	 */
	protected final FastItemIndex<I> iIndex;

	/**
	 * Constructor.
	 *
	 * @param uIndex
	 *            user index
	 * @param iIndex
	 *            item index
	 */
	public AbstractFastTourRecommender(FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
		super();

		this.uIndex = uIndex;
		this.iIndex = iIndex;
	}

	@Override
	public int numUsers() {
		return uIndex.numUsers();
	}

	@Override
	public int user2uidx(U u) {
		return uIndex.user2uidx(u);
	}

	@Override
	public U uidx2user(int uidx) {
		return uIndex.uidx2user(uidx);
	}

	@Override
	public int numItems() {
		return iIndex.numItems();
	}

	@Override
	public int item2iidx(I i) {
		return iIndex.item2iidx(i);
	}

	@Override
	public I iidx2item(int iidx) {
		return iIndex.iidx2item(iidx);
	}

	@Override
	public Recommendation<U, I> getRecommendation(U u, int maxLength, Long timestamp, I item0) {
		FastRecommendation rec = getRecommendation(user2uidx(u), maxLength, timestamp, this.item2iidx(item0));

		return new Recommendation<>(uidx2user(rec.getUidx()),
				rec.getIidxs().stream().map(this::iidx2item).collect(toList()));
	}

	@Override
	public FastRecommendation getRecommendation(int uidx, Long timestamp, int iidx0) {
		return getRecommendation(uidx, Integer.MAX_VALUE, timestamp, iidx0);
	}

	@Override
	public FastRecommendation getRecommendation(int uidx, int maxLength, Long timestamp, int iidx0) {
		return getRecommendation(uidx, maxLength, iidx -> true, timestamp, iidx0);
	}

	@Override
	public Recommendation<U, I> getRecommendation(U u, int maxLength, Predicate<I> filter, Long timestamp, I item0) {
		FastRecommendation rec = getRecommendation(user2uidx(u), maxLength, iidx -> filter.test(iidx2item(iidx)),
				timestamp, this.item2iidx(item0));

		return new Recommendation<>(uidx2user(rec.getUidx()),
				rec.getIidxs().stream().map(this::iidx2item).collect(toList()));
	}

	@Override
	public FastRecommendation getRecommendation(int uidx, IntPredicate filter, Long timestamp, int iidx0) {
		return getRecommendation(uidx, Integer.MAX_VALUE, filter, timestamp, iidx0);
	}

	@Override
	public abstract FastRecommendation getRecommendation(int uidx, int maxLength, IntPredicate filter, Long timestamp,
			int iidx0);

	@Override
	public Recommendation<U, I> getRecommendation(U u, Stream<I> candidates, Long timestamp, I item0) {
		FastRecommendation rec = getRecommendation(user2uidx(u), candidates.mapToInt(this::item2iidx), timestamp,
				this.item2iidx(item0));

		return new Recommendation<>(uidx2user(rec.getUidx()),
				rec.getIidxs().stream().map(this::iidx2item).collect(toList()));
	}

	@Override
	public FastRecommendation getRecommendation(int uidx, IntStream candidates, Long timestamp, int iidx0) {
		IntSet set = new IntOpenHashSet();
		candidates.forEach(set::add);

		return getRecommendation(uidx, set::contains, timestamp, iidx0);
	}

}
