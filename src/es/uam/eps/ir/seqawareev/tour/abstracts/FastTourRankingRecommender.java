/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.tour.abstracts;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntPredicate;

import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.ranksys.fast.FastRecommendation;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.utils.topn.IntDoubleTopN;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;

import static java.lang.Math.min;
import static java.util.stream.Collectors.toList;

/***
 * Fast tour recommender abstract class (is the same as FastRankingRecommender
 * but for tour recommender)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public abstract class FastTourRankingRecommender<U, I> extends AbstractFastTourRecommender<U, I> {

	/**
	 * Constructor.
	 *
	 * @param uIndex
	 *            user index
	 * @param iIndex
	 *            item index
	 */
	public FastTourRankingRecommender(FastUserIndex<U> uIndex, FastItemIndex<I> iIndex) {
		super(uIndex, iIndex);
	}

	@Override
	public FastRecommendation getRecommendation(int uidx, int maxLength, IntPredicate filter, Long timestamp,
			int iidx0) {
		if (uidx == -1) {
			return new FastRecommendation(uidx, new ArrayList<>(0));
		}

		Int2DoubleMap scoresMap = getScoresMap(uidx, timestamp, iidx0);

		final IntDoubleTopN topN = new IntDoubleTopN(min(maxLength, scoresMap.size()));
		scoresMap.int2DoubleEntrySet().forEach(e -> {
			int iidx = e.getIntKey();
			double score = e.getDoubleValue();
			if (filter.test(iidx)) {
				topN.add(iidx, score);
			}
		});

		topN.sort();

		List<Tuple2id> items = topN.reverseStream().collect(toList());

		return new FastRecommendation(uidx, items);
	}

	/**
	 * Returns a map of item-score pairs.
	 *
	 * @param uidx
	 *            index of the user whose scores are predicted
	 * @return a map of item-score pairs
	 */
	public abstract Int2DoubleMap getScoresMap(int uidx, Long timestamp, int iidx0);

}
