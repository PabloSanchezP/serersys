/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.tour.interfaces;

import java.util.function.IntPredicate;
import java.util.stream.IntStream;

import es.uam.eps.ir.ranksys.fast.FastRecommendation;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;

/**
 * Fast tour recommender interface (same as the Fast Recommender interface of
 * ranksys)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public interface FastTourRecommender<U, I> extends TourRecommenderIF<U, I>, FastUserIndex<U>, FastItemIndex<I> {

	/**
	 * Free recommendation. Generate recommendations without any restriction on the
	 * items being recommended.
	 *
	 * @param uidx
	 *            index of the user to be issued a recommendation
	 * @return a (fast) recommendation list
	 */
	FastRecommendation getRecommendation(int uidx, Long timestamp, int iidx0);

	/**
	 * Free recommendation. Generate recommendations without any restriction on the
	 * items being recommended, but with a limit on the list size.
	 *
	 * @param uidx
	 *            index of the user to be issued a recommendation
	 * @param maxLength
	 *            maximum length of recommendation
	 * @return a (fast) recommendation list
	 */
	FastRecommendation getRecommendation(int uidx, int maxLength, Long timestamp, int iidx0);

	/**
	 * Filter recommendation. Recommends only the items that pass the filter.
	 *
	 * @param uidx
	 *            index of the user to be issued a recommendation
	 * @param filter
	 *            (fast) filter to decide which items might be recommended
	 * @return a (fast) recommendation list
	 */
	FastRecommendation getRecommendation(int uidx, IntPredicate filter, Long timestamp, int iidx0);

	/**
	 * Filter recommendation. Recommends only the items that pass the filter up to a
	 * maximum list size.
	 *
	 * @param uidx
	 *            index of the user to be issued a recommendation
	 * @param maxLength
	 *            maximum length of recommendation
	 * @param filter
	 *            (fast) filter to decide which items might be recommended
	 * @return a (fast) recommendation list
	 */
	FastRecommendation getRecommendation(int uidx, int maxLength, IntPredicate filter, Long timestamp, int iidx0);

	/**
	 * Candidates ranking. Create a list that may contain only the items in the
	 * candidates set.
	 *
	 * @param uidx
	 *            item of the user to be issued a recommendation
	 * @param candidates
	 *            candidate items to be included in the recommendation
	 * @return a recommendation list
	 */
	FastRecommendation getRecommendation(int uidx, IntStream candidates, Long timestamp, int iidx0);
}
