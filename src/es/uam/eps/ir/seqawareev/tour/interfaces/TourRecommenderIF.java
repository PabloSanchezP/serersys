/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.tour.interfaces;

import java.util.function.Predicate;
import java.util.stream.Stream;

import es.uam.eps.ir.ranksys.core.Recommendation;

/***
 * Tour recommender Interface (similar the Recommender interface in RankSys but
 * with coordinates and a timestamp to start making recommendations)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public interface TourRecommenderIF<U, I> {
	/**
	 * Free recommendation. Generate recommendations without any restriction on the
	 * items being recommended.
	 *
	 * @param u
	 *            user to be issued a recommendation
	 * @return a recommendation list
	 */
	public Recommendation<U, I> getRecommendation(U u, Long timestamp, I item0);

	/**
	 * Free recommendation. Generate recommendations without any restriction on the
	 * items being recommended, but with a limit on the list size.
	 *
	 * @param u
	 *            user to be issued a recommendation
	 * @param maxLength
	 *            maximum length of recommendation
	 * @return a recommendation list
	 */
	public Recommendation<U, I> getRecommendation(U u, int maxLength, Long timestamp, I item0);

	/**
	 * Filter recommendation. Recommends only the items that pass the filter.
	 *
	 * @param u
	 *            user to be issued a recommendation
	 * @param filter
	 *            filter to decide which items might be recommended
	 * @return a recommendation list
	 */
	public Recommendation<U, I> getRecommendation(U u, Predicate<I> filter, Long timestamp, I item0);

	/**
	 * Filter recommendation. Recommends only the items that pass the filter up to a
	 * maximum list size.
	 *
	 * @param u
	 *            user to be issued a recommendation
	 * @param maxLength
	 *            maximum length of recommendation
	 * @param filter
	 *            filter to decide which items might be recommended
	 * @return a recommendation list
	 */
	public Recommendation<U, I> getRecommendation(U u, int maxLength, Predicate<I> filter, Long timestamp, I item0);

	/**
	 * Candidates ranking. Create a list that may contain only the items in the
	 * candidates set.
	 *
	 * @param u
	 *            user to be issued a recommendation
	 * @param candidates
	 *            candidate items to be included in the recommendation
	 * @return a recommendation list
	 */
	public Recommendation<U, I> getRecommendation(U u, Stream<I> candidates, Long timestamp, I item0);

}
