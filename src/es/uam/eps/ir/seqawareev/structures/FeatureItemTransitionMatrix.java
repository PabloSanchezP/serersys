/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.structures;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.jooq.lambda.tuple.Tuple2;

import com.google.common.util.concurrent.AtomicDouble;

import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.seqawareev.utils.CernMatrixUtils;

/***
 * Feature item Transition matrix (It assumes that the item has ONLY ONE
 * FEATURE)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <I>
 * @param <F>
 */
public class FeatureItemTransitionMatrix<U, I, F> extends ItemTransitionMatrix<U, I> {
	private Map<F, Integer> featureTofidx;
	private Map<Integer, F> fidxToFeature;
	private final FeatureData<I, F, ?> fD;

	public FeatureItemTransitionMatrix(FastTemporalPreferenceDataIF<U, I> temporalPreferenceData,
			FeatureData<I, F, ?> fD) {
		super(temporalPreferenceData);
		this.fD = fD;
	}

	public void initializeFor1User(int uidx) {
		this.featureTofidx = new TreeMap<>();
		this.fidxToFeature = new TreeMap<>();
		this.finalDimension = fD.numFeaturesWithItems();

		transitionMatrix = new DenseDoubleMatrix2D(finalDimension, finalDimension);
		transitionMatrix.assign(x -> x = 0);

		priors = new DenseDoubleMatrix1D(finalDimension);
		priors.assign(x -> 0);

		// All features to idxs
		AtomicInteger count = new AtomicInteger(0);
		fD.getAllFeatures().forEach(f -> {
			this.featureTofidx.put(f, count.get());
			this.fidxToFeature.put(count.get(), f);
			count.incrementAndGet();
		});

		// Building the matrix
		Integer fidxPrev = null;
		List<IdxTimePref> prefsUser = temporalPreferenceData.getUidxTimePreferences(uidx)
				.sorted(PreferenceComparators.timeComparatorIdxTimePref).collect(Collectors.toList());
		for (IdxTimePref pref : prefsUser) {

			// For every preference of the user (ordered from the most ancient to the most
			// recent)
			Optional<Integer> featureNextOP = this.fD.getItemFeatures(this.temporalPreferenceData.iidx2item(pref.v1))
					.map(t -> featureTofidx(t.v1)).findFirst();
			if (featureNextOP.isPresent()) {
				Integer featureNext = featureNextOP.get();
				if (fidxPrev != null) {
					this.transitionMatrix.setQuick(fidxPrev, featureNext,
							this.transitionMatrix.getQuick(fidxPrev, featureNext) + 1.0);
				}
				fidxPrev = featureNext;
			}
		}

		// Now, we divide all the values in every row by the sum of every row (to
		// compute the probabilities)
		CernMatrixUtils.normalizeRowsMatrix(this.transitionMatrix);
		double numPrefsUser = prefsUser.size();

		for (IdxTimePref pref : prefsUser) {
			Tuple2<F, ?> t = fD.getItemFeatures(this.temporalPreferenceData.iidx2item(pref.v1)).findFirst()
					.orElse(null);

			if (t != null) {
				F f = t.v1;
				int fidx = this.featureTofidx(f);

				this.priors.setQuick(fidx, this.priors.getQuick(fidx) + 1);
			}
		}

		for (int i = 0; i < this.priors.size(); i++) {
			this.priors.setQuick(i, this.priors.get(i) / numPrefsUser);
		}
	}

	@Override
	public void initialize() {
		this.featureTofidx = new TreeMap<>();
		this.fidxToFeature = new TreeMap<>();
		this.finalDimension = fD.numFeaturesWithItems();

		transitionMatrix = new DenseDoubleMatrix2D(finalDimension, finalDimension);
		transitionMatrix.assign(x -> x = 0);

		priors = new DenseDoubleMatrix1D(finalDimension);
		priors.assign(x -> 0);

		// All features to idxs
		AtomicInteger count = new AtomicInteger(0);
		fD.getAllFeatures().forEach(f -> {
			this.featureTofidx.put(f, count.get());
			this.fidxToFeature.put(count.get(), f);
			count.incrementAndGet();
		});

		// Building the matrix
		temporalPreferenceData.getUidxWithPreferences().forEach(uidx -> {
			Integer fidxPrev = null;
			List<IdxTimePref> prefsUser = temporalPreferenceData.getUidxTimePreferences(uidx)
					.sorted(PreferenceComparators.timeComparatorIdxTimePref).collect(Collectors.toList());
			for (IdxTimePref pref : prefsUser) {

				// For every preference of the user (ordered from the most ancient to the most
				// recent)
				Optional<Integer> featureNextOP = this.fD
						.getItemFeatures(this.temporalPreferenceData.iidx2item(pref.v1)).map(t -> featureTofidx(t.v1))
						.findFirst();
				if (featureNextOP.isPresent()) {
					Integer featureNext = featureNextOP.get();
					if (fidxPrev != null) {
						this.transitionMatrix.setQuick(fidxPrev, featureNext,
								this.transitionMatrix.getQuick(fidxPrev, featureNext) + 1.0);
					}
					fidxPrev = featureNext;
				}
			}
		});

		// Now, we divide all the values in every row by the sum of every row (to
		// compute the probabilities)
		CernMatrixUtils.normalizeRowsMatrix(this.transitionMatrix);

		fD.getFeaturesWithItems().forEach(f -> {
			int fidx = this.featureTofidx(f);
			AtomicDouble featurePrefs = new AtomicDouble(0);

			fD.getFeatureItems(f).forEach(t -> {
				if (this.temporalPreferenceData.containsItem(t.v1)) {
					featurePrefs.addAndGet(this.temporalPreferenceData.getItemPreferences(t.v1).count());
				}
			});

			this.priors.setQuick(fidx, featurePrefs.get() / this.temporalPreferenceData.numPreferences());
		});

	}

	public FeatureData<I, F, ?> getfD() {
		return fD;
	}

	public F fidxToFeature(int fidx) {
		return this.fidxToFeature.get(fidx);
	}

	public int featureTofidx(F feature) {
		return this.featureTofidx.get(feature);
	}

	public Map<F, Integer> getFeatureTofidx() {
		return featureTofidx;
	}

	public void setFeatureTofidx(Map<F, Integer> featureTofidx) {
		this.featureTofidx = featureTofidx;
	}

	public Map<Integer, F> getFidxToFeature() {
		return fidxToFeature;
	}

	public void setFidxToFeature(Map<Integer, F> fidxToFeature) {
		this.fidxToFeature = fidxToFeature;
	}

}
