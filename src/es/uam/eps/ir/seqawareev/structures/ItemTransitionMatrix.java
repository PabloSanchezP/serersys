/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.structures;

import java.util.concurrent.atomic.AtomicInteger;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.seqawareev.smooth.SmoothingProbabilityIF;
import es.uam.eps.ir.seqawareev.utils.CernMatrixUtils;

/**
 * Class to build a item x item transition matrix (k-order = 1) -Rows represent
 * the current state and columns represent the probability to go to the next
 * item
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class ItemTransitionMatrix<U, I> implements ItemTransitionMatrixIF {
	protected DenseDoubleMatrix2D transitionMatrix;
	protected DenseDoubleMatrix1D priors;
	protected final FastTemporalPreferenceDataIF<U, I> temporalPreferenceData;
	protected int finalDimension;

	public ItemTransitionMatrix(FastTemporalPreferenceDataIF<U, I> temporalPreferenceData) {
		this.temporalPreferenceData = temporalPreferenceData;
	}

	@Override
	public void initialize() {
		this.finalDimension = temporalPreferenceData.numItems();
		int totalPrefs = temporalPreferenceData.numPreferences();

		transitionMatrix = new DenseDoubleMatrix2D(this.finalDimension, this.finalDimension);
		transitionMatrix.assign(x -> x = 0); // Init all the matrix to 0s

		priors = new DenseDoubleMatrix1D(this.finalDimension);
		priors.assign(x -> 0);

		// Build the sequence
		temporalPreferenceData.getUidxWithPreferences().forEach(uidx -> {
			AtomicInteger iidxPrev = new AtomicInteger(-1);

			// For every preference of the user (ordered from the most ancient to the most
			// recent)
			temporalPreferenceData.getUidxTimePreferences(uidx).sorted(PreferenceComparators.timeComparatorIdxTimePref)
					.forEach(idxtimepref -> {
						if (iidxPrev.get() != -1) {
							transitionMatrix.set(iidxPrev.get(), idxtimepref.v1,
									transitionMatrix.get(iidxPrev.get(), idxtimepref.v1) + 1.0);
						}
						iidxPrev.set(idxtimepref.v1);
					});

		});

		// Now, we divide all the values in every row by the sum of every row (to
		// compute the probabilities)
		CernMatrixUtils.normalizeRowsMatrix(this.transitionMatrix);

		// Finally, the priors
		temporalPreferenceData.getAllIidx().forEach(iidx -> {
			long nPref = temporalPreferenceData.getIidxTimePreferences(iidx).count();
			priors.setQuick(iidx, (double) nPref / totalPrefs);
		});

	}

	@Override
	public double priorProbabilty(int iidx) {
		return priors.get(iidx);
	}

	@Override
	public DoubleMatrix1D getItemRow(int iidx) {
		return transitionMatrix.viewRow(iidx);
	}

	@Override
	public double conditionalProbItemToItem(int iidxPrevius, int iidxNext) {
		return transitionMatrix.get(iidxPrevius, iidxNext);
	}

	@Override
	public DenseDoubleMatrix2D getItemToitemM() {
		return transitionMatrix;
	}

	@Override
	public void setItemToitemM(DenseDoubleMatrix2D itemToitemM) {
		this.transitionMatrix = itemToitemM;
	}

	@Override
	public DenseDoubleMatrix1D priorMultiplyItemTransitionMatrix() {
		DenseDoubleMatrix1D result = new DenseDoubleMatrix1D(this.priors.size());
		for (int i = 0; i < result.size(); i++) {
			result.setQuick(i, priors.zDotProduct(this.transitionMatrix.viewColumn(i)));
		}
		return result;
	}

	@Override
	public double getProbabilitySmoothing(int iidxPrev, int iidxNext, SmoothingProbabilityIF smoothF) {
		return smoothF.getProbability(this.conditionalProbItemToItem(iidxPrev, iidxNext),
				this.priorProbabilty(iidxNext));
	}

}
