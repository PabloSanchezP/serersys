/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.structures.suffix;

import java.util.ArrayList;
import java.util.List;

/***
 * Generic suffix tree implementation. Version implemented on:
 * https://rosettacode.org/wiki/Suffix_tree
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <T>
 */
public class GenericSuffixTree<T> {
	private List<Node<T>> nodes;

	public GenericSuffixTree(List<T> str) {
		nodes = new ArrayList<>();
		nodes.add(new Node<>());

		for (int i = 0; i < str.size(); ++i) {
			addSuffix(str.subList(i, str.size()));
		}

	}

	private void addSuffix(List<T> suf) {
		int n = 0;
		int i = 0;
		while (i < suf.size()) {
			T b = suf.get(i);
			List<Integer> children = nodes.get(n).children;
			int x2 = 0;
			int n2;
			while (true) {
				if (x2 == children.size()) {
					// no matching child, remainder of suf becomes new node.
					n2 = nodes.size();
					Node<T> temp = new Node<T>();
					temp.elements = suf.subList(i, suf.size());
					nodes.add(temp);
					children.add(n2);
					return;
				}
				n2 = children.get(x2);
				if (nodes.get(n2).elements.get(0).equals(b)) {
					break;
				}
				x2++;
			}
			// find prefix of remaining suffix in common with child
			List<T> sub2 = nodes.get(n2).elements;
			int j = 0;
			while (j < sub2.size()) {
				if (suf.get(i + j) == null || sub2.get(j) == null || !suf.get(i + j).equals(sub2.get(j))) {
					// split n2
					int n3 = n2;
					// new node for the part in common
					n2 = nodes.size();
					Node<T> temp = new Node<>();

					temp.elements = sub2.subList(0, j);
					temp.children.add(n3);
					nodes.add(temp);
					nodes.get(n3).elements = sub2.subList(j, sub2.size()); // old node loses the part in common
					nodes.get(n).children.set(x2, n2);
					break; // continue down the tree
				}
				j++;
			}
			i += j; // advance past part in common
			n = n2; // continue down the tree
		}
	}

	public boolean exist(List<T> pattern) {
		if (pattern.isEmpty()) {
			return true;
		}
		return existRecursive(pattern, nodes.get(0));
	}

	private boolean existRecursive(List<T> pattern, Node<T> search) {
		if (pattern.isEmpty()) {
			return true;
		}

		int indexToMove;
		for (Integer search2 : search.children) {
			indexToMove = matching(pattern, this.nodes.get(search2));
			if (indexToMove != -1) {
				List<T> newPattern = pattern.subList(indexToMove, pattern.size());
				return existRecursive(newPattern, this.nodes.get(search2));
			}
		}

		return false;

	}

	private int matching(List<T> pattern, Node<T> search) {
		int acc = 0;

		for (int k = 0; k < search.elements.size(); k++) {
			if (k >= pattern.size()) {
				return acc;
			}

			if (!pattern.get(k).equals(search.elements.get(k))) {
				return -1;
			}
			acc++;
		}

		return acc;
	}



}
