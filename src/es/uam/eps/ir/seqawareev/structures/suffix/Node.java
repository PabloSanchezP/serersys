/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.structures.suffix;

import java.util.ArrayList;
import java.util.List;

/***
 * Node class for Suffix tree problem
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <T>
 */
public class Node<T> {
	protected List<T> elements; // the elements of the node (substring)
	protected List<Integer> children;//

	public Node() {
		elements = new ArrayList<>();
		children = new ArrayList<>();
	}

	public int getLength() {
		return elements.size();
	}
}
