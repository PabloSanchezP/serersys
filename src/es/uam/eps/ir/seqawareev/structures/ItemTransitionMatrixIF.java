/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.structures;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import es.uam.eps.ir.seqawareev.smooth.SmoothingProbabilityIF;

/***
 * Interface of a transition matrix
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public interface ItemTransitionMatrixIF {

	/***
	 * Initialize the matrix
	 */
	public void initialize();

	/**
	 * Obtain the conditional probability of two items
	 * 
	 * @param iidxPrevius
	 *            the previous item
	 * @param iidxNext
	 *            the next item
	 * @return the conditional probability
	 */
	public double conditionalProbItemToItem(int iidxPrevius, int iidxNext);

	/**
	 * Obtain the row probabilities
	 * 
	 * @param iidx
	 *            the index of the row
	 * @return all the columns of that row
	 */
	public DoubleMatrix1D getItemRow(int iidx);

	/**
	 * Method to obtain the transition matrix
	 * 
	 * @return
	 */
	public DenseDoubleMatrix2D getItemToitemM();

	/***
	 * Method to
	 * 
	 * @param itemToitemM
	 */
	public void setItemToitemM(DenseDoubleMatrix2D itemToitemM);

	/***
	 * Method to obtain the prior probability of an index
	 * 
	 * @param iidx
	 * @return the prior probability
	 */
	public double priorProbabilty(int iidx);

	/***
	 * Method to multiply the prior vector and the matrix
	 * 
	 * @return the result vector
	 */
	public DenseDoubleMatrix1D priorMultiplyItemTransitionMatrix();

	/***
	 * Method to get the probability applying a smooth function
	 * 
	 * @param iidxPrev
	 *            the previous index
	 * @param iidxNext
	 *            the next
	 * @param smoothF
	 *            the smooth function
	 * @return
	 */
	public double getProbabilitySmoothing(int iidxPrev, int iidxNext, SmoothingProbabilityIF smoothF);

}
