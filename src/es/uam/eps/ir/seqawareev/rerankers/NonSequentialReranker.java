/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rerankers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;

/**
 * Non Sequential reranker (reranker that does not need to work with the first
 * item in the test set)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public abstract class NonSequentialReranker<U, I> extends LambdaRerankerRelevance<U, I> {
	private final boolean maintainFirst;

	public NonSequentialReranker(double lambda, FILL_RERANKING_STRATEGY strat,
			FastTemporalPreferenceDataIF<U, I> temporalPreferenceData, FastPreferenceData<U, I> preferenceData,
			boolean maintainFirst) {
		super(lambda, strat, temporalPreferenceData, preferenceData);
		this.maintainFirst = maintainFirst;
	}

	public abstract void generateRerank(U user, Set<I> candidates, List<Tuple2od<I>> result);

	@Override
	public List<Tuple2od<I>> rerankRecommendation(Recommendation<U, I> recommendation) {
		Set<I> leftCandidates = recommendation.getItems().stream().map(rec -> rec.v1).collect(Collectors.toSet());
		List<Tuple2od<I>> passedItems = new ArrayList<>();
		int realMaxLength = recommendation.getItems().size();

		if (maintainFirst) {
			leftCandidates.remove(recommendation.getItems().get(0).v1);
			passedItems.add(new Tuple2od<>(recommendation.getItems().get(0).v1, realMaxLength));
		}

		generateRerank(recommendation.getUser(), leftCandidates, passedItems);
		return passedItems;
	}

}
