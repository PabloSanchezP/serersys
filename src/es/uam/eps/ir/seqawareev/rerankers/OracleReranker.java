/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rerankers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;

/***
 * Oracle reranker. This reranker will receive the test set. It will recommend
 * in the same order as in the test set the items that are in the test set and
 * that have been returned by the recommender
 * 
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class OracleReranker<U, I> extends NonSequentialReranker<U, I> {

	private final FastTemporalPreferenceDataIF<U, I> testData;

	public OracleReranker(double lambda, FILL_RERANKING_STRATEGY strat,
			FastTemporalPreferenceDataIF<U, I> temporalPreferenceData, FastPreferenceData<U, I> preferenceData,
			FastTemporalPreferenceDataIF<U, I> testData, boolean maintainFirst) {
		super(lambda, strat, temporalPreferenceData, preferenceData, maintainFirst);
		this.testData = testData;
	}

	@Override
	public void generateRerank(U user, Set<I> candidates, List<Tuple2od<I>> result) {
		List<I> itemsRatedUser = testData.getUidxTimePreferences(testData.user2uidx(user))
				.sorted(PreferenceComparators.timeComparatorIdxTimePref).map(prefx -> this.testData.iidx2item(prefx.v1))
				.collect(Collectors.toList());

		int realMaxLength = candidates.size();
		for (I item : itemsRatedUser) {
			if (candidates.contains(item)) {
				result.add(new Tuple2od<>(item, realMaxLength));
				candidates.remove(item);
				realMaxLength--;
				if (realMaxLength <= 0) {
					break;
				}
			}
		}
	}

}
