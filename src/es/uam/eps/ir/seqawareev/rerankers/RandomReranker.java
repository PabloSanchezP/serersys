/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rerankers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;

/***
 * Random reranker. It will reorder the recommended list randomly
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class RandomReranker<U, I> extends NonSequentialReranker<U, I> {

	public RandomReranker(double lambda, FILL_RERANKING_STRATEGY strat,
			FastTemporalPreferenceDataIF<U, I> temporalPreferenceData, FastPreferenceData<U, I> preferenceData,
			boolean maintainFirst) {
		super(lambda, strat, temporalPreferenceData, preferenceData, maintainFirst);
	}

	@Override
	public void generateRerank(U user, Set<I> candidates, List<Tuple2od<I>> result) {
		int realMaxLength = candidates.size();
		List<I> candidatesAux = new ArrayList<>(candidates);
		Collections.shuffle(candidatesAux);

		for (int i = 0; i < realMaxLength - 1; i++) {
			result.add(new Tuple2od<>(candidatesAux.get(i), realMaxLength - 1 - i));
		}
	}

}
