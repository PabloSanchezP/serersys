/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rerankers;

import java.util.List;
import java.util.Set;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.rec.Recommender;

/***
 * Generic recommender reranker. It will rerank the items using any generic
 * recommender
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class GenericRecommenderReranker<U, I> extends NonSequentialReranker<U, I> {

	private final Recommender<U, I> recommender;

	public GenericRecommenderReranker(double lambda, FILL_RERANKING_STRATEGY strat,
			FastTemporalPreferenceDataIF<U, I> temporalPreferenceData, FastPreferenceData<U, I> preferenceData,
			Recommender<U, I> recommender, boolean maintainFirst) {
		super(lambda, strat, temporalPreferenceData, preferenceData, maintainFirst);
		this.recommender = recommender;
	}

	@Override
	public void generateRerank(U user, Set<I> candidates, List<Tuple2od<I>> result) {
		Recommendation<U, I> newRec = this.recommender.getRecommendation(user, candidates.stream());

		int sizeRerankking = newRec.getItems().size();
		for (int i = 0; i < sizeRerankking; i++) {
			result.add(new Tuple2od<>(newRec.getItems().get(i).v1, sizeRerankking - i));
		}
	}

}
