/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rerankers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.novdiv.reranking.Reranker;
import es.uam.eps.ir.seqawareev.comparators.WeightComparatorTuple3;
import es.uam.eps.ir.seqawareev.utils.ReRankingUtils;

/***
 * Lambda Relevance Reranker (combines the returned list of the recommender with
 * the reranker)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public abstract class LambdaRerankerRelevance<U, I> implements Reranker<U, I> {
	private final double lambda;
	private final FILL_RERANKING_STRATEGY strat;

	protected FastTemporalPreferenceDataIF<U, I> temporalPreferenceData;
	protected FastPreferenceData<U, I> preferenceData;

	public enum FILL_RERANKING_STRATEGY {
		NONE("NONE"), ORIGINAL_ORDER("OR_ORDER"), POPULARITY("POP");

		private String shortName;

		private FILL_RERANKING_STRATEGY(String name) {
			this.shortName = name;
		}

		public String getShortName() {
			return shortName;
		}

	};

	public LambdaRerankerRelevance(double lambda, FILL_RERANKING_STRATEGY strat,
			FastTemporalPreferenceDataIF<U, I> temporalPreferenceData, FastPreferenceData<U, I> preferenceData) {
		this.lambda = lambda;
		this.temporalPreferenceData = temporalPreferenceData;
		this.preferenceData = preferenceData;
		this.strat = strat;
	}

	private void fillRerankedList(List<Tuple2od<I>> original, List<Tuple2od<I>> rerankedList) {
		Set<I> rerankedItems = rerankedList.stream().map(t -> t.v1).collect(Collectors.toSet());
		// We need to maintain the order
		List<I> originalItems = original.stream().map(t -> t.v1).collect(Collectors.toList());

		switch (this.strat) {
		case NONE:
			break;
		case ORIGINAL_ORDER: {
			if (rerankedList.size() == 0) {
				for (int i =0 ; i < originalItems.size() ; i++) {
					I item = originalItems.get(i);
					if (!rerankedItems.contains(item)) {
						rerankedList.add(new Tuple2od<I>(item, originalItems.size() - i));
					}
					
				}
			}
			else {
				for (I i : originalItems) {
					if (!rerankedItems.contains(i)) {
						rerankedList.add(new Tuple2od<I>(i, rerankedList.get(rerankedList.size() - 1).v2 - 0.1));
					}
				}
			}
		}
			break;
		case POPULARITY: {
			Set<I> candidates = new HashSet<>(originalItems);
			List<Tuple2od<I>> toAdd = null;
			if (this.temporalPreferenceData != null) {
				toAdd = ReRankingUtils.getCandidatesOrderedByPopularity(candidates, this.temporalPreferenceData);
			} else {
				toAdd = ReRankingUtils.getCandidatesOrderedByPopularity(candidates, this.preferenceData);
			}

			rerankedList.addAll(toAdd);
		}
			break;
		default:
			System.out.println("Filling strategy is null, taking NONE");
			break;
		}
	}

	@Override
	public Recommendation<U, I> rerankRecommendation(Recommendation<U, I> recommendation, int maxLength) {
		int realMaxLength = Math.min(maxLength, recommendation.getItems().size());
		List<Tuple2od<I>> originalRecommendation = recommendation.getItems().subList(0, realMaxLength);
		Recommendation<U, I> parsedRecommendation = new Recommendation<U, I>(recommendation.getUser(),
				originalRecommendation);

		U u = parsedRecommendation.getUser();

		// ESTO
		List<Tuple2od<I>> reranked = obtainReranked(parsedRecommendation);

		List<Tuple2od<I>> originalReranked = parsedRecommendation.getItems();
		fillRerankedList(originalReranked, reranked);
		List<Tuple2od<I>> lst = combinedLists(originalReranked, reranked, realMaxLength);

		return new Recommendation<U, I>(u, lst);
	}

	private List<Tuple2od<I>> combinedLists(List<Tuple2od<I>> originalRecommendation,
			List<Tuple2od<I>> rerankRecommendation, int maxLength) {
		Map<I, Double> scores = new HashMap<>();
		List<Tuple2od<I>> result = new ArrayList<>();

		double c = maxLength;
		if (this.lambda != 0) {
			for (Tuple2od<I> t : originalRecommendation) {
				scores.put(t.v1, c * lambda);
				c--;
				if (c == 0) {
					break;
				}
			}
		}

		c = maxLength;
		if (this.lambda != 1) {
			for (Tuple2od<I> t : rerankRecommendation) {
				if (scores.get(t.v1) == null) {
					scores.put(t.v1, 0.0);
				}
				scores.put(t.v1, scores.get(t.v1) + c * (1 - lambda));
				c--;
				if (c == 0) {
					break;
				}
			}
		}

		for (Map.Entry<I, Double> entry : scores.entrySet()) {
			result.add(new Tuple2od<>(entry.getKey(), entry.getValue()));
		}

		// In case we are working with temporal pref datas
		if (temporalPreferenceData != null) {
			result = result.stream()
					.map(tod -> new Tuple3<>(temporalPreferenceData.item2iidx(tod.v1), tod.v2,
							(double) temporalPreferenceData.getItemPreferences(tod.v1).count()))
					.sorted(new WeightComparatorTuple3().reversed())
					.map(t3 -> new Tuple2od<>(temporalPreferenceData.iidx2item(t3.v1), t3.v2)).limit(maxLength)
					.collect(Collectors.toList());
		} else {
			result = result.stream()
					.map(tod -> new Tuple3<>(preferenceData.item2iidx(tod.v1), tod.v2,
							(double) preferenceData.getItemPreferences(tod.v1).count()))
					.sorted(new WeightComparatorTuple3().reversed())
					.map(t3 -> new Tuple2od<>(preferenceData.iidx2item(t3.v1), t3.v2)).limit(maxLength)
					.collect(Collectors.toList());
		}

		return result;
	}

	/**
	 * Method to obtain the reranked list if the data of the reranker contains the
	 * user. If not, it wont recommend anything
	 * 
	 * @param parsedRecommendation
	 * @return
	 */
	private List<Tuple2od<I>> obtainReranked(Recommendation<U, I> parsedRecommendation) {
		boolean userFound = false;
		U user = parsedRecommendation.getUser();
		if (temporalPreferenceData != null) {
			userFound = temporalPreferenceData.containsUser(user)
					&& temporalPreferenceData.getUserPreferences(user).count() > 0;
		} else {
			userFound = preferenceData.containsUser(user) && preferenceData.getUserPreferences(user).count() > 0;
		}
		if (userFound) {
			return rerankRecommendation(parsedRecommendation);
		}
		return parsedRecommendation.getItems();
	}

	public abstract List<Tuple2od<I>> rerankRecommendation(Recommendation<U, I> recommendation);

}
