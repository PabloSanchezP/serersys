/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rerankers.seq;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.seqawareev.rerankers.LambdaRerankerRelevance;
import es.uam.eps.ir.seqawareev.sim.AbstractTourCachedItemSimilarity;
import es.uam.eps.ir.seqawareev.utils.ReRankingUtils;

/***
 * Generic Item similarity reranker. Can work either with a temporal and a
 * non-temporal preference data
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class GenericItemSimilarityReranker<U, I> extends LambdaRerankerRelevance<U, I> {
	private final AbstractTourCachedItemSimilarity<I> iSim;

	public GenericItemSimilarityReranker(double lambda, FILL_RERANKING_STRATEGY strat,
			FastTemporalPreferenceDataIF<U, I> temporalPreferenceData, FastPreferenceData<U, I> preferenceData,
			AbstractTourCachedItemSimilarity<I> isim) {
		super(lambda, strat, temporalPreferenceData, preferenceData);
		this.iSim = isim;
	}

	@Override
	public List<Tuple2od<I>> rerankRecommendation(Recommendation<U, I> recommendation) {
		Set<I> candidates = recommendation.getItems().stream().map(rec -> rec.v1).collect(Collectors.toSet());
		int realMaxLength = candidates.size();
		return ReRankingUtils.getSequenceItemOrderSimByPopularity(recommendation.getItems().get(0).v1, iSim, candidates,
				realMaxLength);
	}

}
