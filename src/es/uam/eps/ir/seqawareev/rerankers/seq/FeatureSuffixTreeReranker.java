/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rerankers.seq;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.seqawareev.rerankers.LambdaRerankerRelevance;
import es.uam.eps.ir.seqawareev.structures.FeatureItemTransitionMatrix;
import es.uam.eps.ir.seqawareev.utils.ReRankingUtils;

/***
 * Feature Suffix tree reranker (maximizes the item that follow the suffix tree sequence by categories)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 */
public class FeatureSuffixTreeReranker<U, I, F> extends LambdaRerankerRelevance<U, I> {

	private final float[][] distanceMatrix;
	private final FeatureData<I, F, Double> featureData;
	private final int patternKOrder;

	public FeatureSuffixTreeReranker(double lambda, FILL_RERANKING_STRATEGY strat,
			FastTemporalPreferenceDataIF<U, I> temporalPreferenceData, FastPreferenceData<U, I> preferenceData,
			float[][] distanceMatrix, FeatureData<I, F, Double> featureData, int patternKOrder) {
		super(lambda, strat, temporalPreferenceData, preferenceData);
		this.featureData = featureData;
		this.distanceMatrix = distanceMatrix;
		this.patternKOrder = patternKOrder;
	}

	@Override
	public List<Tuple2od<I>> rerankRecommendation(Recommendation<U, I> recommendation) {

		FeatureItemTransitionMatrix<U, I, F> iT = new FeatureItemTransitionMatrix<>(temporalPreferenceData,
				this.featureData);
		iT.initializeFor1User(this.temporalPreferenceData.user2uidx(recommendation.getUser()));

		U user = recommendation.getUser();
		I item0 = recommendation.getItems().get(0).v1;
		Set<I> candidates = recommendation.getItems().stream().map(rec -> rec.v1).collect(Collectors.toSet());
		int realMaxLength = candidates.size();

		return ReRankingUtils.getSequenceSuffixTreeItemOrderByPopularity(item0, user, candidates,
				this.temporalPreferenceData, this.featureData, iT, distanceMatrix, this.patternKOrder, realMaxLength);

	}

}
