/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.sim;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;

import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import es.uam.eps.ir.ranksys.metrics.rank.LogarithmicDiscountModel;

/**
 * * Final class that have all the methods to compute different variations of
 * LCS
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class LongestCommonSubsequence {

	/**
	 * Enumeration indicating possible ways to operate with LCS when it is a
	 * matching: -INCREMENT1: normal way to operate. We add 1 to the index of the
	 * matrix -MULTIPLY_RATINGS: operating like cosine similarity
	 * -MULTIPLY_RATINGS_AVERAGE: operating like Pearson correlation -ALPHA: instead
	 * of adding 1, we add equivalent of alpha value -ALPHATHREHOLD: in this case
	 * LCS in a coincidence will be: Threshold+(|r_u-r_v|)
	 *
	 * @author pablo
	 *
	 */
	public static enum LCSOperationMatching {
		INCREMENT1, MULTIPLY_RATINGS, MULTIPLY_RATINGS_AVERAGE, ALPHA, GAPTHRESHOLD, MEANTHRESHOLD,
		// evaluation metrics
		METRIC_PRECISION, METRIC_RECALL, METRIC_NDCG_LOG, METRIC_MRR, METRIC_ARHR
	};

	public static Tuple3<Double, Integer, Integer> longestCommonSubsequenceMixed(Integer[] x, Integer[] y, Double[] xr,
			LCSOperationMatching op, double alpha, boolean backTracking) {
		int m = x.length;
		int n = y.length;
		int[][] c = new int[m + 1][n + 1];
		double s = 0.0;
		int finalIndexi = 0;
		int finalIndexj = 0;

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (x[i].equals(y[j])) {
					c[i + 1][j + 1] = c[i][j] + 1;
				} else {
					c[i + 1][j + 1] = Math.max(c[i + 1][j], c[i][j + 1]);
				}
			}
		}
		if (backTracking) {
			Tuple2<List<Integer>, List<Integer>> backTrackingIndexes = backTrackingIndexes(c, x, y);
			s = operationBackTracking(xr, backTrackingIndexes.v1, backTrackingIndexes.v2, alpha, op);
		}

		return resultOperation(s, c[m][n], op, alpha, finalIndexi, finalIndexj);
	}

	public static Integer longestCommonSubsequence(Integer[] x, Integer[] y) {
		int m = x.length;
		int n = y.length;
		int[][] c = new int[m + 1][n + 1];

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (x[i].equals(y[j])) {
					c[i + 1][j + 1] = c[i][j] + 1;
				} else {
					c[i + 1][j + 1] = Math.max(c[i + 1][j], c[i][j + 1]);
				}
			}
		}

		return c[m][n];
	}

	private static Tuple2<List<Integer>, List<Integer>> backTrackingIndexes(int[][] c, Integer[] x, Integer[] y) {
		List<Integer> resultX = new ArrayList<Integer>();
		List<Integer> resultY = new ArrayList<Integer>();

		int i = x.length;
		int j = y.length;
		while (i > 0 && j > 0) {
			// If current character in X[] and Y are same, then
			// current character is part of LCS
			if (x[i - 1].equals(y[j - 1])) {
				resultX.add(0, i - 1);
				resultY.add(0, j - 1);
				i--;
				j--;
			}

			// If not same, then find the larger of two and
			else if (c[i - 1][j] > c[i][j - 1]) {
				i--;
			} else {
				j--;
			}
		}

		return new Tuple2<>(resultX, resultY);
	}

	public static double operationBackTracking(Double[] xr, List<Integer> indexesXMatching,
			List<Integer> indexesYMatching, double alpha, LCSOperationMatching op) {
		double res = 0.0;
		switch (op) {
		case METRIC_PRECISION:
			// With increment (normal lcs), we assume the second array is the one of the
			// recommended list (in this case, the cutoff)
			// return new Tuple2<Integer, Double>(1, 1.0/yr.length);
			// the size of the recommended list does not always match the cutoff, so we
			// receive this cutoff in the alpha variable
			res = indexesYMatching.size() / alpha;
			break;
		case METRIC_RECALL:
			// With increment (normal lcs), we assume the first array is the one of the test
			// set (in this case, size of the user test)
			res = (double) indexesYMatching.size() / xr.length;
			break;
		case METRIC_MRR:
			// With increment (normal lcs), we assume the second array is the one of the
			// recommended list
			if (!indexesYMatching.isEmpty()) {
				res = 1.0 / (indexesYMatching.get(0) + 1.0);
			}
			break;
		case METRIC_ARHR:
			for (Integer index : indexesYMatching) {
				res += 1.0 / (index + 1.0);
			}
			break;
		case METRIC_NDCG_LOG:
			// With increment (normal lcs), we assume the second array is the one of the
			// recommended list
			double ndcgThreshold = 1.0;
			for (int i = 0; i < indexesXMatching.size(); i++) {
				res += (Math.pow(2, xr[indexesXMatching.get(i)] - ndcgThreshold + 1.0) - 1.0)
						* new LogarithmicDiscountModel().disc(indexesYMatching.get(i));
			}
			break;
		default:
			break;
		}
		return res;
	}

	/***
	 * Method to compute the LCS between two sequences of interactions but taking
	 * into account the item transition probabilities and the prior probabilities
	 * The identifiers and the ratings must be separated
	 * 
	 * @param xIdentifiers
	 *            the sequences of items ordered (first sequence)
	 * @param yIdentifiers
	 *            the sequences of items ordered (second sequence)
	 * @param matchingThreshold
	 *            the matching threshold
	 * 
	 * @param xr
	 *            the ratings of the first user
	 * @param yr
	 *            the ratings of the second user
	 * 
	 * @param transitionMatrix
	 *            the transition matrix of the MC of order 1
	 * @param priorProbabilities
	 *            the prior probabilites of the items
	 * @return
	 */
	public static Tuple3<Double, Integer, Integer> longestCommonSubsequenceMarkovChain(Integer[] xIdentifiers,
			Integer[] yIdentifiers, double matchingThreshold, Double[] xr, Double[] yr,
			DenseDoubleMatrix2D transitionMatrix, Map<Integer, Double> priorProbabilities) {
		int m = xIdentifiers.length;
		int n = yIdentifiers.length;

		double[][] c = new double[m + 1][n + 1];
		int finalIndexi = 0;
		int finalIndexj = 0;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (xIdentifiers[i].equals(xIdentifiers[j]) && Math.abs(xr[i] - yr[i]) <= matchingThreshold) {
					int itemI = xIdentifiers[i];
					int itemJ = yIdentifiers[j];

					double itemIincr = (i == 0) ? priorProbabilities.get(itemI)
							: transitionMatrix.get(itemI, xIdentifiers[i - 1]) + priorProbabilities.get(itemI);
					double itemJincr = (j == 0) ? priorProbabilities.get(itemJ)
							: transitionMatrix.get(itemJ, yIdentifiers[j - 1]) + priorProbabilities.get(itemJ);
					c[i + 1][j + 1] = c[i][j] + itemIincr + itemJincr;
				} else {
					c[i + 1][j + 1] = Math.max(c[i + 1][j], c[i][j + 1]);

				}
			}
		}

		return new Tuple3<Double, Integer, Integer>(c[m][n], finalIndexi, finalIndexj);
	}

	/**
	 * * Method that will be called at the end of the lcs algorithm.
	 *
	 * @param rs
	 *            the rating result of the operations
	 * @param lcs
	 *            the lcs of the sequences
	 * @param opthe
	 *            matching operation
	 * @param alpha
	 *            the alpha value to consider
	 * @return the result of the operation
	 */
	public static Tuple3<Double, Integer, Integer> resultOperation(double rs, int lcs, LCSOperationMatching op,
			double alpha, int lastX, int lastY) {
		switch (op) {
		case INCREMENT1:
			return new Tuple3<Double, Integer, Integer>((double) lcs, lastX, lastY);
		case MULTIPLY_RATINGS:
		case MULTIPLY_RATINGS_AVERAGE:
		case METRIC_PRECISION:
		case METRIC_RECALL:
		case METRIC_NDCG_LOG:
		case METRIC_ARHR:
		case METRIC_MRR:
			return new Tuple3<Double, Integer, Integer>(rs, lastX, lastY);
		case ALPHA:
			return new Tuple3<Double, Integer, Integer>((lcs / (1 / alpha)), lastX, lastY);
		default:
			return new Tuple3<Double, Integer, Integer>((double) lcs, lastX, lastY);
		}

	}

}
