/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.sim;

import org.jooq.lambda.tuple.Tuple3;

/***
 * Similarity interface to work with the last indexes of the similarities (work
 * with temporal information). Will be used to store and load similarities.
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <T>
 */
public interface SimilarityIndexIF<T> extends SimilarityIF<T> {

	/**
	 * Method to obtain a tuple of 3 of similarities
	 * 
	 * @param first
	 *            the first element
	 * @param second
	 *            the second elements
	 * @return a tuple indicating the value of similarity and the two indexes of the
	 *         coincidence
	 */
	public Tuple3<Double, Integer, Integer> completeSimilarity(T first, T second);

	/***
	 * Method to load the indexes of the similarities
	 */
	public void loadIndexesFile(String path);

}
