/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.sim;

import java.util.Map;
import java.util.function.IntToDoubleFunction;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.core.util.tuples.Tuple2id;
import org.ranksys.formats.parsing.Parser;

import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.nn.sim.Similarity;

/***
 * Class that integrates our abstract index similarity with a user similarity
 * from RankSys. As this is a user similarity, it needs an user abstract index
 * similarity
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 */
public class UserIndexReadingSimilarity<U extends Comparable<U>> extends AbstractIndexSimilarity<U>
		implements Similarity {

	protected FastUserIndex<U> uIndex;

	public UserIndexReadingSimilarity(Map<SymmetricSimilarityBean<U>, Tuple3<Double, Integer, Integer>> similarities,
			FastUserIndex<U> ranksysTrainData) {
		this(ranksysTrainData);
		this.similarities = similarities;
	}

	public UserIndexReadingSimilarity(String path, Double confidence, Parser<U> parser, FastUserIndex<U> uIndex) {
		super(path, confidence, parser);
		this.uIndex = uIndex;
	}

	public UserIndexReadingSimilarity(String path, Parser<U> parser, FastUserIndex<U> uIndex) {
		super(path, Double.NaN, parser);
		this.uIndex = uIndex;
	}

	public UserIndexReadingSimilarity(FastUserIndex<U> uIndex) {
		this.uIndex = uIndex;
	}

	@Override
	public IntToDoubleFunction similarity(int idx) {
		U first = uIndex.uidx2user(idx);
		return idx2 -> {
			U second = uIndex.uidx2user(idx2);
			return getSimilarity(first, second);
		};
	}

	@Override
	public Stream<Tuple2id> similarElems(int idx) {
		U first = this.uIndex.uidx2user(idx);
		Stream<Tuple2id> result = getSimilarities(first).entrySet().stream().map(e -> {
			int idx2 = uIndex.user2uidx(e.getKey());
			return new Tuple2id(idx2, e.getValue());
		});
		return result;
	}

	@Override
	public String info() {
		// TODO Auto-generated method stub
		return null;
	}

}
