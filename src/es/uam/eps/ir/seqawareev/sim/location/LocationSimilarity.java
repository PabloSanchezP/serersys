/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.sim.location;

import java.util.Map;
import java.util.Set;
import java.util.function.IntToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.crossdomainPOI.utils.FoursqrProcessData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.nn.sim.Similarity;

/***
 * Element location similarity (the closer the distance the higher the
 * similarity)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class LocationSimilarity implements Similarity {

	private final Map<Integer, Tuple2<Double, Double>> mapElementLocations;
	private final float[][] distanceMatrix;

	private Set<Integer> dataIdxWithRatings;

	public LocationSimilarity(FastPreferenceData<?, ?> data, Map<Integer, Tuple2<Double, Double>> mapElementLocations) {
		this.mapElementLocations = mapElementLocations;
		this.dataIdxWithRatings = data.getUidxWithPreferences().boxed().collect(Collectors.toSet());
		this.distanceMatrix = null;
	}

	public LocationSimilarity(FastPreferenceData<?, ?> data, float[][] distanceMatrix) {
		this.dataIdxWithRatings = data.getUidxWithPreferences().boxed().collect(Collectors.toSet());
		this.mapElementLocations = null;
		this.distanceMatrix = distanceMatrix;
	}

	@Override
	public IntToDoubleFunction similarity(int idx) {
		if (distanceMatrix == null) {
			return idx2 -> {
				return sim(idx, idx2);
			};
		} else {
			return idx2 -> {
				return 1.0 / (distanceMatrix[idx][idx2] + 0.01);
			};
		}

	}

	@Override
	public Stream<Tuple2id> similarElems(int idx) {
		if (distanceMatrix == null) {
			return this.mapElementLocations.keySet().stream()
					.filter(idx2 -> dataIdxWithRatings.contains(idx2) && idx2 != idx).map(idx2 -> {
						return new Tuple2id(idx2, sim(idx, idx2));
					});
		} else {
			return dataIdxWithRatings.stream().filter(idx2 -> !idx2.equals(idx)).map(iidx2 -> {
				return new Tuple2id(iidx2, 1.0 / (distanceMatrix[idx][iidx2] + 0.01));
			});
		}
	}

	/***
	 * Method to obtain a similarity between two users
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	private double sim(Integer first, Integer second) {
		Tuple2<Double, Double> u1Coordinates = this.mapElementLocations.get(first);
		Tuple2<Double, Double> u2Coordinates = this.mapElementLocations.get(second);
		return 1.0
				/ (FoursqrProcessData.haversine(u1Coordinates.v1, u1Coordinates.v2, u2Coordinates.v1, u2Coordinates.v2)
						+ 0.01);
	}

}
