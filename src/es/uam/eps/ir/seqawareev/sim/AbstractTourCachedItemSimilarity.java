/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.sim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.core.util.tuples.Tuple2id;
import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity;
import es.uam.eps.ir.ranksys.nn.sim.Similarity;
import es.uam.eps.ir.seqawareev.comparators.WeightComparatorTuple2;
import es.uam.eps.ir.seqawareev.comparators.WeightComparatorTuple3;

/***
 * Method to store the n most similar items so that we dot need to compute
 * always the similarity between items. Also, it will only store the most
 * important items (in method getMostSimilarItem) in order to sort only a small
 * part.
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <I>
 */
public abstract class AbstractTourCachedItemSimilarity<I> extends ItemSimilarity<I> {
	private Map<Integer, List<Integer>> mostSimilars;
	private final FastPreferenceData<?, I> data;
	private final FastTemporalPreferenceDataIF<?, I> dataTemporal;
	private final FastItemIndex<I> itemIndex;
	private final int numberCached;
	private final int numberItemsCompute;

	protected AbstractTourCachedItemSimilarity(FastPreferenceData<?, I> data, Similarity sim, int numberCached,
			int numberItemsCompute) {
		super(data, sim);
		this.data = data;
		this.itemIndex = data;
		this.dataTemporal = null;
		this.numberCached = numberCached;
		this.numberItemsCompute = numberItemsCompute;

		computeMostSimilar();
	}

	protected AbstractTourCachedItemSimilarity(FastTemporalPreferenceDataIF<?, I> data, Similarity sim,
			int numberCached, int numberItemsCompute) {
		super(data, sim);
		this.data = null;
		this.itemIndex = data;
		this.dataTemporal = data;
		this.numberCached = numberCached;
		this.numberItemsCompute = numberItemsCompute;

		computeMostSimilar();
	}

	private void computeMostSimilar() {
		this.mostSimilars = new HashMap<>();

		IntStream iidxMostPopular = null;
		if (this.data != null) {
			iidxMostPopular = this.data.getIidxWithPreferences()
					.mapToObj(iidx -> new Tuple2<Integer, Double>(iidx,
							(double) this.data.getIidxPreferences(iidx).count()))
					.sorted(new WeightComparatorTuple2().reversed()).limit(this.numberItemsCompute)
					.mapToInt(t2 -> t2.v1);
		} else {
			iidxMostPopular = this.dataTemporal.getIidxWithPreferences()
					.mapToObj(iidx -> new Tuple2<Integer, Double>(iidx,
							(double) this.dataTemporal.getIidxTimePreferences(iidx).count()))
					.sorted(new WeightComparatorTuple2().reversed()).limit(this.numberItemsCompute)
					.mapToInt(t2 -> t2.v1);
		}

		iidxMostPopular.forEach(iidx -> {
			mostSimilars.put(iidx, new ArrayList<Integer>());

			List<Tuple3<Integer, Double, Double>> lstOrdered = null;
			if (this.data != null) {
				lstOrdered = this.sim.similarElems(iidx).filter(t -> !Double.isNaN(t.v2))
						.map(t2 -> new Tuple3<>(t2.v1, t2.v2, (double) this.data.getIidxPreferences(t2.v1).count()))
						.sorted(new WeightComparatorTuple3().reversed()).collect(Collectors.toList());
			} else {
				lstOrdered = this.sim.similarElems(iidx).filter(t -> !Double.isNaN(t.v2))
						.map(t2 -> new Tuple3<>(t2.v1, t2.v2,
								(double) this.dataTemporal.getIidxTimePreferences(t2.v1).count()))
						.sorted(new WeightComparatorTuple3().reversed()).collect(Collectors.toList());
			}
			int n = this.numberCached;

			for (Tuple3<Integer, Double, Double> t3 : lstOrdered) {
				if (n <= 0) {
					break;
				}
				mostSimilars.get(iidx).add(t3.v1);
				n--;
			}
		});
	}

	public List<Integer> getMostSimilars(int iidx) {
		return mostSimilars.get(iidx);
	}

	public List<Integer> getMostSimilars(int iidx, Set<Integer> candidates) {
		return mostSimilars.get(iidx).stream().filter(iidx2 -> candidates.contains(iidx2)).collect(Collectors.toList());
	}

	public List<I> getMostSimilars(I item) {
		return mostSimilars.get(this.itemIndex.item2iidx(item)).stream().map(iidx2 -> this.itemIndex.iidx2item(iidx2))
				.collect(Collectors.toList());
	}

	public List<I> getMostSimilars(I item, Set<I> candidates) {
		return mostSimilars.get(this.itemIndex.item2iidx(item)).stream().map(iidx2 -> this.itemIndex.iidx2item(iidx2))
				.filter(item2 -> candidates.contains(item2)).collect(Collectors.toList());
	}

	public boolean isInMostSimilars(int iidx) {
		return mostSimilars.containsKey(iidx);
	}

	public boolean isInMostSimilars(I item) {
		return mostSimilars.containsKey(this.itemIndex.item2iidx(item));
	}

	public Map<Integer, List<Integer>> getMostSimilars() {
		return mostSimilars;
	}

	public int getNumberCached() {
		return numberCached;
	}

	/***
	 * Method to get the most similar item (if there is a tie in similarity, then we
	 * solve the tie by popularity, so the item that is more popular will be placed
	 * first)
	 * 
	 * @param item
	 *            the item
	 * @param candidates
	 *            the set of possible candidates
	 * @return
	 */
	public Tuple2od<I> getMostSimilarItem(I item, Set<I> candidates) {
		List<Tuple3<Integer, Double, Double>> proposed = new ArrayList<>();
		int iidx = this.itemIndex.item2iidx(item);
		List<Tuple2id> simElems = this.sim.similarElems(iidx)
				.filter(tid -> candidates.contains(this.iIndex.iidx2item(tid.v1))).filter(tid -> !Double.isNaN(tid.v2))
				.collect(Collectors.toList());

		for (Tuple2id tid : simElems) {
			double popOfItem = 0;
			if (this.data != null) {
				popOfItem = this.data.getIidxPreferences(tid.v1).count();
			} else {
				popOfItem = this.dataTemporal.getIidxTimePreferences(tid.v1).count();
			}

			if (proposed.isEmpty() || proposed.get(0).v2 < tid.v2) {
				proposed = new ArrayList<>();
				proposed.add(new Tuple3<>(tid.v1, tid.v2, popOfItem));
			} else if (proposed.get(0).v2.equals(tid.v2)) {
				// Same value of similarity
				proposed.add(new Tuple3<>(tid.v1, tid.v2, popOfItem));
			}
		}
		Collections.sort(proposed, new WeightComparatorTuple3().reversed());
		if (proposed.isEmpty()) {
			return null;
		}
		return new Tuple2od<I>(this.iidx2item(proposed.get(0).v1), proposed.get(0).v2);
	}

}
