/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.sim.probability;

import java.util.function.IntToDoubleFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.ranksys.nn.sim.Similarity;
import es.uam.eps.ir.seqawareev.smooth.SmoothingProbabilityIF;
import es.uam.eps.ir.seqawareev.structures.ItemTransitionMatrixIF;

/***
 * Similarity based on transition probabilities between items
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class TransitionSimilarity implements Similarity {

	private final ItemTransitionMatrixIF transitionMatrix;
	private final SmoothingProbabilityIF smoothF;

	public TransitionSimilarity(ItemTransitionMatrixIF transitionMatrix, SmoothingProbabilityIF smoothF) {
		this.transitionMatrix = transitionMatrix;
		this.smoothF = smoothF;
	}

	@Override
	public IntToDoubleFunction similarity(int idx) {
		return idx2 -> {
			double prob = transitionMatrix.getProbabilitySmoothing(idx, idx2, smoothF);
			if (prob == 0) {
				return Double.NaN;
			}
			return prob;
		};

	}

	@Override
	public Stream<Tuple2id> similarElems(int idx) {
		int totalRows = transitionMatrix.getItemToitemM().rows();
		return IntStream.range(0, totalRows).filter(idx2 -> idx2 != idx).boxed().map(idx2 -> {
			double prob = transitionMatrix.getProbabilitySmoothing(idx, idx2, smoothF);
			if (prob == 0) {
				return new Tuple2id(idx2, Double.NaN);
			}
			return new Tuple2id(idx2, prob);
		});
	}

}
