/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.sim.probability;

import java.util.function.IntToDoubleFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.nn.sim.Similarity;
import es.uam.eps.ir.seqawareev.smooth.SmoothingProbabilityIF;
import es.uam.eps.ir.seqawareev.structures.FeatureItemTransitionMatrix;

/***
 * Transition feature
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 */
public class FeatureTransitionSimilarity<U, I, F> implements Similarity {
	private final SmoothingProbabilityIF smoothF;
	private final FeatureItemTransitionMatrix<U, I, F> transitionMatrix;
	private final FastTemporalPreferenceDataIF<U, I> dataPrefData;

	public FeatureTransitionSimilarity(FastTemporalPreferenceDataIF<U, I> dataPrefData,
			FeatureItemTransitionMatrix<U, I, F> transitionMatrix, SmoothingProbabilityIF smoothF) {
		this.smoothF = smoothF;
		this.transitionMatrix = transitionMatrix;
		this.dataPrefData = dataPrefData;
	}

	@Override
	public IntToDoubleFunction similarity(int idx) {
		// transitionMatrix.getfD().getItemFeatures(dataPrefData.iidx2item(idx)).findFirst()
		Tuple2<F, ?> featureItemOP = this.transitionMatrix.getfD().getItemFeatures(dataPrefData.iidx2item(idx))
				.findFirst().orElse(null);
		if (featureItemOP == null) {
			return idx2 -> {
				return Double.NaN;
			};

		}
		F featureItem = featureItemOP.v1;

		int fidxfeatureItem = transitionMatrix.featureTofidx(featureItem);

		return idx2 -> {
			Tuple2<F, ?> featureItem2Op = transitionMatrix.getfD().getItemFeatures(dataPrefData.iidx2item(idx2))
					.findFirst().orElse(null);
			if (featureItem2Op == null) {
				return Double.NaN;
			}

			F featureItem2 = featureItem2Op.v1;
			int fidxfeatureItem2 = transitionMatrix.featureTofidx(featureItem2);
			double prob = transitionMatrix.getProbabilitySmoothing(fidxfeatureItem, fidxfeatureItem2, smoothF);
			if (prob == 0) {
				return Double.NaN;
			}
			return prob;
		};
	}

	@Override
	public Stream<Tuple2id> similarElems(int idx) {
		int totalItems = (int) dataPrefData.getAllIidx().count();

		Tuple2<F, ?> featureItemOP = this.transitionMatrix.getfD().getItemFeatures(dataPrefData.iidx2item(idx))
				.findFirst().orElse(null);
		if (featureItemOP == null) {
			return IntStream.range(0, totalItems).filter(idx2 -> idx2 != idx).boxed().map(idx2 -> {
				return new Tuple2id(idx2, Double.NaN);
			});
		}

		F featureItem = featureItemOP.v1;
		int fidxfeatureItem = transitionMatrix.featureTofidx(featureItem);

		return IntStream.range(0, totalItems).filter(idx2 -> idx2 != idx).boxed().map(idx2 -> {
			Tuple2<F, ?> featureItem2Op = transitionMatrix.getfD().getItemFeatures(dataPrefData.iidx2item(idx2))
					.findFirst().orElse(null);
			if (featureItem2Op == null) {
				return new Tuple2id(idx2, Double.NaN);
			}

			F featureItem2 = featureItem2Op.v1;

			int fidxfeatureItem2 = transitionMatrix.featureTofidx(featureItem2);
			double prob = transitionMatrix.getProbabilitySmoothing(fidxfeatureItem, fidxfeatureItem2, smoothF);
			if (prob == 0) {
				return new Tuple2id(idx2, Double.NaN);
			}
			return new Tuple2id(idx2, prob);
		});
	}

}
