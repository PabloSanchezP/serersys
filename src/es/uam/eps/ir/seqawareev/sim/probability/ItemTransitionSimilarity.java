/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.sim.probability;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.nn.sim.Similarity;
import es.uam.eps.ir.seqawareev.sim.AbstractTourCachedItemSimilarity;
import es.uam.eps.ir.seqawareev.smooth.SmoothingProbabilityIF;
import es.uam.eps.ir.seqawareev.structures.ItemTransitionMatrixIF;

/***
 * Similarity based on transition probabilities between items
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <I>
 */
public class ItemTransitionSimilarity<I> extends AbstractTourCachedItemSimilarity<I> implements Similarity {

	public ItemTransitionSimilarity(FastTemporalPreferenceDataIF<?, I> data, ItemTransitionMatrixIF transitionMatrix,
			SmoothingProbabilityIF smoothF, int cached, int numberItemsCompute) {
		super(data, new TransitionSimilarity(transitionMatrix, smoothF), cached, numberItemsCompute);
	}

}
