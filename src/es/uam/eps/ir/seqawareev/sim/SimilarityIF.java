/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.sim;

import java.util.Map;

/**
 * Similarity interface to store and save similarities.
 * 
 * All similarities (also the ones that work with RankSys should admit those
 * methods)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <T>
 */
public interface SimilarityIF<T> {
	/***
	 * Check if the similarity is symmetric or not
	 * 
	 * @return
	 */
	public boolean isSymmetric();

	/**
	 * Get the value of the similarity between the two elements
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public Double getSimilarity(T first, T second);

	/***
	 * Get all the similarities of an element
	 * 
	 * @param first
	 * @return
	 */
	public Map<T, Double> getSimilarities(T first);

	/**
	 * Method to obtain the similarities and store them in a file
	 * 
	 * @param path
	 */
	public void save(String path, Double confidence);

	/**
	 * Method to load similarities from a file. The similarities are only stored if
	 * the value of that similarity is higher of that confidence threshold
	 * 
	 * @param path
	 *            the path of the file
	 * @param confidence
	 *            the value that works as a bound to store similarities
	 */
	public void load(String path, double confidenceThreshold);

	/***
	 * Method to obtain the information associated to the similarity
	 * 
	 * @return
	 */
	public String info();

}
