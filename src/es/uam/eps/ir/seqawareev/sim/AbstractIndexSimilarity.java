/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.sim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.formats.parsing.Parser;

/***
 * Abstract similarity that implements methods of the similarity index interface
 * This class can work with both users and items
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public abstract class AbstractIndexSimilarity<T extends Comparable<T>> implements SimilarityIndexIF<T> {

	protected String fileName;

	// Map of similarities between elements. Its symmetric.
	protected Map<SymmetricSimilarityBean<T>, Tuple3<Double, Integer, Integer>> similarities;
	protected Set<T> totalElements; // total elements of the similarities (users or items)

	private int element1index = 0; // The indexes of the file can be changed using setters
	private int element2index = 1;
	private int simIndex = 2;
	private Parser<T> parser;

	public AbstractIndexSimilarity() {

	}

	public AbstractIndexSimilarity(String path, Double confidence, Parser<T> parser) {
		this.similarities = new HashMap<SymmetricSimilarityBean<T>, Tuple3<Double, Integer, Integer>>();
		this.totalElements = new LinkedHashSet<T>();
		this.parser = parser;
		this.fileName = new File(path).getName();

		load(path, confidence);
	}

	public AbstractIndexSimilarity(String path, Parser<T> parser) {
		this(path, Double.NaN, parser);
	}

	@Override
	public Double getSimilarity(T first, T second) {
		SymmetricSimilarityBean<T> s = new SymmetricSimilarityBean<T>(first, second);
		if (similarities.get(s) == null) {
			return Double.NaN;
		}

		return similarities.get(s).v1;
	}

	@Override
	public Map<T, Double> getSimilarities(T first) {
		return getEfficientSimilarities(first);
	}

	private Map<T, Double> getEfficientSimilarities(T first) {
		Map<T, Double> map = new HashMap<>();

		// This should speed up the computations...
		for (T second : totalElements) {
			SymmetricSimilarityBean<T> s = new SymmetricSimilarityBean<T>(first, second);
			if (second.equals(first) || similarities.get(s) == null) {
				continue;
			}

			Double sim = similarities.get(s).v1;
			if (s.getElement1().equals(first)) {
				map.put(s.getElement2(), sim);
			} else if (s.getElement2().equals(first)) {
				map.put(s.getElement1(), sim);
			}
		}
		return map;

	}

	@Override
	public void save(String path, Double confidence) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(path);

			for (SymmetricSimilarityBean<T> sim : similarities.keySet()) {
				if (confidence.isNaN() || similarities.get(sim).v1 > confidence)
					writer.println(sim.getElement1() + "\t" + sim.getElement2() + "\t" + similarities.get(sim).v1 + "\t"
							+ similarities.get(sim).v2 + "\t" + similarities.get(sim).v3);
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error in path: " + path);
		}
	}

	@Override
	public void load(String path, double confidenceThreshold) {
		/*
		 * Assumes that we have at least 3 columns. Column 0 -> first user Column 1 ->
		 * second user Column 2 -> similarity value in double. 4th and 5th column will
		 * be the last index of the users
		 */

		// line = br.readLine();
		try (Stream<String> stream = Files.lines(Paths.get(path))) {
			stream.forEach(line -> {
				if (!line.matches("[a-zA-Z].*")) { // Ignore lines THAT START with letters
					String[] data = line.split("\t"); // Userid, itemID, Rating,
														// timestamp
					T element1 = parser.parse(data[element1index]);
					T element2 = parser.parse(data[element2index]);
					totalElements.add(element1);
					totalElements.add(element2);
					Double sim = Double.parseDouble(data[simIndex]);
					int indexEl1 = -1;
					int indexEl2 = -1;
					if (data.length > 3) { // Has the indexes
						indexEl1 = Integer.parseInt(data[simIndex + 1]);
						indexEl2 = Integer.parseInt(data[simIndex + 2]);
					}

					SymmetricSimilarityBean<T> s = new SymmetricSimilarityBean<T>(element1, element2);
					if (Double.isNaN(confidenceThreshold) || sim > confidenceThreshold) { // If threshold is NaN or sim
																							// is higher than the
																							// threshold,then we store
																							// that similarity
						similarities.put(s, new Tuple3<Double, Integer, Integer>(sim, indexEl1, indexEl2));
					}
				}
			});
		} catch (FileNotFoundException e) {
			System.out.println("Error loading " + path + ". File not found");
		} catch (IOException e) {
			System.out.println("Error loading " + path + ". IOException");
		}
	}

	@Override
	public void loadIndexesFile(String path) {
		try (Stream<String> stream = Files.lines(Paths.get(path))) {
			stream.forEach(line -> {
				String[] data = line.split("\t"); // element1, element2, index1, index2

				T element1 = parser.parse(data[0]);
				T element2 = parser.parse(data[1]);
				SymmetricSimilarityBean<T> simBean = new SymmetricSimilarityBean<>(element1, element2);

				int indexEl1 = Integer.parseInt(data[2]);
				int indexEl2 = Integer.parseInt(data[3]);

				Tuple3<Double, Integer, Integer> tuple = similarities.get(simBean);
				similarities.put(simBean, new Tuple3<Double, Integer, Integer>(tuple.v1, indexEl1, indexEl2));
			});
		} catch (FileNotFoundException e) {
			System.out.println("Error loading " + path + ". File not found");
		} catch (IOException e) {
			System.out.println("Error loading " + path + ". IOException");
		}
	}

	@Override
	public Tuple3<Double, Integer, Integer> completeSimilarity(T first, T second) {
		return new Tuple3<Double, Integer, Integer>(similarities.get(new SymmetricSimilarityBean<>(first, second)));
	}

	@Override
	public boolean isSymmetric() {
		return true;
	}

	// Getters and setters
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getElement1index() {
		return element1index;
	}

	public void setElement1index(int element1index) {
		this.element1index = element1index;
	}

	public int getElement2index() {
		return element2index;
	}

	public void setElement2index(int element2index) {
		this.element2index = element2index;
	}

	public int getSimIndex() {
		return simIndex;
	}

	public void setSimIndex(int simIndex) {
		this.simIndex = simIndex;
	}

	public Parser<T> getParser() {
		return parser;
	}

	public void setParser(Parser<T> parser) {
		this.parser = parser;
	}

}
