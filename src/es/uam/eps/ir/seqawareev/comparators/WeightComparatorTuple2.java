/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.comparators;

import java.util.Comparator;

import org.jooq.lambda.tuple.Tuple2;

/***
 * Double Comparator. First by score then by index
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class WeightComparatorTuple2 implements Comparator<Tuple2<Integer, Double>> {

	@Override
	public int compare(Tuple2<Integer, Double> o1, Tuple2<Integer, Double> o2) {
		if (!o1.v2.equals(o2.v2)) {
			return o1.v2.compareTo(o2.v2);
		}
		return o1.v1.compareTo(o2.v1);
	}

}
