/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.comparators;

import java.util.Comparator;

import org.jooq.lambda.tuple.Tuple3;

/***
 * Weight comparator for tuples consisting on the index of the item and 2
 * scores. The order will be always by the first score. In case of tie, by the
 * second score and in case of tie of both scores, by the index (first element
 * of the tuple)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class WeightComparatorTuple3 implements Comparator<Tuple3<Integer, Double, Double>> {

	@Override
	public int compare(Tuple3<Integer, Double, Double> o1, Tuple3<Integer, Double, Double> o2) {
		if (!o1.v2.equals(o2.v2)) {
			return o1.v2.compareTo(o2.v2);
		}

		// First score is equal
		if (!o1.v3.equals(o2.v3)) {
			return o1.v3.compareTo(o2.v3);
		}

		// Second score is also equal
		return o1.v1.compareTo(o2.v1);
	}

}
