/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rec;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;

import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/***
 * Modified version of the user neighbourhood with a temporal decay from the
 * paper (page 32): -Time-aware recommender systems: a comprehensive survey and
 * analysis of existing evaluation protocols (2014)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@estudiante.uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class RankSysTemporalRecommender<U, I> extends FastRankingRecommender<U, I> {

	double lambda = 1.0 / 200.0;
	private final double t;
	private final double timeInterval;
	private final double softenMult;

	public RankSysTemporalRecommender(FastTemporalPreferenceDataIF<U, I> data, UserNeighborhood<U> neighborhood, int q,
			double lambda) {
		this(data, neighborhood, q, lambda, 24 * 3600, 2.0);
	}

	public RankSysTemporalRecommender(FastTemporalPreferenceDataIF<U, I> data, UserNeighborhood<U> neighborhood, int q,
			double lambda, double timeInterval, double softMultiplier) {
		super(data, data);
		this.data = data;
		this.neighborhood = neighborhood;
		this.q = q;
		this.lambda = lambda;
		t = 1.0 / lambda;
		this.timeInterval = timeInterval;
		this.softenMult = softMultiplier;
	}

	/**
	 * Preference data.
	 */
	protected final FastTemporalPreferenceDataIF<U, I> data;

	/**
	 * User neighborhood.
	 */
	protected final UserNeighborhood<U> neighborhood;

	/**
	 * Exponent of the similarity.
	 */
	protected final int q;

	@Override
	public Int2DoubleMap getScoresMap(int uidx) {

		Int2DoubleOpenHashMap scoresMap = new Int2DoubleOpenHashMap();
		scoresMap.defaultReturnValue(0.0);

		// We must obtain last timestampOrdered time of user u
		IdxTimePref lastPreferenceUser = data.getUidxTimePreferences(uidx)
				.sorted(PreferenceComparators.timeComparatorIdxTimePref.reversed()).findFirst().orElseGet(null);

		neighborhood.getNeighbors(uidx).forEach(vs -> {

			int neighbourIndex = vs.v1; // ranksysIndex
			double w = Math.pow(vs.v2, q);
			data.getUidxTimePreferences(vs.v1).forEach(iv -> {
				IdxTimePref lastPreferenceNeighbour = data.getUidxTimePreferences(neighbourIndex)
						.sorted(PreferenceComparators.timeComparatorIdxTimePref.reversed()).findFirst().orElseGet(null);

				Long diff = lastPreferenceUser.v3 - lastPreferenceNeighbour.v3;
				int ndays = (int) Math.round(diff / (this.timeInterval));

				// If the number of days is negative and the absolute is higher than the number
				// of t of the lambda * the Mult
				if (ndays < 0 && Math.abs(ndays) > (this.t * this.softenMult)) {
					ndays = -(int) (this.softenMult * this.t);
				}

				double p = w * iv.v2 * Math.pow(Math.E, -lambda * (ndays));
				scoresMap.addTo(iv.v1, p);
			});

		});

		return scoresMap;
	}

}
