/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rec;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdTimePref;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.fast.FastRecommendation;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.rec.fast.AbstractFastRecommender;
import es.uam.eps.ir.seqawareev.sim.SimilarityIndexIF;
import es.uam.eps.ir.seqawareev.utils.SequenceAwareAndRerankingUtils;

/***
 * BF recommender implementation (Revisiting Neighbourhood-Based Recommenders For Temporal Scenarios in Temporal Reasoning in Recommender Systems Workshop (RecSys2017))
 * using the Longest Common Subsequence (LCS) algorithm, although it can work with any similarity 
 * 
 * @author Pablo Sanchez (pablo.sanchezp@estudiante.uam.es)
 *
 */
public class UBLCSKNNForwardBackwardRecommender extends AbstractFastRecommender<Long, Long> {

	protected final static int DEFAULT_BACKWARDS = 3;
	protected final static int DEFAULT_FORWARDS = 3;

	protected int indexBackWards = DEFAULT_BACKWARDS;
	protected int indexForwards = DEFAULT_FORWARDS;

	protected boolean obtainBestIndexes = false;

	protected final FastTemporalPreferenceDataIF<Long, Long> data;
	protected final UserNeighborhood<Long> urneighborhood;

	protected final SimilarityIndexIF<Long> similarity;

	public UBLCSKNNForwardBackwardRecommender(FastTemporalPreferenceDataIF<Long, Long> data,
			UserNeighborhood<Long> neigh, SimilarityIndexIF<Long> sim) {
		super(data, data);
		urneighborhood = neigh;
		this.data = data;
		this.similarity = sim;
	}

	public UBLCSKNNForwardBackwardRecommender(FastTemporalPreferenceDataIF<Long, Long> data,
			UserNeighborhood<Long> neigh, int indexbackwards, int indexforwards, SimilarityIndexIF<Long> sim) {
		super(data, data);
		this.indexBackWards = indexbackwards;
		this.indexForwards = indexforwards;
		this.urneighborhood = neigh;
		this.data = data;
		this.similarity = sim;
	}

	private Tuple2<Double, List<Tuple2<Long, Double>>> listOfUserBackwardsTimeStamp(Long u1, Long neigh, int index,
			double sim, List<Long> timeStampOrdered, IntPredicate filter) {
		List<Tuple2<Long, Double>> result = new ArrayList<Tuple2<Long, Double>>();

		/*
		 * 1 - Limit has the counter of maximum items that can be recommended by that
		 * user 2 - As we move backwards, the index indicated the last index that
		 * matched with this user 3 - Index must be greater than zero so we don't obtain
		 * a negative index. Moreover, index must be lower than the list of users size
		 */
		for (int i = 0; i < indexBackWards && ((timeStampOrdered.size() - 1) >= (index - 1))
				&& ((index - 1) >= 0); i++) {
			Long idItem = timeStampOrdered.get(index - 1);
			// if u1 has not rated it
			if (filter.test(this.data.item2iidx(idItem))) {
				// Double ratingNeigh = this.data.getPreferences(neigh,
				// idItem).findFirst().get().v2;
				Double ratingNeigh = this.data.getUserPreferences(neigh).filter(pref -> pref.v1.equals(idItem))
						.findFirst().get().v2;
				result.add(new Tuple2<Long, Double>(idItem, ratingNeigh));
			} else {
				// If we have a match, we must decrement to retrieve limit items
				i--;
			}
			// check the next item
			index--;
		}
		return new Tuple2<Double, List<Tuple2<Long, Double>>>(sim, result);
	}

	private Tuple2<Double, List<Tuple2<Long, Double>>> listOfUserForwardsTimeStamp(Long u1, Long neigh, int index,
			double sim, List<Long> timeStampOrdered, IntPredicate filter) {

		List<Tuple2<Long, Double>> result = new ArrayList<Tuple2<Long, Double>>();

		/*
		 * 1 - Limit has the counter of maximum items that can be recommended by that
		 * user 2 - As we move forward, the index indicated the last index that matched
		 * with this user 3 - Index must be greater than zero so we don't obtain a
		 * negative index. Moreover, index must be lower than the list of users size
		 */
		for (int i = 0; i < indexForwards && ((timeStampOrdered.size() - 1) >= (index + 1))
				&& ((index + 1) >= 0); i++) {
			Long idItem = timeStampOrdered.get(index + 1);
			// if u1 has not rated it
			if (filter.test(this.data.item2iidx(idItem))) {
				Double ratingNeigh = this.data.getUserPreferences(neigh).filter(pref -> pref.v1.equals(idItem))
						.findFirst().get().v2;
				result.add(new Tuple2<Long, Double>(idItem, ratingNeigh));
			} else {
				// If we have a match, we must decrement to retrieve limit items
				i--;
			}
			// check the next item
			index++;
		}
		return new Tuple2<Double, List<Tuple2<Long, Double>>>(sim, result);
	}

	public boolean isObtainBestIndexes() {
		return obtainBestIndexes;
	}

	public void setObtainBestIndexes(boolean obtainBestIndexes) {
		this.obtainBestIndexes = obtainBestIndexes;
	}

	@Override
	public String toString() {
		String s = "BKRecommender: indexesBackward" + indexBackWards + " indexeForward" + indexForwards + "similarity "
				+ similarity;
		return s;
	}

	@Override
	public FastRecommendation getRecommendation(int uidx, int maxLength, IntPredicate filter) {
		if (uidx == -1) {
			return new FastRecommendation(uidx, new ArrayList<>(0));
		}

		List<Tuple2id> items = new ArrayList<>();

		Long user = data.uidx2user(uidx);

		List<Tuple2<Double, List<Tuple2<Long, Double>>>> lst = new ArrayList<Tuple2<Double, List<Tuple2<Long, Double>>>>();

		// new
		// ArrayList<Long>(LCS4RecSysUtils.sortByValue(this.dataModel.getUsers().get(user).getItemsTimeStamp(),false).keySet());
		final List<Long> timeStampOrderedU1 = data.getUserPreferences(user)
				.sorted(PreferenceComparators.timeComparatorIdTimePref).map(IdTimePref<Long>::v1)
				.collect(Collectors.toList());

		// System.out.println("recommendation for " + user);
		urneighborhood.getNeighbors(user).forEach(n -> {
			Long neigh = n.v1;
			Tuple3<Double, Integer, Integer> t3d = similarity.completeSimilarity(user, neigh);

			List<Long> timeStampOrdered = data.getUserPreferences(neigh)
					.sorted(PreferenceComparators.timeComparatorIdTimePref).map(IdTimePref<Long>::v1)
					.collect(Collectors.toList());

			Tuple3<Double, Integer, Integer> copy;
			// check indices
			Integer newv2 = t3d.v2;
			Integer newv3 = t3d.v3;

			if (obtainBestIndexes || ((t3d.v2 == -1) || (t3d.v3 == -1))) {
				Tuple2<Integer, Integer> t = null;
				t = SequenceAwareAndRerankingUtils.obtainBestIndexesLCSEquivalent(timeStampOrderedU1, timeStampOrdered);
				copy = new Tuple3<Double, Integer, Integer>(t3d.v1, t.v1, t.v2);

				// If the indexes were -1 then update them
				if (((newv2 == -1) || (newv3 == -1))) {
					if (user.compareTo(neigh) < 0) {
						newv2 = t.v1;
						newv3 = t.v2;
					} else {
						newv2 = t.v2;
						newv3 = t.v1;
					}
				}
			}
			copy = new Tuple3<Double, Integer, Integer>(t3d.v1, newv2, newv3);

			if (user.compareTo(neigh) < 0) {
				lst.add(listOfUserBackwardsTimeStamp(user, neigh, copy.v3, copy.v1, timeStampOrdered, filter));
				lst.add(listOfUserForwardsTimeStamp(user, neigh, copy.v3, copy.v1, timeStampOrdered, filter));
			} else {
				lst.add(listOfUserBackwardsTimeStamp(user, neigh, copy.v2, copy.v1, timeStampOrdered, filter));
				lst.add(listOfUserForwardsTimeStamp(user, neigh, copy.v2, copy.v1, timeStampOrdered, filter));
			}

		});

		Map<Long, Double> m = SequenceAwareAndRerankingUtils.obtainMapOrdered(lst);
		for (Long i : m.keySet()) {
			if (filter.test(data.item2iidx(i))) {
				items.add(new Tuple2id(data.item2iidx(i), m.get(i)));
			}
		}
		// It should be ordered
		Collections.sort(items, PreferenceComparators.recommendationComparatorTuple2id.reversed());
		// Avoid IndexOutOfBoundException
		items = items.subList(0, (maxLength > items.size()) ? items.size() : maxLength);
		return new FastRecommendation(uidx, items);
	}

}
