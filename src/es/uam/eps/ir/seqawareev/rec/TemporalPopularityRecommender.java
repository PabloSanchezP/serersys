/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rec;

import org.jooq.lambda.tuple.Tuple2;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import es.uam.eps.ir.seqawareev.utils.SequenceAwareAndRerankingUtils;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/***
 * Popularity recommender with a temporal component so that the preferences
 * closer to the last timestamp of the system have more importance
 * 
 * @author Pablo Sanchez (pablo.sanchezp@estudiante.uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class TemporalPopularityRecommender<U, I> extends FastRankingRecommender<U, I> {
	private final Int2DoubleOpenHashMap genericMap;
	private final double maxTimeStamp;
	private final double minTimeStamp;

	public TemporalPopularityRecommender(FastTemporalPreferenceDataIF<U, I> dataTrain) {
		super(dataTrain, dataTrain);

		Tuple2<Long, Long> minMaxTime = SequenceAwareAndRerankingUtils.getMinMaxTimestampOfDataset(dataTrain);
		this.minTimeStamp = minMaxTime.v1;
		this.maxTimeStamp = minMaxTime.v2;

		genericMap = new Int2DoubleOpenHashMap();
		genericMap.defaultReturnValue(0.0);

		dataTrain.getIidxWithPreferences().forEach(iidx -> {
			dataTrain.getIidxTimePreferences(iidx).forEach(iidxTimePref -> {
				double weight = (iidxTimePref.v3 - this.minTimeStamp) / (this.maxTimeStamp - this.minTimeStamp);
				genericMap.put(iidx, genericMap.get(iidx) + weight);
			});
		});
	}

	@Override
	public Int2DoubleMap getScoresMap(int uidx) {
		return genericMap;
	}
}
