/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rec;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdTimePref;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils;
import es.uam.eps.ir.ranksys.fast.FastRecommendation;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.seqawareev.sim.SimilarityIndexIF;
import es.uam.eps.ir.seqawareev.utils.SequenceAwareAndRerankingUtils;
import es.uam.eps.nets.rankfusion.GenericRankAggregator;
import es.uam.eps.nets.rankfusion.GenericResource;
import es.uam.eps.nets.rankfusion.GenericSearchResults;
import es.uam.eps.nets.rankfusion.interfaces.IFCombiner;
import es.uam.eps.nets.rankfusion.interfaces.IFNormalizer;
import es.uam.eps.nets.rankfusion.interfaces.IFRankAggregator;
import es.uam.eps.nets.rankfusion.interfaces.IFResource;
import es.uam.eps.nets.rankfusion.interfaces.IFSearchResults;

/***
 * Implementation of UBLCSKNNForwardBackwardRecommender but using the ranking fusion library
 * 
 * @author Pablo Sanchez (pablo.sanchezp@estudiante.uam.es)
 *
 */
public class UBLCSKNNForwardBackwardRFRecommender extends UBLCSKNNForwardBackwardRecommender {

	private boolean weightNeighboursBySimilarity;

	private IFRankAggregator rankAggregator;
	boolean debug = true;

	public UBLCSKNNForwardBackwardRFRecommender(FastTemporalPreferenceDataIF<Long, Long> data,
			UserNeighborhood<Long> neigh, int indexbackwards, int indexforwards, SimilarityIndexIF<Long> sim,
			String normalizer, String combiner, boolean weightNeighboursBySimilarity) {
		super(data, neigh, indexbackwards, indexforwards, sim);

		this.weightNeighboursBySimilarity = weightNeighboursBySimilarity;

		IFNormalizer norm = SequentialRecommendersUtils.getNormalizer(normalizer, null, null);
		IFCombiner comb = SequentialRecommendersUtils.getCombiner(combiner);
		rankAggregator = new GenericRankAggregator(norm, comb);
		System.out.println("Not recommending the same item more tan once for BF");
	}

	//
	public UBLCSKNNForwardBackwardRFRecommender(FastTemporalPreferenceDataIF<Long, Long> data,
			UserNeighborhood<Long> neigh, SimilarityIndexIF<Long> sim) {
		this(data, neigh, DEFAULT_BACKWARDS, DEFAULT_FORWARDS, sim, "defaultnorm", "defaultcomb", true);
	}

	@Override
	public FastRecommendation getRecommendation(int uidx, int maxLength, IntPredicate filter) {
		if (uidx == -1) {
			return new FastRecommendation(uidx, new ArrayList<>(0));
		}

		List<Tuple2id> items = new ArrayList<>();

		Long user = data.uidx2user(uidx);
		final List<Long> timeStampOrderedU1 = data.getUserPreferences(user)
				.sorted(PreferenceComparators.timeComparatorIdTimePref).map(IdTimePref<Long>::v1)
				.collect(Collectors.toList());

		List<IFSearchResults> neighbourList = new ArrayList<IFSearchResults>();

		// System.out.println("recommendation for " + user);
		urneighborhood.getNeighbors(user).forEach(n -> {
			Long neigh = n.v1;
			Tuple3<Double, Integer, Integer> t3d = similarity.completeSimilarity(user, neigh);

			List<Long> timeStampOrdered = data.getUserPreferences(neigh)
					.sorted(PreferenceComparators.timeComparatorIdTimePref).map(IdTimePref<Long>::v1)
					.collect(Collectors.toList());

			Tuple3<Double, Integer, Integer> copy;
			// check indices

			Integer newv2 = t3d.v2;
			Integer newv3 = t3d.v3;
			if (obtainBestIndexes || ((t3d.v2 == -1) || (t3d.v3 == -1))) {
				Tuple2<Integer, Integer> t = null;
				t = SequenceAwareAndRerankingUtils.obtainBestIndexesLCSEquivalent(timeStampOrderedU1, timeStampOrdered);
				copy = new Tuple3<Double, Integer, Integer>(t3d.v1, t.v1, t.v2);

				// If the indexes were -1 then update them
				// If the indexes were -1 then update them
				if (((newv2 == -1) || (newv3 == -1))) {
					if (debug) {
						System.out.println("Similarities with indexes negatives. Computed the indexes and updated");
						debug = false;
					}
					if (user.compareTo(neigh) < 0) {
						newv2 = t.v1;
						newv3 = t.v2;
					} else {
						newv2 = t.v2;
						newv3 = t.v1;
					}
				}
				if (debug) {
					System.out.println("Similarities with real indexes. Not updated idexes");
					debug = false;
				}
			}
			copy = new Tuple3<Double, Integer, Integer>(t3d.v1, newv2, newv3);
			/*
			 * Map<Long, IFResource> neighbourRanking = new HashMap<>();
			 * 
			 * if (user.compareTo(neigh) < 0) { userForward(neighbourRanking, user, neigh,
			 * copy.v3, copy.v1,timeStampOrdered,filter); userBackwards(neighbourRanking,
			 * user, neigh, copy.v3, copy.v1,timeStampOrdered,filter); } else {
			 * userForward(neighbourRanking, user, neigh, copy.v2,
			 * copy.v1,timeStampOrdered,filter); userBackwards(neighbourRanking, user,
			 * neigh, copy.v2, copy.v1,timeStampOrdered,filter); }
			 */
			List<Long> iF = null;
			List<Long> iB = null;
			if (user.compareTo(neigh) < 0) {
				iF = userForward(user, neigh, copy.v3, copy.v1, timeStampOrdered, filter);
				iB = userBackwards(user, neigh, copy.v3, copy.v1, timeStampOrdered, filter);
			} else {
				iF = userForward(user, neigh, copy.v2, copy.v1, timeStampOrdered, filter);
				iB = userBackwards(user, neigh, copy.v2, copy.v1, timeStampOrdered, filter);
			}
			Map<Long, IFResource> neighbourRanking = new HashMap<>();

			int counter = 0;
			int rank = 0;
			while (true) {
				if (iF.size() > counter) {
					Long IdItemL = iF.get(counter);
					String idItem = IdItemL.toString();
					neighbourRanking
							.put(new Long(idItem.hashCode()),
									generateScore(
											idItem, rank, this.data.getUserPreferences(neigh)
													.filter(pref -> pref.v1.equals(IdItemL)).findFirst().get().v2,
											copy.v1));
					rank++;
				}

				if (iB.size() > counter) {
					Long IdItemL = iB.get(counter);
					String idItem = IdItemL.toString();
					neighbourRanking
							.put(new Long(idItem.hashCode()),
									generateScore(
											idItem, rank, this.data.getUserPreferences(neigh)
													.filter(pref -> pref.v1.equals(IdItemL)).findFirst().get().v2,
											copy.v1));
					rank++;
				}
				counter++;

				if (iF.size() <= counter && iB.size() <= counter) {
					break;
				}
			}

			double neighbourWeight = (weightNeighboursBySimilarity ? copy.v1 : 1.0);
			IFSearchResults neighbourResults = new GenericSearchResults(neighbourRanking, neighbourWeight);
			neighbourResults.setNotRetrievedNormalizedValue(0.0);
			neighbourList.add(neighbourResults);

		});

		if (rankAggregator != null && neighbourList != null && neighbourList.size() != 0) {
			List<IFResource> aggResults = rankAggregator.aggregate(neighbourList)
					.getSortedResourceList(GenericSearchResults.I_RESOURCE_ORDER_COMBINED_VALUE);
			for (IFResource aggResult : aggResults) {
				// System.out.println(aggResult.getId() + " - " + aggResult.getCombinedValue());
				Long i = Long.parseLong(aggResult.getId());
				if (filter.test(data.item2iidx(i))) {
					items.add(new Tuple2id(data.item2iidx(i), aggResult.getCombinedValue()));
				}
			}
		}

		// It should be ordered
		Collections.sort(items, PreferenceComparators.recommendationComparatorTuple2id.reversed());
		// Avoid IndexOutOfBoundException
		items = items.subList(0, (maxLength > items.size()) ? items.size() : maxLength);
		return new FastRecommendation(uidx, items);

	}

	private List<Long> userBackwards(Long u1, Long neigh, int index, double sim, List<Long> timeStampOrdered,
			IntPredicate filter) {

		List<Long> result = new ArrayList<>();
		/*
		 * 1 - Limit has the counter of maximum items that can be recommended by that
		 * user 2 - As we move backwards, the index indicated the last index that
		 * matched with this user 3 - Index must be greater than zero so we don't obtain
		 * a negative index. Moreover, index must be lower than the list of users size
		 */
		for (int i = 0; i < indexBackWards && ((timeStampOrdered.size() - 1) >= (index - 1))
				&& ((index - 1) >= 0); i++) {
			Long idItem = timeStampOrdered.get(index - 1);

			// if u1 has not rated it
			if (filter.test(this.data.item2iidx(idItem)) && !result.contains(idItem)) {
				// result.add(new Tuple2D<Long, Double>(idItem, ratingNeigh));
				result.add(idItem);
			} else {
				// If we have a match, we must decrement to retrieve limit items
				i--;
			}
			// check the next item
			index--;
		}
		return result;
	}

	private List<Long> userForward(Long u1, Long neigh, int index, double sim, List<Long> timeStampOrdered,
			IntPredicate filter) {
		List<Long> result = new ArrayList<>();

		/*
		 * 1 - Limit has the counter of maximum items that can be recommended by that
		 * user 2 - As we move forward, the index indicated the last index that matched
		 * with this user 3 - Index must be greater than zero so we don't obtain a
		 * negative index. Moreover, index must be lower than the list of users size
		 */
		for (int i = 0; i < indexForwards && ((timeStampOrdered.size() - 1) >= (index + 1))
				&& ((index + 1) >= 0); i++) {
			Long idItem = timeStampOrdered.get(index + 1);

			// if u1 has not rated it

			if (filter.test(this.data.item2iidx(idItem)) && !result.contains(idItem)) {
				// Double ratingNeigh = this.data.getPreferences(neigh,
				// idItem).findFirst().get().v2;
				// result.add(new Tuple2D<Long, Double>(idItem, ratingNeigh));
				result.add(idItem);

			} else {
				// If we have a match, we must decrement to retrieve limit items
				i--;
			}
			// check the next item
			index++;
		}
		return result;
	}

	private IFResource generateScore(String id, int rankPosition, double rating, double sim) {
		return new GenericResource(id, rating, rankPosition);
	}

	@Override
	public boolean isObtainBestIndexes() {
		return obtainBestIndexes;
	}

	@Override
	public void setObtainBestIndexes(boolean obtainBestIndexes) {
		this.obtainBestIndexes = obtainBestIndexes;
	}

	@Override
	public String toString() {
		String s = "BKRecommender: indexesBackward" + indexBackWards + " indexeForward" + indexForwards + "similarity "
				+ similarity;
		return s;
	}

}
