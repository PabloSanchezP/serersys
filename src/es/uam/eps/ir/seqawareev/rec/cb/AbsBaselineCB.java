/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rec.cb;

import java.util.Map;

import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import es.uam.eps.ir.seqawareev.utils.CBBaselinesUtils;

/***
 * Abstract content-based recommender. Stores the different mappings of item and
 * users-features
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 */
public abstract class AbsBaselineCB<U, I, F> extends FastRankingRecommender<U, I> {

	protected final FeatureData<I, F, Double> featureData;

	// First long item -> Map of features
	protected Map<I, Map<F, Double>> itemFeatures;
	// First long user -> Map of features
	protected Map<U, Map<F, Double>> userFeatures;

	// Map containing the number of items in each feature
	protected Map<F, Double> featuresItems;

	protected CBBaselinesUtils.USERTRANSFORMATION ut;
	protected CBBaselinesUtils.ITEMTRANSFORMATION it;
	protected boolean normalize;

	public AbsBaselineCB(FastUserIndex<U> uIndex, FastItemIndex<I> iIndex, FeatureData<I, F, Double> featureData,
			boolean normalize) {
		super(uIndex, iIndex);
		this.featureData = featureData;
		this.normalize = normalize;

		this.itemFeatures = null;
		this.userFeatures = null;

		ut = CBBaselinesUtils.USERTRANSFORMATION.WITHRATING;
		it = CBBaselinesUtils.ITEMTRANSFORMATION.BINARY;
	}

	protected abstract double computeScore(U user, I item);

	public CBBaselinesUtils.USERTRANSFORMATION getUt() {
		return ut;
	}

	public void setUt(CBBaselinesUtils.USERTRANSFORMATION ut) {
		this.ut = ut;
	}

	public CBBaselinesUtils.ITEMTRANSFORMATION getIt() {
		return it;
	}

	public void setIt(CBBaselinesUtils.ITEMTRANSFORMATION it) {
		this.it = it;
	}

	public Map<I, Map<F, Double>> getItemsFeatures() {
		return itemFeatures;
	}

	public void setItemsFeatures(Map<I, Map<F, Double>> itemsFeatures) {
		this.itemFeatures = itemsFeatures;
	}

	public Map<U, Map<F, Double>> getUserFeatures() {
		return userFeatures;
	}

	public void setUserFeatures(Map<U, Map<F, Double>> userFeatures) {
		this.userFeatures = userFeatures;
	}
}
