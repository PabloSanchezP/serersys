/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.rec.cb;

import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.seqawareev.utils.CBBaselinesUtils;

/***
 * Content based recommender build a user profile and a item profile based on
 * the users profiles.
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 * @param <F>
 * @param <V>
 */
public class UserItemProfileContentRecommender<U, I, F> extends AbsBaselineCBPreferenceData<U, I, F> {

	public UserItemProfileContentRecommender(FastPreferenceData<U, I> prefData, FeatureData<I, F, Double> featData,
			boolean normalized) {
		super(prefData, featData, normalized);
		trainFeaturesItemsByUsers();
	}

	public void trainFeaturesItemsByUsers() {
		this.userFeatures = CBBaselinesUtils.getUsersCBTransformation(dataModel, featureData, ut, normalize);
		this.itemFeatures = CBBaselinesUtils.itemTransformationByUsers(dataModel, featureData, userFeatures, normalize);

	}

	@Override
	protected double computeScore(U user, I item) {
		return CBBaselinesUtils.cosineCBFeatureSimilarity(this.userFeatures.get(user), this.itemFeatures.get(item));
	}

}
