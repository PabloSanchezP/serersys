/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.main;

import static org.ranksys.formats.parsing.Parsers.lp;
import static org.ranksys.formats.parsing.Parsers.sp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.formats.feature.SimpleFeaturesReader;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;
import org.ranksys.formats.rec.RecommendationFormat;
import org.ranksys.formats.rec.SimpleRecommendationFormat;

import es.uam.eps.ir.antimetrics.utils.AverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.metrics.system.RealAggregateDiversity;
import es.uam.eps.ir.crossdomainPOI.metrics.system.UserCoverage;
import es.uam.eps.ir.crossdomainPOI.utils.FoursqrProcessData;
import es.uam.eps.ir.crossdomainPOI.utils.PredicatesStrategies;
import es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils;
import es.uam.eps.ir.crossdomainPOI.utils.UsersMidPoints.SCORES_FREQUENCY;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.feature.SimpleFeatureData;
import es.uam.eps.ir.ranksys.core.preference.ConcatPreferenceData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;
import es.uam.eps.ir.ranksys.diversity.intentaware.FeatureIntentModel;
import es.uam.eps.ir.ranksys.diversity.intentaware.IntentModel;
import es.uam.eps.ir.ranksys.diversity.sales.metrics.AggregateDiversityMetric;
import es.uam.eps.ir.ranksys.diversity.sales.metrics.GiniIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;
import es.uam.eps.ir.ranksys.metrics.basic.AverageRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.rank.NoDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.BinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.NoRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.novdiv.distance.CosineFeatureItemDistanceModel;
import es.uam.eps.ir.ranksys.novdiv.distance.ItemDistanceModel;
import es.uam.eps.ir.ranksys.rec.Recommender;
import es.uam.eps.ir.seqawareev.rec.UBLCSKNNForwardBackwardRFRecommender;
import es.uam.eps.ir.seqawareev.rerankers.LambdaRerankerRelevance.FILL_RERANKING_STRATEGY;
import es.uam.eps.ir.seqawareev.sim.SymmetricSimilarityBean;
import es.uam.eps.ir.seqawareev.sim.UserIndexReadingSimilarity;
import es.uam.eps.ir.seqawareev.sim.UserIndexReadingSimilarityRankSysWrapper;
import es.uam.eps.ir.seqawareev.smooth.SmoothingProbabilityIF;
import es.uam.eps.ir.seqawareev.tour.abstracts.AbstractFastTourRecommender;
import es.uam.eps.ir.seqawareev.utils.CBBaselinesUtils.USERTRANSFORMATION;
import es.uam.eps.ir.seqawareev.utils.ProcessSessionData;
import es.uam.eps.ir.seqawareev.utils.SemanticTrailsPOIProcessData;
import es.uam.eps.ir.seqawareev.utils.SequenceAwareAndRerankingUtils;
import es.uam.eps.ir.seqawareev.utils.UsersSessions;
import es.uam.eps.ir.seqawareev.wrappers.SimpleFastTemporalFeaturePreferenceDataWrapper;

public class Experiment {
	/**********************************/
	/** All options of the arguments **/
	/**********************************/

	/** Option of the case **/
	private static final String OPT_CASE = "option";

	/** Train and test files **/
	private static final String OPT_TRAIN_FILE = "train file";
	private static final String OPT_TRAIN_FILE_2 = "train file 2";
	private static final String OPT_TEST_FILE = "test file";

	/** For recommendation (general recommendation) **/
	// Some parameters
	private static final String OPT_ITEMS_RECOMMENDED = "items recommended";
	private static final String OPT_RECOMMENDATION_STRATEGY = "recommendation strategy";
	private static final String OPT_NEIGH = "neighbours";
	private static final String OPT_COMPLETE_INDEXES = "complete indexes";
	private static final String OPT_RANKSYS_SIM = "ranksys similarity";
	private static final String OPT_SIMFILE = "similarity file";
	private static final String OPT_RANKSYS_REC = "ranksysRecommender";
	private static final String OPT_RANKSYS_REC2 = "ranksysRecommender2";

	private static final String OPT_CONFIDENCE = "confidencesimilarity";
	private static final String OPT_LAMBDA = "lambda variable";
	private static final String OPT_LAMBDA_2 = "lambda variable (2)";

	// Indexes of BF recommender
	private static final String OPT_INDEX_BACKWARDS = "index backwards";
	private static final String OPT_INDEX_FORWARDS = "index forwards";
	private static final String OPT_IN_INDEX_FILE = "input indexes file";

	// Parameters for Ranksys Recommender MatrixFactorization
	private static final String OPT_K_FACTORIZER = "k factorizer value";
	private static final String OPT_ALPHA_FACTORIZER = "alpha factorizer value";
	private static final String OPT_LAMBDA_FACTORIZER = "lambda factorizer value";
	private static final String OPT_NUM_INTERACTIONS = "num interactions value";

	// Feature to use in cb similarity
	private static final String OPT_NORMALIZE = "normalize";
	private static final String OPT_TEMPORAL_LAMBDA = "temporal lambda";

	// Parameters of Aggregation library
	private static final String OPT_NORM_AGGREGATE_LIBRARY = "norm of aggregate library";
	private static final String OPT_COMB_AGGREGATE_LIBRARY = "comb of aggregate library";
	private static final String OPT_WEIGHT_AGGREGATE_LIBRARY = "weight of aggregate library";

	// Parameters for RankGeoFm
	private static final String OPT_SVD_REG_BIAS = "regularization for biases for SVD";
	private static final String OPT_SVD_LEARN_RATE = "learning rate for SVD";
	private static final String OPT_SVD_MAX_LEARN_RATE = "maximum learning rate for SVD";
	private static final String OPT_SVD_DECAY = "decay for SVD";
	private static final String OPT_SVD_IS_BOLD_DRIVER = "bold river for SVD";
	private static final String OPT_SVD_REG_IMP_ITEM = "regularization for implicit items for SVD";

	// Parameters for RankGeoFM
	private static final String OPT_EPSILON = "epsilon";
	private static final String OPT_C = "c";

	// For caching Items (tour recommenders)
	private static final String OPT_CACHED_ITEMS = "cachedItems";
	private static final String OPT_NUM_ITEMS_COMPUTE = "number of items compute";

	// POI and tour recommenders

	// Reranker
	private static final String OPT_RERANKERS = "re-rankers";
	private static final String OPT_PROB_SMOOTH = "probability smoothing";
	private static final String OPT_PATTERN_LENGHT = "length of the pattern to search";
	private static final String OPT_FILL_STRATEGY = "opt filling strategy";

	/** For generating result files **/
	private static final String OPT_OUT_RESULT_FILE = "output result file";
	private static final String OPT_OUT_RESULT_FILE_2 = "output result file 2";
	private static final String OPT_OVERWRITE = "output result file overwrite";
	private static final String OPT_OUT_SIM_FILE = "output similarity file";

	/** Feature variables (content based support and so on) **/
	private static final String OPT_SAVING_FEATURES_SCHEME = "savingFeaturesScheme";
	private static final String OPT_FEATURE_READING_FILE = "featureFile";
	private static final String OPT_FEATURE_READING_FILE2 = "featureFile2";
	private static final String OPT_CB_USER_TRANSFORMATION = "cb user transformation";

	/** Variables only used in evaluation **/
	private static final String OPT_RECOMMENDED_FILE = "recommendedFile";
	private static final String OPT_RECOMMENDED_FILES = "recommendedFiles";
	private static final String OPT_CUTOFF = "ranksysCutoff";
	private static final String OPT_MIN_ITEMS = "minRatingItems";
	private static final String OPT_MIN_USERS = "minRatingUsers";
	private static final String OPT_THRESHOLD = "relevance or matching threshold";

	// Parameters for RankSys non accuracy evaluation
	private static final String OPT_RANKSYS_RELEVANCE_MODEL = "ranksys relevance model";
	private static final String OPT_RANKSYS_DISCOUNT_MODEL = "ranksys discount model";
	private static final String OPT_RANKSYS_BACKGROUND = "ranksys background for relevance model";
	private static final String OPT_RANKSYS_BASE = "ranksys base for discount model";

	// For evaluation of non accuracy metrics, distance
	private static final String OPT_COMPUTE_ONLY_ACC = "compute also dividing by the number of users in the test set";

	private static final String OPT_SCORE_FREQ = "use simple score or the frequency for the AvgDis";
	private static final String OPT_LCS_EVALUATION = "LCS evaluation for taking into ccount the temporal order of the items";
	private static final String OPT_COMP_DISTANCES = "compute distances ev";

	/** Generate and filter new datasets **/

	// Mappings
	private static final String OPT_MAPPING_ITEMS = "file of mapping items";
	private static final String OPT_MAPPING_USERS = "file of mapping users";
	private static final String OPT_MAPPING_CATEGORIES = "file of mapping categories";

	// Generate datasets
	private static final String OPT_NEW_DATASET = "file for the new dataset";
	private static final String OPT_COORD_FILE = "poi coordinate file";
	private static final String OPT_WRAPPER_STRATEGY = "wrapper strategy for ranksys (removing timestamps)";
	private static final String OPT_WRAPPER_STRATEGY_TIMESTAMPS = "wrapper strategy for ranksys (strategy for the timestamps)";
	private static final String OPT_USING_WRAPPER = "using wrapper in ranksys recommenders";

	// Working with sessions
	private static final String OPT_MIN_SESSIONS = "min number of sessions";
	private static final String OPT_LIMIT_BETWEEN_ITEMS = "Limit condition between the items to be considered them as a session";
	private static final String OPT_SESSION_TYPE = "Type of session (distance, time etc)";
	private static final String OPT_MIN_DIFF_TIME = "min diff time";
	private static final String OPT_MAX_DIFF_TIME = "Max difference between timestamps";
	private static final String OPT_MIN_DIFF_TIME_2 = "min diff time2";
	private static final String OPT_MIN_CLOSE_PREF_BOT = "min close pref bot";

	/***********************/
	/** Some default values **/
	/***********************/

	/** For Recommendation **/
	// General
	private static final String DEFAULT_COMPDISTANCES = "false";
	private static final String DEFAULT_NEIGH = "100";
	private static final String DEFAULT_NORMALIZE = "true";
	private static final String DEFAULT_OPT_PATTERN_LENGHT = "2";

	// Some default values from MFs
	private static final String DEFAULT_FACTORS = "20";
	private static final String DEFAULT_ITERACTIONS = "20";

	// Some default values for HKV
	private static final String DEFAULT_ALPHA_HKV = "1";
	private static final String DEFAULT_LAMBDA_HKV = "0.1";

	// SVD parameters from librec:
	// https://github.com/guoguibing/librec/blob/3ab54c4432ad573678e7b082f05785ed1ff31c5c/core/src/main/java/net/librec/recommender/MatrixFactorizationRecommender.java
	private static final String DEFAULT_SVD_LEARNING_RATE = "0.01f";
	private static final String DEFAULT_SVD_MAX_LEARNING_RATE = "1000f";
	private static final String DEFAULT_SVD_REG_BIAS = "0.01f";
	private static final String DEFAULT_IS_BOLD_DRIVER = "false";
	private static final String DEFAULT_DECAY = "1f";
	private static final String DEFAULT_REG_IMP_ITEM = "0.025f";

	// Some values for rank geoFM from Librec
	private static final String DEFAULT_EPSILON = "0.3";
	private static final String DEFAULT_C = "1.0";
	// Some default values for recommenders
	private static final String DEFAULT_CACHED_ITEMS = "3";
	private static final String DEFAULT_NUM_ITEMS_COMPUTE = "200";
	private static final String DEFAULT_COMPLETE_INDEXES = "false";
	private static final String DEFAULT_SCORE = "SIMPLE";
	private static final String DEFAULT_CB_USER_TRANSFORMATION = "WITHRATING";

	// Generate evaluation
	private static final String DEFAULT_ITEMS_RECOMMENDED = "100";
	private static final String DEFAULT_RECOMMENDATION_STRATEGY = "TRAIN_ITEMS";
	private static final String DEFAULT_LCS_CONFIDENCE = "0";
	private static final String DEFAULT_OVERWRITE = "false";
	private static final String DEFAULT_OPT_COMPUTE_ONLY_ACC = "true";

	private static final String DEFAULT_LCS_EVALUTATION = "false";

	public static void main(String[] args) throws Exception {
		String step = "";
		CommandLine cl = getCommandLine(args);
		if (cl == null) {
			System.out.println("Error in arguments");
			return;
		}

		// Obtain the arguments these 2 are obligatory
		step = cl.getOptionValue(OPT_CASE);
		String trainFile = cl.getOptionValue(OPT_TRAIN_FILE);

		System.out.println(step);
		switch (step) {

		case "FoursqrGenerateCategoryLevel": {
			System.out.println(
					"-o FoursqrGenerateCategoryLevel -trf poi_file category_tree [levels_to_be_considered] [output_files_for_each_level]");
			System.out.println(Arrays.toString(args));

			String poiFile = trainFile;
			String categoriesFromFrsqFile = args[4];
			String levelsString = args[5];
			String outputFiles = args[6];
			String[] levels = levelsString.split(",");
			String[] files = outputFiles.split(",");
			if (levels.length != files.length) {
				System.out.println("Not matching arguments: " + levelsString + " <--> " + outputFiles);
				return;
			}
			final Map<String, String[]> categoriesWithLevels = ProcessSessionData
					.readFoursquareCategories(categoriesFromFrsqFile);

			final Map<Integer, PrintStream> outMap = new HashMap<>();
			IntStream.range(0, levels.length).forEach(i -> {
				try {
					outMap.put(Integer.parseInt(levels[i]), new PrintStream(files[i]));
				} catch (FileNotFoundException ex) {
					Logger.getLogger(Experiment.class.getName()).log(Level.SEVERE, null, ex);
				}
			});
			//
			Stream<String> stream = Files.lines(Paths.get(poiFile), StandardCharsets.ISO_8859_1);
			stream.forEach(line -> {
				String[] data = line.split("\t");
				String poiCategory = data[3];
				outMap.keySet().forEach(k -> {
					String parsedCategory = ProcessSessionData.matchingFoursquareCheckinCategory(poiCategory, k,
							categoriesWithLevels);
					if (parsedCategory != null) {
						outMap.get(k).println(
								data[0] + "\t" + data[1] + "\t" + data[2] + "\t" + parsedCategory + "\t" + data[4]);
					} else {
						System.out.println("Category " + poiCategory + " not found!");
					}
				});
			});
			stream.close();
			//
			outMap.keySet().forEach(k -> outMap.get(k).close());
		}
			break;

		case "FoursqrCheckinsPerCity": {
			System.out.println("-o FoursqrCheckinsPerCity -trf checkins venues cities destinationPath MappingFile");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.generateFilesPerCity(args[4], args[5], trainFile, args[6], args[7]);
		}
			break;

		case "FoursqrObtainPoisCoords": {
			System.out.println(
					"-o FoursqrObtainPoisCoords -trf originalPOISFile -IMapping fileMapItems -coordFile destinationCoords");
			System.out.println(Arrays.toString(args));

			String poisCoordsFile = cl.getOptionValue(OPT_COORD_FILE);
			String mappingItems = cl.getOptionValue(OPT_MAPPING_ITEMS);
			FoursqrProcessData.generatePoisCoords(trainFile, mappingItems, poisCoordsFile);
		}
			break;

		// Obtain files of new Indexes for users and items
		case "FoursqrObtainMapUsersItems": {
			System.out.println(
					"-o FoursqrObtainMapUsersItems -trf checkins -UMapping destinationUsers -IMapping destinationItems");
			System.out.println(Arrays.toString(args));

			String resultMappingItems = cl.getOptionValue(OPT_MAPPING_ITEMS);
			String resultMappingUsers = cl.getOptionValue(OPT_MAPPING_USERS);

			FoursqrProcessData.generateMapOfUsersVenues(trainFile, resultMappingUsers, resultMappingItems);
		}
			break;

		case "FoursqrObtainPoisFeatures": {
			System.out.println(
					"-o FoursqrObtainPoisFeatures -trf originalPOISFile -IMapping fileMapItems -CMapping fileMapCategories -sfs destinationFile");
			System.out.println(Arrays.toString(args));

			String featureFile = cl.getOptionValue(OPT_SAVING_FEATURES_SCHEME);
			String mappingItems = cl.getOptionValue(OPT_MAPPING_ITEMS);
			String resultMappingCategories = cl.getOptionValue(OPT_MAPPING_CATEGORIES);

			FoursqrProcessData.generateFeatureFile(trainFile, mappingItems, resultMappingCategories, featureFile);
		}
			break;

		// Mapping categories
		case "FoursqrObtainMapCategories": {
			String resultMappingCategories = cl.getOptionValue(OPT_MAPPING_CATEGORIES);
			System.out.println(
					"-o FoursqrObtainMapItemsCategories -trf originalPOISFile -CMapping resultMappingCategories");
			System.out.println(Arrays.toString(args));
			FoursqrProcessData.generateMapOfVenuesCategories(trainFile, resultMappingCategories);
		}
			break;

		case "generateNewCheckingFileWithTimeStamps": {
			System.out.println(Arrays.toString(args));

			String userMapFile = cl.getOptionValue(OPT_MAPPING_USERS);
			String itemsMapFile = cl.getOptionValue(OPT_MAPPING_ITEMS);
			String newDataset = cl.getOptionValue(OPT_NEW_DATASET);

			FoursqrProcessData.generateNewCheckinFileWithTimeStamps(trainFile, userMapFile, itemsMapFile, newDataset);
		}
			break;

		// Apply a k-core to a dataset (careful, changed to use parameters)
		case "Kcore":
		case "DatasetReduction": {
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			Integer minUserRatings = Integer.parseInt(cl.getOptionValue(OPT_MIN_USERS));
			Integer minItemRatings = Integer.parseInt(cl.getOptionValue(OPT_MIN_ITEMS));
			ProcessSessionData.DatasetReductionRatings(trainFile, outputFile, minUserRatings, minItemRatings);
		}
			break;

		case "filterBotsFromPreferences": {
			System.out.println(Arrays.toString(args));
			String outputResFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			Long minDiff = Long.parseLong(cl.getOptionValue(OPT_MIN_DIFF_TIME));
			int minclosePrefForBots = Integer.parseInt(cl.getOptionValue(OPT_MIN_CLOSE_PREF_BOT));
			FastTemporalPreferenceDataIF<Long, Long> rankSysTemporalTestData = null;

			rankSysTemporalTestData = ExperimentUtils.loadTrainFastTemporalFeaturePreferenceData(trainFile, trainFile,
					false, true);

			ProcessSessionData.filterOutBotsTimestampUsers(rankSysTemporalTestData, outputResFile, minDiff,
					minclosePrefForBots);
		}
			break;

		case "ParseMyMediaLite": {
			System.out.println("-o ParseMyMediaLite -trf myMediaLiteRecommendation testFile newRecommendation");
			System.out.println(Arrays.toString(args));
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = ExperimentUtils
					.retrieveTrainTestIndexes(args[4], args[4], false, lp, lp);

			List<Long> usersTest = indexes.v1;
			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());

			SequentialRecommendersUtils.parseMyMediaLite(trainFile, userIndexTest, args[5]);
		}
			break;

		// generate sessions
		case "generateUserSessions": {
			System.out.println(Arrays.toString(args));

			String outputResultFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			Double limitBetweenItems = Double.parseDouble(cl.getOptionValue(OPT_LIMIT_BETWEEN_ITEMS));
			String maxDiffBetweenFirstAndLastS = cl.getOptionValue(OPT_MAX_DIFF_TIME);
			String fileCoordinates = cl.getOptionValue(OPT_COORD_FILE);
			String typeSession = cl.getOptionValue(OPT_SESSION_TYPE);
			Double maxDiffBetweenFirstAndLast = Double.MAX_VALUE;

			if (maxDiffBetweenFirstAndLastS != null) {
				maxDiffBetweenFirstAndLast = Double.parseDouble(maxDiffBetweenFirstAndLastS);
			}

			FastTemporalPreferenceDataIF<Long, Long> rankSysData = ExperimentUtils
					.loadTrainFastTemporalFeaturePreferenceData(trainFile, trainFile, false, true);

			Map<Long, Tuple2<Double, Double>> coordinates = SequentialRecommendersUtils
					.POICoordinatesMap(fileCoordinates, lp);

			UsersSessions<Long, Long> userSessions = new UsersSessions<Long, Long>(rankSysData, coordinates);

			ProcessSessionData.printSessions(userSessions, outputResultFile, limitBetweenItems,
					maxDiffBetweenFirstAndLast, ExperimentUtils.obtUserSession(typeSession));
		}
			break;

		case "filterSessions": {
			System.out.println(Arrays.toString(args));
			String outputResultFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			Integer minDiffTime = Integer.parseInt(cl.getOptionValue(OPT_MIN_DIFF_TIME));
			Integer minItemsSession = Integer.parseInt(cl.getOptionValue(OPT_MIN_ITEMS));
			Integer minimumPreferences = Integer.parseInt(cl.getOptionValue(OPT_MIN_USERS, "0"));

			Long minDiff2 = Long.parseLong(cl.getOptionValue(OPT_MIN_DIFF_TIME_2, "100000000"));
			int minclosePrefForBots = Integer.parseInt(cl.getOptionValue(OPT_MIN_CLOSE_PREF_BOT, "10000000"));

			ProcessSessionData.filterSessions(trainFile, outputResultFile, minItemsSession, minDiffTime,
					minimumPreferences, minDiff2, minclosePrefForBots);
		}
			break;

		case "generateTouristLocalBotFile": {
			System.out.println(Arrays.toString(args));
			String outputResultFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			Long maxDiff = Long.parseLong(cl.getOptionValue(OPT_MAX_DIFF_TIME));
			Long minDiff = Long.parseLong(cl.getOptionValue(OPT_MIN_DIFF_TIME));
			int minclosePrefForBots = Integer.parseInt(cl.getOptionValue(OPT_MIN_CLOSE_PREF_BOT));
			ProcessSessionData.obtainLocalsAndTourists(trainFile, outputResultFile, maxDiff, minDiff,
					minclosePrefForBots);
		}
			break;

		case "fixSessionSplit": {
			System.out.println(Arrays.toString(args));
			String originalFile = cl.getOptionValue(OPT_TRAIN_FILE);
			String outputTrainFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String outputTestFile = cl.getOptionValue(OPT_OUT_RESULT_FILE_2);
			Integer minNumberSessions = Integer.parseInt(cl.getOptionValue(OPT_MIN_SESSIONS));
			Integer minimumNumberItems = Integer.parseInt(cl.getOptionValue(OPT_MIN_ITEMS));

			ProcessSessionData.fixSessionSplit(originalFile, outputTrainFile, outputTestFile, minNumberSessions,
					minimumNumberItems);
		}
			break;

		case "AggregateWithWrapperTimeStamps": {
			System.out.println(Arrays.toString(args));
			String newDataset = cl.getOptionValue(OPT_NEW_DATASET);
			String wrapperStrategyTime = cl.getOptionValue(OPT_WRAPPER_STRATEGY_TIMESTAMPS);
			String wrapperStrategyPref = cl.getOptionValue(OPT_WRAPPER_STRATEGY);

			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETE_INDEXES, DEFAULT_COMPLETE_INDEXES));

			FastTemporalPreferenceDataIF<Long, Long> ranksysTrainTemporal = ExperimentUtils
					.loadTrainFastTemporalFeaturePreferenceData(trainFile, trainFile, completeOrNot, true);
			// Create the wrapper to be able to work with the ranksys datamodel
			SimpleFastTemporalFeaturePreferenceDataWrapper<Long, Long> wrpTrain = new SimpleFastTemporalFeaturePreferenceDataWrapper<Long, Long>(
					ranksysTrainTemporal, ExperimentUtils.obtRepetitionsStrategy(wrapperStrategyPref),
					ExperimentUtils.obtRepetitionsStrategyTime(wrapperStrategyTime));

			wrpTrain.writePreferencesTimeStamps(newDataset);
		}
			break;

		case "generateClustersCoordsTripbuilder": {
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			ProcessSessionData.generateClustersCoordsTripbuilder(trainFile, outputFile);
		}
			break;

		case "generatePOIGrupedCategoryTripBuilderByMax": {
			System.out.println(Arrays.toString(args));
			String resultFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String featureFileGroupped = cl.getOptionValue(OPT_FEATURE_READING_FILE);
			ProcessSessionData.generatePOIGrupedCategoryTripBuilderByMax(trainFile, featureFileGroupped, resultFile);
		}
			break;

		case "processCityTrajectoriesTripBuilder": {
			System.out.println(Arrays.toString(args));

			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			ProcessSessionData.processCityTrajectoriesTripBuilder(trainFile, outputFile);
		}
			break;

		/***
		 * Foursquare, tripbuilder and so on POI preprocessing section
		 */
		case "mappingUserItemsSemanticTrails": {
			System.out.println(Arrays.toString(args));
			String uMapping = cl.getOptionValue(OPT_MAPPING_USERS);
			String iMapping = cl.getOptionValue(OPT_MAPPING_ITEMS);
			SemanticTrailsPOIProcessData.mappingUserItemsSemanticTrails(trainFile, uMapping, iMapping);

		}
			break;

		case "obtainCheckingsDifferentCitiesSemanticTrails": {
			System.out.println(Arrays.toString(args));
			String citiesFile = cl.getOptionValue(OPT_SIMFILE);
			String pathDest = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String uMapping = cl.getOptionValue(OPT_MAPPING_USERS);
			String iMapping = cl.getOptionValue(OPT_MAPPING_ITEMS);

			SemanticTrailsPOIProcessData.obtainCheckingsDifferentCities(trainFile, citiesFile.split(","), pathDest,
					uMapping, iMapping, true);
		}
			break;

		case "generateTestOnlyFirstItem": {
			System.out.println(Arrays.toString(args));
			String testFile = cl.getOptionValue(OPT_TEST_FILE);
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE);

			FastTemporalPreferenceDataIF<Long, Long> rankSysTestData = ExperimentUtils
					.loadTrainFastTemporalFeaturePreferenceData(trainFile, testFile, true, false);

			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			ProcessSessionData.generateTestOnlyFirstItem(rankSysTestData, outputFile);
		}
			break;

		/***
		 * Skylines recommenders that read the test file and perform the test
		 * recommendations
		 */

		case "skylineRecommenders":
		case "testRecommenders": {
			// They need to receive the test set. We do not work with the train file
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);
			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETE_INDEXES, DEFAULT_COMPLETE_INDEXES));
			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATION_STRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);
			final int numberItemsRecommend = Integer
					.parseInt(cl.getOptionValue(OPT_ITEMS_RECOMMENDED, DEFAULT_ITEMS_RECOMMENDED));

			String rankSysRecommender = cl.getOptionValue(OPT_RANKSYS_REC);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);

			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			FastPreferenceData<Long, Long> ranksysTrainDataOriginal = ExperimentUtils
					.loadTrainFastPreferenceData(trainFile, testFile, completeOrNot, true);

			FastPreferenceData<Long, Long> ranksysTestDataOriginal = ExperimentUtils
					.loadTrainFastPreferenceData(trainFile, testFile, true, false);

			FastTemporalPreferenceDataIF<Long, Long> ranksysTestTemporal = ExperimentUtils
					.loadTrainFastTemporalFeaturePreferenceData(trainFile, testFile, completeOrNot, false);

			Recommender<Long, Long> rec = SequentialRecommendersUtils.obtRankSysSkylineRecommender(rankSysRecommender,
					ranksysTestTemporal);

			PredicatesStrategies.ranksysWriteRanking(ranksysTrainDataOriginal, ranksysTestDataOriginal, rec, outputFile,
					numberItemsRecommend, ExperimentUtils.obtRecommendationStrategy(recommendationStrategy));

		}
			break;

		case "Reranking": {
			System.out.println(Arrays.toString(args));

			int cutoff = Integer.parseInt(cl.getOptionValue(OPT_CUTOFF));
			String testFile = cl.getOptionValue(OPT_TEST_FILE);
			String featurePath = cl.getOptionValue(OPT_FEATURE_READING_FILE);
			String recommendationFile = cl.getOptionValue(OPT_RECOMMENDED_FILE);
			// Compulsory
			Double lambdaReranker = Double.parseDouble(cl.getOptionValue(OPT_LAMBDA));

			Double lambdaSmooth = Double.parseDouble(cl.getOptionValue(OPT_LAMBDA_2, "0"));

			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String similarity = cl.getOptionValue(OPT_RANKSYS_SIM);
			String fullRerankers = cl.getOptionValue(OPT_RERANKERS);
			String mapCoordiantesFile = cl.getOptionValue(OPT_COORD_FILE);
			String probabilitySmoothing = cl.getOptionValue(OPT_PROB_SMOOTH);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);
			String fillStrategy = cl.getOptionValue(OPT_FILL_STRATEGY);
			Integer kNeighs = Integer.parseInt(cl.getOptionValue(OPT_NEIGH, DEFAULT_NEIGH));
			Integer cachedItems = Integer.parseInt(cl.getOptionValue(OPT_CACHED_ITEMS, DEFAULT_CACHED_ITEMS));
			Integer numberItemsCompute = Integer
					.parseInt(cl.getOptionValue(OPT_NUM_ITEMS_COMPUTE, DEFAULT_NUM_ITEMS_COMPUTE));
			Integer lenghtPatternToSearch = Integer
					.parseInt(cl.getOptionValue(OPT_PATTERN_LENGHT, DEFAULT_OPT_PATTERN_LENGHT));

			FILL_RERANKING_STRATEGY strat = ExperimentUtils.otFillRerankingStrategy(fillStrategy);

			FastPreferenceData<Long, Long> ranksysTrainData = null;
			FastTemporalPreferenceDataIF<Long, Long> simpleFastTrainData = null;
			FastTemporalPreferenceDataIF<Long, Long> simpleFastTestData = null;

			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}
			FeatureData<Long, Long, Double> featureData = null;
			if (featurePath != null) {
				System.out.println("Reading feature file");
				featureData = SimpleFeatureData.load(SimpleFeaturesReader.get().read(featurePath, lp, lp));
			}

			ranksysTrainData = ExperimentUtils.loadTrainFastPreferenceData(trainFile, testFile, true, true);
			simpleFastTrainData = ExperimentUtils.loadTrainFastTemporalFeaturePreferenceData(trainFile, testFile, true,
					true);
			simpleFastTestData = ExperimentUtils.loadTrainFastTemporalFeaturePreferenceData(trainFile, testFile, true,
					false);

			SmoothingProbabilityIF smooth = SequenceAwareAndRerankingUtils.getSmoothingProbability(probabilitySmoothing,
					lambdaSmooth);
			ExperimentUtils.ranksysReranking(strat, ranksysTrainData, simpleFastTrainData, simpleFastTestData,
					featureData, cutoff, lambdaReranker, recommendationFile, fullRerankers, similarity, kNeighs,
					mapCoordiantesFile, outputFile, smooth, cachedItems, numberItemsCompute, lenghtPatternToSearch);
		}
			break;

		/***
		 * Pure ranksys recommenders sections (using Preference data with NO timestamps)
		 */
		// RankSys only (not reading similarities). Not working with time nor
		// repetitions datasets
		// (working with FastPreferenceData only)
		case "ranksysOnlyComplete": {
			System.out.println(Arrays.toString(args));

			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String ranksysSimilarity = cl.getOptionValue(OPT_RANKSYS_SIM);

			String ranksysRecommender = cl.getOptionValue(OPT_RANKSYS_REC);
			String additionalRankysRecommenders = cl.getOptionValue(OPT_RANKSYS_REC2);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);

			// MFs Parameters
			Integer numIterations = Integer.parseInt(cl.getOptionValue(OPT_NUM_INTERACTIONS, DEFAULT_ITERACTIONS));
			Integer numFactors = Integer.parseInt(cl.getOptionValue(OPT_K_FACTORIZER, DEFAULT_FACTORS));

			// SVD Parameters
			Double regBias = Double.parseDouble(cl.getOptionValue(OPT_SVD_REG_BIAS, DEFAULT_SVD_REG_BIAS));
			Double learnRate = Double.parseDouble(cl.getOptionValue(OPT_SVD_LEARN_RATE, DEFAULT_SVD_LEARNING_RATE));
			Double maxRate = Double
					.parseDouble(cl.getOptionValue(OPT_SVD_MAX_LEARN_RATE, DEFAULT_SVD_MAX_LEARNING_RATE));
			Boolean isboldDriver = Boolean
					.parseBoolean(cl.getOptionValue(OPT_SVD_IS_BOLD_DRIVER, DEFAULT_IS_BOLD_DRIVER));
			Double regImpItem = Double.parseDouble(cl.getOptionValue(OPT_SVD_REG_IMP_ITEM, DEFAULT_REG_IMP_ITEM));
			Double decay = Double.parseDouble(cl.getOptionValue(OPT_SVD_DECAY, DEFAULT_DECAY));
			// Rank Geo
			Double epsilon = Double.parseDouble(cl.getOptionValue(OPT_EPSILON, DEFAULT_EPSILON));
			Double c = Double.parseDouble(cl.getOptionValue(OPT_C, DEFAULT_C));

			// HKV Factorizer
			Double alphaFactorizer = Double.parseDouble(cl.getOptionValue(OPT_ALPHA_FACTORIZER, DEFAULT_ALPHA_HKV));
			Double lambdaFactorizer = Double.parseDouble(cl.getOptionValue(OPT_LAMBDA_FACTORIZER, DEFAULT_LAMBDA_HKV));

			// For candidates items matching a category
			String realFeatureFile = cl.getOptionValue(OPT_FEATURE_READING_FILE2);

			String poiCoordsFile = cl.getOptionValue(OPT_COORD_FILE);

			String scoreFreq = cl.getOptionValue(OPT_SCORE_FREQ, DEFAULT_SCORE);
			Boolean normalize = Boolean.parseBoolean(cl.getOptionValue(OPT_NORMALIZE, DEFAULT_NORMALIZE));

			final int numberItemsRecommend = Integer.parseInt(cl.getOptionValue(OPT_ITEMS_RECOMMENDED));
			int neighbours = Integer.parseInt(cl.getOptionValue(OPT_NEIGH));

			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATION_STRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);
			String userCBTransformationS = cl.getOptionValue(OPT_CB_USER_TRANSFORMATION,
					DEFAULT_CB_USER_TRANSFORMATION);

			SCORES_FREQUENCY scoref = es.uam.eps.ir.crossdomainPOI.mains.ExperimentUtils.obtScoreFreq(scoreFreq);
			USERTRANSFORMATION ut = ExperimentUtils.obtCBUserTransformation(userCBTransformationS);

			if (ranksysSimilarity == null) {
				System.out.println("Not working with any recommender using similarities");
			}

			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);

			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && !Boolean.parseBoolean(overwrite)) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETE_INDEXES, DEFAULT_COMPLETE_INDEXES));

			FastPreferenceData<Long, Long> trainPrefData = null;
			FastPreferenceData<Long, Long> testPrefData = null;

			trainPrefData = ExperimentUtils.loadTrainFastPreferenceData(trainFile, testFile, completeOrNot, true);

			FeatureData<Long, String, Double> featureData = null;
			if (realFeatureFile != null) {
				featureData = SimpleFeatureData.load(SimpleFeaturesReader.get().read(realFeatureFile, lp, sp));
			}

			System.out.println("Not using wrapper (working with no timestamps)");

			Recommender<Long, Long> rankSysrec = es.uam.eps.ir.seqawareev.utils.SequenceAwareAndRerankingUtils
					.obtRankSysRecommeder(ranksysRecommender, additionalRankysRecommenders, ranksysSimilarity,
							trainPrefData, neighbours, numFactors, alphaFactorizer, lambdaFactorizer, numIterations,
							poiCoordsFile, scoref, learnRate, maxRate, regBias, decay, isboldDriver, regImpItem,
							normalize, epsilon, c, featureData, ut);

			System.out.println("Analyzing " + testFile);
			System.out.println("Recommender file " + outputFile);

			testPrefData = ExperimentUtils.loadTrainFastPreferenceData(trainFile, testFile, true, false);

			System.out.println(
					"Writing recommended file. Not items candidates file provided. All candidates are the items not seen by that user in train.");
			PredicatesStrategies.ranksysWriteRanking(trainPrefData, testPrefData, rankSysrec, outputFile,
					numberItemsRecommend, ExperimentUtils.obtRecommendationStrategy(recommendationStrategy));

		}
			break;

		// BackwardRecommender. BFLCS. Map aggregation
		case "lcs4recSysBackwardMapAgregator": {
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String simFile = cl.getOptionValue(OPT_SIMFILE);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);
			final int numberItemsRecommend = Integer.parseInt(cl.getOptionValue(OPT_ITEMS_RECOMMENDED));
			int neighbours = Integer.parseInt(cl.getOptionValue(OPT_NEIGH));
			File f = new File(outputFile);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);
			String indexBackwards = cl.getOptionValue(OPT_INDEX_BACKWARDS);
			String indexForwards = cl.getOptionValue(OPT_INDEX_FORWARDS);
			int indexBackwardsParsed = Integer.parseInt(indexBackwards);
			int indexForwardsParsed = Integer.parseInt(indexForwards);

			String combiner = cl.getOptionValue(OPT_COMB_AGGREGATE_LIBRARY);
			String normalizer = cl.getOptionValue(OPT_NORM_AGGREGATE_LIBRARY);
			String weightaux = cl.getOptionValue(OPT_WEIGHT_AGGREGATE_LIBRARY, "false");
			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATION_STRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);

			boolean weight = Boolean.parseBoolean(weightaux);
			System.out.println("Weight is " + weight);

			String inputIndexesFile = cl.getOptionValue(OPT_IN_INDEX_FILE);

			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETE_INDEXES, DEFAULT_COMPLETE_INDEXES));
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			FastTemporalPreferenceDataIF<Long, Long> rankSysData = ExperimentUtils
					.loadTrainFastTemporalFeaturePreferenceData(trainFile, testFile, completeOrNot, true);

			UserIndexReadingSimilarity<Long> ubSim = new UserIndexReadingSimilarity<Long>(simFile, lp, rankSysData);

			UserIndexReadingSimilarityRankSysWrapper<Long, Long, Long> simUB = new UserIndexReadingSimilarityRankSysWrapper<Long, Long, Long>(
					rankSysData, ubSim);
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simUB, neighbours);
			UBLCSKNNForwardBackwardRFRecommender rcUBLCS = new UBLCSKNNForwardBackwardRFRecommender(rankSysData,
					urneighborhood, indexBackwardsParsed, indexForwardsParsed, simUB.getOurSim(), normalizer, combiner,
					weight);

			// If not null, we are reading a index File
			if (inputIndexesFile != null) {
				simUB.getOurSim().loadIndexesFile(inputIndexesFile);
				rcUBLCS.setObtainBestIndexes(false);
			} else {
				rcUBLCS.setObtainBestIndexes(true);
			}

			FastPreferenceData<Long, Long> ranksysTestData = ExperimentUtils.loadTrainFastPreferenceData(trainFile,
					testFile, true, false);

			FastPreferenceData<Long, Long> ranksysTrainDataNoTemporals = ExperimentUtils
					.loadTrainFastPreferenceData(trainFile, testFile, completeOrNot, true);

			// For each user of the second dataset
			PredicatesStrategies.ranksysWriteRanking(ranksysTrainDataNoTemporals, ranksysTestData, rcUBLCS, outputFile,
					numberItemsRecommend, ExperimentUtils.obtRecommendationStrategy(recommendationStrategy));

		}
			break;

		// Save a UB sim file from a ranksys similarity
		case "ranksysSaveUBSimFile": {
			System.out.println(Arrays.toString(args));
			String outputSimFile = cl.getOptionValue(OPT_OUT_SIM_FILE);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);
			String ransksysSim = cl.getOptionValue(OPT_RANKSYS_SIM);
			String confidenceS = cl.getOptionValue(OPT_CONFIDENCE, "0");
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, "false");

			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETE_INDEXES, DEFAULT_COMPLETE_INDEXES));
			double confidence = Double.parseDouble(confidenceS);

			File f = new File(outputSimFile);

			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			FastPreferenceData<Long, Long> ranksysTrainData = ExperimentUtils.loadTrainFastPreferenceData(trainFile,
					testFile, completeOrNot, true);
			FastPreferenceData<Long, Long> ranksysTestData = ExperimentUtils.loadTrainFastPreferenceData(trainFile,
					testFile, completeOrNot, false);

			es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simRankSys = SequentialRecommendersUtils
					.obtRanksysUserSimilarity(ranksysTrainData, ransksysSim);
			Map<SymmetricSimilarityBean<Long>, Tuple3<Double, Integer, Integer>> similarities = new HashMap<SymmetricSimilarityBean<Long>, Tuple3<Double, Integer, Integer>>();

			// For all users
			ranksysTestData.getUsersWithPreferences().filter(u -> ranksysTrainData.containsUser(u)).forEach(u1 -> {
				// Similar to the user
				simRankSys.similarUsers(u1).forEach(sim -> {
					similarities.put(new SymmetricSimilarityBean<Long>(u1, sim.v1),
							new Tuple3<Double, Integer, Integer>(sim.v2, -1, -1));
				});
			});
			UserIndexReadingSimilarity<Long> userReadingSim = new UserIndexReadingSimilarity<Long>(similarities,
					ranksysTrainData);
			userReadingSim.save(outputSimFile, confidence);
		}
			break;

		case "printClosestPOIs": {
			System.out.println(Arrays.toString(args));

			// String trainingFile, String poisCoordsFile, String resultFileNN, String
			// resultFileCoordsOfTrain, int nn
			FoursqrProcessData.printClosestPOIs(trainFile, args[4], args[5], args[6], Integer.parseInt(args[7]));
		}
			break;

		/***
		 * RankSys temporal recommenders section. Recommenders extending from ranksys
		 * using timestamps
		 */
		case "ranksysTemporalOnlyComplete": {
			/*
			 * Necessary arguments: -Option of the case. -Training file. -OutputFile -Users
			 * file. -Items file. -testFile -ranksysimilarity the string of the similarity
			 * that ranksys framework will use -ranksysRecommender the string of the
			 * recommeder that ranksys framework will use -numberItemsRecommend in the
			 * recommendation -neighbours to use in the recommendations
			 * 
			 */
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);

			String ranksysRecommender = cl.getOptionValue(OPT_RANKSYS_REC);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);

			String simFile = cl.getOptionValue(OPT_SIMFILE);

			// Check if we include factorizers
			String kFactorizer = cl.getOptionValue(OPT_K_FACTORIZER);
			String alphaFactorizer = cl.getOptionValue(OPT_ALPHA_FACTORIZER);
			String temporalLambdaS = cl.getOptionValue(OPT_TEMPORAL_LAMBDA);

			// For candidates items matching a category
			String realFeatureFile = cl.getOptionValue(OPT_FEATURE_READING_FILE2);
			System.out.println("real feature file " + realFeatureFile);

			String ranksysSimilarity = cl.getOptionValue(OPT_RANKSYS_SIM);

			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATION_STRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);

			String confidence = cl.getOptionValue(OPT_CONFIDENCE, DEFAULT_LCS_CONFIDENCE);
			Double confidenceD = Double.parseDouble(confidence);

			int kFactorizeri = 50;
			double alphaFactorizeri = 1;

			if (kFactorizer != null) {
				kFactorizeri = Integer.parseInt(kFactorizer);
				System.out.println("Working with kFactorizeri=" + kFactorizeri);
			}

			if (alphaFactorizer != null) {
				alphaFactorizeri = Double.parseDouble(alphaFactorizer);
				System.out.println("Working with alphaFactorizer=" + alphaFactorizeri);
			}

			double temporalLambda = 1.0 / 200.0;
			if (temporalLambdaS != null) {
				temporalLambda = Double.parseDouble(temporalLambdaS);
			}

			final int numberItemsRecommend = Integer.parseInt(cl.getOptionValue(OPT_ITEMS_RECOMMENDED));
			int neighbours = Integer.parseInt(cl.getOptionValue(OPT_NEIGH));

			System.out.println(Arrays.toString(args));

			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);

			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && !Boolean.parseBoolean(overwrite)) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			} else {
				System.out.println(f + " does not exist. Computing.");
			}

			boolean completeOrNot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETE_INDEXES, DEFAULT_COMPLETE_INDEXES));

			FastTemporalPreferenceDataIF<Long, Long> ranksysTrainTemporal = null;
			FastPreferenceData<Long, Long> trainPrefDataNoTimeStampNoRepetions = null;
			FastPreferenceData<Long, Long> testPrefDataNoTimeStampNoRepetions = null;

			ranksysTrainTemporal = ExperimentUtils.loadTrainFastTemporalFeaturePreferenceData(trainFile, testFile,
					completeOrNot, true);
			trainPrefDataNoTimeStampNoRepetions = ExperimentUtils.loadTrainFastPreferenceData(trainFile, testFile,
					completeOrNot, true);

			Recommender<Long, Long> rankSysrec = SequenceAwareAndRerankingUtils.obtRankSysTemporalRecommeder(
					ranksysRecommender, simFile, ranksysTrainTemporal, neighbours, ranksysSimilarity, temporalLambda,
					confidenceD);

			System.out.println("Analyzing " + testFile);
			System.out.println("Recommender file " + outputFile);

			testPrefDataNoTimeStampNoRepetions = ExperimentUtils.loadTrainFastPreferenceData(trainFile, testFile, true,
					false);
			// Normal recommendation. Only recommend for test users items that have not been
			// seen in train

			System.out.println(
					"Writing recommended file. Not items candidates file provided. All candidates are the items not seen by that user in train.");
			PredicatesStrategies.ranksysWriteRanking(trainPrefDataNoTimeStampNoRepetions,
					testPrefDataNoTimeStampNoRepetions, rankSysrec, outputFile, numberItemsRecommend,
					ExperimentUtils.obtRecommendationStrategy(recommendationStrategy));

		}
			break;

		/***
		 * Tour recommenders. Recommenders that receive the first item in the test set
		 * in order to start the recommedations
		 */

		case "TourRecommender": {
			System.out.println(Arrays.toString(args));

			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);
			String mapCoordinates = cl.getOptionValue(OPT_COORD_FILE);
			String recommender = cl.getOptionValue(OPT_RANKSYS_REC);
			String featureFile = cl.getOptionValue(OPT_FEATURE_READING_FILE);

			String smoothFunction = cl.getOptionValue(OPT_PROB_SMOOTH);
			Double lambda = Double.parseDouble(cl.getOptionValue(OPT_LAMBDA_FACTORIZER, DEFAULT_LAMBDA_HKV));

			Boolean completeOrnot = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPLETE_INDEXES, DEFAULT_COMPLETE_INDEXES));
			Integer numberItemsRec = Integer.parseInt(cl.getOptionValue(OPT_ITEMS_RECOMMENDED));
			String recommendationStrategy = cl.getOptionValue(OPT_RECOMMENDATION_STRATEGY,
					DEFAULT_RECOMMENDATION_STRATEGY);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);
			Integer cached = Integer.parseInt(cl.getOptionValue(OPT_CACHED_ITEMS, DEFAULT_CACHED_ITEMS));
			Integer numberItemsCompute = Integer
					.parseInt(cl.getOptionValue(OPT_NUM_ITEMS_COMPUTE, DEFAULT_NUM_ITEMS_COMPUTE));

			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && !Boolean.parseBoolean(overwrite)) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			FastTemporalPreferenceDataIF<Long, Long> trainTempData = ExperimentUtils
					.loadTrainFastTemporalFeaturePreferenceData(trainFile, testFile, completeOrnot, true);
			FastTemporalPreferenceDataIF<Long, Long> testTempData = ExperimentUtils
					.loadTrainFastTemporalFeaturePreferenceData(trainFile, testFile, completeOrnot, false);
			FastPreferenceData<Long, Long> trainData = ExperimentUtils.loadTrainFastPreferenceData(trainFile, testFile,
					completeOrnot, true);

			SmoothingProbabilityIF smooth = ExperimentUtils.obtSmoothingProbability(smoothFunction, lambda);

			AbstractFastTourRecommender<Long, Long> rankSysTourrec = SequenceAwareAndRerankingUtils
					.obtRankSysTourRecommeder(recommender, trainTempData, trainData, mapCoordinates, featureFile,
							smooth, lambda, cached, numberItemsCompute);

			es.uam.eps.ir.seqawareev.utils.PredicatesStrategies.ranksysWriteRankingTourRecommender(trainTempData,
					testTempData, rankSysTourrec, outputFile, numberItemsRec,
					ExperimentUtils.obtRecommendationStrategy(recommendationStrategy));
		}
			break;

		case "generateNewRecFileFstItemTest": {
			System.out.println(Arrays.toString(args));

			String recFile = cl.getOptionValue(OPT_RECOMMENDED_FILE);
			String outputRecFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);

			FastTemporalPreferenceDataIF<Long, Long> simpleFastTestData = null;
			File f = new File(outputRecFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(outputRecFile) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			simpleFastTestData = ExperimentUtils.loadTrainFastTemporalFeaturePreferenceData(trainFile, testFile, true,
					false);

			ExperimentUtils.parseRecWithFirstItemInTest(simpleFastTestData, recFile, outputRecFile);
		}
			break;

		/**
		 * RankSys evaluation section or reranking
		 */
		// Ranksys with non accuracy metrics
		case "ranksysNonAccuracyWithoutFeatureMetricsEvaluation":
		case "ranksysNonAccuracyMetricsEvaluation":
		case "ranksysNonAccuracyMetricsEvaluationPerUser":
		case "ranksysNonAccuracyWithoutFeatureMetricsEvaluationPerUser": {
			/*
			 * -Train file -Test file -Recommended file -Item feature file -Ranksys Metric
			 * -Output file -Threshold -Cutoff
			 */
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUT_RESULT_FILE);
			String testFile = cl.getOptionValue(OPT_TEST_FILE);
			String recommendedFile = cl.getOptionValue(OPT_RECOMMENDED_FILE);
			String itemFeatureFile = cl.getOptionValue(OPT_FEATURE_READING_FILE);

			int threshold = Integer.parseInt(cl.getOptionValue(OPT_THRESHOLD));
			String cutoffs = cl.getOptionValue(OPT_CUTOFF);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, DEFAULT_OVERWRITE);

			Boolean computeOnlyAcc = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMPUTE_ONLY_ACC, DEFAULT_OPT_COMPUTE_ONLY_ACC));

			String ranksysRelevanceModel = cl.getOptionValue(OPT_RANKSYS_RELEVANCE_MODEL);
			String ranksysDiscountModel = cl.getOptionValue(OPT_RANKSYS_DISCOUNT_MODEL);
			String ranksysBackground = cl.getOptionValue(OPT_RANKSYS_BACKGROUND);
			String ranksysBase = cl.getOptionValue(OPT_RANKSYS_BASE);

			String mapCoordinates = cl.getOptionValue(OPT_COORD_FILE);

			Boolean lcsEvaluation = Boolean
					.parseBoolean(cl.getOptionValue(OPT_LCS_EVALUATION, DEFAULT_LCS_EVALUTATION));

			Boolean computeDistances = Boolean
					.parseBoolean(cl.getOptionValue(OPT_COMP_DISTANCES, DEFAULT_COMPDISTANCES));

			Boolean isPerUser = step.contains("PerUser");

			File f = new File(outputFile);
			// If file of ranksys evaluation already exist then nothing
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			if (isPerUser) {
				System.out.println("Per user evaluation. We will compute the metrics for every user indvidually.");
			} else {
				System.out.println("Normal evaluation, computing the average of the results in the metrics.");
			}

			// This section is to use a temporal preference data in the test set, for a time
			// aware metric
			FastTemporalPreferenceDataIF<Long, Long> rankSysTemporalTestData = null;
			if (lcsEvaluation) {
				System.out.println("Reading test set with timestamps for LCS evaluation");

				rankSysTemporalTestData = ExperimentUtils.loadTrainFastTemporalFeaturePreferenceData(trainFile,
						testFile, true, false);
			}
			// End of test Temporal data

			final FastPreferenceData<Long, Long> trainDataFast = ExperimentUtils.loadTrainFastPreferenceData(trainFile,
					testFile, false, true);
			final PreferenceData<Long, Long> trainData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp));
			final PreferenceData<Long, Long> testDataNoFilter = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp));
			final PreferenceData<Long, Long> testData = testDataNoFilter;

			final PreferenceData<Long, Long> totalData = new ConcatPreferenceData<>(trainData, testData);

			final Set<Long> testUsers = testData.getUsersWithPreferences().collect(Collectors.toSet());

			final PreferenceData<Long, Long> originalRecommendedData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(recommendedFile, lp, lp));
			// recommended data has to be filtered to avoid evaluating users not in test
			final PreferenceData<Long, Long> recommendedData = SequentialRecommendersUtils
					.filterPreferenceData(originalRecommendedData, testUsers, null);

			////////////////////////
			// INDIVIDUAL METRICS //
			////////////////////////

			FeatureData<Long, String, Double> featureData = null;
			ItemDistanceModel<Long> dist = null;
			IntentModel<Long, Long, String> intentModel = null;
			if (!computeOnlyAcc) {
				featureData = SimpleFeatureData.load(SimpleFeaturesReader.get().read(itemFeatureFile, lp, sp));
				// COSINE DISTANCE
				dist = new CosineFeatureItemDistanceModel<>(featureData);
				// INTENT MODEL
				intentModel = new FeatureIntentModel<>(totalData, featureData);
			}

			// Binary relevance and anti relevance model for ranking metrics
			BinaryRelevanceModel<Long, Long> binRel = new BinaryRelevanceModel<>(false, testData, threshold);

			// Relevance model for novelty/diversity (can be with or without relevance)
			RelevanceModel<Long, Long> selectedRelevance = null;

			double ranksysBackgroundD = 0.0;
			double ranksysBaseD = 0.0;

			if (ranksysBackground != null) {
				ranksysBackgroundD = Double.parseDouble(ranksysBackground);
			}

			if (ranksysBase != null) {
				ranksysBaseD = Double.parseDouble(ranksysBase);
			}

			if (ranksysRelevanceModel == null) {
				selectedRelevance = new NoRelevanceModel<>();
			} else {
				selectedRelevance = SequentialRecommendersUtils.obtRelevanceModelRanksys(ranksysRelevanceModel,
						testData, threshold, ranksysBackgroundD);
			}

			RankingDiscountModel discModel = null;
			if (ranksysDiscountModel == null) {
				discModel = new NoDiscountModel();
			} else {
				discModel = SequentialRecommendersUtils.obtRankingDiscountModel(ranksysDiscountModel, ranksysBaseD);
			}

			int numUsersTest = testData.numUsersWithPreferences();
			int numUsersRecommended = recommendedData.numUsersWithPreferences();
			int numItems = totalData.numItemsWithPreferences(); // Num items with preferences in the data
			System.out.println("\n\nNum users in the test set " + numUsersTest);
			System.out.println("\n\nNum users to whom we have made recommendations " + numUsersRecommended);

			Map<String, SystemMetric<Long, Long>> sysMetrics = new HashMap<>();
			// Ranking metrics (avg for recommendation)
			Map<String, RecommendationMetric<Long, Long>> recMetricsAvgRelUsers = new HashMap<>();
			Map<String, RecommendationMetric<Long, Long>> recMetricsAllRecUsers = new HashMap<>();

			String[] differentCutoffs = cutoffs.split(",");

			for (String cutoffS : differentCutoffs) {
				int cutoff = Integer.parseInt(cutoffS);
				ExperimentUtils.addMetrics(recMetricsAvgRelUsers, recMetricsAllRecUsers, threshold, cutoff, trainData,
						trainDataFast, testData, selectedRelevance, binRel, discModel, featureData, dist, intentModel,
						step, computeOnlyAcc, rankSysTemporalTestData, lcsEvaluation, computeDistances, mapCoordinates);

				// SYSTEM METRICS. Only for normal evaluation, not per user
				if (!isPerUser) {
					sysMetrics.put("aggrdiv@" + cutoff, new AggregateDiversityMetric<>(cutoff, selectedRelevance));
					sysMetrics.put("gini@" + cutoff, new GiniIndex<>(cutoff, numItems));
					sysMetrics.put("RealAD@" + cutoff, new RealAggregateDiversity<Long, Long>(cutoff));
					sysMetrics.put("usercov", new UserCoverage<Long, Long>());

				}
			}

			// Average of all only for normal evaluation, not per user
			if (!isPerUser) {

				recMetricsAvgRelUsers.forEach((name, metric) -> sysMetrics.put(name + "_rec",
						new AverageRecommendationMetricIgnoreNoRelevantUsersAndNaNs<>(metric, binRel)));

				recMetricsAllRecUsers.forEach((name, metric) -> sysMetrics.put(name + "_rec",
						new AverageRecommendationMetric<>(metric, numUsersRecommended)));

				RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);

				format.getReader(recommendedFile).readAll()
						.forEach(rec -> sysMetrics.values().forEach(metric -> metric.add(rec)));

				PrintStream out = new PrintStream(new File(outputFile));
				sysMetrics.forEach((name, metric) -> out.println(name + "\t" + metric.evaluate()));
				out.close();
			}

		}
			break;

		default:
			System.out.println("Option " + step + " not recognized");
			break;
		}
	}

	/**
	 * Method that will obtain a command line using the available options of the
	 * arguments
	 *
	 * @param args
	 *            the arguments that the program will receive
	 * @return a command line if the arguments are correct or null if an error
	 *         occurred
	 */
	private static CommandLine getCommandLine(String[] args) {
		Options options = new Options();

		// Number of the case
		Option caseIdentifier = new Option("o", OPT_CASE, true, "option of the case");
		caseIdentifier.setRequired(true);
		options.addOption(caseIdentifier);

		// Train file
		Option trainFile = new Option("trf", OPT_TRAIN_FILE, true, "input file train path");
		trainFile.setRequired(true);
		options.addOption(trainFile);

		// Train file (2)
		Option trainFile2 = new Option("trf2", OPT_TRAIN_FILE_2, true, "input file train path (2)");
		trainFile2.setRequired(false);
		options.addOption(trainFile2);

		// Here not required
		// TestFile file
		Option testFile = new Option("tsf", OPT_TEST_FILE, true, "input file test path");
		testFile.setRequired(false);
		options.addOption(testFile);

		// Similarity file
		Option similarityFile = new Option("sf", OPT_SIMFILE, true, "similarity file");
		similarityFile.setRequired(false);
		options.addOption(similarityFile);

		// Neighbours
		Option neighbours = new Option("n", OPT_NEIGH, true, "neighbours");
		neighbours.setRequired(false);
		options.addOption(neighbours);

		// threshold
		Option threshold = new Option("thr", OPT_THRESHOLD, true, "relevance or matching threshold");
		threshold.setRequired(false);
		options.addOption(threshold);

		// NumberItemsRecommended
		Option numberItemsRecommended = new Option("nI", OPT_ITEMS_RECOMMENDED, true, "Number of items recommended");
		numberItemsRecommended.setRequired(false);
		options.addOption(numberItemsRecommended);

		// numberMinRatItems
		Option numberMinRatItems = new Option("mri", OPT_MIN_ITEMS, true, "Minimum number of item ratings");
		numberMinRatItems.setRequired(false);
		options.addOption(numberMinRatItems);

		// numberMinRatUsers
		Option numberMinRatUsers = new Option("mru", OPT_MIN_USERS, true, "Minimum number of user ratings");
		numberMinRatUsers.setRequired(false);
		options.addOption(numberMinRatUsers);

		// OutResultfile
		Option outfile = new Option("orf", OPT_OUT_RESULT_FILE, true, "output result file");
		outfile.setRequired(false);
		options.addOption(outfile);

		// OutResultfile 2
		Option outfile2 = new Option("orf2", OPT_OUT_RESULT_FILE_2, true, "output result file (2)");
		outfile2.setRequired(false);
		options.addOption(outfile2);

		// OutSimilarityfile
		Option outSimfile = new Option("osf", OPT_OUT_SIM_FILE, true, "output similarity file");
		outSimfile.setRequired(false);
		options.addOption(outSimfile);

		// Ranksys similarity
		Option rankSysSim = new Option("rs", OPT_RANKSYS_SIM, true, "ranksys similarity");
		rankSysSim.setRequired(false);
		options.addOption(rankSysSim);

		// Ranksys recommender
		Option rankSysRecommender = new Option("rr", OPT_RANKSYS_REC, true, "ranksys recommeder");
		rankSysRecommender.setRequired(false);
		options.addOption(rankSysRecommender);

		// Ranksys recommender 2
		Option rankSysRecommender2 = new Option("rr2", OPT_RANKSYS_REC2, true, "additional ranksys recommeder");
		rankSysRecommender2.setRequired(false);
		options.addOption(rankSysRecommender2);

		// Overwrite result
		Option outputOverwrite = new Option("ovw", OPT_OVERWRITE, true, "overwrite");
		outputOverwrite.setRequired(false);
		options.addOption(outputOverwrite);

		// SavingFeature
		Option savingFeature = new Option("sfs", OPT_SAVING_FEATURES_SCHEME, true, "saving feature scheme");
		savingFeature.setRequired(false);
		options.addOption(savingFeature);

		// Feature reading file
		Option featureReadingFile = new Option("ff", OPT_FEATURE_READING_FILE, true, "features file");
		featureReadingFile.setRequired(false);
		options.addOption(featureReadingFile);

		// Feature reading file2
		Option featureReadingFile2 = new Option("ff2", OPT_FEATURE_READING_FILE2, true, "features file (2)");
		featureReadingFile2.setRequired(false);
		options.addOption(featureReadingFile2);

		// Feature reading file
		Option recommendedFile = new Option("rf", OPT_RECOMMENDED_FILE, true, "recommended file");
		recommendedFile.setRequired(false);
		options.addOption(recommendedFile);

		// RankSysMetric
		Option ranksysCutoff = new Option("rc", OPT_CUTOFF, true, "ranksyscutoff");
		ranksysCutoff.setRequired(false);
		options.addOption(ranksysCutoff);

		// Confidence similarity (if similarity is lower than this value, the similarity
		// will consider )
		Option confidenceSimilarity = new Option("cs", OPT_CONFIDENCE, true, "confidence similarity");
		confidenceSimilarity.setRequired(false);
		options.addOption(confidenceSimilarity);

		// Recommenders files
		Option recommendedFiles = new Option("rfs", OPT_RECOMMENDED_FILES, true, "recommendenders files");
		recommendedFiles.setRequired(false);
		options.addOption(recommendedFiles);

		// IndexBackWards
		Option indexBackward = new Option("indexb", OPT_INDEX_BACKWARDS, true, "index backwards");
		indexBackward.setRequired(false);
		options.addOption(indexBackward);

		// IndexForwards
		Option indexForward = new Option("indexf", OPT_INDEX_FORWARDS, true, "index forwards");
		indexForward.setRequired(false);
		options.addOption(indexForward);

		// CB User transformation
		Option cbUserTransformation = new Option("userTransformationCB", OPT_CB_USER_TRANSFORMATION, true,
				"cb user transformation");
		cbUserTransformation.setRequired(false);
		options.addOption(cbUserTransformation);

		// RankSys factorizers (k)
		Option kFactorizer = new Option("kFactorizer", OPT_K_FACTORIZER, true, "k factorizer");
		kFactorizer.setRequired(false);
		options.addOption(kFactorizer);

		// RankSys factorizers (alpha)
		Option alhpaFactorizer = new Option("aFactorizer", OPT_ALPHA_FACTORIZER, true, "alpha factorizer");
		alhpaFactorizer.setRequired(false);
		options.addOption(alhpaFactorizer);

		// RankSys factorizers (lambda)
		Option lambdaFactorizer = new Option("lFactorizer", OPT_LAMBDA_FACTORIZER, true, "lambda factorizer");
		lambdaFactorizer.setRequired(false);
		options.addOption(lambdaFactorizer);

		// RankSys factorizers (numInteractions)
		Option numInteractionsFact = new Option("nIFactorizer", OPT_NUM_INTERACTIONS, true,
				"numInteractions factorizer");
		numInteractionsFact.setRequired(false);
		options.addOption(numInteractionsFact);

		// IndexesFile
		Option inindexesFile = new Option("iindexFile", OPT_IN_INDEX_FILE, true, "indexes file (input)");
		inindexesFile.setRequired(false);
		options.addOption(inindexesFile);

		// Lambda parameter for ranksys Time recommender
		Option temporalLambda = new Option("tempLambda", OPT_TEMPORAL_LAMBDA, true, "temporal lambda");
		temporalLambda.setRequired(false);
		options.addOption(temporalLambda);

		// RankLibrary norm value
		Option normAggregate = new Option("normAgLib", OPT_NORM_AGGREGATE_LIBRARY, true,
				"normalization aggregate library");
		normAggregate.setRequired(false);
		options.addOption(normAggregate);

		// RankLibrary comb value
		Option combAggregate = new Option("combAgLib", OPT_COMB_AGGREGATE_LIBRARY, true,
				"combination aggregate library");
		combAggregate.setRequired(false);
		options.addOption(combAggregate);

		// RankLibrary norm value
		Option weightAggregate = new Option("weightAgLib", OPT_WEIGHT_AGGREGATE_LIBRARY, true,
				"weight aggregate library");
		weightAggregate.setRequired(false);
		options.addOption(weightAggregate);

		// RansksyLibrary NonAccuracyEvaluationParameters
		Option ranksysRelevanceModel = new Option("ranksysRelModel", OPT_RANKSYS_RELEVANCE_MODEL, true,
				"ranksys relevance model");
		ranksysRelevanceModel.setRequired(false);
		options.addOption(ranksysRelevanceModel);

		Option ranksysDiscountModel = new Option("ranksysDiscModel", OPT_RANKSYS_DISCOUNT_MODEL, true,
				"ranksys discount model");
		ranksysDiscountModel.setRequired(false);
		options.addOption(ranksysDiscountModel);

		Option ranksysBackground = new Option("ranksysBackRel", OPT_RANKSYS_BACKGROUND, true,
				"ranksys background for relevance model");
		ranksysBackground.setRequired(false);
		options.addOption(ranksysBackground);

		Option ranksysBase = new Option("ranksysBaseDisc", OPT_RANKSYS_BASE, true, "ranksys base for discount model");
		ranksysBase.setRequired(false);
		options.addOption(ranksysBase);

		// File containing 2 columns, old id and new id, for datasets that we have
		// changed the ids (for items)
		Option itemsMapping = new Option("IMapping", OPT_MAPPING_ITEMS, true, "mapping file of items");
		itemsMapping.setRequired(false);
		options.addOption(itemsMapping);

		// File containing 2 columns, old id and new id, for datasets that we have
		// changed the ids
		Option usersMapping = new Option("UMapping", OPT_MAPPING_USERS, true, "mapping file of users");
		usersMapping.setRequired(false);
		options.addOption(usersMapping);

		// File containing 2 columns, old id and new id, for datasets that we have
		// changed the ids
		Option mapCategories = new Option("CMapping", OPT_MAPPING_CATEGORIES, true, "mapping file of categories");
		mapCategories.setRequired(false);
		options.addOption(mapCategories);

		// Option for creating a new dataset when transforming the users and items
		Option newDatasetFile = new Option("newDataset", OPT_NEW_DATASET, true, "new dataset destination");
		newDatasetFile.setRequired(false);
		options.addOption(newDatasetFile);

		// Option for creating a new dataset when transforming the users and items
		Option poiCoordFile = new Option("coordFile", OPT_COORD_FILE, true, "poi coordinates file");
		poiCoordFile.setRequired(false);
		options.addOption(poiCoordFile);

		// Option of the wrapper strategy to parse datasets or to use it in ranksys
		Option wrapperStrategyPreferences = new Option("wStrat", OPT_WRAPPER_STRATEGY, true,
				"strategy to use in the wrapper");
		wrapperStrategyPreferences.setRequired(false);
		options.addOption(wrapperStrategyPreferences);

		// Option of the wrapper strategy to parse datasets or to use it in ranksys
		Option wrapperStrategyTimeStamps = new Option("wStratTime", OPT_WRAPPER_STRATEGY_TIMESTAMPS, true,
				"strategy to use in the wrapper (for timestamps)");
		wrapperStrategyTimeStamps.setRequired(false);
		options.addOption(wrapperStrategyTimeStamps);

		Option usingWrapper = new Option("usingWrapper", OPT_USING_WRAPPER, true, "using wrapper in ranksys");
		usingWrapper.setRequired(false);
		options.addOption(usingWrapper);

		Option useCompleteIndex = new Option("cIndex", OPT_COMPLETE_INDEXES, true,
				"use the complete indexes (train + test)");
		useCompleteIndex.setRequired(false);
		options.addOption(useCompleteIndex);

		Option recommendationStrategy = new Option("recStrat", OPT_RECOMMENDATION_STRATEGY, true,
				"recommendation strategy");
		recommendationStrategy.setRequired(false);
		options.addOption(recommendationStrategy);

		Option svdRegBias = new Option("svdRegBias", OPT_SVD_REG_BIAS, true, "svd regularization for biases");
		svdRegBias.setRequired(false);
		options.addOption(svdRegBias);

		Option svdLearnRate = new Option("svdLearnRate", OPT_SVD_LEARN_RATE, true, "svd learning rate");
		svdLearnRate.setRequired(false);
		options.addOption(svdLearnRate);

		Option svdMaxLearnRate = new Option("svdMaxLearnRate", OPT_SVD_MAX_LEARN_RATE, true,
				"svd maximum learning rate");
		svdMaxLearnRate.setRequired(false);
		options.addOption(svdMaxLearnRate);

		Option svdDecay = new Option("svdDecay", OPT_SVD_DECAY, true, "svd decay");
		svdDecay.setRequired(false);
		options.addOption(svdDecay);

		Option svdIsboldDriver = new Option("svdIsboldDriver", OPT_SVD_IS_BOLD_DRIVER, true, "svd bold driver");
		svdIsboldDriver.setRequired(false);
		options.addOption(svdIsboldDriver);

		Option svdRegImpItem = new Option("svdRegImpItem", OPT_SVD_REG_IMP_ITEM, true, "reg imp item");
		svdRegImpItem.setRequired(false);
		options.addOption(svdRegImpItem);

		Option patternLenght = new Option("patternLenght", OPT_PATTERN_LENGHT, true, "length of the pattern to search");
		patternLenght.setRequired(false);
		options.addOption(patternLenght);

		Option computeOnlyAccuracy = new Option("onlyAcc", OPT_COMPUTE_ONLY_ACC, true,
				"if we are computing just accuracy metrics or also novelty-diversity");
		computeOnlyAccuracy.setRequired(false);
		options.addOption(computeOnlyAccuracy);

		Option scoreFreq = new Option("scoreFreq", OPT_SCORE_FREQ, true, "simple or frequency");
		scoreFreq.setRequired(false);
		options.addOption(scoreFreq);

		Option lcsEv = new Option("lcsEv", OPT_LCS_EVALUATION, true,
				"LCS evaluation for taking into ccount the temporal order of the items");
		lcsEv.setRequired(false);
		options.addOption(lcsEv);

		Option limit = new Option("limitBetweenItems", OPT_LIMIT_BETWEEN_ITEMS, true,
				"limit of time or distance between items");
		limit.setRequired(false);
		options.addOption(limit);

		Option sessionType = new Option("sessionType", OPT_SESSION_TYPE, true, "session stype");
		sessionType.setRequired(false);
		options.addOption(sessionType);

		Option maxDiffBetweenTimestamps = new Option("maxDiffTime", OPT_MAX_DIFF_TIME, true, "maxDifftime");
		maxDiffBetweenTimestamps.setRequired(false);
		options.addOption(maxDiffBetweenTimestamps);

		Option minDiffBetweenTimestamps = new Option("minDiffTime", OPT_MIN_DIFF_TIME, true, "minDifftime");
		minDiffBetweenTimestamps.setRequired(false);
		options.addOption(minDiffBetweenTimestamps);

		Option minDiffBetweenTimestamps2 = new Option("minDiffTime2", OPT_MIN_DIFF_TIME_2, true, "minDifftime2");
		minDiffBetweenTimestamps2.setRequired(false);
		options.addOption(minDiffBetweenTimestamps2);

		Option minClosePrefBot = new Option("minClosePrefBot", OPT_MIN_CLOSE_PREF_BOT, true, "minDifftime");
		minClosePrefBot.setRequired(false);
		options.addOption(minClosePrefBot);

		Option rerankers = new Option("rerankers", OPT_RERANKERS, true, "renrankers");
		rerankers.setRequired(false);
		options.addOption(rerankers);

		Option probabilitySmoothing = new Option("probSmooth", OPT_PROB_SMOOTH, true, "probability smoothing");
		probabilitySmoothing.setRequired(false);
		options.addOption(probabilitySmoothing);

		Option minSessions = new Option("minSessions", OPT_MIN_SESSIONS, true, "minimum number sessions");
		minSessions.setRequired(false);
		options.addOption(minSessions);

		Option computeDistances = new Option("compDistances", OPT_COMP_DISTANCES, true,
				"compute the distances of the paths");
		computeDistances.setRequired(false);
		options.addOption(computeDistances);

		Option normalize = new Option("normalize", OPT_NORMALIZE, true, "apply a normalization function");
		normalize.setRequired(false);
		options.addOption(normalize);

		Option lambda = new Option("lambda", OPT_LAMBDA, true, "lambda variable");
		lambda.setRequired(false);
		options.addOption(lambda);

		Option lambda2 = new Option("lambda2", OPT_LAMBDA_2, true, "lambda variable (2)");
		lambda2.setRequired(false);
		options.addOption(lambda2);

		Option cachedItems = new Option("cachedI", OPT_CACHED_ITEMS, true, "cachedItems");
		cachedItems.setRequired(false);
		options.addOption(cachedItems);

		Option numItemsCompute = new Option("nIComp", OPT_NUM_ITEMS_COMPUTE, true, "number of items to compute");
		numItemsCompute.setRequired(false);
		options.addOption(numItemsCompute);

		Option fillRerankingStrategy = new Option("fillStrat", OPT_FILL_STRATEGY, true, "fill reranking strategy");
		fillRerankingStrategy.setRequired(false);
		options.addOption(fillRerankingStrategy);

		Option epsilon = new Option("epsilon", OPT_EPSILON, true, "epsilon");
		epsilon.setRequired(false);
		options.addOption(epsilon);

		Option c = new Option("c", OPT_C, true, "c");
		c.setRequired(false);
		options.addOption(c);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			return null;
		}
		return cmd;

	}

}
