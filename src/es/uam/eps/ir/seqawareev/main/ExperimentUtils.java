/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.main;

import static org.ranksys.formats.parsing.Parsers.lp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.HashSet;

import java.util.List;
import java.util.Map;

import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.lambda.Unchecked;
import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.core.util.tuples.Tuple2od;
import org.ranksys.formats.parsing.Parser;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;
import org.ranksys.formats.rec.RecommendationFormat;
import org.ranksys.formats.rec.SimpleRecommendationFormat;

import es.uam.eps.ir.crossdomainPOI.datamodel.SimpleFastTemporalFeaturePreferenceData;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.utils.ExtendedPreferenceReader;
import es.uam.eps.ir.crossdomainPOI.utils.PredicatesStrategies;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils;
import es.uam.eps.ir.crossdomainPOI.utils.UsersMidPoints.SCORES_FREQUENCY;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.preference.ConcatPreferenceData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;
import es.uam.eps.ir.ranksys.diversity.distance.metrics.EILD;
import es.uam.eps.ir.ranksys.diversity.distance.reranking.MMR;
import es.uam.eps.ir.ranksys.diversity.intentaware.AspectModel;
import es.uam.eps.ir.ranksys.diversity.intentaware.FeatureIntentModel;
import es.uam.eps.ir.ranksys.diversity.intentaware.IntentModel;
import es.uam.eps.ir.ranksys.diversity.intentaware.ScoresAspectModel;
import es.uam.eps.ir.ranksys.diversity.intentaware.ScoresRelevanceAspectModel;
import es.uam.eps.ir.ranksys.diversity.intentaware.metrics.AlphaNDCG;
import es.uam.eps.ir.ranksys.diversity.intentaware.metrics.ERRIA;
import es.uam.eps.ir.ranksys.diversity.intentaware.reranking.AlphaXQuAD;
import es.uam.eps.ir.ranksys.diversity.intentaware.reranking.XQuAD;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.basic.AveragePrecision;
import es.uam.eps.ir.ranksys.metrics.basic.NDCG;
import es.uam.eps.ir.ranksys.metrics.basic.ReciprocalRank;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.BinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.sim.SetJaccardUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity;
import es.uam.eps.ir.ranksys.novdiv.distance.ItemDistanceModel;
import es.uam.eps.ir.ranksys.novdiv.distance.JaccardFeatureItemDistanceModel;
import es.uam.eps.ir.ranksys.novdiv.reranking.Reranker;
import es.uam.eps.ir.ranksys.novelty.longtail.FDItemNovelty;
import es.uam.eps.ir.ranksys.novelty.longtail.PCItemNovelty;
import es.uam.eps.ir.ranksys.novelty.longtail.metrics.EFD;
import es.uam.eps.ir.ranksys.novelty.longtail.metrics.EPC;

import es.uam.eps.ir.ranksys.novelty.unexp.PDItemNovelty;
import es.uam.eps.ir.ranksys.novelty.unexp.metrics.EPD;
import es.uam.eps.ir.seqawareev.metrics.DistanceEvaluationMetric;
import es.uam.eps.ir.seqawareev.metrics.FeatureLCSPrecision;
import es.uam.eps.ir.seqawareev.metrics.LCSGeneralEvaluationMetric;
import es.uam.eps.ir.seqawareev.metrics.TestFeaturePrecision;
import es.uam.eps.ir.seqawareev.rerankers.GenericRecommenderReranker;
import es.uam.eps.ir.seqawareev.rerankers.LambdaRerankerRelevance.FILL_RERANKING_STRATEGY;
import es.uam.eps.ir.seqawareev.rerankers.seq.FeatureSuffixTreeReranker;
import es.uam.eps.ir.seqawareev.rerankers.seq.GenericItemSimilarityReranker;
import es.uam.eps.ir.seqawareev.rerankers.seq.LCSReranker;
import es.uam.eps.ir.seqawareev.rerankers.OracleReranker;
import es.uam.eps.ir.seqawareev.rerankers.PopularityTourReranker;
import es.uam.eps.ir.seqawareev.rerankers.RandomReranker;
import es.uam.eps.ir.seqawareev.sim.AbstractTourCachedItemSimilarity;
import es.uam.eps.ir.seqawareev.sim.location.ItemLocationSimilarity;
import es.uam.eps.ir.seqawareev.sim.probability.ItemFeatureTransitionSimilarity;
import es.uam.eps.ir.seqawareev.sim.probability.ItemTransitionSimilarity;
import es.uam.eps.ir.seqawareev.smooth.JelineckMercer;
import es.uam.eps.ir.seqawareev.smooth.NoSmoothingCond;
import es.uam.eps.ir.seqawareev.smooth.NoSmoothingPrior;
import es.uam.eps.ir.seqawareev.smooth.SmoothingProbabilityIF;
import es.uam.eps.ir.seqawareev.structures.FeatureItemTransitionMatrix;
import es.uam.eps.ir.seqawareev.structures.ItemTransitionMatrix;
import es.uam.eps.ir.seqawareev.utils.CBBaselinesUtils;
import es.uam.eps.ir.seqawareev.utils.ProcessSessionData;
import es.uam.eps.ir.seqawareev.utils.UsersSessions.UserSession;
import es.uam.eps.ir.seqawareev.wrappers.SimpleFastTemporalFeaturePreferenceDataWrapper.RepetitionsStrategyPreference;
import es.uam.eps.ir.seqawareev.wrappers.SimpleFastTemporalFeaturePreferenceDataWrapper.RepetitionsStrategyTime;

/**
 * This class will have all methods that the experiment will use
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class ExperimentUtils {

	/***
	 * Method to obtain the user and the items from the users and items files
	 * 
	 * @param fileTrain
	 *            the train file
	 * @param fileTest
	 *            the test file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	public static <U, I> Tuple2<List<U>, List<I>> getCompleteUserItems(String fileTrain, String fileTest, Parser<U> up,
			Parser<I> ip) {
		try {
			PreferenceData<U, I> trainData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(fileTrain, up, ip));
			PreferenceData<U, I> testData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(fileTest, up, ip));
			PreferenceData<U, I> totalData = new ConcatPreferenceData<>(trainData, testData);
			List<U> usersList = totalData.getAllUsers().collect(Collectors.toList());
			List<I> itemsList = totalData.getAllItems().collect(Collectors.toList());

			return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
					itemsList.stream().sorted().collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * Method to obtain the recommendation strategy
	 * 
	 * @param stringRS
	 *            the recomendation strategy read
	 * @return the enumeration value or null
	 */
	public static PredicatesStrategies.recommendationStrategy obtRecommendationStrategy(String stringRS) {
		for (PredicatesStrategies.recommendationStrategy ut : PredicatesStrategies.recommendationStrategy.values()) {
			if (ut.toString().equals(stringRS) || ut.getShortName().equals(stringRS)) {
				System.out.println("Selected " + ut.toString());
				return ut;
			}
		}
		return null;
	}

	/***
	 * Method to obtain a tuple of users and items from a file
	 * 
	 * @param file
	 *            the file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	public static <U, I> Tuple2<List<U>, List<I>> getUserItemsFromFile(String file, Parser<U> up, Parser<I> ip) {
		try {
			PreferenceData<U, I> data = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(file, up, ip));
			List<U> usersList = data.getAllUsers().collect(Collectors.toList());
			List<I> itemsList = data.getAllItems().collect(Collectors.toList());

			System.out.println("Ordering by longs");

			return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
					itemsList.stream().sorted().collect(Collectors.toList()));
		} catch (IOException e) {
			System.out.println("Exception catched");
			e.printStackTrace();
			return null;
		}

	}

	/***
	 * Method to retrieve the train test indexes from a file
	 * 
	 * @param completeIndexes
	 *            flag indicating if we are going to concatenate the indexes of
	 *            train and test or not
	 * @param trainFile
	 *            the train file
	 * @param testFile
	 *            the test file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return
	 */
	public static <U, I> Tuple4<List<U>, List<I>, List<U>, List<I>> retrieveTrainTestIndexes(String trainFile,
			String testFile, boolean completeIndexes, Parser<U> up, Parser<I> ip) {
		List<U> usersTrain = null;
		List<I> itemsTrain = null;
		List<U> usersTest = null;
		List<I> itemsTest = null;
		if (completeIndexes) {
			Tuple2<List<U>, List<I>> userItems = ExperimentUtils.getCompleteUserItems(trainFile, testFile, up, ip);
			usersTrain = userItems.v1;
			itemsTrain = userItems.v2;
			usersTest = userItems.v1;
			itemsTest = userItems.v2;
		} else {
			Tuple2<List<U>, List<I>> userItemsTrain = ExperimentUtils.getUserItemsFromFile(trainFile, up, ip);
			usersTrain = userItemsTrain.v1;
			itemsTrain = userItemsTrain.v2;
			Tuple2<List<U>, List<I>> userItemsTest = ExperimentUtils.getUserItemsFromFile(testFile, up, ip);
			usersTest = userItemsTest.v1;
			itemsTest = userItemsTest.v2;

		}
		return new Tuple4<>(usersTrain, itemsTrain, usersTest, itemsTest);
	}

	/***
	 * Method to obtain the type of user session (distance or time)
	 * 
	 * @param userSession
	 * @return
	 */
	public static UserSession obtUserSession(String userSession) {
		for (UserSession m : UserSession.values()) {
			if (m.toString().equals(userSession)) {
				return m;
			}
		}
		return null;
	}

	/**
	 * Method to obtain the reranking strategy
	 * 
	 * @param stringUT
	 *            the strategy in String format
	 * @return
	 */
	public static FILL_RERANKING_STRATEGY otFillRerankingStrategy(String stringUT) {
		for (FILL_RERANKING_STRATEGY strat : FILL_RERANKING_STRATEGY.values()) {
			if (strat.toString().equals(stringUT) || strat.getShortName().equals(stringUT)) {
				return strat;
			}
		}
		return null;
	}

	/**
	 * Method to obtain the repetition strategy for the datamodels (repetition
	 * strategy)
	 * 
	 * @param repStrategy
	 *            the repetition strategy read
	 * @return the enumeration value or null
	 */
	public static RepetitionsStrategyPreference obtRepetitionsStrategy(String repStrategy) {
		for (RepetitionsStrategyPreference rs : RepetitionsStrategyPreference.values()) {
			if (rs.toString().equals(repStrategy)) {
				return rs;
			}
		}
		return null;
	}

	/**
	 * Method to obtain the repetition strategy for the datamodels (repetition
	 * strategy for the time)
	 * 
	 * @param repStrategyTime
	 *            with the time read
	 * @return the enumeration value or null
	 */
	public static RepetitionsStrategyTime obtRepetitionsStrategyTime(String repStrategyTime) {
		for (RepetitionsStrategyTime rs : RepetitionsStrategyTime.values()) {
			if (rs.toString().equals(repStrategyTime)) {
				return rs;
			}
		}
		return null;
	}

	/**
	 * Method to obtain the enumeration of the scoreFreq
	 * 
	 * @param obtScoreFreq
	 *            the score frequency read
	 * @return the enumeration value or null
	 */
	public static SCORES_FREQUENCY obtScoreFreq(String obtScoreFreq) {
		for (SCORES_FREQUENCY sf : SCORES_FREQUENCY.values()) {
			if (sf.toString().equals(obtScoreFreq)) {
				return sf;
			}
		}
		return null;
	}

	/***
	 * Method that will be in charge of add the specific recommendation metrics that
	 * we will use in our experiments
	 * 
	 * @param recMetricsAvgRelUsers
	 *            the Map of recommendation metrics to add
	 * @param threshold
	 *            the relevance threshold
	 * @param cutoff
	 *            the cutOff of the recommendations
	 * @param trainData
	 *            the training data
	 * @param testData
	 *            the test data
	 * @param its
	 *            the timeStampCalculator
	 * @param itemsTimeAvgFile
	 *            the file containing the avg of timestamps of the items
	 * @param itemsTimeFirstFile
	 *            the file containing the first timestamps of the items
	 * @param itemsTimeMedianFile
	 *            the file containing the median of timestamps of the items
	 * @param itemsTimeLastFile
	 *            the file containing the last timestamps of the items
	 * @param selectedRelevance
	 *            the selected relevance for the non accuracy metrics
	 * @param binRel
	 *            the binary relevance model (for accuracy metrics)
	 * @param discModel
	 *            the discount model
	 * @param featureData
	 *            the feature data for other non accuracy metrics
	 * @param dist
	 *            the distance model
	 * @param intentModel
	 *            the intent model
	 * @param itemsReleaseDate
	 *            the release date of the items (if available)
	 * @param step
	 *            the actual step of the recommendations
	 * @throws IOException
	 */
	public static void addMetrics(Map<String, RecommendationMetric<Long, Long>> recMetricsAvgRelUsers,
			Map<String, RecommendationMetric<Long, Long>> recMetricsAllRecUsers, int threshold, int cutoff,
			PreferenceData<Long, Long> trainData, FastPreferenceData<Long, Long> trainDataFast,
			PreferenceData<Long, Long> testData, RelevanceModel<Long, Long> selectedRelevance,
			BinaryRelevanceModel<Long, Long> binRel, RankingDiscountModel discModel,
			FeatureData<Long, String, Double> featureData, ItemDistanceModel<Long> dist,
			IntentModel<Long, Long, String> intentModel, String step, boolean computeOnlyAcc,
			FastTemporalPreferenceDataIF<Long, Long> dataTestTemporal, boolean computeLCSEvaluation,
			boolean computeDistance, String pathCoordinates) throws IOException {

		recMetricsAvgRelUsers.put("Precision@" + cutoff + "_" + threshold,
				new es.uam.eps.ir.ranksys.metrics.basic.Precision<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("MAP@" + cutoff + "_" + threshold, new AveragePrecision<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("Recall@" + cutoff + "_" + threshold,
				new es.uam.eps.ir.ranksys.metrics.basic.Recall<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("MRR@" + cutoff + "_" + threshold, new ReciprocalRank<>(cutoff, binRel));
		recMetricsAvgRelUsers.put("NDCG@" + cutoff + "_" + threshold,
				new NDCG<>(cutoff, new NDCG.NDCGRelevanceModel<>(false, testData, threshold)));

		if (!computeOnlyAcc) {
			recMetricsAllRecUsers.put("epc@" + cutoff,
					new EPC<>(cutoff, new PCItemNovelty<>(trainData), selectedRelevance, discModel));
			recMetricsAllRecUsers.put("efd@" + cutoff,
					new EFD<>(cutoff, new FDItemNovelty<>(trainData), selectedRelevance, discModel));
		}

		if (computeDistance) {
			Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
					.POICoordinatesMap(pathCoordinates, lp);
			recMetricsAllRecUsers.put("DistanceEvMetric@" + cutoff,
					new DistanceEvaluationMetric<>(cutoff, mapCoordinates));
		}

		// If we have features
		if (!computeOnlyAcc && featureData != null) {
			recMetricsAllRecUsers.put("eild@" + cutoff, new EILD<>(cutoff, dist, selectedRelevance, discModel));
			recMetricsAllRecUsers.put("epd@" + cutoff,
					new EPD<>(cutoff, new PDItemNovelty<>(false, trainData, dist), selectedRelevance, discModel));
			recMetricsAllRecUsers.put("err-ia@" + cutoff,
					new ERRIA<>(cutoff, intentModel, new ERRIA.ERRRelevanceModel<>(false, testData, threshold)));
			recMetricsAllRecUsers.put("a-ndcg@" + cutoff, new AlphaNDCG<>(cutoff, 0.5, featureData, binRel));
			recMetricsAllRecUsers.put("TFP@" + cutoff,
					new TestFeaturePrecision<>(cutoff, featureData, testData, binRel));
		}

		if (computeLCSEvaluation) {
			// New LCS evaluations
			recMetricsAvgRelUsers.put("LCSGeneralEvaluationMetricPrecision@" + cutoff + "_" + threshold,
					new LCSGeneralEvaluationMetric<>(cutoff, binRel, dataTestTemporal,
							LCSGeneralEvaluationMetric.Metric.PRECISION));
			recMetricsAvgRelUsers.put("LCSGeneralEvaluationMetricRecall@" + cutoff + "_" + threshold,
					new LCSGeneralEvaluationMetric<>(cutoff, binRel, dataTestTemporal,
							LCSGeneralEvaluationMetric.Metric.RECALL));
			/*
			 * recMetricsAvgRelUsers.put("LCSGeneralEvaluationMetricMRR@" + cutoff, new
			 * LCSGeneralEvaluationMetric<>( cutoff, selectedRelevance, dataTestTemporal,
			 * LCSGeneralEvaluationMetric.Metric.MRR));
			 */
			recMetricsAvgRelUsers.put("LCSGeneralEvaluationMetricNDCG@" + cutoff + "_" + threshold,
					new LCSGeneralEvaluationMetric<>(cutoff, binRel, dataTestTemporal,
							LCSGeneralEvaluationMetric.Metric.NDCG_LOG));
			recMetricsAvgRelUsers.put("LCSGeneralEvaluationMetricARHR@" + cutoff + "_" + threshold,
					new LCSGeneralEvaluationMetric<>(cutoff, binRel, dataTestTemporal,
							LCSGeneralEvaluationMetric.Metric.ARHR));

		}

		if (computeLCSEvaluation && !computeOnlyAcc) {
			recMetricsAvgRelUsers.put("FeatureLCSP@" + cutoff + "_" + threshold,
					new FeatureLCSPrecision<>(cutoff, featureData, dataTestTemporal, binRel));
		}

	}

	/***
	 * Method to obtain the type of smoothing probability
	 * 
	 * @param smooth
	 *            the smooth string format
	 * @param lambda
	 *            the lambda value
	 * @return
	 */
	public static SmoothingProbabilityIF obtSmoothingProbability(String smooth, double lambda) {
		if (smooth == null) {
			return null;
		}
		switch (smooth) {
		case "NoSmoothingPrior":
			return new NoSmoothingPrior();
		case "NoSmoothingCond":
			return new NoSmoothingCond();
		case "JelineckMercer":
			return new JelineckMercer(lambda);
		default:
			return null;
		}
	}

	/***
	 * Method to generate reranking files for ranksys
	 * 
	 * @param trainData
	 *            the training data
	 * @param featureData
	 *            the feature data
	 * @param cutoff
	 *            the cutoff
	 * @param lambda
	 *            the lambda value
	 * @param recommendationFile
	 *            the recommendation file
	 * @param outputFile
	 *            the output file
	 */
	public static void ranksysReranking(FILL_RERANKING_STRATEGY strat, FastPreferenceData<Long, Long> trainData,
			FastTemporalPreferenceDataIF<Long, Long> tempTrainData, FastTemporalPreferenceDataIF<Long, Long> testData,
			FeatureData<Long, Long, Double> featureData, int cutoff, double lambda, String recommendationFile,
			String rerankerS, String sim, int kNeigh, String mapCoordiantesFile, String outputFile,
			SmoothingProbabilityIF smoothingF, int cached, int numberItemsCompute, int lengthPatternToSearch) {

		Map<String, Supplier<Reranker<Long, Long>>> rerankersMap = new HashMap<>();

		System.out.println("Reranker:" + rerankerS);
		System.out.println("Recommender:" + recommendationFile);
		System.out.println("Cutoff: " + cutoff);
		if (strat != null) {
			System.out.println("Strategy: " + strat.toString());
		}

		switch (rerankerS) {
		/***
		 * OurRerankers
		 */
		case "OracleRR":
		case "OracleReranker": {
			rerankersMap.put("OracleReranker", () -> {
				return new OracleReranker<>(lambda, strat, tempTrainData, null, testData, true);
			});
		}
			break;
		case "DistTourRR":
		case "DistanceTourReranker": {
			rerankersMap.put("DistanceTourReranker", () -> {

				Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
						.POICoordinatesMap(mapCoordiantesFile, lp);
				float[][] distanceMatrix = ProcessSessionData.distanceMatrix(
						trainData.getAllItems().collect(Collectors.toList()), trainData, mapCoordinates, false);
				AbstractTourCachedItemSimilarity<Long> iSim = new ItemLocationSimilarity<>(trainData, distanceMatrix,
						cached, numberItemsCompute);
				return new GenericItemSimilarityReranker<>(lambda, strat, tempTrainData, trainData, iSim);
			});
			break;
		}
		case "PopTourRR":
		case "PopularityTourReranker": {
			rerankersMap.put("PopularityTourReranker", () -> {
				return new PopularityTourReranker<>(lambda, strat, tempTrainData, trainData, true);
			});
			break;
		}
		case "ItemMCRR":
		case "ItemMCReranker": {
			rerankersMap.put("ItemMCReranker", () -> {

				ItemTransitionMatrix<Long, Long> im = new ItemTransitionMatrix<Long, Long>(tempTrainData);
				im.initialize();

				AbstractTourCachedItemSimilarity<Long> iSim = new ItemTransitionSimilarity<>(tempTrainData, im,
						smoothingF, cached, numberItemsCompute);
				return new GenericItemSimilarityReranker<>(lambda, strat, tempTrainData, trainData, iSim);

			});
			break;

		}
		case "FeatMCRR":
		case "FeatureMCReranker": {
			rerankersMap.put("FeatureMCReranker", () -> {

				FeatureItemTransitionMatrix<Long, Long, Long> im = new FeatureItemTransitionMatrix<Long, Long, Long>(
						tempTrainData, featureData);
				im.initialize();
				AbstractTourCachedItemSimilarity<Long> iSim = new ItemFeatureTransitionSimilarity<>(tempTrainData, im,
						smoothingF, cached, numberItemsCompute);
				return new GenericItemSimilarityReranker<>(lambda, strat, tempTrainData, trainData, iSim);
			});
			break;
		}
		case "RndRR":
		case "RandomReranker": {
			rerankersMap.put("RandomReranker", () -> {
				return new RandomReranker<>(lambda, strat, tempTrainData, trainData, true);
			});
			break;
		}
		case "UBRR":
		case "UBReranker": {
			System.out.println("Similarity: " + sim);
			System.out.println("KNeighs: " + kNeigh);
			UserSimilarity<Long> similarity = new SetJaccardUserSimilarity<>(trainData, true);
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(similarity, kNeigh);
			rerankersMap.put("UBReranker", () -> {
				return new GenericRecommenderReranker<>(lambda, strat, tempTrainData, trainData,
						new UserNeighborhoodRecommender<>(trainData, urneighborhood, 1), true);
			});
			break;

		}
		case "FeatSuffTreeRR":
		case "FeatureSuffixTreeReranker": {
			Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
					.POICoordinatesMap(mapCoordiantesFile, lp);
			float[][] distanceMatrix = ProcessSessionData.distanceMatrix(
					trainData.getAllItems().collect(Collectors.toList()), trainData, mapCoordinates, false);

			System.out.println("Order pattern search: " + lengthPatternToSearch);
			rerankersMap.put("FeatureSuffixTreeReranker", () -> {
				return new FeatureSuffixTreeReranker<>(lambda, strat, tempTrainData, null, distanceMatrix, featureData,
						lengthPatternToSearch);
			});
		}
			break;
		case "LCSRR":
		case "LCSReranker": {
			Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
					.POICoordinatesMap(mapCoordiantesFile, lp);
			float[][] distanceMatrix = ProcessSessionData.distanceMatrix(
					trainData.getAllItems().collect(Collectors.toList()), trainData, mapCoordinates, false);

			rerankersMap.put("LCSReranker", () -> {
				return new LCSReranker<>(lambda, strat, tempTrainData, null, distanceMatrix, featureData);
			});
		}
			break;
		default:
			System.out.println("Reranker: " + rerankerS + " not recognized");
			return;
		}

		RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);

		rerankersMap.forEach(Unchecked.biConsumer((name, rerankerSupplier) -> {
			System.out.println("Running " + name);
			Reranker<Long, Long> reranker = rerankerSupplier.get();

			try (RecommendationFormat.Writer<Long, Long> writer = format.getWriter(outputFile)) {
				format.getReader(recommendationFile).readAll().map(rec -> reranker.rerankRecommendation(rec, cutoff))
						.forEach(Unchecked.consumer(writer::write));
			}
		}));

	}

	/***
	 * Method to create a SimpleFastTemporalFeaturePreferenceData
	 * 
	 * @param trainFile
	 *            the train file
	 * @param testFile
	 *            the test file
	 * @param completeOrNot
	 *            if we will also use the test indexes to build the data
	 * @return
	 */
	public static FastTemporalPreferenceDataIF<Long, Long> loadTrainFastTemporalFeaturePreferenceData(String trainFile,
			String testFile, boolean completeOrNot, boolean useTrainTest) {
		try {
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = ExperimentUtils
					.retrieveTrainTestIndexes(trainFile, testFile, completeOrNot, lp, lp);
			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;

			List<Long> usersTest = indexes.v3;
			List<Long> itemsTest = indexes.v4;

			// Retrieving train
			if (useTrainTest) {
				FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
				FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());

				Stream<Tuple4<Long, Long, Double, Long>> tuplesStreamTrain = ExtendedPreferenceReader.get()
						.readPreferencesTimeStamp(trainFile, lp, lp);

				SimpleFastTemporalFeaturePreferenceData<Long, Long, Long, Double> ranksysTrainTemporal = SimpleFastTemporalFeaturePreferenceData
						.loadTemporalFeature(tuplesStreamTrain, userIndexTrain, itemIndexTrain);
				return ranksysTrainTemporal;
			} else {
				// Retrieving test
				FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
				FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());

				Stream<Tuple4<Long, Long, Double, Long>> tuplesStreamTest = ExtendedPreferenceReader.get()
						.readPreferencesTimeStamp(testFile, lp, lp);

				SimpleFastTemporalFeaturePreferenceData<Long, Long, Long, Double> ranksysTestTemporal = SimpleFastTemporalFeaturePreferenceData
						.loadTemporalFeature(tuplesStreamTest, userIndexTest, itemIndexTest);
				return ranksysTestTemporal;
			}

		} catch (IOException e) {
			System.out.println("Error while creating SimpleFastTemporalFeaturePreferenceData");
			e.printStackTrace();
		}
		return null;
	}

	/***
	 * Method to create a SimpleFastTemporalFeaturePreferenceData
	 * 
	 * @param trainFile
	 *            the train file
	 * @param testFile
	 *            the test file
	 * @param completeOrNot
	 *            if we will also use the test indexes to build the data
	 * @return
	 */
	public static FastPreferenceData<Long, Long> loadTrainFastPreferenceData(String trainFile, String testFile,
			boolean completeOrNot, boolean useTrainTest) {
		try {
			Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = ExperimentUtils
					.retrieveTrainTestIndexes(trainFile, testFile, completeOrNot, lp, lp);
			List<Long> usersTrain = indexes.v1;
			List<Long> itemsTrain = indexes.v2;
			List<Long> usersTest = indexes.v3;
			List<Long> itemsTest = indexes.v4;

			// Retrieving train
			if (useTrainTest) {
				FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTrain.stream());
				FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTrain.stream());

				return SimpleFastPreferenceData.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp),
						userIndexTrain, itemIndexTrain);
			} else {
				// Retrieving test
				FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
				FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());

				return SimpleFastPreferenceData.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp),
						userIndexTest, itemIndexTest);
			}
		} catch (IOException e) {
			System.out.println("Error while creating SimpleFastTemporalFeaturePreferenceData");
			e.printStackTrace();
		}
		return null;
	}

	/***
	 * Method to generate another recommendation file so that the new recommendation
	 * file have the first item of the test set as the first item recommended
	 * 
	 * @param rankSysTestData
	 *            the test data
	 * @param originRecommendation
	 *            the original recommendation file
	 * @param destNewRecommendation
	 *            the destination of the new recommendation file
	 */
	public static void parseRecWithFirstItemInTest(FastTemporalPreferenceDataIF<Long, Long> rankSysTestData,
			String originRecommendation, String destNewRecommendation) {
		try {
			RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);
			PrintStream out = new PrintStream(destNewRecommendation);
			format.getReader(originRecommendation).readAll().forEach(rec -> {
				Long u = rec.getUser();
				Long fstItemTest = rankSysTestData.getUserPreferences(u)
						.sorted(PreferenceComparators.timeComparatorIdTimePref).map(pref -> pref.v1).findFirst().get();
				Set<Long> addedItems = new HashSet<>();
				List<Tuple2od<Long>> lstRec = rec.getItems();

				int totalCount = lstRec.size();
				addedItems.add(fstItemTest);
				out.println(u + "\t" + fstItemTest + "\t" + totalCount);
				totalCount--;
				for (int i = 0; i < lstRec.size(); i++) {
					Long itemRec = lstRec.get(i).v1;
					if (!addedItems.contains(itemRec)) {
						addedItems.add(itemRec);
						int c = totalCount - i;
						out.println(u + "\t" + itemRec + "\t" + c);
					}
				}

			});
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/***
	 * Method for checking that the reranked file and the original recommender file
	 * contain the same number of items for every user and that the first item in
	 * both list match and that all the items in the reranked file are in the
	 * original file
	 * 
	 * @param originalRecommender
	 *            the original recommender
	 * @param rerankerRecommender
	 *            the reranked recommender
	 */
	public static void checkRerankCorrectionFirstItemAndSameCandidates(String originalRecommender,
			String rerankerRecommender) {
		RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);
		try {
			Map<Long, List<Long>> mapUserItemsRatedOriginal = new HashMap<>();
			format.getReader(originalRecommender).readAll().forEach(rec -> {
				List<Long> itemsRatedOriginal = rec.getItems().stream().map(t -> t.v1).collect(Collectors.toList());
				mapUserItemsRatedOriginal.put(rec.getUser(), itemsRatedOriginal);
			});

			AtomicBoolean firstItemMatch = new AtomicBoolean(true);
			AtomicBoolean allCandidates = new AtomicBoolean(true);
			AtomicBoolean sameNumber = new AtomicBoolean(true);

			format.getReader(rerankerRecommender).readAll().forEach(rec -> {
				List<Long> originalCandidates = mapUserItemsRatedOriginal.get(rec.getUser());
				if (!originalCandidates.get(0).equals(rec.getItems().get(0).v1)) {
					firstItemMatch.set(false);
				}

				if (originalCandidates.size() != rec.getItems().size()) {
					sameNumber.set(false);
				}

				rec.getItems().stream().forEach(tuple -> {
					if (!originalCandidates.contains(tuple.v1)) {
						allCandidates.set(false);
					}
				});

			});
			if (firstItemMatch.get() && allCandidates.get() && sameNumber.get()) {
				System.out.println(
						"All items of each user in the reranked recommender are in the original recommendation for that user");
				System.out.println(
						"The first item recommended in the original recommender is also the first item in the reranked recommender");
				System.out.println("The number of items retrieved for each user in both recommenders in the same");

			}

			if (!firstItemMatch.get()) {
				System.out.println(
						"For some users, the first item in the reranked recommended is not the same as the first in the original recommender");
			}

			if (!allCandidates.get()) {
				System.out.println("For some users, some of the reranked items are not in the original file");
			}

			if (!sameNumber.get()) {
				System.out.println(
						"For some users, the number of reranked items and the number of recommended items is not the same");
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	/**
	 * Method to obtain the content based strategy
	 * 
	 * @param stringUT
	 *            the content based user transformation read
	 * @return
	 */
	public static CBBaselinesUtils.USERTRANSFORMATION obtCBUserTransformation(String stringUT) {
		for (CBBaselinesUtils.USERTRANSFORMATION ut : CBBaselinesUtils.USERTRANSFORMATION.values()) {
			if (ut.toString().equals(stringUT)) {
				return ut;
			}
		}
		return null;
	}

	/**
	 * Method to obtain the repetition strategy for the datamodels
	 * 
	 * @param obtScoreFreq
	 *            the score Frequency read
	 * @return the enumeration value or null
	 */
	public static CBBaselinesUtils.ITEMTRANSFORMATION obtCBItemTransformation(String stringiT) {
		for (CBBaselinesUtils.ITEMTRANSFORMATION it : CBBaselinesUtils.ITEMTRANSFORMATION.values()) {
			if (it.toString().equals(stringiT)) {
				return it;
			}
		}
		return null;
	}

}
