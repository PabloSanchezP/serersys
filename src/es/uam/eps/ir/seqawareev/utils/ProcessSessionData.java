/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdTimePref;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;
import es.uam.eps.ir.crossdomainPOI.utils.FoursqrProcessData;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.seqawareev.comparators.WeightComparatorTuple2String;
import es.uam.eps.ir.seqawareev.utils.UsersSessions.UserSession;

public class ProcessSessionData {

	public static String matchingFoursquareCheckinCategory(String category, int level,
			Map<String, String[]> categoriesWithLevels) {
		// some plurals are not in the file
		String[] categoriesToMatch = new String[] { category, category + "s", category + "es",
				category.substring(0, category.length() - 1) + "ies", parseFoursquareCheckinCategory(category) };
		for (String c : categoriesToMatch) {
			String[] categories = categoriesWithLevels.get(c);
			if (categories != null) {
				// there is a matching: let's find the closest level
				while (level > categories.length) {
					level--;
				}
				String matchedCategory = categories[level - 1];
				return matchedCategory;
			}
		}
		return null;
	}

	private static String parseFoursquareCheckinCategory(String category) {
		switch (category) {
		case "Light Rail":
			return "Light Rail Stations";
		case "College & University":
			return "Colleges & Universities";
		case "Yogurt":
			return "Frozen Yogurt";
		case "Car Dealership":
			return "Auto Dealerships";
		case "Deli / Bodega":
			return "Delis / Bodegas";
		case "Athletic & Sport":
			return "Athletics & Sports";
		case "Subway":
			return "Metro Stations";
		case "Mall":
			return "Shopping Malls";
		case "Spa / Massage":
			return "Spas";
		case "Home (private)":
			return "Homes (private)";
		case "Gym / Fitness Center":
			return "Gyms or Fitness Centers";
		case "Gas Station / Garage":
			return "Gas Stations";
		case "Shop & Service":
			return "Shops & Services";
		case "Tennis":
			return "Tennis Stadiums";
		case "Residential Building (Apartment / Condo)":
			return "Residential Buildings (Apartments / Condos)";
		case "Hiking Trail":
			return "Trails";
		case "Boat or Ferry":
			return "Boats or Ferries";
		case "Embassy / Consulate":
			return "Embassies / Consulates";
		case "Bike Rental / Bike Share":
			return "Bike Rentals / Bike Shares";
		case "General College & University":
			return "General Colleges & Universities";
		case "Drugstore / Pharmacy":
			return "Pharmacies";
		case "Ferry":
			return "Boats or Ferries";
		case "Salon / Barbershop":
			return "Salons / Barbershops";
		case "Malaysian Restaurant":
			return "Malay Restaurants";
		case "Harbor / Marina":
			return "Harbors / Marinas";
		case "Theme Park Ride / Attraction":
			return "Theme Park Rides/Attractions";
		case "Ramen /  Noodle House":
			// this could also be "Noodle Houses"
			return "Ramen Restaurants";
		case "Monument / Landmark":
			return "Monuments / Landmarks";
		}
		if (category.startsWith("Caf") && category.length() < 6) {
			return "Cafés";
		}
		return category;
	}

	public static Map<String, String[]> readFoursquareCategories(String inputFile) throws IOException {
		Map<String, String[]> categoriesWithLevels = new HashMap<>();
		Stream<String> stream = Files.lines(Paths.get(inputFile), StandardCharsets.ISO_8859_1);
		// this file has a header, so we can skip it
		stream.skip(1).forEach(line -> {
			// level1_name,level1_id,level2_name,level2_id,level3_name,level3_id,level4_name,level4_id
			String[] data = line.split(",", -1);
			// System.out.println(data.length + "<->" + line);
			String level1 = data[0];
			String level2 = data[2];
			String level3 = data[4];
			String level4 = data[6];
			List<String> values = new ArrayList<>();
			String key = level4;
			if (key.isEmpty()) {
				key = level3;
				if (key.isEmpty()) {
					key = level2;
					if (key.isEmpty()) {
						key = level1;
						// this level should never be empty!
						values.add(level1);
					} else {
						values.add(level1);
						values.add(level2);
					}
				} else {
					values.add(level1);
					values.add(level2);
					values.add(level3);
				}
			} else {
				values.add(level1);
				values.add(level2);
				values.add(level3);
				values.add(level4);
			}
			String[] valuesAsArray = new String[values.size()];
			values.toArray(valuesAsArray);
			categoriesWithLevels.put(key, valuesAsArray);
		});
		stream.close();
		return categoriesWithLevels;
	}

	/***
	 * Method to compute the matrix distance matrix of a list of pois
	 * 
	 * @param itemStream
	 *            the stream of items
	 * @param itemIndexes
	 *            the indexes
	 * @param coordinates
	 *            the coordinates
	 * @return a matrix with the same indexes as the FastItemIndex
	 */
	public static <I> float[][] distanceMatrix(List<I> itemStream, FastItemIndex<I> itemIndexes,
			Map<I, Tuple2<Double, Double>> coordinates, boolean meters) {
		int size = itemStream.size();

		float[][] result = new float[size][size];

		for (I item1 : itemStream) {
			int indexX = itemIndexes.item2iidx(item1);
			double latx = coordinates.get(item1).v1;
			double longx = coordinates.get(item1).v2;

			for (I item2 : itemStream) {
				if (!item1.equals(item2)) {
					int indexY = itemIndexes.item2iidx(item2);
					if (result[indexX][indexY] == 0.0 || result[indexY][indexX] == 0.0) {
						double laty = coordinates.get(item2).v1;
						double longy = coordinates.get(item2).v2;
						double dist = FoursqrProcessData.haversine(latx, longx, laty, longy);
						result[indexX][indexY] = (float) dist;
						result[indexY][indexX] = (float) dist;
					}
				}
			}
		}

		return result;
	}

	/***
	 * Method to parse a test data from ranksys selecting just the first item in the
	 * test set
	 * 
	 * @param ranksysTestData
	 *            testData from ranksys
	 * @param outputFile
	 *            the output test file containing just the first item in the test
	 *            set
	 */
	public static <U, I> void generateTestOnlyFirstItem(FastTemporalPreferenceDataIF<U, I> ranksysTestData,
			String outputFile) {
		try {
			PrintStream out = new PrintStream(outputFile);
			ranksysTestData.getUsersWithPreferences().forEach(u -> {
				IdxTimePref pref = ranksysTestData.getUidxTimePreferences(ranksysTestData.user2uidx(u))
						.sorted(PreferenceComparators.timeComparatorIdxTimePref).findFirst().get();
				out.println(u + "\t" + ranksysTestData.iidx2item(pref.v1) + "\t" + pref.v2 + "\t" + pref.v3);
			});
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/***
	 * 
	 * @param originalFile
	 * @param groupedCategoriesFile
	 * @param destinationFile
	 */
	public static void generatePOIGrupedCategoryTripBuilderByMax(String originalFile, String groupedCategoriesFile,
			String destinationFile) {
		List<String> possible = new ArrayList<>();
		possible.add("Religious");
		possible.add("Museum");
		possible.add("Architectural");
		possible.add("Shops & Services");
		possible.add("Travel & Transport");
		possible.add("Outdoors & Recreation");

		Map<String, String> groupedCategories = readUniqueCategoriesTripBuilder(groupedCategoriesFile);

		PrintStream out;
		Stream<String> stream;
		int columnCategories = 5;
		int columnCluster = 0;
		try {
			out = new PrintStream(destinationFile);
			stream = Files.lines(Paths.get(originalFile));
			stream.skip(1).forEach(line -> {
				String data[] = line.split(",");
				String categories[] = data[columnCategories].split(";");
				String finalCategory = "";
				Map<String, Integer> grupedCatsCont = new HashMap<>();

				// For finding the maximum
				for (String uniqCate : categories) {
					String grouppedAssociated = groupedCategories.get(uniqCate);
					if (grupedCatsCont.get(grouppedAssociated) == null) {
						grupedCatsCont.put(grouppedAssociated, 0);
					}
					grupedCatsCont.put(grouppedAssociated, grupedCatsCont.get(grouppedAssociated) + 1);
				}

				finalCategory = getFinalGroupCategory(possible, grupedCatsCont);
				out.println(data[columnCluster] + "\t" + finalCategory);
			});

			out.close();
			stream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static Map<String, String> readUniqueCategoriesTripBuilder(String categoriesFile) {
		Map<String, String> result = new HashMap<>();
		Stream<String> stream;
		int columnGrouped = 0;
		int columnUniq = 1;
		try {
			stream = Files.lines(Paths.get(categoriesFile));
			// It has a header
			stream.skip(1).forEach(line -> {
				String[] data = line.split("\t");
				result.put(data[columnUniq], data[columnGrouped]);
			});
			stream.close();
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	/***
	 * Method to parse the trajectories of the tripBuilder dataset
	 * (https://github.com/igobrilhante/TripBuilder)
	 * 
	 * @param originFile
	 * @param outputFile
	 */
	public static void processCityTrajectoriesTripBuilder(String originFile, String outputFile) {
		try {
			Stream<String> stream = Files.lines(Paths.get(originFile));
			PrintStream out = new PrintStream(outputFile);

			AtomicInteger session = new AtomicInteger(1);

			stream.forEach(line -> {

				// Trajectory of a user
				String[] data = line.split("\t");
				Integer user = Integer.parseInt(data[0]);

				for (int i = 1; i < data.length; i++) {
					String[] poiSession = data[i].split(";");
					String item = poiSession[0];
					String startingTime = poiSession[2];

					// User, item, time and session
					out.println(user + "\t" + item + "\t" + "1.0" + "\t" + startingTime + "\t" + session.get());
				}
				session.incrementAndGet();
			});
			stream.close();
			out.close();

		} catch (Exception e) {

		}
	}

	private static String getFinalGroupCategory(List<String> possible, Map<String, Integer> grouppedCont) {

		List<Tuple2<String, Double>> lstOrdered = new ArrayList<>();
		for (String un : grouppedCont.keySet()) {
			lstOrdered.add(new Tuple2<>(un, (double) grouppedCont.get(un)));
		}
		lstOrdered = lstOrdered.stream().sorted(new WeightComparatorTuple2String().reversed())
				.collect(Collectors.toList());

		List<String> result = new ArrayList<>();
		double maxVal = lstOrdered.get(0).v2;
		for (int i = 0; i < lstOrdered.size(); i++) {
			if (lstOrdered.get(i).v2 < maxVal) {
				break;
			}
			result.add(lstOrdered.get(i).v1);
		}
		if (result.size() == 1) {
			return result.get(0);
		}

		for (String p : possible) {
			if (result.contains(p)) {
				return p;
			}
		}

		return null;
	}

	/***
	 * Method to generate a file with the coordinates of the POIS from the original
	 * poi-cluster file
	 * 
	 * @param originalFile
	 *            the original file
	 * @param outputFile
	 *            the output file
	 */
	public static void generateClustersCoordsTripbuilder(String originalFile, String outputFile) {
		try {
			PrintStream out = new PrintStream(outputFile);
			Stream<String> stream = Files.lines(Paths.get(originalFile));
			stream.skip(1).forEach(line -> {
				String[] data = line.split(",");
				String clusterId = data[0];
				Double lat = Double.parseDouble(data[2]);
				Double longitude = Double.parseDouble(data[3]);
				out.println(clusterId + "\t" + lat + "\t" + longitude);
			});
			stream.close();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void fixSessionSplit(String originaFile, String trainFile, String testFile,
			int minNumberOfSessionsTest, int minimumNumberPoistoTest) {
		Map<Long, Map<Long, List<IdTimePref<Long>>>> mapSessions = readSessionFormat(originaFile);
		// Assume the original file is in this way
		try {
			PrintStream writerTrain = new PrintStream(trainFile);
			PrintStream writerTest = new PrintStream(testFile);
			PrintStream actual = null;
			for (Long userid : mapSessions.keySet()) {
				actual = writerTrain;
				int nSessions = mapSessions.get(userid).keySet().size();
				if (nSessions < minNumberOfSessionsTest) {
					for (Long sessionId : mapSessions.get(userid).keySet()) {
						for (IdTimePref<Long> t : mapSessions.get(userid).get(sessionId)) {
							actual.println(userid + "\t" + t.v1 + "\t" + t.v2 + "\t" + t.v3 + "\t" + sessionId);
						}
					}
				} else {
					int count = 1;
					for (Long sessionId : mapSessions.get(userid).keySet()) {
						if (count == nSessions
								&& mapSessions.get(userid).get(sessionId).size() >= minimumNumberPoistoTest) {
							actual = writerTest;
						}

						for (IdTimePref<Long> t : mapSessions.get(userid).get(sessionId)) {
							actual.println(userid + "\t" + t.v1 + "\t" + t.v2 + "\t" + t.v3 + "\t" + sessionId);
						}
						count++;
					}
				}

			}

			writerTest.close();
			writerTrain.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/***
	 * Method for printing the session of the users of a dataset
	 * 
	 * @param us
	 *            the user sessions
	 * @param pathResult
	 *            the result file
	 * @param maxDiff
	 *            the maximum difference allowed (distance or time)
	 * @param userSession
	 *            if the session is for distance or time
	 */
	public static <U, I extends Comparable<I>> void printSessions(UsersSessions<U, I> us, String pathResult,
			double maxDiff, double maxDiffFstLst, UserSession userSession) {
		PrintStream outPutResult;
		try {
			outPutResult = new PrintStream(pathResult);
			AtomicInteger sessionId = new AtomicInteger(1);

			// Print the sessions ordered by user Id
			us.getData().getUsersWithPreferences().sorted().forEach(user -> {
				List<List<IdTimePref<I>>> listUsession = us.getUserSession(user, maxDiff, maxDiffFstLst, userSession);
				for (List<IdTimePref<I>> lst : listUsession) {
					for (IdTimePref<I> pref : lst) {
						outPutResult.println(
								user + "\t" + pref.v1 + "\t" + pref.v2 + "\t" + pref.v3 + "\t" + sessionId.get());
					}
					sessionId.incrementAndGet();
				}
			});
			outPutResult.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/***
	 * Method to reduce the data of a dataset (making a k core)
	 * 
	 * @param srcpath
	 *            the source path of dataset
	 * @param dstPath
	 *            the destination route of the dataset
	 * @param numberOfRatingsUser
	 *            ratings necessary of a user to be maintained in the destination
	 *            dataset
	 * @param numberOfRatingsItems
	 *            ratings necessary of an item to be maintained in the destination
	 *            dataset
	 */
	public static void DatasetReductionRatings(String srcpath, String dstPath, int numberOfRatingsUser,
			int numberOfRatingsItems) {
		boolean implicit = false;
		// Assume structure of the file is: UserdId(long) ItemID(long)
		// rating(double) timestamp(long)

		// Map of users -> userID and list of ItemID-rating-timestamp
		Map<String, List<Tuple3<String, Double, String>>> users = new TreeMap<String, List<Tuple3<String, Double, String>>>();
		// Map of items -> itemID and list of UserID-rating-timestamp
		Map<String, List<Tuple3<String, Double, String>>> items = new TreeMap<String, List<Tuple3<String, Double, String>>>();

		String characterSplit = "\t";

		// Parameters to configure
		int columnUser = 0;
		int columnItem = 1;
		int columnRating = 2;
		int columnTimeStamp = 3;
		// switch to true

		PrintStream writer = null;

		try (Stream<String> stream = Files.lines(Paths.get(srcpath))) {
			stream.forEach(line -> {
				String[] data = line.split(characterSplit);
				String idUser = data[columnUser];
				String idItem = data[columnItem];
				double rating = 0;

				if (!implicit) {
					rating = Double.parseDouble((data[columnRating]));
				} else {
					rating = 1.0;
				}
				String timestamp;

				if (data.length > 3) {
					timestamp = data[columnTimeStamp];
				} else {
					timestamp = "1";
				}

				List<Tuple3<String, Double, String>> lstu = users.get(idUser);
				if (lstu == null) { // New user
					lstu = new ArrayList<>();
					// Add item to list
					users.put(idUser, lstu);
				}
				lstu.add(new Tuple3<String, Double, String>(idItem, rating, timestamp));

				List<Tuple3<String, Double, String>> lsti = items.get(idItem);
				if (lsti == null) { // New item
					lsti = new ArrayList<>();
					// Add item to list
					items.put(idItem, lsti);
				}
				lsti.add(new Tuple3<String, Double, String>(idUser, rating, timestamp));
			});
			stream.close();

			while (true) {
				if (checkStateCore(users, items, numberOfRatingsUser, numberOfRatingsItems)) {
					break;
				}
				updateMaps(users, items, numberOfRatingsUser, numberOfRatingsItems);
			}
			writer = new PrintStream(dstPath);

			// Writing the new data to the file
			for (String user : users.keySet()) {
				List<Tuple3<String, Double, String>> lst = users.get(user);
				for (Tuple3<String, Double, String> t : lst) {
					writer.println(user + "\t" + t.v1 + "\t" + t.v2 + "\t" + t.v3);
				}
			}
			writer.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/***
	 * Method to remove the users and items that have not a certain number of
	 * ratings
	 * 
	 * @param users
	 *            the preferences of the users
	 * @param items
	 *            the preference of the items
	 * @param minimumRatingUsers
	 *            minimum ratings that the users may have
	 * @param minimumRatingItems
	 *            minimum ratings that the items may have
	 */
	private static void updateMaps(Map<String, List<Tuple3<String, Double, String>>> users,
			Map<String, List<Tuple3<String, Double, String>>> items, int minimumRatingUsers, int minimumRatingItems) {
		Set<String> usersWithLess = new TreeSet<>(
				users.entrySet().stream().filter(t -> t.getValue().size() < minimumRatingUsers).map(t -> t.getKey())
						.collect(Collectors.toSet()));
		Set<String> itemsWithLess = new TreeSet<>(
				items.entrySet().stream().filter(t -> t.getValue().size() < minimumRatingItems).map(t -> t.getKey())
						.collect(Collectors.toSet()));

		// Now for the items we must remove the users that have rated that item
		// AND the items that have less than that number of ratings
		Set<String> totalItems = new TreeSet<>(items.keySet());
		for (String item : totalItems) {
			List<Tuple3<String, Double, String>> lst = items.get(item);
			items.put(item, new ArrayList<>(
					lst.stream().filter(t -> !usersWithLess.contains(t.v1)).collect(Collectors.toList())));
		}

		// Now remove the items from the users
		Set<String> totalUsers = new TreeSet<>(users.keySet());
		for (String user : totalUsers) {
			List<Tuple3<String, Double, String>> lst = users.get(user);
			users.put(user, new ArrayList<>(
					lst.stream().filter(t -> !itemsWithLess.contains(t.v1)).collect(Collectors.toList())));
		}

		// Now remove all items and all users that have less than the ratings
		Set<String> it = new TreeSet<>(users.entrySet().stream().filter(t -> t.getValue().size() < minimumRatingUsers)
				.map(t -> t.getKey()).collect(Collectors.toSet()));
		for (String user : it) {
			users.remove(user);
		}

		it = new TreeSet<>(items.entrySet().stream().filter(t -> t.getValue().size() < minimumRatingItems)
				.map(t -> t.getKey()).collect(Collectors.toSet()));
		for (String item : it) {
			items.remove(item);
		}

		//
		totalUsers = new TreeSet<>(users.keySet());
		for (String user : totalUsers) {
			List<Tuple3<String, Double, String>> lst = users.get(user);
			users.put(user,
					new ArrayList<>(lst.stream().filter(t -> (items.get(t.v1) != null)).collect(Collectors.toList())));
		}

		//
		totalItems = new TreeSet<>(items.keySet());
		for (String item : totalItems) {
			List<Tuple3<String, Double, String>> lst = items.get(item);
			items.put(item,
					new ArrayList<>(lst.stream().filter(t -> (users.get(t.v1) != null)).collect(Collectors.toList())));
		}

		System.out.println("Iteration: " + users.size() + " " + items.size());
	}

	/***
	 * Method to check if we are satisfying the k-core property
	 * 
	 * @param users
	 *            the preference of the users
	 * @param items
	 *            the preference of the items
	 * @param minimumRatingUsers
	 *            minimum ratings that the users may have
	 * @param minimumRatingItems
	 *            minimum ratings that the items may have
	 * @return true if the k-core is satisfied, false if not
	 */
	private static boolean checkStateCore(Map<String, List<Tuple3<String, Double, String>>> users,
			Map<String, List<Tuple3<String, Double, String>>> items, int minimumRatingUsers, int minimumRatingItems) {
		for (String user : users.keySet()) {
			if (users.get(user).size() < minimumRatingUsers) {
				return false;
			}
		}

		for (String item : items.keySet()) {
			if (items.get(item).size() < minimumRatingItems) {
				return false;
			}
		}
		return true;
	}

	public static <U extends Comparable<U>, I extends Comparable<I>> void filterOutBotsTimestampUsers(
			FastTemporalPreferenceDataIF<U, I> data, String outputFile, long minDiffforBots,
			int minPrefsToBeConsideredBot) {
		PrintStream out;
		try {
			out = new PrintStream(outputFile);
			data.getUsersWithPreferences().forEach(user -> {
				List<Long> userTimestamps = data.getUserPreferences(user).map(timepref -> timepref.v3)
						.collect(Collectors.toList());
				Collections.sort(userTimestamps);
				if (!isBot(userTimestamps, minDiffforBots, minPrefsToBeConsideredBot)) {
					data.getUserPreferences(user).sorted(PreferenceComparators.timeComparatorIdTimePref())
							.forEach(pref -> {
								out.println(user + "\t" + pref.v1 + "\t" + pref.v2 + "\t" + pref.v3);
							});
				}
			});
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/***
	 * 
	 * @param originalFile
	 * @param outputFile
	 */
	public static void filterSessions(String originalFile, String outputFile, int minNumberLengthSession,
			int minNumberSecondsSession, int minimumPreferencesUser, long minDiffforBots,
			int minPrefsToBeConsideredBot) {
		// Original file format = user id, item id, preference, timestamp, sessionId
		// We wont assume that the items in the sessions are ordered

		// Id user -> sessionId -> Tuple ItemId, preference, timestamp
		Map<Long, Map<Long, List<IdTimePref<Long>>>> map = readSessionFormat(originalFile);
		try {
			PrintStream out = new PrintStream(outputFile);
			for (Long userid : map.keySet()) {
				if (!isBot(getTimestampUser(map.get(userid)), minDiffforBots, minPrefsToBeConsideredBot)) {
					int prefsUser = countNumberPreferencesSessionFormat(map.get(userid));
					if (prefsUser > minimumPreferencesUser) {
						for (Long sessionId : map.get(userid).keySet()) {
							List<IdTimePref<Long>> tuple3lst = map.get(userid).get(sessionId);
							Collections.sort(tuple3lst, PreferenceComparators.timeComparatorIdTimePref);
							double diff = Math.abs(tuple3lst.get(0).v3 - tuple3lst.get(tuple3lst.size() - 1).v3);
							if (diff >= minNumberSecondsSession && tuple3lst.size() >= minNumberLengthSession) {
								for (IdTimePref<Long> t : tuple3lst) {
									out.println(userid + "\t" + t.v1 + "\t" + t.v2 + "\t" + t.v3 + "\t" + sessionId);
								}
							} else {
								System.out.println("User: " + userid + " with session: " + sessionId
										+ " discarded because the sessions length is " + "lower than "
										+ minNumberSecondsSession + "or because the session length is lower than "
										+ minNumberLengthSession);
							}

						}
					}
				}
			}
			out.close();

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	private static int countNumberPreferencesSessionFormat(Map<Long, List<IdTimePref<Long>>> prefsUser) {
		int acc = 0;
		for (Long sessionId : prefsUser.keySet()) {
			acc += prefsUser.get(sessionId).size();
		}
		return acc;
	}

	/***
	 * Method to detect if a user is a bot by a list of timestamps
	 * 
	 * @param timestamps
	 *            the lsit of timestamps
	 * @param minDiffforBots
	 *            the difference between 2 timestamps to be considered as bot
	 * @param minPrefsToBeConsideredBot
	 *            the number of times that the difference need to be lower so it is
	 *            considered as bot
	 * @return
	 */
	private static boolean isBot(List<Long> timestamps, long minDiffforBots, int minPrefsToBeConsideredBot) {
		Long actualTime = timestamps.get(0);
		int count = 0;
		for (int i = 1; i < timestamps.size(); i++) {
			Long newTime = timestamps.get(i);
			Long diff = Math.abs(actualTime - newTime);
			if (diff <= minDiffforBots) {
				count++;
			}
			actualTime = newTime;
		}
		return count >= minPrefsToBeConsideredBot;
	}

	private static List<Long> getTimestampUser(Map<Long, List<IdTimePref<Long>>> userSessions) {
		List<Long> userTimestampsOrdered = new ArrayList<>();
		for (Long sessionId : userSessions.keySet()) {
			userTimestampsOrdered
					.addAll(userSessions.get(sessionId).stream().map(pref -> pref.v3).collect(Collectors.toList()));
		}
		Collections.sort(userTimestampsOrdered);
		return userTimestampsOrdered;
	}

	/***
	 * Method to obtain a result file for tourists and locals
	 * 
	 * @param dataTrain
	 * @param outputLocalTourist
	 * @param maxDiffBetweenFirstAndLastDate
	 */
	public static void obtainLocalsAndTourists(String dataTrain, String outputLocalTourist,
			long maxDiffBetweenFirstAndLastDate, long minDiffforBots, int minPrefsToBeCOnsideredBot) {

		// userid -> list of timestamps
		Map<String, List<Long>> usersTimestamps = new HashMap<>();

		// userid -> set of sessions
		Map<String, Set<String>> usersSessions = new HashMap<>();

		// userid -> List of POIS (can be repeated)
		Map<String, List<String>> usersPois = new HashMap<>();
		Stream<String> stream;
		try {
			PrintStream out = new PrintStream(outputLocalTourist);
			stream = Files.lines(Paths.get(dataTrain));
			stream.forEach(line -> {
				String data[] = line.split("\t");
				String userId = data[0];
				String itemId = data[1];
				//Double score = Double.parseDouble(data[2]);
				Long time = Long.parseLong(data[3]);
				String session = data[4];

				if (usersTimestamps.get(userId) == null) {
					usersTimestamps.put(userId, new ArrayList<>());
					usersSessions.put(userId, new HashSet<>());
					usersPois.put(userId, new ArrayList<>());
				}

				usersTimestamps.get(userId).add(time);
				usersSessions.get(userId).add(session);
				usersPois.get(userId).add(itemId);
			});
			stream.close();
			// All timestamps are stored -> sort and see if it is local (L) or Tourist (T)

			out.println("UserId" + "\t" + "L-T-B" + "\t" + "FirstTimestamp" + "\t" + "LastTimestamp" + "\t"
					+ "HoursDiffFstLstTime" + "\t" + "PoisVisited" + "\t" + "UniquePoisVisited" + "\t"
					+ "NumberSessions" + "\t" + "PoisPerSession" + "\t" + "UniquePoisPerSession");
			for (String user : usersTimestamps.keySet()) {
				Collections.sort(usersTimestamps.get(user));

				Long fst = usersTimestamps.get(user).get(0);
				Long lst = usersTimestamps.get(user).get(usersTimestamps.get(user).size() - 1);
				double hoursDiff = (lst - fst) / 3600.0;
				String localTouristBot = "";
				Set<String> uniquePoisUser = new HashSet<>(usersPois.get(user));
				boolean isbot = isBot(usersTimestamps.get(user), minDiffforBots, minPrefsToBeCOnsideredBot);
				if (isbot) {
					localTouristBot = "B";
				} else {

					if ((lst - fst) > maxDiffBetweenFirstAndLastDate) {
						localTouristBot = "L";
					} else {
						localTouristBot = "T";
					}
				}
				out.println(user + "\t" + localTouristBot + "\t" + fst + "\t" + lst + "\t" + hoursDiff + "\t"
						+ usersPois.get(user).size() + "\t" + uniquePoisUser.size() + "\t"
						+ usersSessions.get(user).size() + "\t"
						+ (double) usersPois.get(user).size() / (double) usersSessions.get(user).size() + "\t"
						+ (double) uniquePoisUser.size() / (double) usersSessions.get(user).size());
			}
			out.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/***
	 * Method to read a session file in the format we want
	 * 
	 * @param originalFile
	 *            the original file
	 * @return a map by userID, sessionId and time preferences
	 */
	private static Map<Long, Map<Long, List<IdTimePref<Long>>>> readSessionFormat(String originalFile) {
		Map<Long, Map<Long, List<IdTimePref<Long>>>> mapSessions = new TreeMap<>();
		try {
			Stream<String> stream = Files.lines(Paths.get(originalFile));
			stream.forEach(line -> {
				String[] data = line.split("\t");
				Long user = Long.parseLong(data[0]);
				Long itemId = Long.parseLong(data[1]);
				Double pref = Double.parseDouble(data[2]);
				Long timestamp = Long.parseLong(data[3]);
				Long sessionId = Long.parseLong(data[4]);

				if (mapSessions.get(user) == null) {
					mapSessions.put(user, new TreeMap<>());
				}

				if (mapSessions.get(user).get(sessionId) == null) {
					mapSessions.get(user).put(sessionId, new ArrayList<>());
				}

				mapSessions.get(user).get(sessionId).add(new IdTimePref<>(itemId, pref, timestamp));

			});
			stream.close();
			return mapSessions;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;

	}

}
