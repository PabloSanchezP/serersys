/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.seqawareev.comparators.WeightComparatorTuple2;
import es.uam.eps.ir.seqawareev.comparators.WeightComparatorTuple3;
import es.uam.eps.ir.seqawareev.sim.AbstractTourCachedItemSimilarity;
import es.uam.eps.ir.seqawareev.sim.LongestCommonSubsequence;
import es.uam.eps.ir.seqawareev.smooth.NoSmoothingCond;
import es.uam.eps.ir.seqawareev.structures.FeatureItemTransitionMatrix;
import es.uam.eps.ir.seqawareev.structures.suffix.GenericSuffixTree;

/***
 * ReRankers. Class that will contain different method to apply the rerankers in
 * order to generate recommendations. All method should return the list of
 * Tuple2od already ordered
 * 
 * All rerankers that receive an item and the list of candidates should
 * introduce in the result list the item received and THEN operatw with the list
 * of candidates.
 * 
 * If the rerankers only receive the list of candidates, the first item in the
 * result list will always be the first candidate
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class ReRankingUtils {

	/***
	 * Reranker based on popularity
	 * 
	 * @param candidates
	 *            the list of candidates
	 * @param data
	 *            the data
	 * @param maxLength
	 *            the maximum length
	 * @return the list ordered by popularity
	 */
	public static <U, I> List<Tuple2od<I>> popularityReRanker(Set<I> candidates, FastPreferenceData<U, I> data,
			int maxLength) {
		int realMaxLength = Math.min(maxLength, candidates.size());

		// First item is always the first candidate
		List<Tuple2od<I>> lstTuplResult = new ArrayList<>();

		List<Tuple2od<I>> orderedByPopularity = getCandidatesOrderedByPopularity(candidates, data);
		for (int i = 0; i < realMaxLength; i++) {
			lstTuplResult.add(new Tuple2od<>(orderedByPopularity.get(i).v1, realMaxLength - i));
		}

		return lstTuplResult;
	}

	/***
	 * Method to obtain the candidates ordered by popularity
	 * 
	 * @param candidates
	 *            the set of candidates
	 * @param data
	 *            the data
	 * @return the list ordered
	 */
	public static <U, I> List<Tuple2od<I>> getCandidatesOrderedByPopularity(Set<I> candidates,
			FastPreferenceData<U, I> data) {
		return candidates.stream()
				.map(item -> new Tuple2<>(data.item2iidx(item), (double) data.getItemPreferences(item).count()))
				.sorted(new WeightComparatorTuple2().reversed()).map(t2 -> new Tuple2od<>(data.iidx2item(t2.v1), t2.v2))
				.collect(Collectors.toList());
	}

	/**
	 * Method to obtain the candidates ordered by popularity (using a temporal pref
	 * data)
	 * 
	 * @param candidates
	 *            the set of candidates
	 * @param data
	 *            the temporal preference data
	 * @return the list ordered
	 */
	public static <U, I> List<Tuple2od<I>> getCandidatesOrderedByPopularity(Set<I> candidates,
			FastTemporalPreferenceDataIF<U, I> data) {
		return candidates.stream()
				.map(item -> new Tuple2<>(data.item2iidx(item), (double) data.getItemPreferences(item).count()))
				.sorted(new WeightComparatorTuple2().reversed()).map(t2 -> new Tuple2od<>(data.iidx2item(t2.v1), t2.v2))
				.collect(Collectors.toList());
	}

	/***
	 * Method to obtain a list of items ordered by similarity with respect the first
	 * item
	 * 
	 * @param item0
	 *            the first item
	 * @param iSim
	 *            the similarity
	 * @param candidates
	 *            the set of candidates
	 * @param data
	 *            the fast preference data
	 * @return
	 */
	public static <I> Tuple2od<I> getNextItemOrderSimByPopularity(I item0, AbstractTourCachedItemSimilarity<I> iSim,
			Set<I> candidates) {
		if (iSim.isInMostSimilars(item0)) {
			List<I> cachedItems = iSim.getMostSimilars(item0, candidates);

			// Check if the cachedItems match the candidates
			if (cachedItems != null && !cachedItems.isEmpty()) {
				return new Tuple2od<>(cachedItems.get(0), iSim.similarity(item0, cachedItems.get(0)));
			}
		}

		// If it is not cached, work in the normal way
		Tuple2od<I> next = iSim.getMostSimilarItem(item0, candidates);
		if (next == null) {
			return null;
		}

		return new Tuple2od<>(next.v1, next.v2);
	}

	/***
	 * Method to obtain the list of candidates items reranked using the LCS maximum
	 * matching approach
	 * 
	 * @param item0
	 *            the first item of the sequence
	 * @param user
	 *            the target user
	 * @param candidates
	 *            the set of candidates
	 * @param dataTemporal
	 *            the temporal data
	 * @param fD
	 *            the feature preference data
	 * @param userTransitionMatrix
	 *            the feature transition matrix
	 * @param poiDistanceMatrix
	 *            the distance matrix
	 * @param maxLength
	 *            the maximum length of the list
	 * @return
	 */
	public static <U, I, F> List<Tuple2od<I>> getSequenceLCSItemOrderByPopularity(I item0, U user, Set<I> candidates,
			FastTemporalPreferenceDataIF<U, I> dataTemporal, FeatureData<I, F, ?> fD,
			FeatureItemTransitionMatrix<U, I, F> userTransitionMatrix, float[][] poiDistanceMatrix, int maxLength) {
		List<Tuple2od<I>> result = new ArrayList<>();

		// Sequence that we will search to compute the LCS
		List<Integer> sequenceToSearchLCS = new ArrayList<>();

		// feature indexes to compute the LCS
		List<Integer> orderedByTimestampFeatures = dataTemporal.getUidxTimePreferences(dataTemporal.user2uidx(user))
				.sorted(PreferenceComparators.timeComparatorIdxTimePref)
				.filter(t3 -> fD.getItemFeatures(dataTemporal.iidx2item(t3.v1)).findFirst().isPresent())
				.map(t3 -> userTransitionMatrix
						.featureTofidx(fD.getItemFeatures(dataTemporal.iidx2item(t3.v1)).findFirst().get().v1))
				.collect(Collectors.toList());

		Set<Integer> uniqueFeaturesUser = new HashSet<>(orderedByTimestampFeatures);

		Tuple2<F, ?> t = fD.getItemFeatures(item0).findFirst().orElse(null);
		result.add(new Tuple2od<>(item0, maxLength));
		candidates.remove(item0);

		// Cant follow the sequence
		if (t == null) {
			return result;
		}
		Integer firstFeature = userTransitionMatrix.featureTofidx(t.v1);
		sequenceToSearchLCS.add(firstFeature);

		int c = maxLength - 1;

		int iidxActualItem = dataTemporal.item2iidx(item0);
		while (c > 0) {

			// Get the next item
			Tuple2od<I> nextItem = getNextItemByLCS(iidxActualItem, uniqueFeaturesUser, orderedByTimestampFeatures,
					dataTemporal, fD, candidates, sequenceToSearchLCS, poiDistanceMatrix, userTransitionMatrix);
			if (nextItem == null) {
				break;
			}

			iidxActualItem = dataTemporal.item2iidx(nextItem.v1);
			result.add(new Tuple2od<>(nextItem.v1, c));
			candidates.remove(nextItem.v1);
			c--;

			sequenceToSearchLCS
					.add(userTransitionMatrix.featureTofidx(fD.getItemFeatures(nextItem.v1).findFirst().get().v1));

		}

		return result;

	}

	/***
	 * Method to get the next item by exploiting the LCS between features
	 * 
	 * @param prevItem
	 *            the previous item
	 * @param uniqueFeaturesUser
	 *            the unique features of the user
	 * @param orderedByTimestampFeatures
	 *            the features of the user ordered by timestamp
	 * @param dataTemporal
	 *            the temporal data
	 * @param fD
	 *            the feature data
	 * @param candidates
	 * @param sequenceToSearchLCS
	 * @param poiDistanceMatrix
	 * @param userTransitionMatrix
	 * @return
	 */
	private static <U, I, F> Tuple2od<I> getNextItemByLCS(int prevItem, Set<Integer> uniqueFeaturesUser,
			List<Integer> orderedByTimestampFeatures, FastTemporalPreferenceDataIF<U, I> dataTemporal,
			FeatureData<I, F, ?> fD, Set<I> candidates, List<Integer> sequenceToSearchLCS, float[][] poiDistanceMatrix,
			FeatureItemTransitionMatrix<U, I, F> userTransitionMatrix) {

		Set<Integer> featuresToSearch = new HashSet<>();
		// we have the previous index of the feature
		Tuple2<F, ?> t = fD.getItemFeatures(dataTemporal.iidx2item(prevItem)).findFirst().orElse(null);
		if (t == null) {
			return null;
		}

		int prevFidx = userTransitionMatrix.featureTofidx(t.v1);

		double maxLCS = Integer.MIN_VALUE;
		for (Integer featureToSearch : uniqueFeaturesUser) {
			sequenceToSearchLCS.add(featureToSearch);
			Integer[] x = new Integer[orderedByTimestampFeatures.size()];
			x = orderedByTimestampFeatures.toArray(x);

			Integer[] y = new Integer[sequenceToSearchLCS.size()];
			y = sequenceToSearchLCS.toArray(y);

			double lcs = LongestCommonSubsequence.longestCommonSubsequence(x, y);
			if (maxLCS < lcs) {
				maxLCS = lcs;
				featuresToSearch.clear();
				featuresToSearch.add(featureToSearch);
			} else if (maxLCS == lcs) {
				featuresToSearch.add(featureToSearch);
			}

			sequenceToSearchLCS.remove(sequenceToSearchLCS.size() - 1);
		}

		// Filter the items that are not in candidates AND that do not match the
		// features to search
		Set<I> realCandidates = candidates.stream().filter(i -> fD.getItemFeatures(i).findFirst().isPresent())
				.filter(i -> featuresToSearch
						.contains(userTransitionMatrix.featureTofidx(fD.getItemFeatures(i).findFirst().get().v1)))
				.collect(Collectors.toSet());

		return getItemListByDistanceAndFeatureScore(prevItem, prevFidx, realCandidates, dataTemporal,
				userTransitionMatrix, fD, poiDistanceMatrix);
	}

	public static <U, I, F> Tuple2od<I> getItemListByDistanceAndFeatureScore(int prevItem, int prevFidx,
			Set<I> realCandidates, FastTemporalPreferenceDataIF<U, I> dataTemporal,
			FeatureItemTransitionMatrix<U, I, F> userTransitionMatrix, FeatureData<I, F, ?> fD,
			float[][] poiDistanceMatrix) {
		ArrayList<Tuple3<Integer, Double, Double>> selectedNextItems = new ArrayList<>();

		Map<I, Double> mapDistance = getScoreByDistance(dataTemporal, prevItem, realCandidates, poiDistanceMatrix);

		for (I item : realCandidates) {
			int iidx = dataTemporal.item2iidx(item);
			int fidxCandItem = userTransitionMatrix.featureTofidx((fD.getItemFeatures(item).findFirst().get().v1));

			double scoreFeature = userTransitionMatrix.getProbabilitySmoothing(prevFidx, fidxCandItem,
					new NoSmoothingCond());
			double scoreDistance = mapDistance.get(item);

			double finalScore = scoreFeature + scoreDistance;

			double popItem = dataTemporal.getItemPreferences(item).count();

			if (selectedNextItems.isEmpty() || selectedNextItems.get(0).v2 < finalScore) {
				selectedNextItems = new ArrayList<>();
				selectedNextItems.add(new Tuple3<>(iidx, finalScore, popItem));
			} else if (selectedNextItems.get(0).v2.equals(finalScore)) {
				// Same value of similarity
				selectedNextItems.add(new Tuple3<>(iidx, finalScore, popItem));
			}
		}

		Collections.sort(selectedNextItems, new WeightComparatorTuple3().reversed());
		if (selectedNextItems.isEmpty()) {
			return null;
		}

		return new Tuple2od<I>(dataTemporal.iidx2item(selectedNextItems.get(0).v1), selectedNextItems.get(0).v2);

	}

	public static <U, I, F> List<Tuple2od<I>> getSequenceSuffixTreeItemOrderByPopularity(I item0, U user,
			Set<I> candidates, FastTemporalPreferenceDataIF<U, I> dataTemporal, FeatureData<I, F, ?> fD,
			FeatureItemTransitionMatrix<U, I, F> userTransitionMatrix, float[][] poiDistanceMatrix, int maxSequence,
			int maxLength) {
		List<Tuple2od<I>> result = new ArrayList<>();
		// The list that we will search in the suffix tree
		List<F> sequenceToSearchInSuffixTree = new ArrayList<>();

		// List of ordered features
		List<F> orderedByTimestampFeatures = dataTemporal.getUidxTimePreferences(dataTemporal.user2uidx(user))
				.sorted(PreferenceComparators.timeComparatorIdxTimePref)
				.filter(t3 -> fD.getItemFeatures(dataTemporal.iidx2item(t3.v1)).findFirst().isPresent())
				.map(t3 -> fD.getItemFeatures(dataTemporal.iidx2item(t3.v1)).findFirst().get().v1)
				.collect(Collectors.toList());

		// Set of features of the user (unique)
		Set<F> uniqueFeaturesUser = new HashSet<>(orderedByTimestampFeatures);

		// In our case, null is the special character for the suffix tree
		orderedByTimestampFeatures.add(null);

		// Create the suffix tree
		GenericSuffixTree<F> userFeaturesSuffixTree = new GenericSuffixTree<>(orderedByTimestampFeatures);

		// Cant continue the sequence
		result.add(new Tuple2od<>(item0, maxLength));
		candidates.remove(item0);

		Tuple2<F, ?> t = fD.getItemFeatures(item0).findFirst().orElse(null);
		if (t == null) {
			return result;
		}

		F firstFeature = t.v1;
		sequenceToSearchInSuffixTree.add(firstFeature);

		int c = maxLength - 1;
		// Now we have the first feature to search
		int iidxActualItem = dataTemporal.item2iidx(item0);
		while (c > 0) {
			// Get the next item
			Tuple2od<I> nextItem = getNextItemBySuffixTree(iidxActualItem, userFeaturesSuffixTree, uniqueFeaturesUser,
					dataTemporal, fD, candidates, sequenceToSearchInSuffixTree, poiDistanceMatrix,
					userTransitionMatrix);
			if (nextItem == null) {
				break;
			}
			iidxActualItem = dataTemporal.item2iidx(nextItem.v1);
			result.add(new Tuple2od<>(nextItem.v1, c));
			candidates.remove(nextItem.v1);
			c--;

			// We add the feature of the next item to the pattern to search. If the size of
			// the list is higher, then we remove the first item
			sequenceToSearchInSuffixTree.add(fD.getItemFeatures(nextItem.v1).findFirst().get().v1);
			if (sequenceToSearchInSuffixTree.size() > maxSequence) {
				sequenceToSearchInSuffixTree.remove(0);
			}
		}

		return result;
	}

	/***
	 * Get the next item to search using the suffix tree approach
	 * 
	 * @param userFeaturesSuffixTree
	 *            the suffix tree of the features of the user
	 * @param uniqueFeaturesUser
	 *            the set of unique features (to use as valid items)
	 * @param dataTemporal
	 *            the temporal data
	 * @param fD
	 *            the feature data
	 * @param candidates
	 *            the set of item candidates
	 * @param patternToSearch
	 *            the pattern to search in the suffix tree
	 * @return
	 */
	private static <U, I, F> Tuple2od<I> getNextItemBySuffixTree(int prevItem,
			GenericSuffixTree<F> userFeaturesSuffixTree, Set<F> uniqueFeaturesUser,
			FastTemporalPreferenceDataIF<U, I> dataTemporal, FeatureData<I, F, ?> fD, Set<I> candidates,
			List<F> patternToSearch, float[][] distanceMatrix,
			FeatureItemTransitionMatrix<U, I, F> userFeatureItemMatrix) {

		Set<F> featuresToSearch = new HashSet<>();

		Tuple2<F, ?> op = fD.getItemFeatures(dataTemporal.iidx2item(prevItem)).findFirst().orElse(null);

		// Can not follow the sequence
		if (op == null) {
			return null;
		}

		// we have the previous index of the feature
		int prevFidx = userFeatureItemMatrix.featureTofidx(op.v1);

		// Select only the features to search
		for (F featureToSearch : uniqueFeaturesUser) {
			patternToSearch.add(featureToSearch);
			if (userFeaturesSuffixTree.exist(patternToSearch)) {
				featuresToSearch.add(featureToSearch);
			}
			patternToSearch.remove(patternToSearch.size() - 1);
		}

		// Filter the items that are not in candidates AND that do not match the
		// features to search
		Set<I> realCandidates = candidates.stream().filter(i -> fD.getItemFeatures(i).findFirst().isPresent())
				.filter(i -> featuresToSearch.contains(fD.getItemFeatures(i).findFirst().get().v1))
				.collect(Collectors.toSet());

		return getItemListByDistanceAndFeatureScore(prevItem, prevFidx, realCandidates, dataTemporal,
				userFeatureItemMatrix, fD, distanceMatrix);
	}

	/***
	 * Method to get the score of a set of candidates by a distance matrix
	 * 
	 * @param itemIndex
	 *            the item index
	 * @param iidx0
	 *            the first item
	 * @param candidates
	 *            the set of candidates
	 * @param distanceMatrix
	 *            the distance matrix
	 * @return a map of items and their score with respect the item 0
	 */
	private static <I> Map<I, Double> getScoreByDistance(FastItemIndex<I> itemIndex, int iidx0, Set<I> candidates,
			float[][] distanceMatrix) {
		Map<I, Double> result = new HashMap<>();
		float[] row = distanceMatrix[iidx0];
		float[] aux = new float[row.length];
		double maxValue = Double.MIN_VALUE;

		for (int i = 0; i < row.length; i++) {
			// ignore origin
			if (i == iidx0) {
				continue;
			}

			if (row[i] <= 0.01) {
				row[i] = 0.01f; // avoid dividing by 0
			}

			aux[i] = 1.0f / row[i];
			if (aux[i] > maxValue) {
				maxValue = aux[i];
			}

		}
		for (I candidate : candidates) {
			result.put(candidate, aux[itemIndex.item2iidx(candidate)] / maxValue);
		}

		return result;
	}

	/***
	 * Method to obtain a list of ordered items by popularity
	 * 
	 * @param item0
	 *            the first item of the list
	 * @param iSim
	 *            the abstract similarity
	 * @param candidates
	 *            the set of candidates
	 * @param dataTemporal
	 *            the temporal preference data
	 * @param maxLength
	 *            the maximum length retrieved
	 * @return a list ordered by similarity (and in the case of ties by popularity)
	 */
	public static <I> List<Tuple2od<I>> getSequenceItemOrderSimByPopularity(I item0,
			AbstractTourCachedItemSimilarity<I> iSim, Set<I> candidates, int maxLength) {
		List<Tuple2od<I>> result = new ArrayList<>();
		Set<I> added = new HashSet<>();
		int c = maxLength - 1;
		result.add(new Tuple2od<>(item0, maxLength));
		candidates.remove(item0);
		added.add(item0);

		// Obtain all the candidates ordered
		while (c > 0) {
			Tuple2od<I> nextItem = getNextItemOrderSimByPopularity(item0, iSim, candidates);
			if (nextItem == null) {
				break;
			}
			if (Double.isNaN(nextItem.v2)) {
				System.out.println("ERROR");
			}
			item0 = nextItem.v1;
			result.add(new Tuple2od<>(nextItem.v1, c));
			added.add(nextItem.v1);
			candidates.remove(nextItem.v1);
			c--;
		}
		return result;
	}

}
