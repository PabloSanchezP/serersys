/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.utils;

import java.util.Random;

import cern.colt.function.DoubleFunction;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;

/***
 * Class of final methods to perform math operations. Initializations of
 * matrices, random numbers generators...
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class CernMatrixUtils {

	/***
	 * Method to normalize all the rows of a matrix (so it sums 1)
	 * 
	 * @param m
	 *            the matrix
	 */
	public static void normalizeRowsMatrix(DenseDoubleMatrix2D m) {
		for (int i = 0; i < m.rows(); i++) {
			DoubleMatrix1D row = m.viewRow(i);
			double rowSum = row.zSum();
			if (rowSum != 0) {
				row.assign(row, (x, y) -> x / rowSum);
			}
		}
	}

	/**
	 * Method that will initialize a Dense double matrix with binary data (0-1)
	 * depending on if the users have rated the specific items
	 * 
	 * @param m
	 *            the matrix to initialize
	 * @param data
	 *            the train data
	 */
	public static <U, I> void initializeBinaryMatrix(DenseDoubleMatrix2D m, FastPreferenceData<U, I> data) {

		// Initializing all the matrix with 0s
		m.assign(x -> x = 0);

		data.getUidxWithPreferences().forEach(uidx -> {
			data.getUidxPreferences(uidx).forEach(iidxPreference -> {
				m.set(uidx, iidxPreference.v1, 1.0);
			});
		});

	}

	/**
	 * Method that will initialize a Dense double matrix with the real ratings of a
	 * preference data depending on if the users have rated the specific items
	 * 
	 * @param m
	 *            the matrix to initialize
	 * @param data
	 *            the train data
	 */
	public static <U, I> void initializeRealMatrix(DenseDoubleMatrix2D m, FastPreferenceData<U, I> data) {

		// Initializing all the matrix with 0s
		m.assign(x -> x = 0);

		data.getUidxWithPreferences().forEach(uidx -> {
			data.getUidxPreferences(uidx).forEach(iidxPreference -> {
				m.set(uidx, iidxPreference.v1, iidxPreference.v2);
			});
		});

	}

	/***
	 * Initialize a matrix with uniform random numbers between 2 values
	 * 
	 * @param m
	 *            the matrix
	 * @param min
	 *            the minimum value
	 * @param max
	 *            the maximum value
	 */
	public static void initilizeRandomBetweenUniform(DenseDoubleMatrix2D m, double min, double max) {
		m.assign(uniformRandomGeneration(min, max));
	}

	/***
	 * Initialize a matrix with uniform random gaussian numbers
	 * 
	 * @param m
	 *            the matrix
	 * @param mean
	 *            the mean
	 * @param std
	 *            the standard deviation
	 */
	public static void initilizeRandomGaussian(DenseDoubleMatrix2D m, double mean, double std) {
		m.assign(gaussianRandomGenerator(mean, std));
	}

	/***
	 * Method to generate gaussian Random Numbers
	 * 
	 * @param mean
	 *            the mean of the random numbers
	 * @param std
	 *            the standard deviation
	 * @return a function of random gaussian numbers
	 */
	public static DoubleFunction gaussianRandomGenerator(double mean, double std) {
		Random r = new Random();
		return x -> r.nextGaussian() * std + mean;
	}

	/***
	 * Method to generate uniform random numbers
	 * 
	 * @param min
	 *            the min value to generate the random numbers
	 * @param max
	 *            the max value to generate the random numbers
	 * @return a function of uniforms random generation
	 */
	public static DoubleFunction uniformRandomGeneration(double min, double max) {
		Random r = new Random();
		return x -> min + (r.nextDouble() * (max - min));
	}

	/***
	 * Random number between 2 values (inclusive)
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static int uniformRandomNumber(int min, int max) {
		Random r = new Random();
		return min + r.nextInt((max - min) + 1);
	}

	public static double logistic(double x) {
		return 1.0 / (1.0 + Math.exp(-x));
	}

	public static double normalizeVector(DoubleMatrix1D vector, double power) {
		double acc = 0;
		for (int i = 0; i < vector.size(); i++) {
			acc += Math.pow(Math.abs(vector.getQuick(i)), power);
		}
		return Math.pow(acc, 1 / power);
	}

	/***
	 * Method to compute the dot product between 2 vectors
	 * 
	 * @param a
	 *            first vector
	 * @param b
	 *            second vector
	 * @return
	 */
	public static double dotProduct(double[] a, double[] b) {
		double sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += a[i] * b[i];
		}
		return sum;
	}

}
