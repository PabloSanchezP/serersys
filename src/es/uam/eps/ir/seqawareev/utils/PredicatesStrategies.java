/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.utils;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.ranksys.core.util.tuples.Tuple2od;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.TemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdxTimePref;
import es.uam.eps.ir.crossdomainPOI.utils.PredicatesStrategies.recommendationStrategy;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;
import es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.seqawareev.tour.interfaces.TourRecommenderIF;

public class PredicatesStrategies {

	/***
	 * Method to write the recommendation of a tour recommender in ranksys. For
	 * every recommendation, we send the first item rated in the test set, to the
	 * recommendation method in the recommender
	 * 
	 * @param rankSysTrainData
	 * @param rankSysTestData
	 * @param rankSysrec
	 * @param outputFile
	 * @param numberItemsRecommend
	 * @param strat
	 */
	public static <U, I> void ranksysWriteRankingTourRecommender(FastTemporalPreferenceDataIF<U, I> rankSysTrainData,
			FastTemporalPreferenceDataIF<U, I> rankSysTestData, TourRecommenderIF<U, I> rankSysrec, String outputFile,
			int numberItemsRecommend, recommendationStrategy strat) {
		PrintStream out;
		try {
			out = new PrintStream(outputFile);
			Stream<U> targetUsers = rankSysTestData.getUsersWithPreferences();
			System.out.println("Users in test data " + rankSysTestData.numUsersWithPreferences());
			System.out.println("Users in test data appearing in train set "
					+ rankSysTestData.getUsersWithPreferences().filter(u -> rankSysTrainData.containsUser(u)).count());

			if (strat.equals(recommendationStrategy.ONLYITEMS_TEST)) {
				numberItemsRecommend = Integer.MAX_VALUE;
			}

			final int numItemsRec = numberItemsRecommend;
			targetUsers.forEach(user -> {
				if (rankSysTestData.getUserPreferences(user).count() != 0L && rankSysTrainData.containsUser(user)) {
					int rank = 1;
					// Get the first element from test
					IdxTimePref pref = rankSysTestData.getUidxTimePreferences(rankSysTestData.user2uidx(user))
							.sorted(PreferenceComparators.timeComparatorIdxTimePref).findFirst().get();
					Recommendation<U, I> rec = rankSysrec.getRecommendation(user, numItemsRec,
							selectRecommendationPredicateTour(strat, user, rankSysTrainData, rankSysTestData), pref.v3,
							rankSysTestData.iidx2item(pref.v1));

					for (Tuple2od<I> tup : rec.getItems()) { // Items recommended
						// We should see if the items are in traingetRecommendation
						out.println(SequentialRecommendersUtils.formatRank(user, tup.v1, tup.v2, rank));
						rank++;
					}
				}
			});
			out.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static <U, I> Predicate<I> selectRecommendationPredicateTour(recommendationStrategy strat, U user,
			TemporalPreferenceDataIF<U, I> trainData, TemporalPreferenceDataIF<U, I> testData) {
		switch (strat) {
		case ALL_ITEMS:
			return isNotInTrain(user, trainData);
		case NO_CONDITION:
			return noCondition();
		case ONLYITEMS_TEST:
			return isInTest(user, testData);
		case ITEMS_IN_TRAIN:
			return itemWithRatingInTrain(trainData);
		case TRAIN_ITEMS:
		default:
			return trainItems(user, trainData);
		}

	}

	public static <U, I> Predicate<I> isNotInTrain(U user, TemporalPreferenceDataIF<U, I> trainData) {
		return p -> trainData.getUserPreferences(user).noneMatch(itemPref -> itemPref.v1.equals(p));
	}

	public static <U, I> Predicate<I> isInTest(U user, TemporalPreferenceDataIF<U, I> testData) {
		return p -> testData.getUserPreferences(user).anyMatch(itemPref -> itemPref.v1.equals(p));
	}

	public static <U, I> Predicate<I> itemWithRatingInTrain(TemporalPreferenceDataIF<U, I> trainData) {
		return p -> trainData.getItemPreferences(p).findFirst().isPresent();
	}

	public static <U, I> Predicate<I> trainItems(U user, TemporalPreferenceDataIF<U, I> trainData) {
		return isNotInTrain(user, trainData).and(itemWithRatingInTrain(trainData));
	}

	/***
	 * No condition predicate (every item is considered as a predicate)
	 * 
	 * @return a predicate
	 */
	public static <I> Predicate<I> noCondition() {
		return p -> true;
	}

}
