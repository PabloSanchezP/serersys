/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.jooq.lambda.tuple.Tuple2;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdTimePref;
import es.uam.eps.ir.crossdomainPOI.utils.FoursqrProcessData;
import es.uam.eps.ir.crossdomainPOI.utils.PreferenceComparators;

/***
 * Class used to build and operate with the sessions of the users
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 * @param <I>
 */
public class UsersSessions<U, I extends Comparable<I>> {
	private FastTemporalPreferenceDataIF<U, I> data;
	private Map<I, Tuple2<Double, Double>> itemsCoordinates;

	public enum UserSession {
		DISTANCE, TIME
	};

	public UsersSessions(FastTemporalPreferenceDataIF<U, I> data, Map<I, Tuple2<Double, Double>> coordinates) {
		this.data = data;
		this.itemsCoordinates = coordinates;
	}

	/***
	 * Method to obtain different sessions of the user according to the different
	 * times of consumption or the distance between the items from the list of
	 * temporal preferences of the user
	 * 
	 * @param user
	 *            the user
	 * @param MaxDiff
	 *            the maximum difference of times between the items in order to be
	 *            considered as items from the same session
	 * @return A list of lists of preferences (each list of preferences represented
	 *         a session)
	 */
	public List<List<IdTimePref<I>>> getUserSession(U user, double maxDiff, double maxDiffBetweenFstLastItem,
			UserSession uSession) {
		List<List<IdTimePref<I>>> result = new ArrayList<List<IdTimePref<I>>>();
		result.add(new ArrayList<IdTimePref<I>>());
		AtomicInteger totalSessions = new AtomicInteger(0);

		// For every preference
		this.data.getUserPreferences(user).sorted(PreferenceComparators.timeComparatorIdTimePref()).forEach(pref -> {
			int actualSession = totalSessions.get();
			List<IdTimePref<I>> actualSessionList = result.get(actualSession);

			if (actualSessionList.isEmpty()) { // if the session is empty we add the preference
				actualSessionList.add(pref);
			}

			else { // otherwise we check the previous time and see if the difference is higher. If
					// not, we add it, otherwise, we create another session
				if (isValid(actualSessionList.get(actualSessionList.size() - 1), pref, maxDiff, uSession)
						&& isValid(pref, actualSessionList.get(0), maxDiffBetweenFstLastItem, uSession)) {
					actualSessionList.add(pref);
				} else {
					totalSessions.incrementAndGet();
					result.add(new ArrayList<IdTimePref<I>>());
					actualSessionList = result.get(totalSessions.get());
					actualSessionList.add(pref);
				}
			}

		});
		return result;
	}

	/***
	 * Method to check if two preferences are in the same session or not
	 * 
	 * @param pref1
	 *            the first preference
	 * @param pref2
	 *            the second preference
	 * @param maxDiff
	 *            the maximum difference allowed in order to consider that the two
	 *            items are in the same session
	 * @param session
	 *            the type of session that we are considering
	 * @return true if it is valid, false otherwise
	 */
	private boolean isValid(IdTimePref<I> pref1, IdTimePref<I> pref2, double maxDiff, UserSession session) {
		switch (session) {
		case TIME:
			return Math.abs(pref1.v3 - pref2.v3) < maxDiff;
		case DISTANCE:
			return FoursqrProcessData.haversine(itemsCoordinates.get(pref1.v1).v1, itemsCoordinates.get(pref1.v1).v2,
					itemsCoordinates.get(pref2.v1).v1, itemsCoordinates.get(pref2.v1).v2) < maxDiff;
		default:
			return false;
		}
	}

	public FastTemporalPreferenceDataIF<U, I> getData() {
		return data;
	}

	public void setData(FastTemporalPreferenceDataIF<U, I> data) {
		this.data = data;
	}

	public Map<I, Tuple2<Double, Double>> getItemsCoordinates() {
		return itemsCoordinates;
	}

	public void setItemsCoordinates(Map<I, Tuple2<Double, Double>> itemsCoordinates) {
		this.itemsCoordinates = itemsCoordinates;
	}

}
