/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.utils;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;

import es.uam.eps.ir.crossdomainPOI.utils.FoursqrProcessData;

/***
 * Module for processing semantic trails dataset
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class SemanticTrailsPOIProcessData {

	/****
	 * Method to mix a set of specific cities from the semanticTrailsDataset that
	 * matches a foursquare city sent by argument
	 * 
	 * @param checkinFileSemanticTrails
	 *            the full semantic trails file
	 * @param fileMappingSemanticCitiesFoursquareCities
	 *            the file mapping from foursquare and
	 * @param foursquareCitySelected
	 *            the foursquare city to mix
	 * @param outputFile
	 *            the output file containing the checkins of all matching cities
	 */
	public static void obtainCheckinsMixingMappingCitiesFoursquare(String checkinFileSemanticTrails,
			String fileMappingSemanticCitiesFoursquareCities, String foursquareCitySelected, String outputFile) {
		try {
			Set<String> semanticTrailsCities = new HashSet<>();
			Stream<String> stream = Files.lines(Paths.get(fileMappingSemanticCitiesFoursquareCities));
			stream.forEach(line -> {
				String data[] = line.split("\t");
				String semanticCity = data[0].substring(data[0].lastIndexOf('/') + 1);
				String foursquareCity = data[1];
				if (foursquareCity.equals(foursquareCitySelected)) {
					semanticTrailsCities.add(semanticCity);
				}
			});
			stream.close();
			// Now we have the semanticTrailsCities selected

			PrintStream out = new PrintStream(outputFile);
			Stream<String> streamSemanticTrails = Files.lines(Paths.get(checkinFileSemanticTrails));
			streamSemanticTrails.forEach(line -> {
				String[] data = line.split(",");
				String user = data[1];
				String item = data[2].substring(data[2].lastIndexOf('/') + 1);
				String city = data[5].substring(data[5].lastIndexOf('/') + 1);
				String timeStamp = data[7];
				String seqID = data[0];

				if (semanticTrailsCities.contains(city)) {
					out.println(user + "\t" + item + "\t" + 1.0 + "\t" + timeStamp + "\t" + seqID);
				}

			});
			out.close();
			streamSemanticTrails.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/***
	 * Method that will compute for every city from the semantic trails dataset, the
	 * closest foursquare city if the distance is lower to an specific threshold
	 * 
	 * @param foursquareCities
	 * @param semanticTrailsCities
	 * @param outputFile
	 */
	public static void mappingSemanticTrailsCitiesFoursquareCities(String foursquareCities, String semanticTrailsCities,
			double threshold, String outputFile) {
		try {
			Map<String, Tuple2<Double, Double>> mapPrintCities = new HashMap<>();

			Stream<String> stream = Files.lines(Paths.get(foursquareCities));
			stream.forEach(line -> {
				String data[] = line.split("\t");
				String city = data[0].replace(" ", "");
				Double lat = Double.parseDouble(data[1]);
				Double lon = Double.parseDouble(data[2]);
				String countryCode = data[3];

				mapPrintCities.put(countryCode + "_" + city, new Tuple2<>(lat, lon));
			});
			stream.close();

			PrintStream out = new PrintStream(outputFile);

			Stream<String> streamSemantic = Files.lines(Paths.get(semanticTrailsCities));
			streamSemantic.skip(1).forEach(line -> {
				String data[] = line.split(",");
				String semanticCity = data[2];
				Double lat = Double.parseDouble(data[0]);
				Double lon = Double.parseDouble(data[1]);

				double minDistance = Double.MAX_VALUE;
				String countryCodeCityMin = "-";
				for (String countrycodeCity : mapPrintCities.keySet()) {
					double distance = FoursqrProcessData.haversine(lat, lon, mapPrintCities.get(countrycodeCity).v1,
							mapPrintCities.get(countrycodeCity).v2);
					if (distance < minDistance && distance <= threshold) {
						countryCodeCityMin = countrycodeCity;
						minDistance = distance;
					}
				}
				out.println(semanticCity + "\t" + countryCodeCityMin);
			});
			streamSemantic.close();
			out.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/***
	 * Method to obtain the checkins of different cities from the original
	 * SemanticTrails dataset
	 * 
	 * @param fullOriginalFile
	 *            the original complete checkin file from semanti trails
	 * @param citiesFilter
	 *            the array of cities to obtain (it will create 1 file for each
	 *            file)
	 * @param outPathCitiesParsedFiles
	 *            the output path
	 * @param oldNewUsers
	 *            user mapping (optional)
	 * @param oldNewItems
	 *            item mapping (optional)
	 * @param utcOrNot
	 *            boolean indicating if the new timestamp is UTC or NOT
	 */
	public static void obtainCheckingsDifferentCities(String fullOriginalFile, String[] citiesFilter,
			String outPathCitiesParsedFiles, String userMapping, String itemMapping, Boolean utcOrNot) {
		try {
			// Old id -> new Id

			Map<String, Long> mapUsers = new HashMap<>();
			if (userMapping != null) {
				// FoursqrProcessData
				FoursqrProcessData.readMap(userMapping, mapUsers);
			}
			Map<String, Long> mapItems = new HashMap<>();
			if (itemMapping != null) {
				FoursqrProcessData.readMap(itemMapping, mapItems);
			}

			List<String> validCities = Arrays.asList(citiesFilter);

			Map<String, PrintStream> mapPrintCities = new HashMap<>();
			// Fir
			validCities.stream().forEach(city -> {
				try {
					mapPrintCities.put(city, new PrintStream(outPathCitiesParsedFiles + "/" + city + ".txt"));
					System.out.println(city + " " + outPathCitiesParsedFiles + "/" + city + ".txt");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});

			Stream<String> stream = Files.lines(Paths.get(fullOriginalFile));

			// First line is the header
			// The columns of the original file are
			// trail_id,user_id,venue_id,venue_category,venue_schema,venue_city,venue_country,timestamp
			stream.skip(1).forEach(line -> {
				String[] data = line.split(",");
				String user = data[1];
				String item = data[2].substring(data[2].lastIndexOf('/') + 1);
				String city = data[5].substring(data[5].lastIndexOf(':') + 1);

				if (mapPrintCities.containsKey(city)) {

					Long userI = null;
					Long itemI = null;
					if (userMapping != null && userMapping != null) {
						userI = mapUsers.get(user);
						itemI = mapItems.get(item);
					}
					String timeStamp = data[7];
					String seqID = data[0];

					PrintStream out = mapPrintCities.get(city);
					String time = timeStamp;
					if (utcOrNot != null) {
						if (utcOrNot) {
							time = parseDateISO8601SemanticTrailsToTimestampUTC(timeStamp).toString();
						} else {
							time = parseDateISO8601SemanticTrailsToTimestampLocal(timeStamp).toString();
						}
					}
					if (userMapping != null && userMapping != null) {
						out.println(userI + "\t" + itemI + "\t" + 1.0 + "\t" + time + "\t" + seqID);
					} else {
						out.println(user + "\t" + item + "\t" + 1.0 + "\t" + time + "\t" + seqID);
					}
				}

			});
			stream.close();
			for (String pt : mapPrintCities.keySet()) {
				mapPrintCities.get(pt).close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void mappingUserItemsSemanticTrails(String fullOriginalFile, String outputMappingUsers,
			String outputMappingItems) {
		try {
			AtomicInteger countUsers = new AtomicInteger(1);
			AtomicInteger countItems = new AtomicInteger(1);

			Map<String, Integer> oldNewUsers = new TreeMap<>();
			Map<String, Integer> oldNewItems = new TreeMap<>();

			// Our format is user, item, rating, timestamp, seqID
			Stream<String> stream = Files.lines(Paths.get(fullOriginalFile));
			PrintStream outputMapUsers = new PrintStream(outputMappingUsers);
			PrintStream outputMapItems = new PrintStream(outputMappingItems);

			// First line is the header
			// The columns of the original file are
			// trail_id,user_id,venue_id,venue_category,venue_schema,venue_city,venue_country,timestamp
			stream.skip(1).forEach(line -> {
				String[] data = line.split(",");
				String user = data[1];
				String item = data[2].substring(data[2].lastIndexOf('/') + 1);

				if (oldNewUsers.get(user) == null) {
					oldNewUsers.put(user, countUsers.get());
					countUsers.incrementAndGet();
				}

				if (oldNewItems.get(item) == null) {
					oldNewItems.put(item, countItems.get());
					countItems.incrementAndGet();
				}

			});
			stream.close();

			for (String oldUser : oldNewUsers.keySet()) {
				outputMapUsers.println(oldUser + "\t" + oldNewUsers.get(oldUser));
			}

			for (String oldItem : oldNewItems.keySet()) {
				outputMapItems.println(oldItem + "\t" + oldNewItems.get(oldItem));
			}

			outputMapUsers.close();
			outputMapItems.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Long parseDateISO8601SemanticTrailsToTimestampUTC(String date) {
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX", Locale.ENGLISH);
		try {
			Date d = df1.parse(date);
			return d.getTime() / 1000;
		} catch (ParseException e) {
			System.out.println("Date " + date + " not supported");
			throw new IllegalArgumentException("Incorrect date " + date);
		}

	}

	public static Long parseDateISO8601SemanticTrailsToTimestampLocal(String date) {
		String date2 = date.substring(0, date.length() - 6); // 6 for +XX:XX
		date2 += "+00:00"; // deleting the offsets
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX", Locale.ENGLISH);
		try {
			Date d = df1.parse(date2);
			return d.getTime() / 1000;
		} catch (ParseException e) {
			System.out.println("Date " + date2 + " not supported");
			throw new IllegalArgumentException("Incorrect date " + date2);
		}

	}

}
