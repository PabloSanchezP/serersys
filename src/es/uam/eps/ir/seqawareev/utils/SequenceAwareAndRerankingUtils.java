/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.utils;

import static org.ranksys.formats.parsing.Parsers.lp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Collectors;

import org.jooq.lambda.tuple.Tuple2;
import org.ranksys.formats.feature.SimpleFeaturesReader;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.recommenders.AverageDistanceToUserGEO;
import es.uam.eps.ir.crossdomainPOI.recommenders.KDERecommender;
import es.uam.eps.ir.crossdomainPOI.recommenders.PopGeoNN;
import es.uam.eps.ir.crossdomainPOI.recommenders.TrainRecommender;
import es.uam.eps.ir.crossdomainPOI.utils.KernelDensityEstimation;
import es.uam.eps.ir.crossdomainPOI.utils.KernelDensityEstimationCoordinates;
import es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils;
import es.uam.eps.ir.crossdomainPOI.utils.UsersMidPoints;
import es.uam.eps.ir.crossdomainPOI.utils.UsersMidPoints.SCORES_FREQUENCY;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.feature.SimpleFeatureData;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.mf.Factorization;
import es.uam.eps.ir.ranksys.mf.als.HKVFactorizer;
import es.uam.eps.ir.ranksys.mf.als.PZTFactorizer;
import es.uam.eps.ir.ranksys.mf.plsa.PLSAFactorizer;
import es.uam.eps.ir.ranksys.mf.rec.MFRecommender;
import es.uam.eps.ir.ranksys.nn.item.ItemNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.CachedItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.ItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.TopKItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity;
import es.uam.eps.ir.ranksys.rec.Recommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.PopularityRecommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.RandomRecommender;
import es.uam.eps.ir.seqawareev.rec.RankSysTemporalRecommender;
import es.uam.eps.ir.seqawareev.rec.TemporalPopularityRecommender;
import es.uam.eps.ir.seqawareev.rec.cb.BaselineCB;
import es.uam.eps.ir.seqawareev.rec.cb.UserItemProfileContentRecommender;
import es.uam.eps.ir.seqawareev.sim.AbstractTourCachedItemSimilarity;
import es.uam.eps.ir.seqawareev.sim.UserIndexReadingSimilarity;
import es.uam.eps.ir.seqawareev.sim.UserIndexReadingSimilarityRankSysWrapper;
import es.uam.eps.ir.seqawareev.sim.cb.CBCosineUserSimilarity;
import es.uam.eps.ir.seqawareev.sim.location.ItemLocationSimilarity;
import es.uam.eps.ir.seqawareev.sim.probability.ItemFeatureTransitionSimilarity;
import es.uam.eps.ir.seqawareev.sim.probability.ItemTransitionSimilarity;
import es.uam.eps.ir.seqawareev.smooth.JelineckMercer;
import es.uam.eps.ir.seqawareev.smooth.NoSmoothingCond;
import es.uam.eps.ir.seqawareev.smooth.NoSmoothingPrior;
import es.uam.eps.ir.seqawareev.smooth.SmoothingProbabilityIF;
import es.uam.eps.ir.seqawareev.structures.FeatureItemTransitionMatrix;
import es.uam.eps.ir.seqawareev.structures.ItemTransitionMatrix;
import es.uam.eps.ir.seqawareev.tour.abstracts.AbstractFastTourRecommender;
import es.uam.eps.ir.seqawareev.tour.recommenders.GenericItemSimilarityTourRecommender;
import es.uam.eps.ir.seqawareev.tour.recommenders.POI.RankGeoFMRecommender;
import es.uam.eps.ir.seqawareev.utils.CBBaselinesUtils.USERTRANSFORMATION;
import es.uam.eps.ir.seqawareev.wrappers.SimpleFastTemporalFeaturePreferenceDataWrapper;
import es.uam.eps.ir.seqawareev.wrappers.SimpleFastTemporalFeaturePreferenceDataWrapper.RepetitionsStrategyPreference;

public final class SequenceAwareAndRerankingUtils {

	public static SmoothingProbabilityIF getSmoothingProbability(String smoothString, double lambda) {
		if (smoothString == null) {
			System.out.println("Smoothing probability is null");
			return null;
		}
		switch (smoothString) {
		case "JelineckMercer":
			return new JelineckMercer(lambda);
		case "NoSmoothingPrior":
			return new NoSmoothingPrior();
		case "NoSmoothingCond":
			return new NoSmoothingCond();
		default:
			System.out.println("Smoothing probability is null");
			return null;
		}

	}

	public static Recommender<Long, Long> obtRankSysTemporalRecommeder(String rec, String pathSimilarity,
			FastTemporalPreferenceDataIF<Long, Long> dataTemporalTrain, int kNeighbours, String similarity,
			double lambdaTemporal, double confidence) {
		switch (rec) {
		// Temporal decay of Ding adapted for an UB approach
		case "RankSysTemporalRec":
		case "RankSysTemporalRecommender": {
			System.out.println("RankSysTemporalRecommender");
			System.out.println("SimPath: " + pathSimilarity);
			System.out.println("Confidence: " + confidence);
			System.out.println("Neighbours: " + kNeighbours);
			System.out.println("Temporal lambda: " + lambdaTemporal);

			UserIndexReadingSimilarity<Long> simUNR = new UserIndexReadingSimilarity<Long>(pathSimilarity, confidence,
					lp, dataTemporalTrain);

			UserSimilarity<Long> simRankSys = new UserIndexReadingSimilarityRankSysWrapper<Long, Long, Long>(
					dataTemporalTrain, simUNR);
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simRankSys, kNeighbours);
			return new RankSysTemporalRecommender<Long, Long>(dataTemporalTrain, urneighborhood, 1, lambdaTemporal,
					24 * 3600, 2.0);
		}
		case "RankSysTemporalRecNoRead":
		case "RankSysTemporalRecommenderNoRead": {
			System.out.println("RankSysTemporalRecNoRead");
			System.out.println("Similarity: " + similarity);
			System.out.println("Neighbours: " + kNeighbours);
			System.out.println("Temporal lambda: " + lambdaTemporal);
			FastPreferenceData<Long, Long> dataForSim = SimpleFastTemporalFeaturePreferenceDataWrapper
					.createFastPreferenceData(dataTemporalTrain, RepetitionsStrategyPreference.SUM);
			UserSimilarity<Long> simRankSys = es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils
					.obtRanksysUserSimilarity(dataForSim, similarity);
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simRankSys, kNeighbours);
			return new RankSysTemporalRecommender<Long, Long>(dataTemporalTrain, urneighborhood, 1, lambdaTemporal,
					24 * 3600, 2.0);
		}
		case "TemporalPopularityRecommender": {
			System.out.println("TemporalPopularityRecommender");
			return new TemporalPopularityRecommender<>(dataTemporalTrain);
		}

		default:
			System.out.println("Carefull. Temporal Recommender is null (recommender not recognized)");
			return null;
		}

	}

	/***
	 * Method to obtain the last indexes in a equivalent way that are obtained in
	 * the LCS algorithm
	 * 
	 * @param timeStampOrderedU1
	 *            the items consumed by the first user (ordered by timestamp, from
	 *            the lower to the higher one)
	 * @param timeStampOrderedU2
	 *            the items consumed by the second user (ordered by timestamp, from
	 *            the lower to the higher one)
	 * @return a tuple indicating the last indexes of both users
	 */
	public static Tuple2<Integer, Integer> obtainBestIndexesLCSEquivalent(List<Long> timeStampOrderedU1,
			List<Long> timeStampOrderedU2) {
		int indexU1 = -1;
		int indexU2 = -1;

		Set<Long> timeStampOrderedU1Set = new HashSet<>(timeStampOrderedU1);
		Set<Long> timeStampOrderedU2Set = new HashSet<>(timeStampOrderedU2);

		// Obtain the index of the first user
		for (int i = timeStampOrderedU1.size() - 1; i >= 0; i--) {
			Long itemU1 = timeStampOrderedU1.get(i);

			if (timeStampOrderedU2Set.contains(itemU1)) {
				indexU1 = i;
				break;
			}

		}

		// Obtain the index of the second user
		for (int i = timeStampOrderedU2.size() - 1; i >= 0; i--) {
			Long itemU2 = timeStampOrderedU2.get(i);
			if (timeStampOrderedU1Set.contains(itemU2)) {
				indexU2 = i;
				break;
			}

		}
		return new Tuple2<Integer, Integer>(indexU1, indexU2);

	}

	public static Map<Long, Double> obtainMapOrdered(List<Tuple2<Double, List<Tuple2<Long, Double>>>> list) {
		Map<Long, Double> m = new HashMap<Long, Double>();
		for (Tuple2<Double, List<Tuple2<Long, Double>>> t : list) { // External list. First element of tuple is the
																	// neigh sim. Second element list of items rated by
																	// that neighbour

			List<Tuple2<Long, Double>> items = t.v2;

			for (Tuple2<Long, Double> item : items) {
				double s = t.v1 * item.v2;
				if (m.get(item.v1) == null) {
					// Rating of the neighbour multiplied by its similarity
					m.put(item.v1, s);
				} else {
					m.put(item.v1, s + m.get(item.v1));
				}
			}

		}

		return m;
	}

	public static AbstractFastTourRecommender<Long, Long> obtRankSysTourRecommeder(String rec,
			FastTemporalPreferenceDataIF<Long, Long> prefDataTemporal, FastPreferenceData<Long, Long> prefData,
			String fileCoordinatesItem, String FeatureFile, SmoothingProbabilityIF smooth, double lambda, int cached,
			int numberItemsCompute) {
		try {
			switch (rec) {
			case "NNVenuesRecommender": {
				System.out.println("NNVenuesRecommender");

				Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
						.POICoordinatesMap(fileCoordinatesItem, lp);
				float[][] distanceMatrix = ProcessSessionData.distanceMatrix(
						prefData.getAllItems().collect(Collectors.toList()), prefData, mapCoordinates, false);
				AbstractTourCachedItemSimilarity<Long> iSim = new ItemLocationSimilarity<>(prefData, distanceMatrix,
						cached, numberItemsCompute);
				return new GenericItemSimilarityTourRecommender<Long, Long>(prefData, iSim);
			}

			case "ItemToItemMC": {
				System.out.println("ItemToItemMC");

				ItemTransitionMatrix<Long, Long> im = new ItemTransitionMatrix<Long, Long>(prefDataTemporal);
				im.initialize();
				AbstractTourCachedItemSimilarity<Long> iSim = new ItemTransitionSimilarity<>(prefDataTemporal, im,
						smooth, cached, numberItemsCompute);
				return new GenericItemSimilarityTourRecommender<Long, Long>(prefDataTemporal, iSim);
			}
			case "FeatureToFeature": {
				System.out.println("FeatureToFeature");
				/*
				 * // Previous FeatureData<Long, Long, Double> featureData = null; featureData =
				 * SimpleFeatureData.load(SimpleFeaturesReader.get().read(FeatureFile, lp, lp));
				 * FeatureItemTransitionMatrix<Long, Long, Long> fItemTrans = new
				 * FeatureItemTransitionMatrix<Long, Long, Long>(prefDataTemporal, featureData);
				 * fItemTrans.initialize(); FeatureToFeatureMC<Long, Long, Long> fIt = new
				 * FeatureToFeatureMC<>(fItemTrans, prefDataTemporal, featureData, smooth);
				 * return fIt;
				 */
				FeatureData<Long, Long, Double> featureData = null;
				featureData = SimpleFeatureData.load(SimpleFeaturesReader.get().read(FeatureFile, lp, lp));
				FeatureItemTransitionMatrix<Long, Long, Long> fItemTrans = new FeatureItemTransitionMatrix<Long, Long, Long>(
						prefDataTemporal, featureData);
				fItemTrans.initialize();
				AbstractTourCachedItemSimilarity<Long> iSim = new ItemFeatureTransitionSimilarity<>(prefDataTemporal,
						fItemTrans, smooth, cached, numberItemsCompute);
				return new GenericItemSimilarityTourRecommender<Long, Long>(prefDataTemporal, iSim);
			}

			default:
				System.out.println("Carefull. Tour Recommender is null (recommender not recognized)");
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * * Method to get the largest timestamp associated with a datamodel
	 *
	 * @param dataModel
	 *            the datamodel
	 * @return a
	 */
	public static <U, I> Tuple2<Long, Long> getMinMaxTimestampOfDataset(FastTemporalPreferenceDataIF<U, I> dataModel) {
		AtomicLong minTime = new AtomicLong(Long.MAX_VALUE);
		AtomicLong maxTime = new AtomicLong(-1);

		dataModel.getUsersWithPreferences().forEach(u -> {
			long maxOfUser = dataModel.getUserPreferences(u).mapToLong(p -> p.v3).max().getAsLong();
			if (maxTime.get() < maxOfUser) {
				maxTime.set(maxOfUser);
			}
			long minOfUser = dataModel.getUserPreferences(u).mapToLong(p -> p.v3).min().getAsLong();
			if (minTime.get() > minOfUser) {
				minTime.set(minOfUser);
			}
		});

		return new Tuple2<>(minTime.get(), maxTime.get());
	}

	public static Recommender<Long, Long> obtRankSysRecommeder(String rec, String additionalRecs, String similarity,
			FastPreferenceData<Long, Long> data, int kNeighbours, int kFactorizer, double alphaF, double lambdaF,
			int numInteractions, String originalPOICoordsFile, SCORES_FREQUENCY score, double learnRate, double maxRate,
			double regBias, double decay, boolean isboldDriver, double regImpItem, boolean normalize, double epsilon,
			double c, FeatureData<Long, String, Double> featureData, USERTRANSFORMATION ut) {

		switch (rec) {
		case "RndRec":
		case "RandomRecommender": {
			System.out.println("RandomRecommender");
			return new RandomRecommender<>(data, data);
		}

		case "PopRec":
		case "PopularityRecommender": {
			System.out.println("PopularityRecommender");
			return new PopularityRecommender<>(data);
		}
		case "UBKnnRec":
		case "UserNeighborhoodRecommender": { // User based. Pure CF recommendation
			es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simUNR = es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils
					.obtRanksysUserSimilarity(data, similarity);
			if (simUNR == null) {
				return null;
			} else {
				System.out.println("UserNeighborhoodRecommender");
				System.out.println("kNeighs: " + kNeighbours);
				System.out.println("Sim: " + similarity);
			}
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simUNR, kNeighbours);
			return new UserNeighborhoodRecommender<>(data, urneighborhood, 1);
		}

		case "IBKnnRec":
		case "ItemNeighborhoodRecommender": {// Item based. Pure CF recommendation
			es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity<Long> simINR = es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils
					.obtRanksysItemSimilarity(data, similarity);
			if (simINR == null) {
				return null;
			} else {
				System.out.println("ItemNeighborhoodRecommender");
				System.out.println("kNeighs: " + kNeighbours);
				System.out.println("Sim: " + similarity);
			}
			ItemNeighborhood<Long> neighborhood = new TopKItemNeighborhood<>(simINR, kNeighbours);
			neighborhood = new CachedItemNeighborhood<>(neighborhood);
			return new ItemNeighborhoodRecommender<>(data, neighborhood, 1);
		}
		case "TrainRec":
		case "TrainRecommender": {
			System.out.println("TrainRecommender");
			return new TrainRecommender<>(data, false);
		}

		case "TrainRecReverse":
		case "TrainRecommenderReverse": {
			System.out.println("TrainRecommender");
			return new TrainRecommender<>(data, true);
		}

		case "MFRecHKV":
		case "MFRecommenderHKV": { // Matrix factorization
			int k = kFactorizer;
			double lambda = lambdaF;
			double alpha = alphaF;
			int numIter = numInteractions;
			System.out.println("MFRecommenderHKV");
			System.out.println("kFactors: " + k);
			System.out.println("lambda: " + lambda);
			System.out.println("alpha: " + alpha);
			System.out.println("numIter: " + numIter);

			DoubleUnaryOperator confidence = x -> 1 + alpha * x;
			Factorization<Long, Long> factorization = new HKVFactorizer<Long, Long>(lambda, confidence, numIter)
					.factorize(k, data);
			return new MFRecommender<>(data, data, factorization);
		}
		case "MFRecPZT":
		case "MFRecommenderPZT": { // Probabilistic latent semantic analysis of
			// Hofmann 2004
			int k = kFactorizer;
			double lambda = lambdaF;
			double alpha = alphaF;
			int numIter = numInteractions;
			System.out.println("MFRecommenderPZT");
			System.out.println("kFactors: " + k);
			System.out.println("lambda: " + lambda);
			System.out.println("alpha: " + alpha);
			System.out.println("numIter: " + numIter);

			DoubleUnaryOperator confidence = x -> 1 + alpha * x;
			Factorization<Long, Long> factorization = new PZTFactorizer<Long, Long>(lambda, confidence, numIter)
					.factorize(k, data);
			return new MFRecommender<>(data, data, factorization);
		}
		case "MFRecPLSA":
		case "MFRecommenderPLSA": { // Probabilistic latent semantic analysis of
			// Hofmann 2004
			int k = kFactorizer;
			int numIter = numInteractions;
			System.out.println("MFRecommenderPLSA");
			System.out.println("kFactors: " + k);
			System.out.println("numIter: " + numIter);

			Factorization<Long, Long> factorization = new PLSAFactorizer<Long, Long>(numIter).factorize(k, data);
			return new MFRecommender<>(data, data, factorization);
		}

		/**
		 * POI Recommendation algorithms
		 */
		// Basic Geographical recommender
		case "AvgDis":
		case "AverageDistanceUserGEO": {
			System.out.println("AverageDistanceUserGEO");

			Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
					.POICoordinatesMap(originalPOICoordsFile, lp);

			UsersMidPoints<Long, Long> usermid = new UsersMidPoints<>(data, mapCoordinates, score);
			return new AverageDistanceToUserGEO<>(data, usermid);
		}
		case "KDERec":
		case "KDEstimatorRecommender": {
			System.out.println("KDEstimatorRecommender");

			Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
					.POICoordinatesMap(originalPOICoordsFile, lp);
			Map<Long, List<Tuple2<Double, Double>>> userCoordinates = SequentialRecommendersUtils
					.getCoordinatesUser(data, mapCoordinates);
			KernelDensityEstimation<Long> kde = new KernelDensityEstimationCoordinates<>(userCoordinates);
			return new KDERecommender<>(data, kde, mapCoordinates);
		}
		case "RankGeoFMRec":
		case "RankGeoFMRecommender": {
			System.out.println("RankGeoFMRecommender");
			System.out.println("kFactors: " + kFactorizer);
			System.out.println("kNeighbours " + kNeighbours);
			System.out.println("decay: " + decay);
			System.out.println("isboldDriver: " + isboldDriver);
			System.out.println("numIter: " + numInteractions);
			System.out.println("learnRate: " + learnRate);
			System.out.println("maxRate: " + maxRate);
			System.out.println("alpha: " + alphaF);
			System.out.println("c: " + c);
			System.out.println("epsilon: " + epsilon);

			Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
					.POICoordinatesMap(originalPOICoordsFile, lp);
			float[][] distanceMatrix = ProcessSessionData
					.distanceMatrix(data.getAllItems().collect(Collectors.toList()), data, mapCoordinates, false);

			// C and epsilon
			return new RankGeoFMRecommender<>(data, distanceMatrix, kFactorizer, kNeighbours, alphaF, c, epsilon,
					numInteractions, learnRate, maxRate, isboldDriver, decay);
		}

		// Algorithm that combines popularity, knn and geospatial
		case "PopGeoNN": {
			PopularityRecommender<Long, Long> popRec = new PopularityRecommender<Long, Long>(data);
			Map<Long, Tuple2<Double, Double>> mapCoordinates = SequentialRecommendersUtils
					.POICoordinatesMap(originalPOICoordsFile, lp);
			UsersMidPoints<Long, Long> usermid = new UsersMidPoints<>(data, mapCoordinates, score);

			AverageDistanceToUserGEO<Long, Long> distanceRev = new AverageDistanceToUserGEO<Long, Long>(data, usermid);

			es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simUNR = es.uam.eps.ir.crossdomainPOI.utils.SequentialRecommendersUtils
					.obtRanksysUserSimilarity(data, similarity);
			if (simUNR == null) {
				return null;
			}
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simUNR, kNeighbours);
			UserNeighborhoodRecommender<Long, Long> ubRec = new UserNeighborhoodRecommender<>(data, urneighborhood, 1);
			System.out.println("PopGeoNN");
			System.out.println("kNeighbours: " + kNeighbours);
			System.out.println("Sim: " + similarity);

			return new PopGeoNN<>(data, popRec, distanceRev, ubRec);
		}

		case "UIProfCBRec":
		case "UserItemProfileContentRecommender": {
			return new UserItemProfileContentRecommender<>(data, featureData, normalize);

		}
		case "UBCBRec":
		case "UBContentBasedRecommender": {
			System.out.println("kNeighs: " + kNeighbours);
			Map<Long, Map<String, Double>> mapUsers = CBBaselinesUtils.getUsersCBTransformation(data, featureData, ut,
					normalize);
			Map<Integer, Map<String, Double>> mapUidx = CBBaselinesUtils.getUidxCBTransFormation(data, mapUsers);
			CBCosineUserSimilarity<Long, String> ucbCosSim = new CBCosineUserSimilarity<Long, String>(data, mapUidx);
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(ucbCosSim, kNeighbours);
			return new UserNeighborhoodRecommender<>(data, urneighborhood, 1);

		}
		case "BaseCB":
		case "BaselineCB": {
			return new BaselineCB<>(data, featureData);
		}

		default:
			System.out.println("Carefull. Recommender is null");
			return null;
		}

	}

}
