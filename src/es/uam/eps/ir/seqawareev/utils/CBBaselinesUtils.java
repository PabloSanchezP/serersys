/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.seqawareev.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.FastTemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.interfaces.TemporalPreferenceDataIF;
import es.uam.eps.ir.crossdomainPOI.datamodel.temporal.preferences.IdTimePref;
import es.uam.eps.ir.ranksys.core.feature.FeatureData;
import es.uam.eps.ir.ranksys.core.preference.IdPref;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.novelty.temporal.ItemFreshness;

/***
 * Class for Content based utils
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class CBBaselinesUtils {
	public enum USERTRANSFORMATION {
		WITHRATING, WITHOUTRATING
	};

	public enum ITEMTRANSFORMATION {
		BINARY, TF_IDF
	};

	/***
	 * Method to compute the cosine similarity between two maps
	 * 
	 * @param map
	 * @param map2
	 * @return
	 */
	public static <F> double cosineCBFeatureSimilarity(Map<F, Double> map, Map<F, Double> map2) {
		if (map == null || map2 == null) {
			return 0.0;
		}
		double mod1 = 0;
		double mod2 = 0;
		double res = 0;
		// Here we compute numerator and denominator of first map
		for (F idFeature : map.keySet()) {
			if (map2.get(idFeature) != null) {
				res += map.get(idFeature) * map2.get(idFeature);
			}
			mod1 += Math.pow(map.get(idFeature), 2);
		}

		for (F idFeature : map2.keySet()) {
			mod2 += Math.pow(map2.get(idFeature), 2);
		}

		return res / Math.sqrt(mod1 * mod2);
	}

	/***
	 * Method to obtain a content-based profile from a user
	 * 
	 * @param fData
	 *            the feature data
	 * @param data
	 *            the train data
	 * @param user
	 *            the user
	 * @return a map in which for each feature, we have the score of the user for
	 *         each feature
	 */
	private static <U, I, F> Map<F, Double> getUserCBTransformation(FeatureData<I, F, Double> fData,
			Stream<? extends IdPref<I>> preferencesUser, USERTRANSFORMATION ut, boolean normalize) {
		Map<F, Double> result = new HashMap<>();

		// preferences user
		preferencesUser.forEach(pref -> {
			double ratingItem = pref.v2;
			fData.getItemFeatures(pref.v1).forEach(tuplf -> {
				F featureItem = tuplf.v1;
				double valUser = usertransformation(ut, ratingItem, tuplf.v2);
				if (result.get(featureItem) == null) {
					result.put(featureItem, 0.0);
				}
				result.put(featureItem, result.get(featureItem) + valUser);

			});
		});

		// normalize
		if (normalize) {
			normalizeMap(result);
		}

		return result;
	}

	/**
	 * Method to get the user transformation by the freshness
	 * 
	 * @param fData
	 *            the feature data
	 * @param freshnessItem
	 *            the freshness of the item
	 * @param tc
	 *            the timestampCalculator
	 * @param preferencesUser
	 *            the preferences of the user
	 * @param ut
	 *            the user transformation
	 * @param normalize
	 *            the normalization
	 * @return
	 */
	private static <U, I, F> Map<F, Double> getUserCBTransformationFreshness(FeatureData<I, F, Double> fData,
			ItemFreshness<U, I> freshnessItem, Double timestampforNormalizeUser,
			Stream<? extends IdTimePref<I>> preferencesUser, USERTRANSFORMATION ut, boolean normalize) {
		Map<F, Double> result = new HashMap<>();

		// preferences user
		preferencesUser.forEach(pref -> {
			double ratingItem = pref.v2;
			fData.getItemFeatures(pref.v1).forEach(tuplf -> {
				F featureItem = tuplf.v1;

				// Take into account the freshness of the item
				double valUser = usertransformation(ut, ratingItem, tuplf.v2) * freshnessItem.novelty(pref.v1) * pref.v3
						/ timestampforNormalizeUser;
				if (result.get(featureItem) == null) {
					result.put(featureItem, 0.0);
				}
				result.put(featureItem, result.get(featureItem) + valUser);

			});
		});

		// normalize
		if (normalize) {
			normalizeMap(result);
		}

		return result;
	}

	/***
	 * Method to obtain the transformation of all users by the features of the items
	 * they have consumed
	 * 
	 * @param prefData
	 * @param fData
	 * @param ut
	 * @param normalize
	 * @return
	 */
	public static <U, I, F> Map<U, Map<F, Double>> getUsersCBTransformationFreshness(
			TemporalPreferenceDataIF<U, I> prefData, FeatureData<I, F, Double> fData, ItemFreshness<U, I> freshnessItem,
			Double timestampforNormalizeUser, USERTRANSFORMATION ut, boolean normalize) {
		Map<U, Map<F, Double>> result = new HashMap<>();
		prefData.getUsersWithPreferences().forEach(u -> {
			result.put(u, getUserCBTransformationFreshness(fData, freshnessItem, timestampforNormalizeUser,
					prefData.getUserPreferences(u), ut, normalize));
		});
		return result;
	}

	public static <U, I, F> Map<Integer, Map<F, Double>> getUidxCBTransFormation(FastUserIndex<U> uindx,
			Map<U, Map<F, Double>> originalMap) {
		Map<Integer, Map<F, Double>> result = new HashMap<>();
		for (Entry<U, Map<F, Double>> entry : originalMap.entrySet()) {
			result.put(uindx.user2uidx(entry.getKey()), entry.getValue());
		}
		return result;
	}

	public static <U, I, F> Map<Integer, Map<F, Double>> getIidxCBTransFormation(FastItemIndex<I> iindx,
			Map<I, Map<F, Double>> originalMap) {
		Map<Integer, Map<F, Double>> result = new HashMap<>();
		for (Entry<I, Map<F, Double>> entry : originalMap.entrySet()) {
			result.put(iindx.item2iidx(entry.getKey()), entry.getValue());
		}
		return result;
	}

	/***
	 * Method to obtain the transformation of all users by the features of the items
	 * they have consumed
	 * 
	 * @param prefData
	 * @param fData
	 * @param ut
	 * @param normalize
	 * @return
	 */
	public static <U, I, F> Map<U, Map<F, Double>> getUsersCBTransformation(PreferenceData<U, I> prefData,
			FeatureData<I, F, Double> fData, USERTRANSFORMATION ut, boolean normalize) {
		Map<U, Map<F, Double>> result = new HashMap<>();
		prefData.getUsersWithPreferences().forEach(u -> {
			result.put(u, getUserCBTransformation(fData, prefData.getUserPreferences(u), ut, normalize));
		});
		return result;
	}

	/***
	 * Method to obtain the value that a user has for a specific feature (taking
	 * into account the rating of the item or not)
	 * 
	 * @param ut
	 * @param ratingItem
	 *            the rating
	 * @param featureValue
	 *            the feature value of the item
	 * @return
	 */
	private static double usertransformation(USERTRANSFORMATION ut, Double ratingItem, Double featureValue) {
		switch (ut) {
		case WITHRATING:
			return featureValue * ratingItem;
		default:
			return featureValue;
		}
	}

	/**
	 * Method to compute item transformation of features
	 */
	public static <U, I, F> Map<I, Map<F, Double>> itemTransformation(PreferenceData<U, I> dataModel,
			FeatureData<I, F, Double> fData, Map<F, Double> featuresItems, ITEMTRANSFORMATION it, boolean normalize) {
		switch (it) {
		case BINARY:
			return binaryItemTranformation(dataModel, fData);
		default:
			return TFIDFItemTranformation(dataModel, fData, featuresItems, normalize);
		}

	}

	public static <U, I, F> Map<F, Double> computeFeaturesItemsSimple(PreferenceData<U, I> dataModel,
			FeatureData<I, F, Double> featureData, boolean normalize) {
		Map<F, Double> featuresItems = new HashMap<>();
		dataModel.getAllItems().forEach(item -> {
			featureData.getItemFeatures(item).forEach(tuple2feature -> {
				if (featuresItems.get(tuple2feature.v1) == null) {
					featuresItems.put(tuple2feature.v1, 0.0);
				}
				featuresItems.put(tuple2feature.v1, featuresItems.get(tuple2feature.v1) + tuple2feature.v2);
			});
		});

		if (normalize) {
			normalizeMap(featuresItems);
		}

		return featuresItems;
	}

	/***
	 * binaryItemTransformation: In this method, we will obtain the feature values
	 * for each item
	 */
	public static <U, I, F> Map<I, Map<F, Double>> binaryItemTranformation(PreferenceData<U, I> dataModel,
			FeatureData<I, F, Double> fData) {
		Map<I, Map<F, Double>> itemFeatures = new HashMap<>();

		dataModel.getAllItems().forEach(item -> {
			if (itemFeatures.get(item) == null) {
				itemFeatures.put(item, new HashMap<F, Double>());
			}

			fData.getItemFeatures(item).forEach(tuple2feature -> {
				itemFeatures.get(item).put(tuple2feature.v1, tuple2feature.v2);
			});
		});

		return itemFeatures;
	}

	/**
	 * TFIDFItemTranformation: In this method items will have tf-idf value of a
	 * specific feature
	 */
	public static <U, I, F> Map<I, Map<F, Double>> TFIDFItemTranformation(PreferenceData<U, I> dataModel,
			FeatureData<I, F, Double> fData, Map<F, Double> featuresItems, boolean normalize) {
		Map<I, Map<F, Double>> itemFeatures = new HashMap<>();

		dataModel.getAllItems().forEach(item -> {
			if (itemFeatures.get(item) == null) {
				itemFeatures.put(item, new HashMap<F, Double>());
			}
			fData.getItemFeatures(item).forEach(tuple2feature -> {
				double tfidfItem = tfIdfItemFeature(dataModel, featuresItems, item, tuple2feature.v1);
				itemFeatures.get(item).put(tuple2feature.v1, tfidfItem);
			});

			if (normalize) {
				normalizeMap(itemFeatures.get(item));
			}

		});
		return itemFeatures;
	}

	public static <U, I, F> Map<I, Map<F, Double>> itemTransformationByUsers(FastPreferenceData<U, I> dataModel,
			FeatureData<I, F, Double> featureData, Map<U, Map<F, Double>> userFeatures, boolean normalize) {

		Map<I, Map<F, Double>> itemFeatures = new HashMap<>();

		// For each item in the system
		dataModel.getAllItems().forEach(item -> {
			if (itemFeatures.get(item) == null) {
				itemFeatures.put(item, new HashMap<F, Double>());
			}
			// We add every users preferences that have consumed the item
			dataModel.getItemPreferences(item).forEach(prefUser -> {
				Map<F, Double> mapUser = userFeatures.get(prefUser.v1);

				for (Map.Entry<F, Double> entry : mapUser.entrySet()) {
					if (itemFeatures.get(item).get(entry.getKey()) == null) {
						itemFeatures.get(item).put(entry.getKey(), 0.0);
					}
					itemFeatures.get(item).put(entry.getKey(),
							itemFeatures.get(item).get(entry.getKey()) + mapUser.get(entry.getKey()));
				}
			});

			if (normalize) {
				normalizeMap(itemFeatures.get(item));
			}
		});
		return itemFeatures;
	}

	public static <U, I, F> Map<I, Map<F, Double>> itemTransformationByUsersFreshness(
			FastTemporalPreferenceDataIF<U, I> dataModel, FeatureData<I, F, Double> featureData,
			Map<U, Map<F, Double>> userFeatures, boolean normalize) {

		Map<I, Map<F, Double>> itemFeatures = new HashMap<>();

		// For each item in the system
		dataModel.getAllItems().forEach(item -> {
			if (itemFeatures.get(item) == null) {
				itemFeatures.put(item, new HashMap<F, Double>());
			}
			// We add every users preferences that have consumed the item
			dataModel.getItemPreferences(item).forEach(prefUser -> {
				Map<F, Double> mapUser = userFeatures.get(prefUser.v1);

				for (Map.Entry<F, Double> entry : mapUser.entrySet()) {
					if (itemFeatures.get(item).get(entry.getKey()) == null) {
						itemFeatures.get(item).put(entry.getKey(), 0.0);
					}
					itemFeatures.get(item).put(entry.getKey(),
							itemFeatures.get(item).get(entry.getKey()) + mapUser.get(entry.getKey()));
				}
			});

			if (normalize) {
				normalizeMap(itemFeatures.get(item));
			}
		});
		return itemFeatures;
	}

	/***
	 * Method to compute tf-idf of an item
	 * 
	 * @param idItem
	 *            the id of a item
	 * @param idFeature
	 *            the id of a feature
	 * @return tf-idf of a feature
	 */
	private static <U, I, F> double tfIdfItemFeature(PreferenceData<U, I> dataModel, Map<F, Double> featuresItems,
			I idItem, F idFeature) {
		// TF is 1
		double n = dataModel.getAllItems().count(); // Total of items in the system
		double nf = featuresItems.get(idFeature); // Total of items that have that feature
		return 1 * Math.log10(n / nf);
	}

	/***
	 * Method to normalize a map so that all the values sum 1
	 * 
	 * @param map
	 */
	public static <T> void normalizeMap(Map<T, Double> map) {
		double allValues = map.values().stream().mapToDouble(d -> d).sum();
		for (T value : map.keySet()) {
			map.put(value, map.get(value) / allValues);
		}
	}

}
