#!/bin/bash
#Necessary to execute with ./
: '
Experiments for the city of Tokyo (Foursquare dataset)
'

# Global variables for jvm
JAR=target/SeReRSys-0.0.1-SNAPSHOT.jar
jvmMemory=-Xmx30G
javaCommand=java

recPrefix=rec
itemsRecommended=100 # Number of items that will be recommender for each user
recommendationFolder=RecommendationFolder
SimFolder=Sims
simPrefix=sim

# Neighbours for knn approaches
allneighbours="40 60 80 100 120"
# Temporal lambda for Ding (Temporal decay recommender)
lambdaDing="0.05 0.1"

# Parameters for HKV model
allKFactorizerRankSys="10 50 100"
allLambdaFactorizerRankSys="0.1 1"
allAlphaFactorizerRankSys="0.1 1"

# Backward forward approach parameters
allindexesBackwardForward="5 10"
normalizerBackwardForward="defaultnorm stdnorm"

# Smoothing conditions
smooths="NoSmoothingCond JelineckMercer"
lambdaSmooths="0.1 0.5 0.9"


originalCities=OriginalCitiesFoursquare
coordFile=$originalCities/"POIS_Coords_Foursquare.txt"

destinationSessionCities=SessionCitiesTrainFiles

: '
T_I is train items (item candidates are the items in the training set not seen by target user)
and I_I_T is items in train (item candidates are the items in the training set, seen or not by the user)
'
recStrats="T_I" #"I_I_T"
cutoffs="5,10,20,50"
nonaccresultsPrefix="naeval"

rerankers="LCSReranker DistanceTourReranker FeatureMCReranker RandomReranker ItemMCReranker OracleReranker FeatureSuffixTreeReranker UBReranker"
lambdaRerankers="0 0.1 0.5 0.9"
lengthPatternRerankers="2 4"
fillStrats="ORIGINAL_ORDER"

typeFREQTRAIN=FrTr # When we are using the aggregating train
typeTEMPTRAIN=ReTr # When we are using the train with repetitions
typeRecommendationStrategy=RStg # The recommendation strategy
categories_lvl="1" #"2" #So far we are only using level2


resultsFolder=ResultsFolder"$cutoffs"
resultsFolderFsItemTest=ResultsFolderFsItemTest"$cutoffs"



# Parametrocs Fossil
fossilDir=Fossil_RankingModification
allRuiningHEKs="2 5 10 20"
allRuiningHELs="1 2 3"
allRuiningHeLambdas="0.1 0.2"

# Caser parameters
caser_Ls="2"
caser_Ts="1 2"
caser_n_iters="30" # default is 50, but i will use 30
caser_seeds="1234"
caser_batch_sizes="512"
caser_learning_rates="0.003"
caser_l2s="0.000001"
caser_neg_samples="3"
caser_use_cuda="False"
caser_ds="10 50"
caser_nvs="4"
caser_nhs="4 16"
caser_drops="0.5"
caser_ac_convs="relu"
caser_ac_fcs="relu"
pathCaser=Caser_python_adapted



mymedialitePath=MyMediaLite-3.11/bin
BPRFactors=$allKFactorizerRankSys
BPRBiasReg="0 0.5 1"
BPRLearnRate=0.05
BPRNumIter=50
BPRRegU="0.0025 0.001 0.005 0.01 0.1"
BPRRegJ="0.00025 0.0001 0.0005 0.001 0.01"
extensionMyMediaLite=MyMedLt

fullpathMatlab="$(locate matlab | grep bin/matlab | head -1)"
# CAUTION. This command is used to locate the instalation of Matlab in your computer. If there is an error launching the script, substitute the variable pathMatlab with the full path of your matlab executable
pathMatlab="$(dirname $fullpathMatlab)"

fullPath="$(pwd)"
pathIrenMF=$fullPath/IRenMF
pathIrenMFTesWithTrain=$fullPath/IRenMFTestWithTrain
pathDest=$fullPath

# Variables for IrenMF (paper of ExperimentalEvaluation. Most of these variables are updated in the configureFile)
lambda1=0.015
lambda2=0.015
GeoNN=10
extensionCoords=_Coords.txt


allKIRENMF="100 50"
allAlphaIRENMF="0.4 0.6"
allLambda3IRENMF="1 0.1"
clustersIRENMF="5 50"


# For rankGeoFM
cs_RANKGEOFM="1"
alphas_RANKGEOFM="0.1 0.2"
epsilons_RANKGEOFM="0.3"
kfactors_RANKGEOFM=$allKIRENMF
ns_RANKGEOFM="10 50 100 200"
iters_RANKGEOFM="120"
decays_RANKGEOFM="1"
isboldDriver_RANKGEOFM="true"
learnRates_RANKGEOFM="0.001"
maxRates_RANKGEOFM="0.001"

candidatesRerank="20" #"$itemsRecommended 20 50"


# For IRenMF
function obtainConfigureFile() #The arguments are the k, the alpha and the lambda3
{
  configureFileSimple=""
  if [ "$1" == "100" ] && [ "$2" == "0.4" ] && [ "$3" == "1" ] && [ "$4" == "50" ] ; then
    configureFileSimple="configure00" #Configure 0
  fi

  if [ "$1" == "100" ] && [ "$2" == "0.4" ] && [ "$3" == "0.1" ] && [ "$4" == "50" ] ; then
    configureFileSimple="configure01" #Configure 1
  fi

  if [ "$1" == "100" ] && [ "$2" == "0.6" ] && [ "$3" == "1" ] && [ "$4" == "50" ] ; then
    configureFileSimple="configure02" #Configure 2
  fi

  if [ "$1" == "100" ] && [ "$2" == "0.6" ] && [ "$3" == "0.1" ] && [ "$4" == "50" ] ; then
    configureFileSimple="configure03" #Configure 3
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.4" ] && [ "$3" == "1" ] && [ "$4" == "50" ] ; then
    configureFileSimple="configure04" #Configure 4
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.4" ] && [ "$3" == "0.1" ] && [ "$4" == "50" ] ; then
    configureFileSimple="configure05" #Configure 5
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.6" ] && [ "$3" == "1" ] && [ "$4" == "50" ] ; then
    configureFileSimple="configure06" #Configure 6
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.6" ] && [ "$3" == "0.1" ] && [ "$4" == "50" ] ; then
    configureFileSimple="configure07" #Configure 7
  fi

  #Change of clusters
  if [ "$1" == "100" ] && [ "$2" == "0.4" ] && [ "$3" == "1" ] && [ "$4" == "5" ] ; then
    configureFileSimple="configure08" #Configure 0
  fi

  if [ "$1" == "100" ] && [ "$2" == "0.4" ] && [ "$3" == "0.1" ] && [ "$4" == "5" ] ; then
    configureFileSimple="configure09" #Configure 1
  fi

  if [ "$1" == "100" ] && [ "$2" == "0.6" ] && [ "$3" == "1" ] && [ "$4" == "5" ] ; then
    configureFileSimple="configure10" #Configure 2
  fi

  if [ "$1" == "100" ] && [ "$2" == "0.6" ] && [ "$3" == "0.1" ] && [ "$4" == "5" ] ; then
    configureFileSimple="configure11" #Configure 3
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.4" ] && [ "$3" == "1" ] && [ "$4" == "5" ] ; then
    configureFileSimple="configure12" #Configure 4
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.4" ] && [ "$3" == "0.1" ] && [ "$4" == "5" ] ; then
    configureFileSimple="configure13" #Configure 5
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.6" ] && [ "$3" == "1" ] && [ "$4" == "5" ] ; then
    configureFileSimple="configure14" #Configure 6
  fi

  if [ "$1" == "50" ] && [ "$2" == "0.6" ] && [ "$3" == "0.1" ] && [ "$4" == "5" ] ; then
    configureFileSimple="configure15" #Configure 7
  fi


  echo "$configureFileSimple"
}

files="JP_Tokyo"

for file in $files
do

  trainfile=$destinationSessionCities/$file"AggrTrain".txt
  trainfileTemporal=$destinationSessionCities/$file"TempTrain".txt
  testfile=$destinationSessionCities/$file"Test".txt



  # Save some sims
  mkdir -p $SimFolder
  mkdir -p $recommendationFolder

  for UBsimilarity in SetJaccardUserSimilarity VectorCosineUserSimilarity
  do
    outputSimfile=$SimFolder/$simPrefix"_"$file"_"$UBsimilarity".txt"
    $javaCommand $jvmMemory -jar $JAR -o ranksysSaveUBSimFile -trf $trainfile -tsf $testfile -cIndex false -rs $UBsimilarity -osf $outputSimfile
  done # End sim
  wait


  for recommendationStrategy in $recStrats
  do

    # Popularity, random and train recommenders (train recommenders will retrieve empty recommendations for T_I methodology)
    for ranksysRecommender in PopularityRecommender RandomRecommender TrainRecommender TrainRecommenderReverse
    do
      # RankSys
      outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"$ranksysRecommender"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
      $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr "$ranksysRecommender" -rs "notUsed" -n 20 -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

    done
    wait

    # SkylineTestOrder
    outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"SkylineTestOrder"_"$typeTEMPTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
    $javaCommand $jvmMemory -jar $JAR -o skylineRecommenders -trf $trainfileTemporal -tsf $testfile -cIndex true -rr "SkylineTestOrder" -rs "notUsed" -n 20 -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

    # SkylineTestOrder Reverse
    outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"SkylineTestOrderReverse"_"$typeTEMPTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
    $javaCommand $jvmMemory -jar $JAR -o skylineRecommenders -trf $trainfileTemporal -tsf $testfile -cIndex true -rr "SkylineTestOrderReverse" -rs "notUsed" -n 20 -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy


    # UB Knn

    for neighbours in $allneighbours
    do
      for UBsimilarity in SetJaccardUserSimilarity VectorCosineUserSimilarity
      do
        outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_UB_"$UBsimilarity"_k"$neighbours"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr UserNeighborhoodRecommender -rs $UBsimilarity -n $neighbours -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

      done
      wait # UB sim

      for IBsimilarity in SetJaccardItemSimilarity VectorCosineItemSimilarity
      do
        outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_IB_"$IBsimilarity"_k"$neighbours"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr ItemNeighborhoodRecommender -rs $IBsimilarity -n $neighbours -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

      done
      wait # IB sim

    done # Neighbours
    wait

    # HKV
    for rankRecommenderNoSim in MFRecommenderHKV
    do
      for kFactor in $allKFactorizerRankSys
      do
        for lambdaValue in $allLambdaFactorizerRankSys
        do
          for alphaValue in $allAlphaFactorizerRankSys
          do
            # Neighbours is put to 20 because this recommender does not use it
            outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"$rankRecommenderNoSim"_k"$kFactor"_a"$alphaValue"_l"$lambdaValue"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
            $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr $rankRecommenderNoSim -rs "notUsed" -kFactorizer $kFactor -aFactorizer $alphaValue -lFactorizer $lambdaValue -n 20 -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

          done
          wait #End alpha values
        done
        wait #End lambda
      done
      wait #End KFactor
    done
    wait #End RankRecommender

    for poiRecommender in KDEstimatorRecommender AverageDistanceUserGEO
    do
      outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksysPOI_"$poiRecommender"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
      $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr $poiRecommender -rs "notUsed" -n 20 -coordFile $coordFile -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

    done # End poiRecommender
    wait


    # KDE with temporal
    for poiRecommender in KDEstimatorRecommender
    do
      outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksysPOI_"$poiRecommender"_"$typeTEMPTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
      $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfileTemporal -tsf $testfile -cIndex false -rr $poiRecommender -rs "notUsed" -n 20 -coordFile $coordFile -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

    done # End poiRecommender
    wait



    for poiRecommender in PopGeoNN
    do
      for UBsimilarity in SetJaccardUserSimilarity VectorCosineUserSimilarity #SetCosineUserSimilarity VectorCosineUserSimilarity
      do
        for neighbours in $allneighbours
        do
          outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksysPOI_"$poiRecommender"_UB_"$UBsimilarity"_k"$neighbours"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr $poiRecommender -rs $UBsimilarity -coordFile $coordFile -n $neighbours -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

        done # End neighbours
        wait
      done # End UB sim
      wait
    done # End pop Geo
    wait

    # RankGeoTest


    for c in $cs_RANKGEOFM
    do
      for alphaF in $alphas_RANKGEOFM
      do
        for epsilon in $epsilons_RANKGEOFM
        do
          for kFactorizer in $kfactors_RANKGEOFM
          do
            for kNeighbour in $ns_RANKGEOFM
            do
              for numIter in $iters_RANKGEOFM
              do

                for decay in $decays_RANKGEOFM
                do

                  for isboldDriver in $isboldDriver_RANKGEOFM
                  do

                    for learnRate in $learnRates_RANKGEOFM
                    do

                      for maxRate in $maxRates_RANKGEOFM
                      do


                        outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksysPOI_"RankGeoFMRecommender""kFac"$kFactorizer"kNgh"$kNeighbour"dec"$decay"boldDriv"$isboldDriver"Iter"$numIter"lRate"$learnRate"mRate"$maxRate"alpha"$alphaF"c"$c"eps"$epsilon""_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
                        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr "RankGeoFMRecommender" -coordFile $coordFile -n $kNeighbour -nIFactorizer $numIter -kFactorizer $kFactorizer -aFactorizer $alphaF -epsilon $epsilon -c $c -svdDecay $decay -svdIsboldDriver $isboldDriver -svdLearnRate $learnRate -svdMaxLearnRate $maxRate -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

                      done # ENd max learn
                      wait

                    done # Learn rate
                    wait
                  done # End bold
                  wait

                done # End decay
                wait
              done # End iter
              wait

            done # ENd neigh
            wait

          done # End kFactors
          wait
        done # Epsilon
        wait
      done # Alpha
      wait
    done # End c
    wait


    # Temporal
    for ranksysRecommender in TemporalPopularityRecommender
    do
      # RankSys -> Popularity
      outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"$ranksysRecommender"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
      $javaCommand $jvmMemory -jar $JAR -o ranksysTemporalOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr "$ranksysRecommender" -rs "notUsed" -n 20 -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

    done # End tempora pop
    wait


    # For BF and ding
    find $SimFolder/ -name "$simPrefix"*"$file"* | while read simFile; do
      simFileName=$(basename "$simFile" .txt) #extension removed

      for neighbours in $allneighbours
      do
        for recTemporal in RankSysTemporalRecommender
        do
          for lambda in $lambdaDing
          do
            outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"$simFileName"_k"$neighbours"_"templ"$lambda"_""$recTemporal"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
            $javaCommand $jvmMemory -jar $JAR -o ranksysTemporalOnlyComplete -trf $trainfile -tsf $testfile -cIndex false -rr "$recTemporal" -sf $simFile -n $neighbours -tempLambda $lambda -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

          done # End lambdas ding
          wait
        done # End RankSys temporal
        wait


        for index in $allindexesBackwardForward
        do
          for weight in true false
          do
            for normalizer in $normalizerBackwardForward
            do
              # BackwardForward
              outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"$simFileName""BF"_BothFB"$index"_UB_k"$neighbours"_norm"$normalizer"_Weight"$weight"_Csumcomb"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
              $javaCommand $jvmMemory -jar $JAR -o lcs4recSysBackwardMapAgregator -trf $trainfile -tsf $testfile -cIndex false -sf $simFile -n $neighbours -indexb $index -indexf $index -normAgLib $normalizer -combAgLib sumcomb -weightAgLib $weight -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

              outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"$simFileName""BF"_BothFB"$index"_UB_k"$neighbours"_norm"$normalizer"_Weight"$weight"_Csumcomb"$typeTEMPTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
              $javaCommand $jvmMemory -jar $JAR -o lcs4recSysBackwardMapAgregator -trf $trainfileTemporal -tsf $testfile -cIndex false -sf $simFile -n $neighbours -indexb $index -indexf $index -normAgLib $normalizer -combAgLib sumcomb -weightAgLib $weight -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy
            done # End normalizer
            wait

          done # End weight
          wait

        done # End indexes
        wait

      done # End  neigh
      wait

    done # End similarities
    wait


    # Tour recommender -> NNVenuesRecommender
    for TourRecommender in NNVenuesRecommender
    do
      outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"TourRec"_"$TourRecommender"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
      $javaCommand $jvmMemory -jar $JAR -o TourRecommender -trf $trainfile -tsf $testfile -cIndex true -rr $TourRecommender -coordFile $coordFile -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy

    done # End NN recommender
    wait

    for smooth in $smooths
    do
      if [[ $smooth != *JelineckMercer* ]]; then

        # Pure MC from Item to Item
        for TourRecommender in ItemToItemMC
        do
          outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"TourRec"_"$TourRecommender"_"Smooth""$smooth"_"$typeTEMPTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
          $javaCommand $jvmMemory -jar $JAR -o TourRecommender -trf $trainfileTemporal -tsf $testfile -cIndex true -rr $TourRecommender -coordFile $coordFile -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy -probSmooth $smooth

        done # End MC Tour recommender
        wait

        # Feature to Feature recommender

        for TourRecommender in FeatureToFeature
        do
          for cat_level in $categories_lvl
          do
            outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"TourRec"_"$TourRecommender"_"lvl$cat_level"_"Smooth""$smooth"_"$typeTEMPTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
            $javaCommand $jvmMemory -jar $JAR -o TourRecommender -trf $trainfileTemporal -tsf $testfile -cIndex true -rr $TourRecommender -coordFile $coordFile -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy -probSmooth $smooth -ff $originalCities/POIS_Categories_level$cat_level".txt"

          done
          wait
        done # End MC Tour recommender
        wait

      else
        # JelineckMercer
        for lambdaSmooth in $lambdaSmooths
        do
          # Pure MC from Item to Item
          for TourRecommender in ItemToItemMC
          do
            outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"TourRec"_"$TourRecommender"_"Smooth""$smooth"_"lsmmoth"$lambdaSmooth"_""$typeTEMPTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
            $javaCommand $jvmMemory -jar $JAR -o TourRecommender -trf $trainfileTemporal -tsf $testfile -cIndex true -rr $TourRecommender -coordFile $coordFile -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy -probSmooth $smooth -lFactorizer $lambdaSmooth

          done # End MC Tour recommender
          wait

          # Feature to Feature recommender

          for TourRecommender in FeatureToFeature
          do
            for cat_level in $categories_lvl
            do
              outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"TourRec"_"$TourRecommender"_"lvl$cat_level"_"Smooth""$smooth"_"lsmmoth"$lambdaSmooth"_""$typeTEMPTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
              $javaCommand $jvmMemory -jar $JAR -o TourRecommender -trf $trainfileTemporal -tsf $testfile -cIndex true -rr $TourRecommender -coordFile $coordFile -nI $itemsRecommended -orf $outputRecfile -recStrat $recommendationStrategy -probSmooth $smooth -ff $originalCities/POIS_Categories_level$cat_level".txt" -lFactorizer $lambdaSmooth

            done  # End category level
            wait
          done # End MC Tour recommender
          wait
        done # End lambda smooth
        wait

      fi


    done  # End smooth prob
    wait

    for CBRec in UserItemProfileContentRecommender
    do
      for cat_level in $categories_lvl
      do
        outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"CBRec"_"$CBRec"_"lvl$cat_level"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
        $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr $CBRec -n 20 -orf $outputRecfile -recStrat $recommendationStrategy -nI $itemsRecommended -ff2 $originalCities/POIS_Categories_level$cat_level".txt"

      done # End category level
      wait
    done # End content based profile recommender
    wait

    for CBRec in UBContentBasedRecommender
    do
      for neighbours in $allneighbours
      do
        for ubTransform in WITHRATING WITHOUTRATING
        do
          outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_ranksys_"CBRec"_"$CBRec"_ubT_"$ubTransform"_k"$neighbours"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
          $javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -cIndex true -rr $CBRec -n $neighbours -userTransformationCB $ubTransform -orf $outputRecfile -recStrat $recommendationStrategy -nI $itemsRecommended -ff2 $originalCities/POIS_Categories_level$cat_level".txt"
        done # End ub transformation
        wait
      done # End neighs
      wait
    done # End CBRec
    wait

    # CASER PYTHON
    for caser_L in $caser_Ls
    do
      for caser_T in $caser_Ts
      do
        for caser_n_iter in $caser_n_iters
        do
          for caser_batch_size in $caser_batch_sizes
          do
            for caser_learning_rate in $caser_learning_rates
            do
              for caser_l2 in $caser_l2s
              do
                for caser_neg_sample in $caser_neg_samples
                do
                  for caser_d in $caser_ds
                  do
                    for caser_nv in $caser_nvs
                    do
                      for caser_nh in $caser_nhs
                      do
                        for caser_drop in $caser_drops
                        do

                          if [[ $recommendationStrategy == *T_I* ]]; then
                            TrainItems=True
                          else
                            TrainItems=False
                          fi
                          # Using temp train
                          outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_Caser_L"$caser_L"_T"$caser_T"_Iter"$caser_n_iter"_BatchS"$caser_batch_size"_learnR"$caser_learning_rate"_l2"$caser_l2"_negSample"$caser_neg_sample"_d"$caser_d"_nv"$caser_nv"_nh"$caser_nh"_drop"$caser_drop"_"$typeTEMPTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
                          if [ ! -f *"$outputRecfile"* ]; then
                            python3 $pathCaser/train_caser.py --train_root $trainfileTemporal --test_root $testfile --L $caser_L --T $caser_T --n_iter $caser_n_iter --batch_size $caser_batch_size --learning_rate $caser_learning_rate --l2 $caser_l2 --neg_samples $caser_neg_sample --d $caser_d --nv $caser_nv --nh $caser_nh --drop $caser_drop --nItems $itemsRecommended --TrainItems $TrainItems --output $outputRecfile
                          fi

                          # Using aggre train
                          outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_Caser_L"$caser_L"_T"$caser_T"_Iter"$caser_n_iter"_BatchS"$caser_batch_size"_learnR"$caser_learning_rate"_l2"$caser_l2"_negSample"$caser_neg_sample"_d"$caser_d"_nv"$caser_nv"_nh"$caser_nh"_drop"$caser_drop"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
                          if [ ! -f *"$outputRecfile"* ]; then
                            python3 $pathCaser/train_caser.py --train_root $trainfile --test_root $testfile --L $caser_L --T $caser_T --n_iter $caser_n_iter --batch_size $caser_batch_size --learning_rate $caser_learning_rate --l2 $caser_l2 --neg_samples $caser_neg_sample --d $caser_d --nv $caser_nv --nh $caser_nh --drop $caser_drop --nItems $itemsRecommended --TrainItems $TrainItems --output $outputRecfile
                          fi

                        done
                        wait
                      done # End caser nh
                      wait
                    done # End nvs
                    wait
                  done # End ds
                  wait
                done # End negsamples
                wait
              done # End l2
              wait
            done # End learning rate
            wait
          done # End caser batch size
          wait
        done # End caser n iters
        wait
      done # End T caser
      wait
    done # End L caser
    wait



    # Fossil, MC, FPMC

    for repetition in 1 #2 3 4
    do
      for HeK in $allRuiningHEKs
      do
        for HeLambda in $allRuiningHeLambdas
        do
          for HeL in $allRuiningHELs
          do
            for recommmender in Fossil mc fpmc
            do

              if [[ $recommendationStrategy == *T_I* ]]; then
                allowPrevRated=false
              else
                allowPrevRated=true
              fi


              outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_HeBaseline_"$recommmender"_K"$HeK"_Lambda"$HeLambda"_L"$HeL""Rep$repetition"_"$typeTEMPTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
              echo $outputRecfile
              echo " "
              echo "./train $trainfileTemporal 0 0 $HeL $HeK  $HeLambda  100  50000  model_save_path  0 0 $outputRecfile $itemsRecommended $recommmender $testfile $allowPrevRated"
              if [ ! -f *"$outputRecfile"* ]; then
                ./$fossilDir/train $trainfileTemporal 0 0 $HeL $HeK  $HeLambda  100  50000  model_save_path  0 0 $outputRecfile $itemsRecommended $recommmender $testfile $allowPrevRated
              fi

              outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_HeBaseline_"$recommmender"_K"$HeK"_Lambda"$HeLambda"_L"$HeL""Rep$repetition"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
              echo $outputRecfile
              echo " "
              echo "./train $trainfile 0 0 $HeL $HeK  $HeLambda  100  50000  model_save_path  0 0 $outputRecfile $itemsRecommended $recommmender $testfile $allowPrevRated"
              if [ ! -f *"$outputRecfile"* ]; then
                ./$fossilDir/train $trainfile 0 0 $HeL $HeK  $HeLambda  100  50000  model_save_path  0 0 $outputRecfile $itemsRecommended $recommmender $testfile $allowPrevRated
              fi
            done # End recommmender
            wait
          done # End He L
          wait
        done # End He lambda
        wait
      done # End HeK
      wait
    done # End repetition
    wait

    for repetition in 1 #2 3 4
    do
      for factor in $BPRFactors
      do
        for bias_reg in $BPRBiasReg
        do
          for regU in $BPRRegU #Regularization for items and users is the same
          do
            regJ=$(echo "$regU/10" | bc -l)

            outputRecfile=$recommendationFolder/"$recPrefix"_"$file"_"$extensionMyMediaLite"_BPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_RegJ"$regJ""Rep$repetition"_"$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
            echo $outputRecfile
            if [ ! -f *"$outputRecfile"* ]; then
              outputRecfile2=$outputRecfile"Aux".txt
              if [[ $recommendationStrategy == *T_I* ]]; then
                echo "./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --predict-items-number=$itemsRecommended --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter""
                ./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --predict-items-number=$itemsRecommended --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter"
                $javaCommand $jvmMemory -jar $JAR -o ParseMyMediaLite -trf $outputRecfile2 $testfile $outputRecfile
              else
                echo "./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --predict-items-number=$itemsRecommended --repeated-items --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter""
                ./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --predict-items-number=$itemsRecommended --repeated-items --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter"
                $javaCommand $jvmMemory -jar $JAR -o ParseMyMediaLite -trf $outputRecfile2 $testfile $outputRecfile
              fi
              rm $outputRecfile2
            fi
          done # End RegU
          wait
        done # End BPRBiasReg
        wait
      done # End BPRFactors
      wait
    done # End repetition
    wait

    # IRENMF
    resultFileNNCityFile=$pathDest/SessionCitiesTrainFiles/POIS_"$file"_"$GeoNN"NN.txt
    poisCoordsOfCityFile=$pathDest/SessionCitiesTrainFiles/POIS_"$file"_"$extensionCoords"
    $javaCommand $jvmMemory -jar $JAR -o printClosestPOIs -trf $pathDest/$trainfile $coordFile $resultFileNNCityFile $poisCoordsOfCityFile $GeoNN

    for KIRENMF in $allKIRENMF
    do
      for alphaIRENMF in $allAlphaIRENMF
      do
        for lambda3IRENMF in $allLambda3IRENMF
        do
          for clusterIRENMF in $clustersIRENMF
          do
            configureFileSimple=$(obtainConfigureFile "$KIRENMF" "$alphaIRENMF" "$lambda3IRENMF" "$clusterIRENMF")

            echo "Parameters K=$KIRENMF alpha=$alphaIRENMF lambda3=$lambda3IRENMF clusters=$clusterIRENMF"
            echo "File " $file " working with configure " $configureFileSimple
            configureFileToSelect="configure"_"K"$KIRENMF"_Alpha""$alphaIRENMF""_L1_"$lambda1"_L2_"$lambda2"_L3_"$lambda3IRENMF"_GeoNN"$GeoNN"_clusters"$clusterIRENMF
            outputRecfile=$pathDest/$recommendationFolder/"$recPrefix"_"$file"_IRENMF_$configureFileToSelect"_""$typeFREQTRAIN"_"$typeRecommendationStrategy""$recommendationStrategy".txt
            clusterFile=$pathDest/$recommendationFolder/$file"_"Clusters$clusterIRENMF"_"
            rm $clusterFile"_clusters".mat


            if [ ! -f $outputRecfile ]; then
              if [[ $recommendationStrategy == *T_I* ]]; then
                #ItemGroupPOI(configure_file, trainFile, poisCoordsFile,GeoNNFile,testingFile, candidatesFile,outFile,clusterFile)
                $pathMatlab/./matlab -nodisplay -nodesktop -r "cd '$pathIrenMF/'; ItemGroupPOI('$configureFileSimple', '$pathDest/$trainfile', '$poisCoordsOfCityFile', '$resultFileNNCityFile', '$pathDest/$testfile', '$resultFileNNCityFile', '$outputRecfile', '$clusterFile'); quit"
              else
                $pathMatlab/./matlab -nodisplay -nodesktop -r "cd '$pathIrenMFTesWithTrain/'; ItemGroupPOI('$configureFileSimple', '$pathDest/$trainfile', '$poisCoordsOfCityFile', '$resultFileNNCityFile', '$pathDest/$testfile', '$resultFileNNCityFile', '$outputRecfile', '$clusterFile'); quit"
              fi
            fi

            echo "Finished $configureFileSimple"
          done # End clusters
          wait
        done
        wait #End lambda
      done
      wait #End alpha IrenMF
    done
    wait #End KIRENMF



  done # End rec strategies
  wait

  # Force all recommenders to have the first item in the test set
  find $recommendationFolder/ -name "$recPrefix"*"$file"* | while read recFile; do
    recFileName=$(basename "$recFile" .txt) #extension removed
    trainfileEv=$trainfile
    if [[ $recFileName == *TEMPTRAIN* ]]; then
      trainfileEv=$trainfileTemporal
    fi

    # Generate the new recommender file with the first item of the test set
    # As we are extending it, it it already exits, then we do not compute it
    if [[ $recFileName != *FstItemTest* && $recFileName != *Rerank* ]]; then
      outputRecFile=$recommendationFolder/"$recFileName""FstItemTest".txt
      $javaCommand $jvmMemory -jar $JAR -o generateNewRecFileFstItemTest -trf $trainfileEv -tsf $testfile -rf $recFile -orf $outputRecFile
    fi
  done
  wait

  bestRecommendersFst="ranksys_PopularityRecommender_FrTr_RStgT_IFstItemTest MyMedLt_BPRMF_nFact50_nIter50_LearnR0.05_BiasR0_RegU0.005_RegI0.005_RegJ.00050000000000000000Rep1_FrTr_RStgT_IFstItemTest ranksys_sim_JP_Tokyo_VectorCosineUserSimilarityBF_BothFB5_UB_k100_normdefaultnorm_Weightfalse_CsumcombReTr_RStgT_IFstItemTest IRENMF_configure_K100_Alpha0.6_L1_0.015_L2_0.015_L3_1_GeoNN10_clusters50_FrTr_RStgT_IFstItemTest ranksys_SkylineTestOrder_ReTr_RStgT_IFstItemTest rec_JP_Tokyo_ranksys_TourRec_ItemToItemMC_SmoothJelineckMercer_lsmmoth0.5_ReTr_RStgT_IFstItemTest"

  # Rerankers
  for reranker in $rerankers
  do
    for fillStrat in $fillStrats
    do
      for bestRect in $bestRecommendersFst
      do

        for candidateRe in $candidatesRerank
        do
          # We wont compute rerankers of rerankers
          find $recommendationFolder/ -not -name "*Rerank*" -name "$recPrefix"*"$file"*"$bestRect"* | while read recFile; do

            recFileName=$(basename "$recFile" .txt) #extension removed
            trainfileEv=$trainfile
            trainComment="$typeFREQTRAIN"

            for lambda_rerank in $lambdaRerankers
            do

              if [[ $reranker == *MCReranker* || $reranker == *FeatureSuffixTreeReranker || $reranker == *LCSReranker* ]]; then
                trainfileEv=$trainfileTemporal
                trainComment="$typeTEMPTRAIN"
              fi

              # For rerankers we will use no NoSmoothingCond
              smoothCond=NoSmoothingCond
              # User based reranker
              if [[ $reranker == *UBReranker* ]]; then
                for UBsimilarity in VectorCosineUserSimilarity
                do
                  for neighbours in 100
                  do
                    outputRecfile=$recommendationFolder/"$recFileName"$reranker"_"$UBsimilarity"_k"$neighbours"_lRR"$lambda_rerank"_"$trainComment"_"$fillStrat"$candidateRe".txt
                    $javaCommand $jvmMemory -jar $JAR -o Reranking -trf $trainfileEv -tsf $testfile -rerankers $reranker -rs $UBsimilarity -n $neighbours -coordFile $coordFile -rf $recFile -lambda $lambda_rerank -rc $candidateRe -fillStrat $fillStrat -orf $outputRecfile
                  done # End neigh
                  wait
                done # End sim
                wait
                # Feature MC reranker
              elif [[ $reranker == *FeatureMCReranker* || $reranker == *LCSReranker* ]]; then
                for cat_level in $categories_lvl
                do
                  outputRecfile=$recommendationFolder/"$recFileName"$reranker"_"$smoothCond"_"lvl$cat_level"_"lRR$lambda_rerank"_"$trainComment"_"$fillStrat"$candidateRe".txt
                  $javaCommand $jvmMemory -jar $JAR -o Reranking -trf $trainfileEv -tsf $testfile -rerankers $reranker -coordFile $coordFile -probSmooth $smoothCond -rf $recFile -rc $candidateRe -lambda $lambda_rerank -ff $originalCities/POIS_Categories_level$cat_level".txt" -fillStrat $fillStrat -orf $outputRecfile
                done
                wait
                # Other rerankers
              elif [[ $reranker == *ItemMCReranker* ]]; then
                outputRecfile=$recommendationFolder/"$recFileName"$reranker"_"$smoothCond"_"lRR$lambda_rerank"_"$trainComment"_"$fillStrat"$candidateRe".txt
                $javaCommand $jvmMemory -jar $JAR -o Reranking -trf $trainfileEv -tsf $testfile -rerankers $reranker -coordFile $coordFile -probSmooth $smoothCond -rf $recFile -rc $candidateRe -lambda $lambda_rerank -fillStrat $fillStrat -orf $outputRecfile
              elif [[ $reranker == *FeatureSuffixTreeReranker* ]]; then
                for lengthPatternReranker in $lengthPatternRerankers
                do
                  for cat_level in $categories_lvl
                  do
                    outputRecfile=$recommendationFolder/"$recFileName"$reranker"_"$smoothCond"_"LP$lengthPatternReranker"_lvl$cat_level"_lRR$lambda_rerank"_"$trainComment"_"$fillStrat"$candidateRe".txt
                    $javaCommand $jvmMemory -jar $JAR -o Reranking -trf $trainfileEv -tsf $testfile -rerankers $reranker -coordFile $coordFile -probSmooth $smoothCond -rf $recFile -rc $candidateRe -lambda $lambda_rerank -patternLenght $lengthPatternReranker -ff $originalCities/POIS_Categories_level$cat_level".txt" -fillStrat $fillStrat -orf $outputRecfile
                  done # End category elevel
                  wait
                done # End pattern length
                wait
              else # No similarity nor neighs
                outputRecfile=$recommendationFolder/"$recFileName"$reranker"_"lRR$lambda_rerank"_"$trainComment"_"$fillStrat"$candidateRe".txt
                $javaCommand $jvmMemory -jar $JAR -o Reranking -trf $trainfileEv -tsf $testfile -rerankers $reranker -coordFile $coordFile -rf $recFile -lambda $lambda_rerank -rc $candidateRe -fillStrat $fillStrat -orf $outputRecfile
              fi
            done # End lambda of reranker
            wait

          done # End find recommendationFiles
          wait
        done # End number of candidates
        wait
      done # Best recommenders
      wait
    done # End fill strategy for recommenders
    wait

  done # End reranker
  wait




  mkdir -p $resultsFolder
  mkdir -p $resultsFolderFsItemTest





  # Now, evaluation (generating files containing the results of each recommender)

  find $recommendationFolder/ -name "$recPrefix"*"$file"* | while read recFile; do

    recFileName=$(basename "$recFile" .txt) #extension removed
    trainfileEv=$trainfile
    finalResultFolder=$resultsFolder


    if [[ $recFileName == *FstItemTest* ]]; then
      finalResultFolder=$resultsFolderFsItemTest
    fi

    if [[ $recFileName == *_* ]]; then
      for evthreshold in 1 # we work with implicit data, so every item in the test set is relevant
      do
        for cat_level in $categories_lvl
        do

          outputResultfile=$finalResultFolder/"$nonaccresultsPrefix"_EvTh"$evthreshold"_"Evlvl$cat_level"_"$recFileName".txt
          if [ ! -f "$outputResultfile" ]; then
            echo 'train file is ' $trainfileEv
            $javaCommand $jvmMemory -jar $JAR -o ranksysNonAccuracyMetricsEvaluation -trf $trainfileEv -tsf $testfile -rf $recFile -thr $evthreshold -rc $cutoffs -orf $outputResultfile -lcsEv true -compDistances true -coordFile $coordFile -onlyAcc false -ff $originalCities/POIS_Categories_level$cat_level".txt"

          fi

        done # Category level
        wait

      done # End ev th
      wait
    fi

  done # End find recommendationFolder
  wait

done # End files (cities)
wait
