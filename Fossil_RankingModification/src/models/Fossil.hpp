#pragma once

#include "model.hpp"

class Fossil : public model
{
public:
	Fossil( corpus* corp, int L, int K, double lambda, double bias_reg)
			: model(corp)
			, L(L)
			, K(K)
			, lambda(lambda)
			, bias_reg(bias_reg) {}

	~Fossil(){}

	void init();
	void cleanUp();

	double prediction(int user, int prev_item, int item) { return -1; }
	double prediction(int user, vector<int>& prev_items, int item);
	void getParametersFromVector(	double*   g,
									double**  beta,
									double**  WT,
									double*** WTu,
									double*** U,
									double*** V,
									action_t  action);

	int sampleUser();
	void train(int iterations, double learn_rate);
	void oneiteration(double learn_rate);
	void updateFactors(int user_id, vector<int>& prev_items, int pos_item_id, int neg_item_id, double learn_rate);


	void sampleAUC(double* AUC_val, double* AUC_test, double* var);
	void sampleAUCWithoutTest(double* AUC_val, double* var);
	void AUC(double* AUC_val, double* AUC_test, double* var);
	void AUC_coldItem(double* AUC_test, double* var, int* num_user);
	void AUC_coldUser(double* AUC_test, double* var, int* num_user);
	void obtainRanking(const char *path, int limit,const char *testFile,  bool allow_recomendations_rated_by_user_in_train);

	string toString();

	/* parameters */
	double* beta;
	double** U;
	double** V;
	double* WT;
	double** WTu;

	/* hyper-parameters */
	int L;
	int K;
	double lambda;
	double bias_reg;

	/* helper */
	vector<int>* user_matrix;
};
