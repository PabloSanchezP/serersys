#pragma once

#include "model.hpp"

class Random : public model
{
public:
	Random(corpus* corp) : model(corp) 
	{
		random_list = new int [nItems];
		for (int i = 0; i < nItems; i ++) {
			random_list[i] = (int)rand();
		}
	}

	~Random()
	{
		delete [] random_list;
	}

	double prediction(int user, int item_prev, int item);

	string toString();

	// have to use some structure to store random numbers 
	// due to rand() has multithreading issue
	int* random_list;  
};
