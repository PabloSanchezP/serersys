#include "FISMrmse.hpp"

void FISMrmse::init()
{
	NW = nUsers + nItems + 2 * K * nItems;
	W = new double [NW];
	bestW = new double [NW];

	getParametersFromVector(W, &beta_u, &beta_i, &P, &Q, INIT);

	for (int w = 0; w < NW; w ++) {
		W[w] = rand() * 0.002 / RAND_MAX - 0.001;
	}
}

void FISMrmse::cleanUp()
{
	getParametersFromVector(W, &beta_u, &beta_i, &P, &Q, FREE);
	
	delete [] W;
	delete [] bestW;
}

void FISMrmse::getParametersFromVector(	double*   g,
										double**  beta_u, 
										double**  beta_i,
										double*** P,
										double*** Q,
										action_t  action)
{
	if (action == FREE) {
		delete [] (*P);
		delete [] (*Q);
		return;
	}

	if (action == INIT)	{
		*P = new double* [nItems];
		*Q = new double* [nItems];
	}

	int ind = 0;

	*beta_u = g + ind;
	ind += nUsers;

	*beta_i = g + ind;
	ind += nItems;

	for (int i = 0; i < nItems; i ++) {
		(*P)[i] = g + ind;
		ind += K;
	}
	for (int i = 0; i < nItems; i ++) {
		(*Q)[i] = g + ind;
		ind += K;
	}

	if (ind != NW) {
		printf("Got bad index (FISMrmse.cpp, line %d)", __LINE__);
		exit(1);
	}
}

// The model does not use prev_item 
double FISMrmse::prediction(int user, int prev_item, int item)
{
	double* x = new double [K];
	for (int k = 0; k < K; k ++) {
		x[k] = 0;
	}

	int cnt = 0;
	for (vector<pair<int,int> >::iterator it = corp->pos_per_user[user].begin(); it != corp->pos_per_user[user].end(); it ++) {
		int j = it->first;

		if (j != item) {
			cnt ++;
			for (int k = 0; k < K; k ++) {
				x[k] += P[j][k];
			}
		}
	}

	double wu = pow(cnt * 1.0, -alpha);
	for (int k = 0; k < K; k ++) {
		x[k] *= wu;
	}

	double r_ui = beta_u[user] + beta_i[item] + inner(x, Q[item], K);
	delete [] x;
	return r_ui;
}

double FISMrmse::oneiteration(double learn_rate)
{
	// working memory
	set<pair<int,int> > events;
	vector<rating> train_set;
	for (int u = 0; u < nUsers; u ++) {
		for (auto& pr : corp->pos_per_user[u]) {
			train_set.push_back(rating(u, pr.first, 1.0));
			events.insert(make_pair(u, pr.first));
		}
	}

	int target = (int) ((1 + rho) * events.size());
	while ((int)train_set.size() < target) {
		int user_id = rand() % nUsers;
		int item_id = rand() % nItems;

		// if not already in training set
		if (events.find(make_pair(user_id, item_id)) == events.end()) {
			train_set.push_back(rating(user_id, item_id, 0.0));
			events.insert(make_pair(user_id, item_id));
		}
	}

	random_shuffle(train_set.begin(), train_set.end());

	double total_loss = 0;
	for (vector<rating>::iterator it = train_set.begin(); it != train_set.end(); it ++) {
		total_loss += updateFactors(it->user, it->item, it->val, learn_rate);
	}

	return total_loss;
}

double FISMrmse::updateFactors(int user, int item, float val, double learn_rate)
{
	double* x = new double [K];
	for (int k = 0; k < K; k ++) {
		x[k] = 0;
	}

	int cnt = 0;
	for (vector<pair<int,int> >::iterator it = corp->pos_per_user[user].begin(); it != corp->pos_per_user[user].end(); it ++) {
		int j = it->first;

		if (j != item) {
			cnt ++;
			for (int k = 0; k < K; k ++) {
				x[k] += P[j][k];
			}
		}
	}

	double wu = cnt > 0 ? pow(cnt * 1.0, -alpha) : 0;
	for (int k = 0; k < K; k ++) {
		x[k] *= wu;
	}

	double r_ui = beta_u[user] + beta_i[item] + inner(x, Q[item], K);
	double e_ui = r_ui - val;
	double loss = e_ui * e_ui;

	beta_u[user] -= learn_rate * (e_ui + bias_reg * beta_u[user]);
	beta_i[item] -= learn_rate * (e_ui + bias_reg * beta_i[item]);

	double deri = e_ui * wu;
	for (vector<pair<int,int> >::iterator it = corp->pos_per_user[user].begin(); it != corp->pos_per_user[user].end(); it ++) {
		int j = it->first;

		if (j != item) {
			for (int k = 0; k < K; k ++) {
				P[j][k] -= learn_rate * (deri * Q[item][k] + lambda * P[j][k]);
			}
		}
	}

	for (int k = 0; k < K; k ++) {
		Q[item][k] -= learn_rate * (e_ui * x[k] + lambda * Q[item][k]);
	}

	delete [] x;
	return loss;
}

void FISMrmse::train(int iterations, double learn_rate)
{
	printf("%s", ("\n<<< " + toString() + " >>>\n\n").c_str());

	double bestValidAUC = -1;
	int best_iter = 0;

	// SGD begins
	for (int iter = 1; iter <= iterations; iter ++) {
		
		// perform one iter of SGD
		double l_dlStart = clock_();
		double loss = oneiteration(learn_rate);
		if (std::isnan(loss) || std::isinf(loss)) {
			printf("Loss is NaN or Inf!\n");
			exit(1);
		}

		printf("Iter: %d, took %f, loss = %f\n", iter, clock_() - l_dlStart, loss);
		fflush(stdout);

		if(iter % 10 == 0) {
			double valid, test, var;
			//sampleAUC(&valid, &test, &var);
			sampleAUCWithoutTest(&valid, &var);
			printf("[Valid AUC = %f], Test AUC = %f, Test var = %f\n", valid, test, var);
			fflush(stdout);
			
			if (bestValidAUC < valid) {
				bestValidAUC = valid;
				best_iter = iter;
				copyBestModel();
			} else if (iter > best_iter + 50) {
				printf("Overfitted. Exiting... \n");
				break;
			}
		}
	}

	// copy back best parameters
	for (int w = 0; w < NW; w ++) {
		W[w] = bestW[w];
	}
	
	/*	
	double valid, test, var;
	int num_item;
	
	AUC(&valid, &test, &var);
	printf("\n\n <<< %s >>> Test AUC = %f, Test var = %f\n", toString().c_str(), test, var);

	AUC_coldUser(&test, &var, &num_item);
	printf("\n\n <<< %s >>> User Cold Start: #User = %d, Test AUC = %f, Test Var = %f\n", toString().c_str(), num_item, test, var);

	AUC_coldItem(&test, &var, &num_item);
	printf("\n\n <<< %s >>> Item Cold Start: #Item = %d, Test AUC = %f, Test var = %f\n", toString().c_str(), num_item, test, var);
	*/
}

string FISMrmse::toString()
{
	char str[100];
	sprintf(str, "FISMrmse__K_%d_rho_%.2f_alpha_%.4f_lambda_%.4f_biasReg_%.4f", K, rho, alpha, lambda, bias_reg);
	return str;
}
