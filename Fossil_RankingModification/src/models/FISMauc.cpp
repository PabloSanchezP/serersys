#include "FISMauc.hpp"

void FISMauc::init()
{
	NW = nItems + 2 * K * nItems;
	W = new double [NW];
	bestW = new double [NW];

	getParametersFromVector(W, &beta_i, &P, &Q, INIT);

	for (int w = 0; w < NW; w ++) {
		W[w] = rand() * 0.002 / RAND_MAX - 0.001;
	}
}

void FISMauc::cleanUp()
{
	getParametersFromVector(W, &beta_i, &P, &Q, FREE);
	
	delete [] W;
	delete [] bestW;
}

void FISMauc::getParametersFromVector(	double*   g,
										double**  beta_i,
										double*** P,
										double*** Q,
										action_t  action)
{
	if (action == FREE) {
		delete [] (*P);
		delete [] (*Q);
		return;
	}

	if (action == INIT)	{
		*P = new double* [nItems];
		*Q = new double* [nItems];
	}

	int ind = 0;

	*beta_i = g + ind;
	ind += nItems;

	for (int i = 0; i < nItems; i ++) {
		(*P)[i] = g + ind;
		ind += K;
	}
	for (int i = 0; i < nItems; i ++) {
		(*Q)[i] = g + ind;
		ind += K;
	}

	if (ind != NW) {
		printf("Got bad index (FISMauc.cpp, line %d)", __LINE__);
		exit(1);
	}
}

// The model does not use prev_item 
double FISMauc::prediction(int user, int prev_item, int item)
{
	double* x = new double [K];
	for (int k = 0; k < K; k ++) {
		x[k] = 0;
	}

	int cnt = 0;
	for (vector<pair<int,int> >::iterator it = corp->pos_per_user[user].begin(); it != corp->pos_per_user[user].end(); it ++) {
		int j = it->first;

		if (j != item) {
			cnt ++;
			for (int k = 0; k < K; k ++) {
				x[k] += P[j][k];
			}
		}
	}

	double wu = pow(cnt * 1.0, -alpha);
	for (int k = 0; k < K; k ++) {
		x[k] *= wu;
	}

	double r_ui = beta_i[item] + inner(x, Q[item], K);
	delete [] x;
	return r_ui;
}


double FISMauc::oneiteration(double learn_rate)
{
	// working memory
	vector<int>* user_matrix = new vector<int> [nUsers];
	for (int u = 0; u < nUsers; u ++) {
		for (vector<pair<int,int> >::iterator it = corp->pos_per_user[u].begin(); it != corp->pos_per_user[u].end(); it ++) {
			user_matrix[u].push_back(it->first);
		}
	}

	double* x = new double [K];
	double* t = new double [K];
	double* sum_p = new double [K];

	double loss = 0;
	for (int u = 0; u < nUsers; u ++) {
		if ( clicked_per_user[u].size() < 1) {
			continue;
		}

		double wu = clicked_per_user[u].size() - 1 > 0 ? pow(clicked_per_user[u].size() - 1, -alpha) : 0;

		for (int k = 0; k < K; k ++) {
			sum_p[k] = 0;
			for (unordered_set<int>::iterator it = clicked_per_user[u].begin(); it != clicked_per_user[u].end(); it ++) {
				sum_p[k] += P[*it][k];
			}
		}

		for (vector<int>::iterator i = user_matrix[u].begin(); i != user_matrix[u].end(); i ++) {
			for (int k = 0; k < K; k ++) {
				t[k] = (sum_p[k] - P[*i][k]) * wu;
				x[k] = 0;
			}

			set<int> Z;
			while (Z.size() < rho) {
				int j = rand() % nItems;
				if (clicked_per_user[u].find(j) == clicked_per_user[u].end()) {
					Z.insert(j);
				}
			}

			for (set<int>::iterator j = Z.begin(); j != Z.end(); j ++) {
				double r_ui = beta_i[*i] + inner(t, Q[*i], K);
				double r_uj = beta_i[*j] + inner(t, Q[*j], K);
				double e = (r_ui - r_uj) - 1.0;

				loss += e * e;

				beta_i[*i] -= learn_rate * ( e + bias_reg * beta_i[*i]);
				beta_i[*j] -= learn_rate * (-e + bias_reg * beta_i[*j]);

				for (int k = 0; k < K; k ++) {
					x[k] += e * (Q[*i][k] - Q[*j][k]);
					Q[*i][k] -= learn_rate * ( e * t[k] + lambda * Q[*i][k]);
					Q[*j][k] -= learn_rate * (-e * t[k] + lambda * Q[*j][k]);
				}
			}

			for (vector<int>::iterator ii = user_matrix[u].begin(); ii != user_matrix[u].end(); ii ++) {
				if (ii != i) {
					double deri = wu / rho;
					for (int k = 0; k < K; k ++) {
						P[*ii][k] -= learn_rate * (deri * x[k] + lambda * P[*i][k]);
					}
				}
			}
		}
 	}
	delete [] x;
	delete [] t;
	delete [] sum_p;
	delete [] user_matrix;
        if (loss == 0)
            printf("Loss is zero\n");
	return loss;
}

void FISMauc::updateFactors(int user, int i, int j, double learn_rate)
{
	double* sum_pos = new double [K];
	double* sum_neg = new double [K];

	for (int k = 0; k < K; k ++) {
		sum_pos[k] = sum_neg[k] = 0;
	}

	for (unordered_set<int>::iterator it = clicked_per_user[user].begin(); it != clicked_per_user[user].end(); it ++) {
		if (*it != i) {
			for (int k = 0; k < K; k ++) {
				sum_pos[k] += P[*it][k];
			}
		}
		for (int k = 0; k < K; k ++) {
			sum_neg[k] += P[*it][k];
		}
	}

	double wu_pos = clicked_per_user[user].size() - 1 > 0 ? pow(clicked_per_user[user].size() - 1, -alpha) : 0;
	double wu_neg = clicked_per_user[user].size() > 0 ? pow(clicked_per_user[user].size(), -alpha) : 0;
	for (int k = 0; k < K; k ++) {
		sum_pos[k] *= wu_pos;
		sum_neg[k] *= wu_neg;
	}

	double x_uij = beta_i[i] - beta_i[j];
	x_uij += inner(sum_pos, Q[i], K) - inner(sum_neg, Q[j], K);

	double deri = 1 / (1 + exp(x_uij));

	beta_i[i] += learn_rate * ( deri - bias_reg * beta_i[i]);
	beta_i[j] += learn_rate * (-deri - bias_reg * beta_i[j]);

	for (unordered_set<int>::iterator it = clicked_per_user[user].begin(); it != clicked_per_user[user].end(); it ++) {
		if (*it == i) {
			double deri_k = -deri * wu_neg;
			for (int k = 0; k < K; k ++) {
				P[i][k] += learn_rate * (deri_k * Q[j][k] - lambda * P[i][k]);
			}
		} else {
			for (int k = 0; k < K; k ++) {
				P[*it][k] += learn_rate * (deri * (wu_pos * Q[i][k] - wu_neg * Q[j][k]) - lambda * P[*it][k]);
			}
		}
	}

	for (int k = 0; k < K; k ++) {
		Q[i][k] += learn_rate * ( deri * sum_pos[k] - lambda * Q[i][k]);
		Q[j][k] += learn_rate * (-deri * sum_neg[k] - lambda * Q[j][k]);
	}

	delete [] sum_pos;
	delete [] sum_neg;
}

void FISMauc::train(int iterations, double learn_rate)
{
	printf("%s", ("\n<<< " + toString() + " >>>\n\n").c_str());

	double bestValidAUC = -1;
	int best_iter = 0;

	// SGD begins
	for (int iter = 1; iter <= iterations; iter ++) {
		
		// perform one iter of SGD
		double l_dlStart = clock_();
		double loss = oneiteration(learn_rate);
		if (std::isnan(loss) || std::isinf(loss)) {
			printf("Loss is NaN or Inf!\n");
			exit(1);
		}

		printf("Iter: %d, took %f, loss = %f\n", iter, clock_() - l_dlStart, loss);
		fflush(stdout);

		if(iter % 20 == 0) {
			double valid, test, var;
			//sampleAUC(&valid, &test, &var);
			sampleAUCWithoutTest(&valid, &var);
			printf("[Valid AUC = %f], Test AUC = %f, Test var = %f\n", valid, test, var);
			fflush(stdout);
			
			if (bestValidAUC < valid) {
				bestValidAUC = valid;
				best_iter = iter;
				copyBestModel();
			} else if (iter > best_iter + 50) {
				printf("Overfitted. Exiting... \n");
				break;
			}
		}
	}

	// copy back best parameters
	
	for (int w = 0; w < NW; w ++) {
		W[w] = bestW[w];
	}
	
	/*
	double valid, test, var;
	int num_item;
	
	AUC(&valid, &test, &var);
	printf("\n\n <<< %s >>> Test AUC = %f, Test var = %f\n", toString().c_str(), test, var);

	AUC_coldUser(&test, &var, &num_item);
	printf("\n\n <<< %s >>> User Cold Start: #User = %d, Test AUC = %f, Test Var = %f\n", toString().c_str(), num_item, test, var);

	AUC_coldItem(&test, &var, &num_item);
	printf("\n\n <<< %s >>> Item Cold Start: #Item = %d, Test AUC = %f, Test var = %f\n", toString().c_str(), num_item, test, var);
	*/
}

string FISMauc::toString()
{
	char str[100];
	sprintf(str, "FISMauc__K_%d_rho_%.2f_alpha_%.4f_lambda_%.4f_biasReg_%.4f", K, rho, alpha, lambda, bias_reg);
	return str;
}
