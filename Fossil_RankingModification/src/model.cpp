#include "model.hpp"
#include <tuple>
#include <iostream>
#include <string>
#include <fstream>

void model::AUC(double* AUC_val, double* AUC_test, double* var)
{
	vector<double> AUC_u_val(nUsers, -1);
	vector<double> AUC_u_test(nUsers, -1);

	#pragma omp parallel for schedule(dynamic)
	for (int u = 0; u < nUsers; u ++) {
		int item_test 		= test_per_user[u].first;
		int item_test_prev  = test_per_user[u].second;
		int item_val  		= val_per_user[u].first;
		int item_val_prev 	= val_per_user[u].second;

		if (item_test == -1) {  // invalid user
			continue;
		}

		double x_u_test = prediction(u, item_test_prev, item_test);
		double x_u_val  = prediction(u, item_val_prev, item_val);



		int count_val = 0;
		int count_test = 0;
		int max = 0;
		for (int j = 0; j < nItems; j ++) {
			if (clicked_per_user[u].find(j) != clicked_per_user[u].end()
				|| j == item_test
				|| j == item_val) {
				continue;
			}
			max ++;
			double x_uj = prediction(u, item_val_prev, j);
			if (x_u_val > x_uj) {
				count_val ++;
			}

			x_uj = prediction(u, item_test_prev, j);
			if (x_u_test > x_uj) {
				count_test ++;
			}
		}
		AUC_u_val[u] = 1.0 * count_val / max;
		AUC_u_test[u] = 1.0 * count_test / max;
	}

	// sum up AUC
	*AUC_val = 0;
	*AUC_test = 0;
	int num_user = 0;
	for (int u = 0; u < nUsers; u ++) {
		if (AUC_u_test[u] != -1) {
			*AUC_val += AUC_u_val[u];
			*AUC_test += AUC_u_test[u];
			num_user ++;
		}
	}
	*AUC_val /= num_user;
	*AUC_test /= num_user;

	// calculate standard deviation
	double variance = 0;
	for (int u = 0; u < nUsers; u ++) {
		if (AUC_u_test[u] != -1) {
			variance += square(AUC_u_test[u] - *AUC_test);
		}
	}
	*var = variance / num_user;
}

void model::sampleAUC(double* AUC_val, double* AUC_test, double* var)
{
	vector<double> AUC_u_val(nUsers, -1);
	vector<double> AUC_u_test(nUsers, -1);

	#pragma omp parallel for schedule(dynamic)
	for (int u = 0; u < nUsers; u ++) {
		int item_test 		= test_per_user[u].first;
		int item_test_prev  = test_per_user[u].second;
		int item_val  		= val_per_user[u].first;
		int item_val_prev 	= val_per_user[u].second;

		if (item_test == -1) {  // invalid user
			continue;
		}

		double x_u_test = prediction(u, item_test_prev, item_test);
		double x_u_val  = prediction(u, item_val_prev, item_val);




		int count_val = 0;
		int count_test = 0;
		int max = 0;
		for (int ind = 0; ind < 2000; ind ++) {
			int j = rand() % nItems;

			if (clicked_per_user[u].find(j) != clicked_per_user[u].end()
				|| j == item_test
				|| j == item_val) {
				continue;
			}
			max ++;
			double x_uj = prediction(u, item_val_prev, j);
			if (x_u_val > x_uj) {
				count_val ++;
			}

			x_uj = prediction(u, item_test_prev, j);
			if (x_u_test > x_uj) {
				count_test ++;
			}
		}
		AUC_u_val[u] = 1.0 * count_val / max;
		AUC_u_test[u] = 1.0 * count_test / max;
	}

	// sum up AUC
	*AUC_val = 0;
	*AUC_test = 0;
	int num_user = 0;
	for (int u = 0; u < nUsers; u ++) {
		if (AUC_u_test[u] != -1) {
			*AUC_val += AUC_u_val[u];
			*AUC_test += AUC_u_test[u];
			num_user ++;
		}
	}
	*AUC_val /= num_user;
	*AUC_test /= num_user;

	// calculate standard deviation
	double variance = 0;
	for (int u = 0; u < nUsers; u ++) {
		if (AUC_u_test[u] != -1) {
			variance += square(AUC_u_test[u] - *AUC_test);
		}
	}
	*var = variance / num_user;
}

void model::sampleAUCWithoutTest(double* AUC_val, double* var)
{
	vector<double> AUC_u_val(nUsers, -1);

	#pragma omp parallel for schedule(dynamic)
	for (int u = 0; u < nUsers; u ++) {
		int item_val  		= val_per_user[u].first;
		int item_val_prev 	= val_per_user[u].second;

		if (item_val == -1 || item_val_prev==-1)
			continue;

		double x_u_val  = prediction(u, item_val_prev, item_val);


		int count_val = 0;
		int max = 0;
		for (int ind = 0; ind < 2000; ind ++) {
			int j = rand() % nItems;

			if (clicked_per_user[u].find(j) != clicked_per_user[u].end()
				|| j == item_val) {
				continue;
			}
			max ++;
			double x_uj = prediction(u, item_val_prev, j);
			if (x_u_val > x_uj) {
				count_val ++;
			}

		}
		AUC_u_val[u] = 1.0 * count_val / max;
	}

	// sum up AUC
	*AUC_val = 0;
	int num_user = 0;
	for (int u = 0; u < nUsers; u ++) {
		*AUC_val += AUC_u_val[u];
		num_user ++;
	}
	*AUC_val /= num_user;

	// calculate standard deviation
	double variance = 0;
	*var = variance / num_user;
}

void model::AUC_coldItem(double* AUC_test, double* var, int* num_user)
{
	vector<int> num_pos_per_item(nItems, 0);
	for (int u = 0; u < nUsers; u ++) {
		for (unsigned i = 0; i < corp->pos_per_user[u].size(); i ++) {
			int item = corp->pos_per_user[u][i].first;
			num_pos_per_item[item] += 1;
		}
	}

	vector<double> AUC_u_test(nUsers, -1);

	#pragma omp parallel for schedule(dynamic)
	for (int u = 0; u < nUsers; u ++) {
		int item_test 		= test_per_user[u].first;
		int item_test_prev  = test_per_user[u].second;
		int item_val  		= val_per_user[u].first;

		if (item_test == -1) {  // invalid user
			continue;
		}

		if (num_pos_per_item[item_test] >= 5) {
			continue;
		}

		double x_u_test = prediction(u, item_test_prev, item_test);

		int count_test = 0;
		int max = 0;
		for (int j = 0; j < nItems; j ++) {
			if (clicked_per_user[u].find(j) != clicked_per_user[u].end()
				|| j == item_test
				|| j == item_val) {
				continue;
			}
			max ++;
			double x_uj = prediction(u, item_test_prev, j);
			if (x_u_test > x_uj) {
				count_test ++;
			}
		}
		AUC_u_test[u] = 1.0 * count_test / max;
	}

	// sum up AUC
	*AUC_test = 0;
	*num_user = 0;
	for (int u = 0; u < nUsers; u ++) {
		if (AUC_u_test[u] != -1) {
			*AUC_test += AUC_u_test[u];
			(*num_user) ++;
		}
	}
	*AUC_test /= (*num_user);

	// calculate standard deviation
	double variance = 0;
	for (int u = 0; u < nUsers; u ++) {
		if (AUC_u_test[u] != -1) {
			variance += square(AUC_u_test[u] - *AUC_test);
		}
	}
	*var = variance / *num_user;
}

void model::AUC_coldUser(double* AUC_test, double* var, int* num_user)
{
	vector<double> AUC_u_test(nUsers, -1);

	#pragma omp parallel for schedule(dynamic)
	for (int u = 0; u < nUsers; u ++) {
		int item_test 		= test_per_user[u].first;
		int item_test_prev  = test_per_user[u].second;
		int item_val  		= val_per_user[u].first;

		if (item_test == -1) {  // invalid user
			continue;
		}

		if (corp->pos_per_user[u].size() >= 5) {
			continue;
		}

		double x_u_test = prediction(u, item_test_prev, item_test);

		int count_test = 0;
		int max = 0;
		for (int j = 0; j < nItems; j ++) {
			if (clicked_per_user[u].find(j) != clicked_per_user[u].end()
				|| j == item_test
				|| j == item_val) {
				continue;
			}
			max ++;
			double x_uj = prediction(u, item_test_prev, j);
			if (x_u_test > x_uj) {
				count_test ++;
			}
		}
		AUC_u_test[u] = 1.0 * count_test / max;
	}

	// sum up AUC
	*AUC_test = 0;
	*num_user = 0;
	for (int u = 0; u < nUsers; u ++) {
		if (AUC_u_test[u] != -1) {
			*AUC_test += AUC_u_test[u];
			(*num_user) ++;
		}
	}
	*AUC_test /= (*num_user);

	// calculate standard deviation
	double variance = 0;
	for (int u = 0; u < nUsers; u ++) {
		if (AUC_u_test[u] != -1) {
			variance += square(AUC_u_test[u] - *AUC_test);
		}
	}
	*var = variance / *num_user;
}

void model::copyBestModel()
{
	for (int w = 0; w < NW; w ++) {
		bestW[w] = W[w];
	}
}

void model::saveModel(const char* path)
{
	FILE* f = fopen_(path, "w");
	fprintf(f, "{\n");
	fprintf(f, "  \"NW\": %d,\n", NW);

	fprintf(f, "  \"W\": [");
	for (int w = 0; w < NW; w ++) {
		fprintf(f, "%f", bestW[w]);
		if (w < NW - 1) fprintf(f, ", ");
	}
	fprintf(f, "]\n");
	fprintf(f, "}\n");
	fclose(f);

	printf("\nModel saved to %s.\n", path);
}

/// model must be first initialized before calling this function
void model::loadModel(const char* path)
{
	printf("\n  loading parameters from %s.\n", path);
	ifstream in;
	in.open(path);
	if (! in.good()){
		printf("Can't read init solution from %s.\n", path);
		exit(1);
	}
	string line;
	string st;
	char ch;
	while(getline(in, line)) {
		stringstream ss(line);
		ss >> st;
		if (st == "\"NW\":") {
			int nw;
			ss >> nw;
			if (nw != NW) {
				printf("NW not match.");
				exit(1);
			}
			continue;
		}

		if (st == "\"W\":") {
			ss >> ch; // skip '['
			for (int w = 0; w < NW; w ++) {
				if (! (ss >> W[w] >> ch)) {
					printf("Read W[] error.");
					exit(1);
				}
			}
			break;
		}
	}
	in.close();
}

void model::obtainRanking(const char *path, int limit,const char *testFile, bool allow_recomendations_rated_by_user_in_train){
	vector<double> AUC_u_val(nUsers, -1);
	vector<double> AUC_u_test(nUsers, -1);

	string uName; // User name
	string iName; // Item name
	float value;  // rating
	int voteTime; // Time rating was entered

	//readTestFile
	igzstream in;
	in.open(testFile);
	string line;
	unordered_map<string, int> userIdsTest;
	int nUsersTest = 0;
	while (getline(in, line)) {
		stringstream ss(line);
		ss >> uName >> iName >> value >> voteTime;
		//Check if the user exits

		if (userIdsTest.find(uName) == userIdsTest.end()){
			userIdsTest[uName] = nUsersTest ++;
		}
	}
	printf("Users in test %d ",nUsersTest);
	in.close();
	//userIdsTest will have the map of test users





	//Open file
	ofstream rankingFile;
  	rankingFile.open(path);

	//For every user
	for (int u = 0; u < nUsers; u ++) {
		//Obtain validation item and previous of validation
		int item_val  		= val_per_user[u].first;
		//int item_val_prev 	= val_per_user[u].second;

		//If
		if (item_val == -1 || (userIdsTest.find(corp->rUserIds[u]) == userIdsTest.end())){
			continue;
		}
		vector<std::tuple<string, string,double>> lst;
		if ( u% 1000 == 0)
			printf("%d out of %d \n", u, nUsers);

		/*
		//We already have the previous item rated so there is no need to store it again
		double x_u_val  = prediction(u, item_val_prev, item_val);


		lst.emplace_back(corp->rUserIds[u].c_str(), corp->rItemIds[item_val].c_str(),x_u_val);*/


		/*int count_val = 0;
		int count_test = 0;
		int max = 0; */
		for (int j = 0; j < nItems; j ++) {
			//If the item has been rated or the item is validation, then continue
			if (!allow_recomendations_rated_by_user_in_train) {
				if (clicked_per_user[u].find(j) != clicked_per_user[u].end()
					|| j == item_val) {
					continue;
				}
			}

			//Validation item fixed, the prediction will be using this item fixed and the rest of items
			double x_uj = prediction(u, item_val, j);

			lst.emplace_back(corp->rUserIds[u].c_str(), corp->rItemIds[j].c_str(),x_uj);
		}
		//Sort the list by value of the prediction
		sort(lst.begin(),lst.end(),
		       [](const tuple<string, string,double>& a,
		       const tuple<string, string,double>& b) -> bool
		       {
			 return std::get<2>(a) > std::get<2>(b);
		       });

		//Print to the file
		int c = 1;
		for (const auto &element : lst ){
			if (c > limit)
				break;
	   		rankingFile << std::get<0>(element).c_str() << "\t" << std::get<1>(element) << "\t" << std::get<2>(element) << "\n";
			c++;
		}

	}

	  	rankingFile.close();

}

string model::toString()
{
	return "Empty Model!";
}
