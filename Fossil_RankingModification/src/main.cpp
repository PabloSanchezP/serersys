#include "corpus.hpp"
#include "Random.hpp"
#include "MostPopular.hpp"
#include "BPRMF.hpp"
#include "MC.hpp"
#include "FPMC.hpp"
#include "FISMrmse.hpp"
#include "FISMauc.hpp"
#include "FossilSimple.hpp"
#include "Fossil.hpp"

#define TAM_STRING 1000

void go_Random(corpus* corp, const char* resultRanking,int limit, char * testFile, bool allow_recomendations_rated_by_user_in_train)
{
	printf("Random\n");
	fflush(stdout);
	Random md(corp);

	/*double valid, test, var;
	int num_item;

	md.AUC(&valid, &test, &var);
	printf("\n\n <<< Random >>> Test AUC = %f, Test var = %f\n", test, var);

	md.AUC_coldItem(&test, &var, &num_item);
	printf("\n\n <<< Random >>> Cold Start: #Item = %d, Test AUC = %f, Test var = %f\n", num_item, test, var);

	md.AUC_coldUser(&test, &var, &num_item);
	printf("\n\n <<< Random >>> User Cold Start: #Item = %d, Test AUC = %f, Test var = %f\n", num_item, test, var);*/

	char fileRes[TAM_STRING]="";
	strcpy(fileRes,resultRanking);
	strcat(fileRes,"Random.txt");
	md.obtainRanking(fileRes, limit, testFile, allow_recomendations_rated_by_user_in_train);
	printf("End Random\n");
	fflush(stdout);
}

void go_MP(corpus* corp,const char* resultRanking,int limit, char * testFile, bool allow_recomendations_rated_by_user_in_train)
{
	printf("Popularity\n");
	fflush(stdout);
	MostPopular md(corp);

	/*double valid, test, var;
	int num_item;

	md.AUC(&valid, &test, &var);
	printf("\n\n <<< Popularity >>> Test AUC = %f, Test var = %f\n", test, var);

	md.AUC_coldItem(&test, &var, &num_item);
	printf("\n\n <<< Popularity >>> Item Cold Start: #Item = %d, Test AUC = %f, Test var = %f\n", num_item, test, var);

	md.AUC_coldUser(&test, &var, &num_item);
	printf("\n\n <<< Popularity >>> User Cold Start: #Item = %d, Test AUC = %f, Test var = %f\n", num_item, test, var);*/


	char fileRes[TAM_STRING]="";
	strcpy(fileRes,resultRanking);
	strcat(fileRes,"Popularity.txt");
	md.obtainRanking(fileRes,limit,testFile, allow_recomendations_rated_by_user_in_train);
	printf("End Popularity\n");
	fflush(stdout);
}

void go_BPRMF(corpus* corp, int K, double lambda, double bias_reg, int iterations, const char* model_path,const char* resultRanking,int limit, char * testFile, bool allow_recomendations_rated_by_user_in_train)
{
	printf("BPRMF\n");
	fflush(stdout);
	BPRMF md(corp, K, lambda, bias_reg);
	md.init();


	md.train(iterations, 0.01);
	/*md.saveModel((string(model_path) + "__" + md.toString() + ".txt").c_str());*/


	char fileRes[TAM_STRING];
	strcpy(fileRes,resultRanking);
	strcat(fileRes,"BPRMF.txt");
	md.obtainRanking(fileRes,limit,testFile, allow_recomendations_rated_by_user_in_train);

	md.cleanUp();
	printf("End BPRMF\n");
	fflush(stdout);
}

void go_MC(corpus* corp, int K, double lambda, int iterations, const char* model_path,const char* resultRanking,int limit, char * testFile, bool allow_recomendations_rated_by_user_in_train)
{
	printf("MC\n");
	fflush(stdout);
	MC md(corp, K, lambda);
	md.init();



	md.train(iterations, 0.01);
	/*md.saveModel((string(model_path) + "__" + md.toString() + ".txt").c_str());*/


	char fileRes[TAM_STRING]="";
	strcpy(fileRes,resultRanking);
	strcat(fileRes,"MC.txt");
	md.obtainRanking(fileRes,limit,testFile, allow_recomendations_rated_by_user_in_train);
	md.cleanUp();

	printf("End MC\n");
	fflush(stdout);
}

void go_FPMC(corpus* corp, int K, int KK, double lambda, int iterations, const char* model_path,const char* resultRanking,int limit, char * testFile, bool allow_recomendations_rated_by_user_in_train)
{
	printf("FPMC\n");
	fflush(stdout);
	FPMC md(corp, K, KK, lambda);
	md.init();

	md.train(iterations, 0.01);
	/*md.saveModel((string(model_path) + "__" + md.toString() + ".txt").c_str());*/


	char fileRes[TAM_STRING]="";
	strcpy(fileRes,resultRanking);
	strcat(fileRes,"FPMC.txt");
	md.obtainRanking(fileRes,limit,testFile, allow_recomendations_rated_by_user_in_train);
	md.cleanUp();

	printf("End FPMC\n");
	fflush(stdout);
}


void go_FISMrmse(corpus* corp, int K, double rho, double alpha, double lambda, double bias_reg, int iterations, const char* model_path,const char* resultRanking,int limit, char * testFile, bool allow_recomendations_rated_by_user_in_train)
{
	printf("FISMrmse\n");
	fflush(stdout);

	FISMrmse md(corp, K, rho, alpha, lambda, bias_reg);
	md.init();
	printf("FISMrmse\n");
	fflush(stdout);

	md.train(iterations, 0.01);
	/*md.saveModel((string(model_path) + "__" + md.toString() + ".txt").c_str());*/


	char fileRes[TAM_STRING]="";
	strcpy(fileRes,resultRanking);
	strcat(fileRes,"FISMRMSE.txt");
	md.obtainRanking(fileRes,limit,testFile, allow_recomendations_rated_by_user_in_train);
	md.cleanUp();

	printf("End FISMrmse\n");
	fflush(stdout);
}

void go_FISMauc(corpus* corp, int K, double rho, double alpha, double lambda, double bias_reg, int iterations, const char* model_path,const char* resultRanking,int limit, char * testFile, bool allow_recomendations_rated_by_user_in_train)
{
	printf("FISMauc\n");
	fflush(stdout);

	FISMauc md(corp, K, rho, alpha, lambda, bias_reg);
	md.init();

	md.train(iterations, 0.01);
	/*md.saveModel((string(model_path) + "__" + md.toString() + ".txt").c_str());*/


	char fileRes[TAM_STRING]="";
	strcpy(fileRes,resultRanking);
	strcat(fileRes,"FISMauc.txt");
	md.obtainRanking(fileRes,limit,testFile, allow_recomendations_rated_by_user_in_train);
	md.cleanUp();

	printf("End FISMauc\n");
	fflush(stdout);
}

void go_FossilSimple(corpus* corp, int K, double lambda, double bias_reg, int iterations, const char* model_path,const char* resultRanking,int limit, char * testFile, bool allow_recomendations_rated_by_user_in_train)
{
	printf("FossilSimple\n");
	fflush(stdout);

	FossilSimple md(corp, K, lambda, bias_reg);
	md.init();

	md.train(iterations, 0.01);
	/*md.saveModel((string(model_path) + "__" + md.toString() + ".txt").c_str());*/


	char fileRes[TAM_STRING]="";
	strcpy(fileRes,resultRanking);
	strcat(fileRes,"FossilSimple.txt");
	md.obtainRanking(fileRes,limit,testFile, allow_recomendations_rated_by_user_in_train);
	md.cleanUp();

	printf("End FossilSimple\n");
	fflush(stdout);
}

void go_Fossil(corpus* corp, int L, int K, double lambda, double bias_reg, int iterations, const char* model_path,const char* resultRanking,int limit, char * testFile, bool allow_recomendations_rated_by_user_in_train)
{
	printf("Fossil\n");
	fflush(stdout);

	Fossil md(corp, L, K, lambda, bias_reg);
	md.init();

	md.train(iterations, 0.01);
	/*md.saveModel((string(model_path) + "__" + md.toString() + ".txt").c_str());*/



	char fileRes[TAM_STRING]="";
	strcpy(fileRes,resultRanking);
	strcat(fileRes,"Fossil.txt");
	md.obtainRanking(fileRes,limit,testFile, allow_recomendations_rated_by_user_in_train);

	md.cleanUp();

	printf("End Fossil\n");
	fflush(stdout);
}


int main(int argc, char** argv)
{
	srand(0);

	if (argc != 17) {
		printf(" Parameters as following: \n");
		printf(" 1. Click triples path \n");
		printf(" 2. user min \n");
		printf(" 3. item min \n");

		printf(" 4. Markov Chain order (L)\n");
		printf(" 5. Latent Feature Dimension (K) \n");
		printf(" 6. lambda (L2-norm regularizer) \n");
		printf(" 7. bias_reg (L2-norm regularizer for bias terms) \n");

		printf(" 8. Maximum number of iterations \n");
		printf(" 9. Model path \n");

		printf("10. rho (negative sample rate for FISM models)\n");
		printf("11. alpha (discount factor for FISM models)\n\n");
		//New 4 arguments added
		printf("12. Recommendation file\n\n");
		printf("13. Limit of recommendations\n\n");
		printf("14. Recommender\n\n");
		printf("15. Test file\n\n");
		printf("16. Allow recommending items previously rated by the user in the train set\n");
		exit(1);
	}

	char* data_path = argv[1];
	int user_min = atoi(argv[2]);
	int item_min = atoi(argv[3]);
	int L  = atoi(argv[4]);
	int K  = atoi(argv[5]);
	double lambda = atof(argv[6]);
	double bias_reg = atof(argv[7]);
	int iter = atoi(argv[8]);
	char* model_path = argv[9];

	double rho = atof(argv[10]);
	double alpha = atof(argv[11]);

	char * resultFile = argv[12];

	int limit = atoi(argv[13]);
	char * recommender = argv[14];

	char * pathTestFile = argv[15];

	bool allow_recomendations_rated_by_user_in_train = false;

	if (argc > 16) {
		char * allow_recomendations_rated_by_user_in_train_s = argv[16];
		if (strcmp(allow_recomendations_rated_by_user_in_train_s, "true") == 0|| strcmp(allow_recomendations_rated_by_user_in_train_s, "yes") == 0){
			allow_recomendations_rated_by_user_in_train = true;
			printf("Recommending thins already rated by the users in the train set\n");
		}
	}



	corpus corp;
	corp.loadData(data_path, user_min, item_min);

	char res [TAM_STRING] ="";
	strcpy(res,resultFile);

	printf("Version with Fossil considering the validation item and the other last L-1 items\n");
	printf("Added option to recommend items that have been previously rated by the user in train\n");
	fflush(stdout);


	if (strcmp(recommender,"random") == 0)
		go_Random(&corp, res, limit, pathTestFile, allow_recomendations_rated_by_user_in_train);
	else if (strcmp(recommender,"mp") == 0)
		go_MP(&corp, res, limit, pathTestFile, allow_recomendations_rated_by_user_in_train);
	else if (strcmp(recommender, "bprmf" ) == 0)
		go_BPRMF(&corp, K, lambda, bias_reg, iter, model_path, res, limit, pathTestFile, allow_recomendations_rated_by_user_in_train);
	else if (strcmp(recommender,"mc") == 0)
		go_MC(&corp, K, lambda, iter, model_path, res, limit, pathTestFile, allow_recomendations_rated_by_user_in_train);
	else if (strcmp(recommender,"fpmc") == 0)
		go_FPMC(&corp, K, K, lambda, iter, model_path, res, limit, pathTestFile, allow_recomendations_rated_by_user_in_train);
	else if (strcmp(recommender,"FISMrmse") == 0)
		go_FISMrmse(&corp, K, rho, alpha, lambda, bias_reg, iter, model_path, res,limit, pathTestFile, allow_recomendations_rated_by_user_in_train);
	else if (strcmp(recommender,"FISMauc") == 0)
		go_FISMauc(&corp, K, rho, alpha, lambda, bias_reg, iter, model_path, res,limit, pathTestFile, allow_recomendations_rated_by_user_in_train);
	else if (strcmp(recommender,"FossilSimple") == 0)
		go_FossilSimple(&corp, K, lambda, bias_reg, iter, model_path, res, limit, pathTestFile, allow_recomendations_rated_by_user_in_train);
	else if (strcmp(recommender,"Fossil") == 0)
		go_Fossil(&corp, L, K, lambda, bias_reg, iter, model_path, res, limit, pathTestFile, allow_recomendations_rated_by_user_in_train);

	return 0;
}
